<?php

namespace App\Http\Controllers\BetHistory;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;


class BetMigrationController extends Controller
{


  public function betMigration()
    {


      $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];


        $models=null;
        $where = ([['status',1],['result','!=','PENDING']]);

        $fancyCurrentBet = DB::table('tbl_bet_pending_2')->select('*')->where($where)->orderBy('created_on' ,'ASC')->get();
        if(!$fancyCurrentBet->isEmpty()) {
            foreach ($fancyCurrentBet as $value) {
                $models[] = $value;
            }
        }

        $BookmakerCurrentBet = DB::table('tbl_bet_pending_1')->select('*')->where($where)->orderBy('created_on' ,'ASC')->get();
        if(!$BookmakerCurrentBet->isEmpty()) {
            foreach ($BookmakerCurrentBet as $value) {
                $models[] = $value;
            }
        }

       $khadoBallCurrentBet = DB::table('tbl_bet_pending_3')->select('*')->where($where)->orderBy('created_on' ,'ASC')->get();
        if(!$khadoBallCurrentBet->isEmpty()) {
            foreach ($khadoBallCurrentBet as $value) {
                $models[] = $value;
            }
        }

      $jackpotLotteryCurrentBet = DB::table('tbl_bet_pending_4')->select('*')->where($where)->orderBy('created_on' ,'ASC')->get();
        if(!$jackpotLotteryCurrentBet->isEmpty()) {
            foreach ($jackpotLotteryCurrentBet as $value) {
                $models[] = $value;
            }
        }
 //$models = json_decode(json_encode($models),true);
if(!empty($models)) {
    foreach ($models as $models) {

        $arr[] = [

            'systemId' => $models->systemId,
            'betId' => $models->id,
            'uid' => $models->uid,
            'sid' => $models->sid,
            'eid' => $models->eid,
            'mid' => $models->mid,
            'secId' => $models->secId,
            'price' => $models->price,
            'rate' => $models->rate,
            'size' => $models->size,
            'diff' => $models->diff,
            'win' => $models->win,
            'loss' => $models->loss,
            'ccr' => $models->ccr,
            'client' => $models->client,
            'master' => $models->master,
            'runner' => $models->runner,
            'market' => $models->market,
            'event' => $models->event,
            'sport' => $models->sport,
            'bType' => $models->bType,
            'mType' => $models->mType,
            'result' => $models->result,
            'description' => $models->description,
            'is_match' => $models->is_match,
            'ip_address' => $models->ip_address,
            'created_on' => $models->created_on,
            'updated_on' => $models->updated_on,
            'status' => $models->status

        ];

        if ($models->mType == 'fancy' || $models->mType == 'fancy2' || $models->mType == 'fancy3') {

            DB::table('tbl_bet_pending_2')->where([['uid', $models->uid], ['id', $models->id]])->delete();

        } elseif ($models->mType == 'bookmaker' || $models->mType == 'match_odd') {

            DB::table('tbl_bet_pending_1')->where([['uid', $models->uid], ['id', $models->id]])->delete();

        } elseif ($models->mType == 'khado' || $models->mType == 'ballbyball') {

            DB::table('tbl_bet_pending_3')->where([['uid', $models->uid], ['id', $models->id]])->delete();

        } else {

            DB::table('tbl_bet_pending_4')->where([['uid', $models->uid], ['id', $models->id]])->delete();

        }
    }

    DB::table('tbl_bet_history')->insert($arr);
}


             echo "<pre>";
print_r($models); die('Done');
       /*  foreach ($models as  $betList) {

             
             
         
         }*/


    }

    public function userBetListUpdateRecall($marketId,$tbl)
    {
        $list_arr=null;
        $betList = DB::table($tbl)->select('*')->where('mid',$marketId)->get();
        if($betList->isEmpty()){
            $betData = DB::table('tbl_bet_history')->select('*')->where('mid',$marketId)->get();
            print_r($betData);
            if(!$betData->isEmpty()) {
                foreach ($betData as $models) {
                    $list_arr[] = [

                        'systemId' => $models->systemId,
                         'id' => $models->betId,
                        'uid' => $models->uid,
                        'sid' => $models->sid,
                        'eid' => $models->eid,
                        'mid' => $models->mid,
                        'secId' => $models->secId,
                        'price' => $models->price,
                        'rate' => $models->rate,
                        'size' => $models->size,
                        'diff' => $models->diff,
                        'win' => $models->win,
                        'loss' => $models->loss,
                        'ccr' => $models->ccr,
                        'client' => $models->client,
                        'master' => $models->master,
                        'runner' => $models->runner,
                        'market' => $models->market,
                        'event' => $models->event,
                        'sport' => $models->sport,
                        'bType' => $models->bType,
                        'mType' => $models->mType,
                        'result' => 'PENDING',
                        'description' => $models->description,
                        'is_match' => $models->is_match,
                        'ip_address' => $models->ip_address,
                        'created_on' => $models->created_on,
                        'updated_on' => $models->updated_on,
                        'status' => $models->status

                    ];
                }

                if($list_arr != null){
                    DB::table($tbl)->insert($list_arr);
                }

            }

        }
    }

}