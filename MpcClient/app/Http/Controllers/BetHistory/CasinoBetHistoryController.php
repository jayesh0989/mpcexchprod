<?php

namespace App\Http\Controllers\BetHistory;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;

class CasinoBetHistoryController extends Controller
{
    public function casinoBetHistory(Request $request)
    {       

      try{

        $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
        $uid = Auth::user()->id;
        
        if( json_last_error() == JSON_ERROR_NONE ){
           

            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                
                        $startDate = date('Y-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                        $endDate = date('Y-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
            }else{

                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('Y-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('Y-m-d h:i:s'); 
             }
      
        if( isset( $request->type ) && $request->type != '' ){
            $type = $request->type;
            $where = ([['mType',$type],['uid',$uid],['systemId',$systemId]]);
        }else{      
            $where = ['uid' => $uid];//$uid ;
        }


        if((isset($request->cancel) && $request->cancel != 1)){
         
         $query = DB::connection('mongodb')->table('tbl_casino_transaction')->select('*')
                  ->where([[$where],['amount','!=',0]])
                  ->whereIn('status',[0,1])
                  ->whereIn('type',['CREDIT','DEBIT'])
                  ->orderBy('created_on' ,'DESC');
        }
        else{  
           $query = DB::connection('mongodb')->table('tbl_casino_transation')->select('_id','amount','game_code','type','created_on','updated_on','status')
                  ->where([[$where],['amount','!=',0]])
                  ->whereIn('status',[1,2])
                  ->WhereIn('result',['CANCELED','PENDING'])
                  ->orderBy('created_on' ,'DESC');
        }   
          
          if(isset($request->isFirst) && $request->isFirst == 1){
                $betList = $query->limit(10)->get();
          }elseif ($request->cancel == 1) {
             $betList = $query->whereBetween('created_on',[$startDate, $endDate])->get();
          }
          else {
             $betList = $query->whereBetween('created_on',[$startDate, $endDate])->get();
          }
     
          $models=[];    

        if( $betList != null ){
            foreach ( $betList as $data ){
               if(isset($data->mType) && $data->mType == 'ballbyball' ){
                      $market = $data->market;
                      $space = ' ';
                      $ball = strstr($market,$space,true);        
                      $data->ball= $ball;
                    }
                    if(isset($data->mType) && $data->mType == 'khado'){
                       $difference = $data->diff - $data->price ;        
                      $data->difference= $difference;
                    }
                $models[]=$data;
             }
         }
         foreach ($betList as $data) {
              $data = (object)$data;
              $game_code = explode("_", $data->game_code);
              $gameCode = $game_code[1];
              
              $list[] = [
                    '_id'  => $data->_id,
                    'description' => $gameCode.' '.'> round #'.$data->round,
                    'amount'       => $data->amount,
                    'type' => $data->type,
                    'date' => $data->created_on,
                  ];
                }    
             // print_r('expression');exit();
         
        if( $models != null ){
            $response = [ "status" => 1 ,'code'=> 200, "data" => ['items'=> $list ] ,'message'=> 'Data Found !!' ];
        }else{
            $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=>'Data not found !!' ];
        }
        
        return $response;
    }
  }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
  }
}
