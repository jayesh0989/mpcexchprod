<?php

namespace App\Http\Controllers\BetHistory;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;


class TeenpattiBetHistoryController extends Controller
{

  public function TeenpattiBetHistory(Request $request)
    {       
       
      try{

         $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
         $uid = Auth::id();
       
        if( json_last_error() == JSON_ERROR_NONE ){
           
            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
               
                        $startDate = date('Y-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";                     
                        $endDate = date('Y-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
             }else{
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('Y-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('Y-m-d h:i:s'); 
             }
              
         if(isset($request->cancel) && $request->cancel != 1){
         $query = DB::connection('mongodb')->table('tbl_bet_history_teenpatti')
                  ->select('_id','price','size','win','rate','loss','mType','bType','result','description','created_on','updated_on')
                  ->where([['status',1],['uid',(int)$uid],['result','!=','PENDING']])
                  ->orderBy('created_on' ,'DESC');

         }else{

          $query = DB::connection('mongodb')->table('tbl_bet_history_teenpatti')
                  ->select('_id','price','size','win','rate','loss','mType','bType','result','description','created_on','updated_on')
                  ->where('uid',(int)$uid)
                  ->whereIn('status',[1,2])
                  ->whereIn('result',['CANCELED','PENDING'])
                  ->orderBy('created_on' ,'DESC');
         }

            //$betList = $query->get();
         //print_r($betList);
             if(isset($request->isFirst) && $request->isFirst == 1){
                $betList = $query->limit(10)->get();
              }else{
                $betList = $query->whereBetween('created_on',[$startDate, $endDate])->get();
              }

           $models=[];  

           if( $betList != null ){

            foreach ( $betList as $list ){
                $list = (object)$list;
                     if($list->bType == 1){
                        $bType = "back";
                     }elseif($list->bType == 2){
                        $bType = "lay";
                     }else{
                        $bType = $list->bType;
                     }

                     $models[] = [
                        '_id' => $list->_id,
                        'description' => $list->description,
                        'bType' => $bType,
                         'price' => $list->price,
                         'size' => $list->size,
                         'updated_on' =>  $list->updated_on,
                          'created_on' =>  $list->created_on,
                         'result' => $list->result,
                         'win'  =>$list->win,
                         'loss' => $list->loss,
                         'rate' => $list->rate,
                         'mType' => $list->mType,
                         'settled_date' => $list->created_on
                  ];
             }
           }
  
           if( $models != null ){

              $response = [ "status" => 1 ,'code'=> 200, "data" => ['items'=> $models ] ,'message'=> 'Data Found !!' ];
           }else{
              $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=>'Data not found !!' ];
           }
             
    }

   return $response;
   
  }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
 }

}
