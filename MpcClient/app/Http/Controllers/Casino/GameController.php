<?php

namespace App\Http\Controllers\Casino;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;


class GameController extends Controller
{


    /**
     * createSignature
     */
    public function createSignature($data)
    {
        $privateKey = openssl_pkey_get_private(file_get_contents(public_path().'/key/privet_key.txt'));
        openssl_sign($data,$signature,$privateKey,'RSA-SHA256');
        return base64_encode($signature);
    }

    /**
     * actionGameListOld
     */
    public function actionGameListAdd()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        return $response;
        try{
            $token = md5('1-22074-ca05-MPCEXCH143');
            //return $token;
            //$url = 'http://83.136.253.37/games/list';
            $url = 'https://api.dreamcasino.live/games/list';
            $post = ['partner_id' => 'mpcexch'];
            $post = json_encode($post);
            $casinoSignature = $this->createSignature($post);

            $headerPost = array( 'Content-Type:application/json','Casino-Signature:'.$casinoSignature);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerPost);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $responseData = curl_exec($ch);

            curl_close($ch);
            $resData = json_decode($responseData);
            echo "<pre>"; print_r($resData); exit;
           // dd($resData);
            if( $resData != null ){
                $insertDataArr = []; $gameIdsArr = [];
                foreach ($resData as $res){
                    if( !in_array($res->game_id,$gameIdsArr) ){
                        $gameIdsArr[] = $res->game_id;
                        $status=2;
                        if($res->product=='Evolution Gaming' || $res->product=='Ezugi'){
                            $status=1;
                        }
                        $insertArr = [
                            'game_id' => $res->game_id,
                            'category' => $res->category,
                            'name' => $res->name,
                            'product' => $res->product,
                            'thumb' => $res->url_thumb,
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s'),
                            'status' => $status
                        ];
                        DB::connection('mongodb')
                            ->table('tbl_casino_games')->insert($insertArr);
                    }
                }

                $response = [ 'status' => 1 ];
            }

            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * actionGameList
     */
    public function actionGameList()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            // $response = [ 'status' => 1, 'data' => null ];
            /*$casinoGames = DB::connection('mongodb')->table('tbl_casino_games')
                ->select(['game_id','name','product','thumb'])
                ->where('status',1)
                ->whereIn('product',['Ezugi','Evolution Gaming'])
                ->whereNotNull('thumb')->limit(300)->get();*/
            $casinoGames[]=array('game_id'=>2162,'name'=>'Evolution Gaming','product'=>'Evolution Gaming','thumb'=>'https://api.mpcexch.com/dashboard_images/evolution-gaming-mpcx.jpg');
            $casinoGames[]=array('game_id'=>3924,'name'=>'Ezugi','product'=>'Ezugi','thumb'=>'https://api.mpcexch.com/dashboard_images/ezugi.jpg');
            if(!empty($casinoGames) ){
                $response = [ 'status' => 1, 'data' => $casinoGames ];
            }else{
                $response = [ 'status' => 1, 'data' => null ];
            }

            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Game Url
     */
    public function checkUserExist($user)
    {
        $conn = DB::connection('mongodb');
        $username = $user->username; $uid = $user->id; $systemId = $user->systemId;
        $casinoUser = $conn->table('tbl_casino_user')
            ->where([['uid',$uid],['username',trim($username)]])->first();
        if( empty($casinoUser) ){
            $token = md5($systemId.'-'.$uid.'-'.$username.'-MPCEXCH143');
            $insertArr = [
                'uid' => $uid,
                'username' => $username,
                'systemId' => $systemId,
                'token' => $token,
                'balance' => 0,
                'status' => 1
            ];

            if( $conn->table('tbl_casino_user')->insert($insertArr) ){
                return true;
            }else{
                return false;
            }
        }
        return true;
    }
    /**
     * checkUserExist
     */
    public function actionGameUrl(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            $post = []; $casinoSignature = null;
            if( isset($request->platform) && isset($request->gameId) ){
                $redis = Redis::connection();
                $casinoStatus = DB::table('tbl_common_setting')->select(['value'])
                    ->where([['status', 1], ['key_name', 'CASINO_GAME_STATUS']])->first();
                if (!empty($casinoStatus) && trim($casinoStatus->value) != 1) {
                    return response()->json($response);
                }

                $user = Auth::user();

//                if( !in_array($user->id,[22070,22071,22072,22073,22074,22066,22067,22079]) ){
//                    return response()->json($response);
//                }

                $token = md5(time().'-'.$user->systemId.'-'.$user->id.'-'.$user->username.'-MPCEXCH143');
                $redis->set('CASINO_TOKEN_'.$user->id,$token);

                $post = [
                    'user' => $user->username,
                    'token' => $token,
                    'partner_id' => 'mpcexch',
                    'platform' => trim($request->platform),
                    'lobby_url' => 'https://mpcexch.com/main',
                    'lang' => 'en',
                    'ip' => $this->getClientServerIp(),
                    'game_id' => (int)$request->gameId,
                    'currency' => 'EUR',
                    'country' => 'EE',
                ];

                $post = json_encode($post);
                $casinoSignature = $this->createSignature($post);

                $headerPost = array( 'Content-Type:application/json','Casino-Signature:'.$casinoSignature);

                //$url = 'http://83.136.253.37/games/url';
                $url = 'https://api.dreamcasino.live/games/url';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headerPost);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                $responseData = curl_exec($ch);
                curl_close($ch);
                $resData = json_decode($responseData);
                $gameUrl = null;
                if( isset($resData->url) ){
                    $gameUrl = $resData->url;
                }
                $response = ['status' => 1, 'gameUrl' => $gameUrl];
            }
            $data['api'] = 'GAME URL API';
            $data['request'] = $post == null ? '' : json_decode($post);
            $data['response'] = isset($resData) ? $resData : null;
            $this->casinoErrorLog($data,$casinoSignature);
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * errorLog
     */
    public function casinoErrorLog($data,$sign)
    {
        $data = json_encode($data);
        $message = 'Data: '.$data.' | Signature: '.$sign;
        Log::channel('casino-log')->info($message);
        return [ 'status' => 0, 'error' => [ 'message' => $message ] ];
    }

}
