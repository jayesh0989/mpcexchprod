<?php

namespace App\Http\Controllers\Casino;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class UsersController extends Controller
{

    /**
     * action User
     */
    public function actionUser(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            if( isset($request->user) ){
                $user = DB::connection('mongodb')->table('tbl_casino_user')
                    ->where([['username',trim($request->user)]])->first();
                if( $user != null ){
                    $response = [
                        'user' => $user['username'],
                        'disabled' => 0,
                        'balance' => (int)($user['balance']*100000)
                    ];
                }else{
                    $response = [
                        'user' => 'None',
                        'disabled' => 'None',
                        'balance' => 'None'
                    ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Game Token
     */
    public function actionGameToken(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{

            if( isset($request->user) ){
                $user = DB::connection('mongodb')->table('tbl_casino_user')
                    ->where([['username',trim($request->user)]])->first();
                if( $user != null ){
                    $user = (object)$user;
                    $token = md5($user->systemId.'-'.$user->uid.'-'.$user->username.'-MPCEXCH143');
                    $response = [ 'token' => $token ];
                }else{
                    $response = [ 'token' => 'None' ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Update Balance
     */
    public function actionUpdateBalance(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{

            if( isset($request->user) && isset($request->balance)){
                $user = DB::connection('mongodb')->table('tbl_casino_user')
                    ->where([['username',trim($request->user)]])->first();
                if( $user != null ){
                    $balance = (int)$request->balance/100000;
                    $updateArr = ['balance' => $balance];
                    if( DB::connection('mongodb')->table('tbl_casino_user')
                        ->where([['username',trim($request->user)]])->update($updateArr) ){
                        $response = [ 'status' => 'ok' ];
                    }else{
                        $response = [ 'status' => 'None' ];
                    }
                }else{
                    $response = [ 'status' => 'None' ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

}
