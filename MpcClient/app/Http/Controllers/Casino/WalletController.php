<?php

namespace App\Http\Controllers\Casino;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;


class WalletController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }

    /**
     * errorLog
     */
    public function casinoErrorLog($data,$sign)
    {
        $data = json_encode($data);
        $message = 'Data: '.$data.' | Signature: '.$sign;
        Log::channel('casino-log')->info($message);
        return [ 'status' => 0, 'error' => [ 'message' => $message ] ];
    }
    /**
     * verifySignature
     */
    public function verifySignature($data,$sign)
    {
        $publicKey = openssl_pkey_get_public(file_get_contents(public_path().'/key/third_public_key.txt'));
        $verify = openssl_verify($data, base64_decode($sign),$publicKey, 'RSA-SHA256');
        if ($verify == 1){
            return true;
        } else {
            return false;
        }
    }

    /**
     * action Balance
     */
    public function actionBalance(Request $request)
    {
        try{
            $redis = Redis::connection();
            $data['api'] = 'BALANCE API';
            $sign = $request->header('Casino-Signature');
            if( !$this->verifySignature(file_get_contents('php://input'),$sign) ){
                $response = [
                    'status' => 'RS_ERROR_INVALID_SIGNATURE',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Invalid signature!'
                ];
                $data['request'] = json_decode(file_get_contents('php://input'));
                $data['response'] = $response;
                $this->casinoErrorLog($data,$sign);
                return response()->json($response);
            }

            if( isset($request->user) && isset($request->token) && isset($request->request_uuid)
                && isset($request->game_id) && isset($request->game_code) ){

                $user = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['uid','username','balance','expose','pl_balance','u.systemId'])
                    ->where([['username',trim($request->user)],['role',4],['status',1]])->first();
                if( $user != null ){
                    $token = trim( $redis->get('CASINO_TOKEN_'.$user->uid) );
                    //$token = md5($request->game_id.'-'.$user->systemId.'-'.$user->uid.'-'.$user->username.'-MPCEXCH143');
                    if( $token == trim($request->token) ){
                        $balance = round($user->balance-$user->expose+$user->pl_balance);
                        $response = [
                            'status' => 'RS_OK',
                            'user' => $user->username,
                            'request_uuid' => $request->request_uuid,
                            'balance' => (int)($balance*100000)
                        ];
                    }else{
                        $response = [
                            'status' => 'RS_ERROR_INVALID_TOKEN',
                            'user' => $request->user,
                            'request_uuid' => $request->request_uuid,
                            'message' => 'Invalid token!'
                        ];
                    }
                }else{
                    $response = [
                        'status' => 'RS_ERROR_USER_DISABLED',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'User not found!'
                    ];
                }
            }else{
                $response = [
                    'status' => 'RS_ERROR_WRONG_SYNTAX',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Request parameter missing!'
                ];
            }
            $data['request'] = json_decode(file_get_contents('php://input'));
            $data['response'] = $response;
            $this->casinoErrorLog($data,$sign);
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Bet
     */
    public function actionBet(Request $request)
    {
        try{
            $redis = Redis::connection();
            $data['api'] = 'BET API';
            $sign = $request->header('Casino-Signature');
            if( !$this->verifySignature(file_get_contents('php://input'),$sign) ){
                $response = [
                    'status' => 'RS_ERROR_INVALID_SIGNATURE',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Invalid signature!'
                ];
                $data['request'] = json_decode(file_get_contents('php://input'));
                $data['response'] = $response;
                $this->casinoErrorLog($data,$sign);
                return response()->json($response);
            }

            if( isset($request->user) && isset($request->transaction_uuid) && isset($request->token)
                && isset($request->request_uuid) && isset($request->round) && isset($request->game_id)
                && isset($request->amount) ){

                $casinoStatus = DB::table('tbl_common_setting')->select(['value'])
                    ->where([['status', 1], ['key_name', 'CASINO_GAME_STATUS']])->first();
                if (!empty($casinoStatus) && trim($casinoStatus->value) != 1) {
                    $response = [
                        'status' => 'RS_ERROR_USER_DISABLED',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'Game not active !'
                    ];
                    $data['request'] = json_decode(file_get_contents('php://input'));
                    $data['response'] = $response;
                    $this->casinoErrorLog($data,$sign);
                    return response()->json($response);
                }

                if( ($request->amount+0) < 0 ){
                    $response = [
                        'status' => 'RS_ERROR_WRONG_TYPES',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'Amount is negative !'
                    ];
                    $data['request'] = json_decode(file_get_contents('php://input'));
                    $data['response'] = $response;
                    $this->casinoErrorLog($data,$sign);
                    return response()->json($response);
                }

                $conn = DB::connection('mongodb');
                $conn2 = DB::connection('mysql');
                $conn3 = DB::connection('mysql3');

                $casinoGames = $conn->table('tbl_casino_games')
                    ->where([['game_id',(int)$request->game_id],['status',1]])->first();
                if( $casinoGames == null ){
                    $response = [
                        'status' => 'RS_ERROR_USER_DISABLED',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'Game not active !'
                    ];
                    $data['request'] = json_decode(file_get_contents('php://input'));
                    $data['response'] = $response;
                    $this->casinoErrorLog($data,$sign);
                    return response()->json($response);
                }

                if( $this->getEventBetAllowAndBlockStatusAdmin($request->game_id) == 1 ){
                     $response = [
                         'status' => 'RS_ERROR_USER_DISABLED',
                         'user' => $request->user,
                         'request_uuid' => $request->request_uuid,
                         'message' => 'Game not active !'
                     ];
                     $data['request'] = json_decode(file_get_contents('php://input'));
                     $data['response'] = $response;
                     $this->casinoErrorLog($data,$sign);
                     return response()->json($response);
                }

                $user = $conn2->table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['uid','parentId','username','balance','expose','pl_balance','u.systemId'])
                    ->where([['username',trim($request->user)],['role',4],['status',1]])->first();
                if( $user != null ){

//                    if( !in_array($user->uid,[22070,22071,22072,22073,22074,22066,22067,22079]) ){
//                        $response = [
//                            'status' => 'RS_ERROR_USER_DISABLED',
//                            'user' => $request->user,
//                            'request_uuid' => $request->request_uuid,
//                            'message' => 'Game not active !'
//                        ];
//                        $data['request'] = json_decode(file_get_contents('php://input'));
//                        $data['response'] = $response;
//                        $this->casinoErrorLog($data,$sign);
//                        return response()->json($response);
//                    }
                    //Check User Block Status
                   if( $this->checkMaxStackLimit($request->input(),$casinoGames,$user->uid) == false ){
                        $response = [
                            'status' => 'RS_ERROR_WRONG_TYPES',
                            'user' => $request->user,
                            'request_uuid' => $request->request_uuid,
                            'message' => 'Max Stack Limit Over!'
                        ];
                        $data['request'] = json_decode(file_get_contents('php://input'));
                        $data['response'] = $response;
                        $this->casinoErrorLog($data,$sign);
                        return response()->json($response);
                    }

                    $checkUserBlockStatus = DB::table('tbl_user_block_status')
                        ->where([['uid', $user->uid]])->first();
                    if ( !empty( $checkUserBlockStatus ) ) {
                        $response = [
                            'status' => 'RS_ERROR_USER_DISABLED',
                            'user' => $request->user,
                            'request_uuid' => $request->request_uuid,
                            'message' => 'Game not active !'
                        ];
                        $data['request'] = json_decode(file_get_contents('php://input'));
                        $data['response'] = $response;
                        $this->casinoErrorLog($data,$sign);
                        return response()->json($response);
                    }

                    if($this->getBlockStatus($user->uid,$request->game_id,9999) != 0 ){
                        $response = [
                            'status' => 'RS_ERROR_USER_DISABLED',
                            'user' => $request->user,
                            'request_uuid' => $request->request_uuid,
                            'message' => 'Game not active !'
                        ];
                        $data['request'] = json_decode(file_get_contents('php://input'));
                        $data['response'] = $response;
                        $this->casinoErrorLog($data,$sign);
                        return response()->json($response);
                    }
                    $token = trim( $redis->get('CASINO_TOKEN_'.$user->uid) );
                    //$token = md5($request->game_id.'-'.$user->systemId.'-'.$user->uid.'-'.$user->username.'-MPCEXCH143');

                    if( $token == trim($request->token) ){
                        if( $request->amount == 0 ){
                            $balance = round($user->balance-$user->expose+$user->pl_balance);
                            $response = [
                                'status' => 'RS_OK',
                                'user' => $user->username,
                                'request_uuid' => $request->request_uuid,
                                'balance' => (int)($balance*100000)
                            ];
                            $data['request'] = json_decode(file_get_contents('php://input'));
                            $data['response'] = $response;
                            $this->casinoErrorLog($data,$sign);
                            return response()->json($response);
                        }
                        $amount = (round((int)$request->amount)/100000);
                        $plBalance = $user->pl_balance-$amount;
                        $balance = round($user->balance-$user->expose+$plBalance);

                        if( $balance < 0 ){
                            $response = [
                                'status' => 'RS_ERROR_NOT_ENOUGH_MONEY',
                                'user' => $request->user,
                                'request_uuid' => $request->request_uuid,
                                'message' => 'Insufficient funds!'
                            ];
                            $data['request'] = json_decode(file_get_contents('php://input'));
                            $data['response'] = $response;
                            $this->casinoErrorLog($data,$sign);
                            return response()->json($response);
                        }

                        $checkTrans = $conn->table('tbl_casino_transaction')
                            ->where([['transaction_uuid',$request->transaction_uuid]])->first();

                        if( !empty($checkTrans) ){
                            $checkTrans = (object)$checkTrans;
                            if( $checkTrans->round == $request->round
                                && $checkTrans->amount == $amount ){
                                $balance = round($user->balance-$user->expose+$user->pl_balance);
                                $response = [
                                    'status' => 'RS_OK',
                                    'user' => $user->username,
                                    'request_uuid' => $request->request_uuid,
                                    'balance' => (int)($balance*100000)
                                ];
                            }else{
                                $response = [
                                    'status' => 'RS_ERROR_DUPLICATE_TRANSACTION',
                                    'user' => $request->user,
                                    'request_uuid' => $request->request_uuid,
                                    'message' => 'Transaction already exist !'
                                ];
                            }

                            $data['request'] = json_decode(file_get_contents('php://input'));
                            $data['response'] = $response;
                            $this->casinoErrorLog($data,$sign);
                            return response()->json($response);
                        }

                        $insertArr = [
                            'uid' => $user->uid,
                            'systemId' => $user->systemId,
                            'username' => $user->username,
                            'transaction_uuid' => $request->transaction_uuid,
                            'supplier_user' => $request->supplier_user,
                            'round_closed' => ( $request->round_closed == true ? 1 : 0 ),
                            'round' => $request->round,
                            'reward_uuid' => $request->reward_uuid,
                            'request_uuid' => $request->request_uuid,
                            'reference_transaction_uuid' => 'NULL',
                            'is_free' => $request->is_free,
                            'is_aggregated' => $request->is_aggregated,
                            'game_id' => $request->game_id,
                            'game_code' => $request->game_code,
                            'currency' => $request->currency,
                            'bet' => $request->bet,
                            'type' => 'DEBIT',
                            'amount' => $amount,
                            'balance' => round((int)$balance),
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s'),
                            'ip_address' => $this->getClientIp(),
                            'status' => 1,
                            'recall' => 0,
                            'recall_transaction_uuid' => 'NULL',
                            'recall_reference_transaction_uuid' => 'NULL'
                        ];

                        if( $conn->table('tbl_casino_transaction')->insert($insertArr) ){

                            $this->summaryUpdate($user->uid,$amount,0,'DEBIT');
                            $balance = $this->currentBalance($user->uid,$amount,'DEBIT','CLIENT');

                            $casinoGameName = isset($casinoGames['name']) ? $casinoGames['name'] : 'GameId: '.$request->game_id;
                            $description = 'Casino > '.$casinoGameName.' > Round #'.$request->round.' > Bet';
                            $transDefaultArr = [
                                'systemId' => $user->systemId,
                                'clientId' => $user->uid,
                                'childId' => 0,
                                'sid' => 9999,
                                'eid' => $request->game_id,
                                'mid' => $request->round.'-B',
                                'result' => '-',
                                'mType' => 'casino',
                                'p_amount' => 0,
                                'c_amount' => 0,
                                'description' => $description,
                                'remark' => 'Transactional Entry',
                                'status' => 1,
                                'created_on' => date('Y-m-d H:i:s'),
                                'updated_on' => date('Y-m-d H:i:s')
                            ];

                            $insertArr = $transDefaultArr;

                            $insertArr['userId'] = $user->uid;
                            $insertArr['parentId'] = $user->parentId;
                            $insertArr['amount'] = $amount;
                            $insertArr['balance'] = $balance;
                            $insertArr['type'] = 'DEBIT';
                            $insertArr['eType'] = 0; // 0 for transaction

                            $checkWhere = [['eid',$request->game_id],['mid',$request->round.'-B'],['userId',$user->uid]];
                            $transTbl = $this->currentTable('tbl_transaction_client');
                            $checkTransClient = $conn3->table($transTbl)
                                ->where($checkWhere);
                            if( $checkTransClient->first() ){
                                $updateTransArr = [
                                    'amount' => $conn->raw('amount + ' . $amount),
                                    'updated_on' => date('Y-m-d H:i:s')
                                ];

                                $transTbl = $this->currentTable('tbl_transaction_client');
                                if( $conn3->table($transTbl)->where($checkWhere)->update($updateTransArr) ){
                                    $cUser = $conn2->table('tbl_user_info')->where([['uid',$user->uid]])->first();
                                    $balance = round($cUser->balance-$cUser->expose+$cUser->pl_balance);

                                    $response = [
                                        'status' => 'RS_OK',
                                        'user' => $user->username,
                                        'request_uuid' => $request->request_uuid,
                                        'balance' => (int)($balance*100000)
                                    ];
                                }else{
                                    $response = [
                                        'status' => 'RS_ERROR_UNKNOWN',
                                        'user' => $request->user,
                                        'request_uuid' => $request->request_uuid,
                                        'message' => 'Error in transaction !'
                                    ];
                                }

                            }else{
                                $transTbl = $this->currentTable('tbl_transaction_client');
                                if( $conn3->table($transTbl)->insert($insertArr) ){
                                    $cUser = $conn2->table('tbl_user_info')->where([['uid',$user->uid]])->first();
                                    $balance = round($cUser->balance-$cUser->expose+$cUser->pl_balance);

                                    $response = [
                                        'status' => 'RS_OK',
                                        'user' => $user->username,
                                        'request_uuid' => $request->request_uuid,
                                        'balance' => (int)($balance*100000)
                                    ];
                                }else{
                                    $response = [
                                        'status' => 'RS_ERROR_UNKNOWN',
                                        'user' => $request->user,
                                        'request_uuid' => $request->request_uuid,
                                        'message' => 'Error in transaction !'
                                    ];
                                }
                            }

                        }else{
                            $response = [
                                'status' => 'RS_ERROR_UNKNOWN',
                                'user' => $request->user,
                                'request_uuid' => $request->request_uuid,
                                'message' => 'Error in transaction !'
                            ];
                        }

                    }else{
                        $response = [
                            'status' => 'RS_ERROR_INVALID_TOKEN',
                            'user' => $request->user,
                            'request_uuid' => $request->request_uuid,
                            'message' => 'Invalid token!'
                        ];
                    }
                }else{
                    $response = [
                        'status' => 'RS_ERROR_USER_DISABLED',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'User not found!'
                    ];
                }
            }else{
                $response = [
                    'status' => 'RS_ERROR_WRONG_SYNTAX',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Request parameter missing!'
                ];
            }

            $data['request'] = json_decode(file_get_contents('php://input'));
            $data['response'] = $response;
            $this->casinoErrorLog($data,$sign);
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Win
     */
    public function actionWin(Request $request)
    {
        try{
            $redis = Redis::connection();
            $data['api'] = 'WIN API';
            $sign = $request->header('Casino-Signature');
            if( !$this->verifySignature(file_get_contents('php://input'),$sign) ){
                $response = [
                    'status' => 'RS_ERROR_INVALID_SIGNATURE',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Invalid signature!'
                ];
                $data['request'] = json_decode(file_get_contents('php://input'));
                $data['response'] = $response;
                $this->casinoErrorLog($data,$sign);
                return response()->json($response);
            }

            if( isset($request->user) && isset($request->transaction_uuid) && isset($request->token)
                && isset($request->round) && isset($request->request_uuid) && isset($request->game_id)
                && isset($request->amount) && isset($request->reference_transaction_uuid) ){

                $conn = DB::connection('mongodb');
                $conn2 = DB::connection('mysql');
                $conn3 = DB::connection('mysql3');

                $casinoGames = $conn->table('tbl_casino_games')->select(['name'])
                    ->where([['game_id',(int)$request->game_id],['status',1]])->first();
                if( $casinoGames == null ){
                    $response = [
                        'status' => 'RS_ERROR_UNKNOWN',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'Game not active !'
                    ];
                    $data['request'] = json_decode(file_get_contents('php://input'));
                    $data['response'] = $response;
                    $this->casinoErrorLog($data,$sign);
                    return response()->json($response);
                }

                $user = $conn2->table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['uid','parentId','username','balance','expose','pl_balance','u.systemId'])
                    ->where([['username',trim($request->user)],['role',4],['status',1]])->first();
                if( $user != null ){
                    //$token = md5($request->game_id.'-'.$user->systemId.'-'.$user->uid.'-'.$user->username.'-MPCEXCH143');
                    //$token = trim( $redis->get('CASINO_TOKEN_'.$user->uid) );
                    //if( $token == trim($request->token) ){

                        $amount = (round((int)$request->amount)/100000);
                        $plBalance = $user->pl_balance+$amount;
                        $balance = round($user->balance-$user->expose+$plBalance);

                        $checkTrans1 = $conn->table('tbl_casino_transaction')
                            ->where([['uid',$user->uid],['transaction_uuid',$request->reference_transaction_uuid]])
                            ->first();

                        $checkTrans2 = $conn->table('tbl_casino_transaction')
                            ->where([['uid',$user->uid],['recall_transaction_uuid',$request->reference_transaction_uuid]])
                            ->first();

                        if( empty($checkTrans1) && empty($checkTrans2) ){
                            $response = [
                                'status' => 'RS_ERROR_TRANSACTION_DOES_NOT_EXIST',
                                'user' => $request->user,
                                'request_uuid' => $request->request_uuid,
                                'message' => 'Transaction does not exist !'
                            ];
                            $data['request'] = json_decode(file_get_contents('php://input'));
                            $data['response'] = $response;
                            $this->casinoErrorLog($data,$sign);
                            return response()->json($response);
                        }elseif ( !empty($checkTrans1) && empty($checkTrans2) ){
                            $checkTrans = $conn->table('tbl_casino_transaction')
                                ->where([['uid',$user->uid],['transaction_uuid',$request->transaction_uuid]])->first();
                        }else{
                            $checkTrans = $conn->table('tbl_casino_transaction')
                                ->where([['uid',$user->uid],['recall_transaction_uuid',$request->transaction_uuid]])->first();
                        }

                        if( !empty($checkTrans) ){
                            $checkTrans = (object)$checkTrans;
                            if( $checkTrans->round == $request->round
                                && $checkTrans->amount == $amount ){
                                $balance = round($user->balance-$user->expose+$user->pl_balance);
                                $response = [
                                    'status' => 'RS_OK',
                                    'user' => $request->user,
                                    'request_uuid' => $request->request_uuid,
                                    'balance' => (int)($balance*100000)
                                ];
                            }else{
                                $response = [
                                    'status' => 'RS_ERROR_DUPLICATE_TRANSACTION',
                                    'user' => $request->user,
                                    'request_uuid' => $request->request_uuid,
                                    'message' => 'Transaction already exist !'
                                ];
                            }
                            $data['request'] = json_decode(file_get_contents('php://input'));
                            $data['response'] = $response;
                            $this->casinoErrorLog($data,$sign);
                            return response()->json($response);
                        }

                        $insertArr = [
                            'uid' => $user->uid,
                            'systemId' => $user->systemId,
                            'username' => $user->username,
                            'transaction_uuid' => $request->transaction_uuid,
                            'supplier_user' => $request->supplier_user,
                            'round_closed' => ( $request->round_closed == true ? 1 : 0 ),
                            'round' => $request->round,
                            'reward_uuid' => $request->reward_uuid,
                            'request_uuid' => $request->request_uuid,
                            'reference_transaction_uuid' => $request->reference_transaction_uuid,
                            'is_free' => $request->is_free,
                            'is_aggregated' => $request->is_aggregated,
                            'game_id' => $request->game_id,
                            'game_code' => $request->game_code,
                            'currency' => $request->currency,
                            'bet' => $request->bet,
                            'type' => 'CREDIT',
                            'amount' => $amount,
                            'balance' => (int)$balance,
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s'),
                            'ip_address' => $this->getClientIp(),
                            'status' => 1,
                            'recall' => 0,
                            'recall_transaction_uuid' => 'NULL',
                            'recall_reference_transaction_uuid' => 'NULL'
                        ];
                        if( $conn->table('tbl_casino_transaction')->insert($insertArr) ){

                            $this->summaryUpdate($user->uid,$amount,0,'CREDIT');
                            $balance = $this->currentBalance($user->uid,$amount,'CREDIT','CLIENT');

                            $casinoGameName = isset($casinoGames['name']) ? $casinoGames['name'] : 'GameId: '.$request->game_id;
                            $description = 'Casino > '.$casinoGameName.' > Round #'.$request->round.' > Win';
                            $transDefaultArr = [
                                'systemId' => $user->systemId,
                                'clientId' => $user->uid,
                                'childId' => 0,
                                'sid' => 9999,
                                'eid' => $request->game_id,
                                'mid' => $request->round.'-W',
                                'result' => '-',
                                'mType' => 'casino',
                                'p_amount' => 0,
                                'c_amount' => 0,
                                'description' => $description,
                                'remark' => 'Transactional Entry',
                                'status' => 1,
                                'created_on' => date('Y-m-d H:i:s'),
                                'updated_on' => date('Y-m-d H:i:s')
                            ];

                            $insertArr = $transDefaultArr;

                            $insertArr['userId'] = $user->uid;
                            $insertArr['parentId'] = $user->parentId;
                            $insertArr['amount'] = $amount;
                            $insertArr['balance'] = $balance;
                            $insertArr['type'] = 'CREDIT';
                            $insertArr['eType'] = 0; // 0 for transaction

                            $checkWhere = [['eid',$request->game_id],['mid',$request->round.'-W'],['userId',$user->uid]];
                            $transTbl = $this->currentTable('tbl_transaction_client');
                            $checkTransClient = $conn3->table($transTbl)
                                ->where($checkWhere);
                            if( $checkTransClient->first() ){
                                $updateTransArr = [
                                    'amount' => $conn->raw('amount + ' . $amount),
                                    'updated_on' => date('Y-m-d H:i:s')
                                ];
                                $transTbl = $this->currentTable('tbl_transaction_client');
                                if( $conn3->table($transTbl)->where($checkWhere)->update($updateTransArr) ){
                                    $cUser = $conn2->table('tbl_user_info')->where([['uid',$user->uid]])->first();
                                    $balance = round($cUser->balance-$cUser->expose+$cUser->pl_balance);

                                    $response = [
                                        'status' => 'RS_OK',
                                        'user' => $user->username,
                                        'request_uuid' => $request->request_uuid,
                                        'balance' => (int)($balance*100000)
                                    ];
                                }else{
                                    $response = [
                                        'status' => 'RS_ERROR_UNKNOWN',
                                        'user' => $request->user,
                                        'request_uuid' => $request->request_uuid,
                                        'message' => 'Error in transaction !'
                                    ];
                                }

                            }else{
                                $transTbl = $this->currentTable('tbl_transaction_client');
                                if( $conn3->table($transTbl)->insert($insertArr) ){
                                    $cUser = $conn2->table('tbl_user_info')->where([['uid',$user->uid]])->first();
                                    $balance = round($cUser->balance-$cUser->expose+$cUser->pl_balance);

                                    $response = [
                                        'status' => 'RS_OK',
                                        'user' => $user->username,
                                        'request_uuid' => $request->request_uuid,
                                        'balance' => (int)($balance*100000)
                                    ];
                                }else{
                                    $response = [
                                        'status' => 'RS_ERROR_UNKNOWN',
                                        'user' => $request->user,
                                        'request_uuid' => $request->request_uuid,
                                        'message' => 'Error in transaction !'
                                    ];
                                }

                            }

                        }else{
                            $response = [
                                'status' => 'RS_ERROR_UNKNOWN',
                                'user' => $request->user,
                                'request_uuid' => $request->request_uuid,
                                'message' => 'Error in insert transaction!'
                            ];
                        }

//                    }else{
//                        $response = [
//                            'status' => 'RS_ERROR_INVALID_TOKEN',
//                            'user' => $request->user,
//                            'request_uuid' => $request->request_uuid,
//                            'message' => 'Invalid token!'
//                        ];
//                    }
                }else{
                    $response = [
                        'status' => 'RS_ERROR_USER_DISABLED',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'User not found!'
                    ];
                }
            }else{
                $response = [
                    'status' => 'RS_ERROR_WRONG_SYNTAX',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Request parameter missing!'
                ];
            }

            $data['request'] = json_decode(file_get_contents('php://input'));
            $data['response'] = $response;
            $this->casinoErrorLog($data,$sign);
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }
    }

    /**
     * action Rollback
     */
    public function actionRollback(Request $request)
    {
        try{
            $redis = Redis::connection();
            $data['api'] = 'ROLLBACK API';
            $sign = $request->header('Casino-Signature');
            if( !$this->verifySignature(file_get_contents('php://input'),$sign) ){
                $response = [
                    'status' => 'RS_ERROR_INVALID_SIGNATURE',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Invalid signature!'
                ];
                $data['request'] = json_decode(file_get_contents('php://input'));
                $data['response'] = $response;
                $this->casinoErrorLog($data,$sign);
                return response()->json($response);
            }

            if( isset($request->user) && isset($request->transaction_uuid) && isset($request->token)
                && isset($request->round_closed) && isset($request->request_uuid) && isset($request->round)
                && isset($request->game_id) && isset($request->reference_transaction_uuid) ){

                $conn = DB::connection('mongodb');
                $conn2 = DB::connection('mysql');

                $user = $conn2->table('tbl_user')
                    ->where([['username',trim($request->user)],['role',4],['status',1]])->first();

                if( $user != null ){
                    $user = (object)$user;
                    $systemId = $user->systemId;
                    $userId = $user->id;
                    $userName = $user->username;
                    //$token = trim( $redis->get('CASINO_TOKEN_'.$user->id) );
                    //$token = md5($request->game_id.'-'.$systemId.'-'.$userId.'-'.$userName.'-MPCEXCH143');
                    //if( $token == trim($request->token) ){

                        $checkTrans = $conn->table('tbl_casino_transaction')
                            ->where([['recall_transaction_uuid',$request->transaction_uuid]])->first();
                        if( !empty($checkTrans) ){
                            $checkTrans = (object)$checkTrans;
                            if( $checkTrans->round == $request->round
                                && $checkTrans->recall_reference_transaction_uuid == $request->reference_transaction_uuid ){
                                $cUser = $conn2->table('tbl_user_info')->where([['uid',$userId]])->first();
                                $balance = $cUser->balance-$cUser->expose+$cUser->pl_balance;
                                $response = [
                                    'status' => 'RS_OK',
                                    'user' => $request->user,
                                    'request_uuid' => $request->request_uuid,
                                    'balance' => (int)($balance*100000)
                                ];
                            }else{
                                $response = [
                                    'status' => 'RS_ERROR_DUPLICATE_TRANSACTION',
                                    'user' => $request->user,
                                    'request_uuid' => $request->request_uuid,
                                    'message' => 'Transaction already exist !'
                                ];
                            }

                            $data['request'] = json_decode(file_get_contents('php://input'));
                            $data['response'] = $response;
                            $this->casinoErrorLog($data,$sign);
                            return response()->json($response);
                        }

                        $where2 = [['uid',$userId],['transaction_uuid',$request->reference_transaction_uuid]];
                        $trans = $conn->table('tbl_casino_transaction')
                            ->where($where2)->first();
                        if( !empty($trans) ){
                            $trans = (object)$trans;

                            if( $trans->recall == 1 ){
                                $cUser = $conn2->table('tbl_user_info')->where([['uid',$userId]])->first();
                                $balance = $cUser->balance-$cUser->expose+$cUser->pl_balance;
                                $response = [
                                    'status' => 'RS_OK',
                                    'user' => $request->user,
                                    'request_uuid' => $request->request_uuid,
                                    'balance' => (int)($balance*100000)
                                ];
                                $data['request'] = json_decode(file_get_contents('php://input'));
                                $data['response'] = $response;
                                $this->casinoErrorLog($data,$sign);
                                return response()->json($response);
                            }

                            if( $trans->type == 'CREDIT' ){
                                $marketId = $trans->round.'-W';
                            }else{
                                $marketId = $trans->round.'-B';
                            }

                            $this->transactionRecall($marketId);

                            $updateArr = [
                                'updated_on' => date('Y-m-d H:i:s'),
                                'recall' => 1,
                                'recall_transaction_uuid' => $request->transaction_uuid,
                                'recall_reference_transaction_uuid' => $request->reference_transaction_uuid
                            ];
                            if( $conn->table('tbl_casino_transaction')
                                ->where($where2)->update($updateArr) ){

                                $cUser = $conn2->table('tbl_user_info')->where([['uid',$userId]])->first();
                                $balance = $cUser->balance-$cUser->expose+$cUser->pl_balance;
                                $response = [
                                    'status' => 'RS_OK',
                                    'user' => $user->username,
                                    'request_uuid' => $request->request_uuid,
                                    'balance' => (int)($balance*100000)
                                ];
                            }else{
                                $response = [
                                    'status' => 'RS_ERROR_UNKNOWN',
                                    'user' => $request->user,
                                    'request_uuid' => $request->request_uuid,
                                    'message' => 'Error in update transaction!'
                                ];
                            }

                        }else{
                            $cUser = $conn2->table('tbl_user_info')->where([['uid',$userId]])->first();
                            $balance = $cUser->balance-$cUser->expose+$cUser->pl_balance;
                            $response = [
                                'status' => 'RS_OK',
                                'user' => $request->user,
                                'request_uuid' => $request->request_uuid,
                                'balance' => (int)($balance*100000)
                            ];
                        }

//                    }else{
//                        $response = [
//                            'status' => 'RS_ERROR_INVALID_TOKEN',
//                            'user' => $request->user,
//                            'request_uuid' => $request->request_uuid,
//                            'message' => 'Invalid token!'
//                        ];
//                    }
                }else{
                    $response = [
                        'status' => 'RS_ERROR_USER_DISABLED',
                        'user' => $request->user,
                        'request_uuid' => $request->request_uuid,
                        'message' => 'User not found!'
                    ];
                }
            }else{
                $response = [
                    'status' => 'RS_ERROR_WRONG_SYNTAX',
                    'user' => $request->user,
                    'request_uuid' => $request->request_uuid,
                    'message' => 'Request parameter missing!'
                ];
            }

            $data['request'] = json_decode(file_get_contents('php://input'));
            $data['response'] = $response;
            $this->casinoErrorLog($data,$sign);
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }
    }

    //transaction Recall
    public function transactionRecall($marketId){
        try {
            $conn3 = DB::connection('mysql3');
            $transArr = [];
            $select = ['userId', 'type', 'amount', 'p_amount', 'eType'];
            $where = [['status', 1],['mid',$marketId]];

            $transTbl = $this->currentTable('tbl_transaction_client');
            $transClient = $conn3->table($transTbl)
                ->select($select)->where($where)->get();
            if ($transClient != null) {
                foreach ( $transClient as $cData ){
                    $transArr[] = $cData;
                }
            }

            $transTbl = $this->currentTable('tbl_transaction_parent');
            $transParent = $conn3->table($transTbl)
                ->select($select)->where($where)->get();
            if ($transParent != null) {
                foreach ( $transParent as $pData ){
                    $transArr[] = $pData;
                }
            }

            $transTbl = $this->currentTable('tbl_transaction_admin');
            $transAdmin = $conn3->table($transTbl)
                ->select($select)->where($where)->get();

            if ($transAdmin != null) {
                foreach ( $transAdmin as $aData ){
                    $transArr[] = $aData;
                }
            }

            if ($transArr != null) {

                foreach ($transArr as $trans) {

                    $user = DB::table('tbl_user_info')->select(['pl_balance'])
                        ->where([['uid', $trans->userId]])->first();
                    if ($user != null) {
                        if ($trans->type == 'CREDIT') {
                            $amount = (-1)*$trans->amount;
                        } else {
                            $amount = $trans->amount;
                        }

                        $updateArr = [
                            'pl_balance' => DB::raw('pl_balance + ' . $amount),
                            'updated_on' => date('Y-m-d H:i:s')
                        ];

                        DB::table('tbl_user_info')->where([['uid', $trans->userId]])
                            ->update($updateArr);

                        $summaryData = [];

                        $summaryData['uid'] = $trans->userId;
                        $summaryData['type'] = $trans->type;

                        $summaryData['ownPl'] = $trans->amount;
                        $summaryData['parentPl'] = $trans->p_amount;

                        $this->summaryRecall($summaryData);

                    }
                }

                $update = ['status' => 2];
                $transTbl = $this->currentTable('tbl_transaction_client');
                $conn3->table($transTbl)->where($where)->update($update);
                $transTbl = $this->currentTable('tbl_transaction_parent');
                $conn3->table($transTbl)->where($where)->update($update);
                $transTbl = $this->currentTable('tbl_transaction_admin');
                $conn3->table($transTbl)->where($where)->update($update);

            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Update Transaction Parent History
    public function updateTransactionParentHistory($marketId)
    {
        try {   //,['userId',$userId]
            $conn = DB::connection('mysql');
            $conn3 = DB::connection('mysql3');
            $transTbl = $this->currentTable('tbl_transaction_client');
            $transData = $conn3->table($transTbl)
                ->where([['status',1],['mid',$marketId]])->first();

            if ( $transData != null ) {
                $newUserArr = [];
                $userId = $transData->userId;
                $type = $transData->type;
                $amount = $transData->amount;
                $sid = $transData->sid;
                $eid = $transData->eid;
                $mid = $transData->mid;
                $result = $transData->result;
                $eType = $transData->eType;
                $mType = $transData->mType;
                $description = $transData->description;

                if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }

                $parentUserData = $conn->table('tbl_user_profit_loss as pl')
                    ->select(['pl.userId as userId','pl.parentId as parentId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
                    ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
                    ->where([['pl.clientId',$userId],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

                $childId = $userId;

                foreach ( $parentUserData as $user ){
                    $cUser = $conn->table('tbl_user')->select(['systemId','parentId','role'])
                        ->where([['id',$user->userId]])->first();
                    $systemId = $cUser->systemId;
                    $parentId = $cUser->parentId;
                    $role = $cUser->role;
                    $tAmount = ( $amount*$user->apl )/100;
                    $pTAmount = ( $amount*(100-$user->gpl) )/100;
                    $cTAmount = ( $amount*($user->gpl-$user->apl) )/100;

                    if( isset($newUserArr[$user->userId]) ){

                        if( $pType != 'CREDIT' ){
                            $tAmount = (-1)*$tAmount;
                            $pTAmount = (-1)*$pTAmount;
                            $cTAmount = (-1)*$cTAmount;
                        }

                        $tAmount = $newUserArr[$user->userId]['tAmount']+$tAmount;
                        $pTAmount = $newUserArr[$user->userId]['pTAmount']+$pTAmount;
                        $cTAmount = $newUserArr[$user->userId]['cTAmount']+$cTAmount;

                        $newUserArr[$user->userId]['tAmount'] = $tAmount;
                        $newUserArr[$user->userId]['pTAmount'] = $pTAmount;
                        $newUserArr[$user->userId]['cTAmount'] = $cTAmount;

                    }else{

                        if( $pType != 'CREDIT' ){
                            $tAmount = (-1)*$tAmount;
                            $pTAmount = (-1)*$pTAmount;
                            $cTAmount = (-1)*$cTAmount;
                        }

                        $newUserArr[$user->userId] = [
                            'role' => $role,
                            'systemId' => $systemId,
                            'clientId' => $userId,
                            'userId' => $user->userId,
                            'childId' => $childId,
                            'parentId' => $parentId,
                            'tAmount' => $tAmount,
                            'pTAmount' => $pTAmount,
                            'cTAmount' => $cTAmount,
                            'sid' => $sid,
                            'eid' => $eid,
                            'mid' => $mid,
                            'result' => $result,
                            'eType' => $eType,
                            'mType' => $mType,
                            'description' => $description,
                        ];
                    }
                    $childId = $user->userId;
                }

                if( $newUserArr != null ){
                    foreach ( $newUserArr as $userData ){
                        if( $userData['tAmount'] < 0 ){
                            $pType = 'DEBIT';
                            $amount = (-1)*round($userData['tAmount'],2);
                            $pAmount = (-1)*round($userData['pTAmount'],2);
                            $cAmount = (-1)*round($userData['cTAmount'],2);
                        }else{
                            $pType = 'CREDIT';
                            $amount = round($userData['tAmount'],2);
                            $pAmount = round($userData['pTAmount'],2);
                            $cAmount = round($userData['cTAmount'],2);
                        }

                        $this->summaryUpdate($userData['userId'],$amount,$pAmount,$pType);

                        $balance = $this->currentBalance($userData['userId'], $amount, $pType,'PARENT');

                        $resultArr = [
                            'systemId' => $userData['systemId'],
                            'clientId' => $userData['clientId'],
                            'userId' => $userData['userId'],
                            'childId' => $userData['childId'],
                            'parentId' => $userData['parentId'],
                            'sid' => $userData['sid'],
                            'eid' => $userData['eid'],
                            'mid' => $userData['mid'],
                            'result' => $userData['result'],
                            'eType' => $userData['eType'],
                            'mType' => $userData['mType'],
                            'type' => $pType,
                            'amount' => $amount,
                            'p_amount' => $pAmount,
                            'c_amount' => $cAmount,
                            'balance' => $balance,
                            'description' => $userData['description'],
                            'remark' => 'Transactional Entry',
                            'status' => 1,
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s')
                        ];

                        if( $userData['role'] == 1 ){ $tbl = 'tbl_transaction_admin';
                        }else{ $tbl = 'tbl_transaction_parent'; }
                        $transTbl = $this->currentTable($tbl);
                        if( $conn3->table($transTbl)->insert($resultArr) ){
                            // do something
                        }

                    }
                }
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    //Summary Update
    public function summaryUpdate($userId,$ownPl,$parentPl,$type){

        try {
            $conn = DB::connection('mysql');
            $userSummary = $conn->table('tbl_settlement_summary')
                ->where([['status',1],['uid',$userId]])->first();
            if( $userSummary != null ){

                $updatedOn = date('Y-m-d H:i:s');
                if( isset( $type ) && $type != 'CREDIT' ){
                    $ownPl = (-1)*round($ownPl,2);
                    $parentPl = (-1)*round($parentPl,2);
                }

                $updateArr = [
                    'ownPl' => $conn->raw('ownPl + ' . $ownPl),
                    'parentPl' => $conn->raw('parentPl + ' . $parentPl),
                    'updated_on' => $updatedOn
                ];

                if( $conn->table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$userId]])->update($updateArr) ){
                }else{
                    $userSummary = $conn->table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$userId]])->first();
                    $updatedOn = date('Y-m-d H:i:s');
                    $betTimeDiff = (strtotime($updatedOn)-strtotime($userSummary->updated_on) );
                    if($betTimeDiff == 0) {
                        usleep(1000);
                        $userSummary = $conn->table('tbl_settlement_summary')
                            ->where([['status',1],['uid',$userId]])->first();
                    }
                    $updateArr = [
                        'ownPl' => $userSummary->ownPl+$ownPl,
                        'parentPl' => $userSummary->parentPl+$parentPl,
                        'updated_on' => date('Y-m-d H:i:s')
                    ];
                    if( $conn->table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$userId]])->update($updateArr) ){
                        //do something
                    }
                }

            }else{
                if( isset( $type ) && $type != 'CREDIT' ){
                    $ownPl = (-1)*round($ownPl,2);
                    $parentPl = (-1)*round($parentPl,2);
                }

                $insertArr = [  'uid' => $userId, 'updated_on' => date('Y-m-d H:i:s'),
                    'ownPl' => $ownPl, 'ownComm' => 0,
                    'parentPl' => $parentPl,'parentComm' => 0 ];

                if( $conn->table('tbl_settlement_summary')->insert($insertArr) ){
                    //do something
                }

            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    //Summary Recall
    public function summaryRecall($summaryData)
    {
        try{
            if (isset($summaryData['uid'])) {
                $uid = $summaryData['uid'];
                $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
                $parentPl = isset($summaryData['ownPl']) ? $summaryData['parentPl'] : 0;

                $userSummary = DB::table('tbl_settlement_summary')
                    ->where([['status', 1], ['uid', $uid]])->first();

                if ($userSummary != null) {

                    if( isset($summaryData['type']) && $summaryData['type'] == 'CREDIT' ){
                        $ownPl = (-1)*round($ownPl,2);
                        $parentPl = (-1)*round($parentPl,2);
                    }

                    $updateArr = [
                        'ownPl' => DB::raw('ownPl + ' . $ownPl),
                        'parentPl' => DB::raw('parentPl + ' . $parentPl),
                        'updated_on' => date('Y-m-d H:i:s')
                    ];

                    DB::table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$uid]])->update($updateArr);

                } else {

                    $insertArr = ['uid' => $uid, 'ownPl' => $ownPl, 'ownComm' => 0,
                        'parentPl' => $parentPl, 'parentComm' => 0];

                    DB::table('tbl_settlement_summary')->insert($insertArr);

                }

            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // get Current Balance
    public function currentBalance($userId,$amount,$type,$role)
    {
        try {
            $conn = DB::connection('mysql');
            if( $type != 'CREDIT' ){ $amount = (-1)*$amount; }
            $updated_on = date('Y-m-d H:i:s');
            $updateArr = [
                'pl_balance' => $conn->raw('pl_balance + ' . $amount),
                'updated_on' => $updated_on
            ];

            if( $conn->table('tbl_user_info')->where([['uid',$userId]])->update($updateArr) ){
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                $balance = $cUserInfo->balance;
                $plBalance = $cUserInfo->pl_balance;
                if( $role != 'CLIENT' ){
                    return round( ( $plBalance ),2);
                }else{
                    return round( ( $balance + $plBalance ),2);
                }
            }else{
                $updated_on = date('Y-m-d H:i:s');
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                $betTimeDiff = (strtotime($updated_on)-strtotime($cUserInfo->updated_on) );
                if($betTimeDiff == 0) {
                    usleep(1000);
                    $cUserInfo = $conn->table('tbl_user_info')
                        ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                }

                $balance = $cUserInfo->balance;
                $newPlBalance = $cUserInfo->pl_balance;

                $updateArr = [
                    'pl_balance' => $newPlBalance,
                    'updated_on' => $updated_on
                ];

                if( $conn->table('tbl_user_info')->where([['uid', $userId]])->update($updateArr) ){
                    if( $role != 'CLIENT' ){
                        return round( ( $newPlBalance ),2);
                    }else{
                        return round( ( $balance + $newPlBalance ),2);
                    }
                }
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Function to get the client IP address
    public function getClientIp() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    // checkMaxStackLimit
    public function checkMaxStackLimit($requestData,$gameData,$uid)
    {
        $conn = DB::connection('mongodb');
        $maxStackLimit = 25000;

        if (isset($gameData['max_stack'])) {
            $maxStackLimit = (int)$gameData['max_stack'];
        }

        $maxStackLimit= (int)($maxStackLimit*100000);
        $reqAmount = (int)$requestData['amount'];
        if ($reqAmount > $maxStackLimit) {
            return false; exit;
        } else {

            $getTrans = $conn->table('tbl_casino_transaction')->select(['amount'])
                ->where([['type','DEBIT'],['round', $requestData['round']],['uid',(int)$uid]])
                ->get();

            if ($getTrans->isNotEmpty()) {
                foreach ($getTrans as $trans){
                    $reqAmount = $reqAmount+(int)$trans['amount'];
                }

                $reqAmount = (int)$requestData['amount'];
                if ($reqAmount > $maxStackLimit) {
                    return false; exit;
                }
            }
        }

        return true;

    }
    // getEventBetAllowAndBlockStatusAdmin
    public function getEventBetAllowAndBlockStatusAdmin($eventId)
    {
        $redis = Redis::connection();
        $blockEventIdsJson = $redis->get('Block_Event_Market');
        $blockEventIds = json_decode($blockEventIdsJson);
        if (!empty($blockEventIds) && in_array($eventId, $blockEventIds)) {
            return 1;
        }

        $betAllowEventIdsJson = $redis->get('Bet_Allowed_Events');
        $betAllowEventIds = json_decode($betAllowEventIdsJson);
        if (!empty($betAllowEventIds) && in_array($eventId, $betAllowEventIds)) {
            return 1;
        }

//        $sportDataJson = $redis->get("Sport_" . 9999);
//        $sportData = json_decode($sportDataJson);
//        if( !empty( $sportData ) && $sportData->is_block != 0){
//            return 1;
//        }

        return 0;

    }

    // get Block Status
    public function getBlockStatus($uid,$eventId,$sportId)
    {
        $checkEvent = DB::table('tbl_user_event_status')
            ->where([['eid',$eventId],['uid',$uid]])->first();
        if( $checkEvent != null ){
            return 1;
        }
        $checkSport = DB::table('tbl_user_sport_status')
            ->where([['sid',$sportId],['uid',$uid]])->first();
        if( $checkSport != null ){
            return 1;
        }

        return 0;
    }

}
