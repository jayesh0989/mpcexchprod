<?php

namespace App\Http\Controllers\Dashboard;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;


class AndroidController extends Controller
{
	
   public function androidVersion(Request $request)
    {

   $response =  response()->json([ "status" => 0 , "code" => 400 , "message" => "Bad request!" ]);
   try{
      //die('android');
      if(isset($request->systemId)){

      	  $version = DB::table('tbl_android_version')->select('id','version_name','version_code','link','status')
                            ->where([['systemId',$request->systemId],['status',1]])
                            ->first();

          if(!empty($version)){

          	$arr =[
                
                'version_name'=>$version->version_name,
                'version_code'=>$version->version_code,
                'link'=>$version->link
               
          	];


           $response =  response()->json([ "status" => 1 , "code" => 200 ,'data'=>$arr, "message" => "data found!!" ]);
          }
      }

    return $response;

   }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


  }