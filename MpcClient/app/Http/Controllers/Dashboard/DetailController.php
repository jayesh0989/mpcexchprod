<?php
namespace App\Http\Controllers\Dashboard;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;


class DetailController extends Controller
{
    private $marketIdsArr = [];
    private $bookMarketArr = [];

    /*
     * Action detail
     * Created by dream on FEB 2020
     **/
    public function detail(Request $request)
    {
        $response = [ 'status' => 0, 'code' => 400, 'message' => 'Bad request !!!', 'data' => null ];

        try{

            if( !$this->checkOrigin() ){
                return response()->json($response);
            }

            $goalsMakerData=$SetMarketData=null;
            $key=$this->haskKey();
            $requestId=$request->tnp;
            $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
            $hashKey = $request->header('hash');
            $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
            if($hashAuth==1) {

                $eventId = $request->route('id');
                //$cache = Redis::connection();
                //$eventData[] = $cache->get("Event_" . $eventId);

                $marketId = null;
                if (null != $request->route('id')) {
                    $uid = Auth::user()->id;
                    $eventId = $request->route('id');
                    $user = DB::table('tbl_user_info_view')->select('updated_on')
                        ->where('uid', $uid)->first();

                    $updatedOn = (strtotime($user->updated_on) + 0);

                    if (in_array($eventId, $this->checkUnBlockList($uid))) {
                        $response = ["status" => 1, 'code' => 404, "updatedOn" => $updatedOn, "data" => null, "message" => "This event is closed !!"];
                        return $response;
                        exit;
                    }

                    $request_data = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);

                    $firstLoad = false;
                    if (json_last_error() == JSON_ERROR_NONE) {
                        $r_data = $request_data;
                        if (isset($r_data['first']) && $r_data['first'] != 0) {
                            $firstLoad = true;
                        }
                    }
                    $eventDataCache = null;

                    $cache = Redis::connection();
                    $eventDataCache = $cache->get("Event_" . $eventId);
                    $eventData = json_decode($eventDataCache);
                    
                    $eventArr = $eventDataRunner = [];

                    if ($eventData != null) {
                        $rnr1 = $rnr2 = 'Undefined';
                        $title = $eventData->name;
                        $sportId = $eventData->sportId;
                        if ($eventDataCache != null) {
                            $eventDataRunner = json_decode($eventData->runners, 16);
                            if ($eventDataRunner != null) {
                                if (isset($eventDataRunner[0]['runner'])) {
                                    $rnr1 = $eventDataRunner[0]['runner'];
                                }
                                if (isset($eventDataRunner[1]['runner'])) {
                                    $rnr2 = $eventDataRunner[1]['runner'];
                                }
//                            if(isset($eventDataRunner[2]['runner'])){
//                                $rnr3 = $eventDataRunner[2]['runner'];
//                            }
                            }
                        }

                        if (in_array($sportId, $this->checkUnBlockSportList($uid))) {
                            $response = ['status' => 1, 'updatedOn' => $updatedOn, 'data' => null, 'message' => 'This Sport Block by Parent!!'];
                            return $response;
                            exit;
                        }


                        if (in_array($sportId, [1,2,4])) {
                            $this->bookMarketArr = $this->getBookMarketArr($uid, $eventId);
                            $matchOddData = $this->getDataMatchOdd($eventData);
                            $completedMatchData = $this->getCompletedMatch($eventData);
                            $bookMakerData = $this->getDataManualMatchOddMulti($eventId, $sportId);
                            $fancy2Data = $this->getDataManualSessionFancy($eventId);
                            $fancy3Data = $this->getSingleDataManualSessionFancy($eventId, $sportId);

                            if ($sportId == 1) {
                                $sportName = 'Football';
                                $goalsMakerData = $this->getDataGoals($eventId, $sportId);
                            } elseif ($sportId == 2) {
                                $sportName = 'Tennis';
                                $SetMarketData = $this->getDataSetMarket($eventId, $sportId);
                            } else {
                                $sportName = 'Cricket';
                            }

                            $eventArr = [
                                'title' => $title,
                                'rnr1' => $rnr1,
                                'rnr2' => $rnr2,
                                'sport' => $sportName,
                                'event_id' => $eventId,
                                'sport_id' => $sportId,
                                'match_odd' => $matchOddData,
                                'bookmaker' => $bookMakerData,
                                'fancy2' => $fancy2Data,
                                'fancy3' => $fancy3Data,
                                'completed_match' => $completedMatchData
                            ];

                            if ($sportId == 4) {
                                $winnerData = $this->getDataWinner($eventId);
                                $fancyData = $this->getDataFancy($eventId);
                                $khadoData = $this->getDatakhado($eventId);
                                $ballbyballData = $this->getDataBallbyball($eventId);
                                $tiedMatchData = $this->getTiedMatch($eventData);
                                $virtualCricket = $this->getDataVirtualCricket($eventId, $sportId);
                                $oddEvenData = $this->getOddEvenData($eventId, $sportId);
                                $meterFancyData = $this->getDataMeterFancy($eventId);
                                $eventArr['meter'] = $meterFancyData;
                                $eventArr['winner'] = $winnerData;
                                $eventArr['fancy'] = $fancyData;
                                $eventArr['khado'] = $khadoData;
                                $eventArr['ballbyball'] = $ballbyballData;
                                $eventArr['tied_match'] = $tiedMatchData;
                                $eventArr['virtual_cricket'] = $virtualCricket;
                                $eventArr['oddeven'] = $oddEvenData;
                            }

                            if($sportId == 1) {
                                $eventArr['goals'] = $goalsMakerData;
                            }
                            if($sportId == 2){
                                $eventArr['set_market'] = $SetMarketData;
                            }

                            $eventArr['is_tv'] = isset($eventData->isTv) ? $eventData->isTv : 0;
                            $eventArr['isScore'] = isset($eventData->isScore) ? $eventData->isScore : 0;

                            $eventArr['channelId'] = isset($eventData->channelId) ? $eventData->channelId : 0;
                            $score=null;
                                if ($cache->exists("EventScore_" . $eventId)) {
                                    $eventScore = $cache->get("EventScore_" . $eventId);
                                    $score = json_decode($eventScore);
                                    
                                }

                            $eventArr['scoreData'] = isset($score[0]->data)?  trim(preg_replace('/\s+/',' ', $score[0]->data)) : null;

                        }


                        if (in_array($sportId, ['998917', '7522', '3503','22', '13', '14','16'])) {
                            $this->bookMarketArr = $this->getBookMarketArr($uid, $eventId);
                            $matchOddData = $this->getDataMatchOdd($eventData);
                            $bookMakerData = $this->getDataManualMatchOddMulti($eventId, $sportId);
                            $fancy2Data = $this->getDataManualSessionFancy($eventId);
                            $fancy3Data = $this->getSingleDataManualSessionFancy($eventId, $sportId);
                            $sportDataCache= $cache->get('Sport_'.$sportId);
                            $sportDataCache = json_decode($sportDataCache);
                            // print_r($sportDataCache); exit;

                            $eventArr = [
                                'title' => $title,
                                'rnr1' => $rnr1,
                                'rnr2' => $rnr2,
                                'sport' => $sportDataCache->name,
                                'event_id' => $eventId,
                                'sport_id' => $sportId,
                                'match_odd' => $matchOddData,
                                'bookmaker' => $bookMakerData,
                                'fancy2' => $fancy2Data,
                                'fancy3' => $fancy3Data,
                                'completed_match' => null,
                                'is_tv' => 0,
                                'isScore' => 0,
                            ];

                            /*
                          $eventArr['fancy'] = null;
                          $eventArr['khado'] = null;
                          $eventArr['ballbyball'] = null;
                          $eventArr['tied_match'] = null;
                          $eventArr['virtual_cricket'] = null;
                          $eventArr['oddeven'] = null;*/

                        }
                        $eventArr['clientIp'] = $this->get_client_ip();
                        $response = ['status' => 1, 'code' => 200, 'updatedOn' => $updatedOn, "data" => ["items" => $eventArr, 'marketIdsArr' => $this->marketIdsArr], 'message' => 'Data Found !!'];

                    } else {
                        $response = ['status' => 1, 'code' => 200, 'message' => 'Data Not Found !!!', 'data' => null];
                    }
                }

                return $response;
            }else{
                return $response;
            }
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }
    function get_client_ip()
    {
        foreach (array(
                    'HTTP_CLIENT_IP',
                    'HTTP_X_FORWARDED_FOR',
                    'HTTP_X_FORWARDED',
                    'HTTP_X_CLUSTER_CLIENT_IP',
                    'HTTP_FORWARDED_FOR',
                    'HTTP_FORWARDED',
                    'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER)) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);
                    if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
                                    FILTER_FLAG_IPV4 |
                                    FILTER_FLAG_NO_PRIV_RANGE |
                                    FILTER_FLAG_NO_RES_RANGE)) {
                        return $ip;
                    }
                }
            }
        }
        return null;
    }


    //getBookMarketArr
    public function getBookMarketArr($uid,$eventId)
    {
        $marketsOnBet = DB::connection('mongodb')->table('tbl_user_market_expose')
            ->where([['eid',(int)$eventId],['uid',$uid],['status',1],['sid','!=',99]])->distinct()->select('mid')->get();
        $marketIds = [];
        if( !empty($marketsOnBet) ){

         // echo "<pre>";  print_r($marketsOnBet); exit;
            foreach ( $marketsOnBet as $key => $market ){
                $marketIds[] = $market;
            }
        }
        return $marketIds;
    }

    //check database function
    public function checkUnBlockList($uId)
    {
        $eventID =[];
        $user = DB::table('tbl_user_event_status')->select('eid')->where('uid',$uId)->get();
        if(!empty($user) && $user != null){
            foreach ($user as $value) {
                $eventID[] = $value->eid;
            }
        }
        return $eventID;
    }

    public function checkUnBlockSportList($uId)
    {
        $sportId =[];
        $user = DB::table('tbl_user_sport_status')->select('sid')->where('uid',$uId)->get();
        if(!empty($user) && $user != null){
            foreach ($user as $value) {
                $sportId[] = $value->sid;
            }
        }
        return $sportId;
    }

    //Event: getSingleDataManualSessionFancy
    public function getSingleDataManualSessionFancy($eventId,$sportId)
    {
        $items = null;
        $cache = Redis::connection();
        $eventData=$cache->get("Fancy_3_".$eventId);
        $marketList = json_decode($eventData);
        if ($marketList != null) {
            $suspended = $ballRunning = 0;
            foreach ($marketList as $data) {
                if($data->status == 1 && $data->game_over == 0){
                    if ( isset($data->suspended) ) { $suspended = $data->suspended; }
                    if ( isset($data->ball_running) ) { $ballRunning = $data->ball_running; }
                    if( in_array($data->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                    $min_stack=$data->min_stack;
                    $max_stack=$data->max_stack;
                    $max_profit_limit=$data->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                    if($marketSettingStatus == 1){
                        $Setting          = $cache->get('OTHERMARKET_SETTING');
                        if(!empty($Setting)){
                            $Setting          = json_decode($Setting);
                            $min_stack        = $Setting->Min_stack;
                            $max_stack        = $Setting->Max_stack;
                            $max_profit_limit = $Setting->Max_profit_limit;
                        }
                    }

                    $userkey="Admin_Market_".$data->marketId;
                    
                    if ($cache->exists($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$data->marketId);
                           $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                    }

                    $items[] = [
                        'id' => $data->id,
                        'eventId' => $data->eid,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'mType' => 'fancy3',
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'sportId' => $sportId,
                        'slug' => 'cricket',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        'info' => $data->info,
                        'back' => 0, 'no_rate' => 0,
                        'lay' => 0, 'yes_rate' => 0,
                    ];
                    $this->marketIdsArr[] =  $data->marketId ;
                }
            }
        }
        return $items;
    }

    //Event: getOddEvenData
    public function getOddEvenData($eventId,$sportId)
    {
        $items = null;
        $cache = Redis::connection();
        $eventData=$cache->get("Odd_Even_".$eventId);
        $marketList = json_decode($eventData);
        if ($marketList != null) {
            $suspended = $ballRunning = 0;
            foreach ($marketList as $data) {
                if($data->status == 1 && $data->game_over == 0){
                    if ( isset($data->suspended) ) { $suspended = $data->suspended; }
                    if ( isset($data->ball_running) ) { $ballRunning = $data->ball_running; }
                    if( in_array($data->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                    $min_stack=$data->min_stack;
                    $max_stack=$data->max_stack;
                    $max_profit_limit=$data->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                    if($marketSettingStatus == 1){
                        $Setting          = $cache->get('ODDEVEN_SETTING');
                        if(!empty($Setting)){
                            $Setting          = json_decode($Setting);
                            $min_stack        = $Setting->Min_stack;
                            $max_stack        = $Setting->Max_stack;
                            $max_profit_limit = $Setting->Max_profit_limit;
                        }
                    }

                    $userkey="Admin_Market_".$data->marketId;
                   if (!empty($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$data->marketId);
                     if (!empty($Setting)) {
                         $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
                    }

                    $items[] = [
                        'id' => $data->id,
                        'eventId' => $data->eid,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'mType' => 'oddeven',
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'sportId' => $sportId,
                        'slug' => 'cricket',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        'info' => $data->info,
                        'odd' => 0, 'no_rate' => 0,
                        'even' => 0, 'yes_rate' => 0,
                    ];
                    $this->marketIdsArr[] =  $data->marketId ;
                }
            }
        }
        return $items;
    }

    //Event: getDataManualMatchOdd
    public function getDataVirtualCricket($eventId, $sportId)
    {
        $items = null;
        $cache = Redis::connection();
        $eventData = $cache->get("Virtual_".$eventId);
        $market = json_decode($eventData);

        $runners = [];
        if ($market != null) {
            foreach ($market as $marketData) {
                $marketId = $marketData->marketId;
                $suspended = $marketData->suspended;
                $ballrunning = $marketData->ball_running;
                $matchOddData = json_decode($marketData->runners, true);

                if( $marketData->status == 1 && $marketData->game_over == 0){
                    if ($matchOddData != null) {
                        $runners = [];
                        foreach ($matchOddData as $data) {

                        $min_stack=$marketData->min_stack;
                        $max_stack=$marketData->max_stack;
                        $max_profit_limit=$marketData->max_profit_limit;

                        $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                            $Setting          = $cache->get('VIRTUAL_CRICKET_SETTING');
                            $Setting          = json_decode($Setting);
                            if(!empty($Setting)){
                                $min_stack        = $Setting->Min_stack;
                                $max_stack        = $Setting->Max_stack;
                                $max_profit_limit = $Setting->Max_profit_limit;
                            }
                        }

                        $userkey="Admin_Market_".$marketData->marketId;
                        if (!empty($userkey)) {
                         $Setting = $cache->get("Admin_Market_".$marketData->marketId);
                         if (!empty($Setting)) {
                          $Setting = json_decode($Setting);
                                $min_stack=$Setting->min_stack;
                                $max_stack=$Setting->max_stack;
                                $max_profit_limit=$Setting->max_profit_limit;
                         }
                        }
                            $runners[] = [
                                'sportId' => $sportId,
                                'marketId' => $marketId,
                                'eventId' => $eventId,
                                'sec_id' => $data['secId'],
                                'runner' => $data['runner'],
                                'mType' => 'virtual_cricket',
                                'suspended' => $suspended,
                                'ballrunning' => $ballrunning,
                                'minStack' => $min_stack,
                                'maxStack' => $max_stack,
                                'maxProfitLimit' => $max_profit_limit,
                                'backprice' =>0,
                                'backsize' => '',
                                'layprice' => 0,
                                'laysize' => ''
                            ];
                        }
                    }
                    $items[] = [
                        'marketName' => $marketData->title,
                        'sportId' => $sportId,
                        'market_id' => $marketId,
                        'event_id' => $eventId,
                        'suspended' => $suspended,
                        'ballrunning' => $ballrunning,
                        'info' => $marketData->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $marketId ;
                }
            }
        }

        return $items;
    }

    //Event: getDataGoals
    public function getDataGoals($eventId, $sportId)
    {
        $items = null; $eventData = null;
        $cache = Redis::connection();
        $eventData = $cache->get("Goals_".$eventId);
        $market = json_decode($eventData);
        $runners = $marketData = [];
        if ($market != null) {
            foreach ($market as $marketData) {
                if(!isset( $marketData->marketId)){
                    $marketData = json_decode($marketData);
                }

                if( $marketData != null ){
                    $marketId = $marketData->marketId;
                    $suspended = $marketData->suspended;
                    $ballrunning = $marketData->ball_running;
                    $matchOddData = json_decode($marketData->runners, true);

                    if( $marketData->status == 1 && $marketData->game_over == 0){
                       if ($matchOddData != null) {
                          $runners = [];
                        foreach ($matchOddData as $data) {

                            $min_stack=$marketData->min_stack;
                            $max_stack=$marketData->max_stack;
                            $max_profit_limit=$marketData->max_profit_limit;

                            $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                            if($marketSettingStatus == 1){
                                //$Setting          = $cache->get('MATCHODD_SETTING');
                                //$Setting          = json_decode($Setting);


                            if($sportId==1) {
                                $eventLimitDataJson1 = $cache->get("FOOTBALL_EVENT_SETTING");
                                $Setting = json_decode($eventLimitDataJson1);
                                }
                                if($sportId==2) {
                                $eventLimitDataJson1 = $cache->get("TENNIS_EVENT_SETTING" );
                                $Setting = json_decode($eventLimitDataJson1);
                                }
                                if($sportId==4) {
                                $eventLimitDataJson1 = $cache->get("CRICKET_EVENT_SETTING");
                                $Setting = json_decode($eventLimitDataJson1);
                                }
                                if(!empty($Setting)){
                                    $min_stack        = $Setting->Min_stack;
                                    $max_stack        = $Setting->Max_stack;
                                    $max_profit_limit = $Setting->Max_profit_limit;
                                }
                            }

                            $userkey="Admin_Market_".$marketData->marketId;
                            if (!empty($userkey)) {
                                $Setting = $cache->get("Admin_Market_".$marketData->marketId);
                                if (!empty($Setting)) {
                                    $Setting = json_decode($Setting);
                                $min_stack=$Setting->min_stack;
                                $max_stack=$Setting->max_stack;
                                $max_profit_limit=$Setting->max_profit_limit;
                                }
                            }
                                $runners[] = [
                                    'sportId' => $sportId,
                                    'marketId' => $marketId,
                                    'eventId' => $eventId,
                                    'sec_id' => $data['secId'],
                                    'runner' => $data['runner'],
                                    'mType' => 'goals',
                                    'suspended' => $suspended,
                                    'ballrunning' => $ballrunning,
                                    'minStack' => $min_stack,
                                    'maxStack' => $max_stack,
                                    'maxProfitLimit' => $max_profit_limit,
                                    'backprice' =>0,
                                    'backsize' => '',
                                    'layprice' => 0,
                                    'laysize' => ''
                                ];
                            }
                        }
                        $items[] = [
                            'marketName' => $marketData->title,
                            'sportId' => $sportId,
                            'market_id' => $marketId,
                            'event_id' => $eventId,
                            'suspended' => $suspended,
                            'ballrunning' => $ballrunning,
                            'info' => $marketData->info,
                            'runners' => $runners,
                        ];
                        $this->marketIdsArr[] =  $marketId ;
                    }
                }
            }
        }

        return $items;
    }


    //Event: getDataSetMarket
    public function getDataSetMarket($eventId, $sportId)
    {
        $items = $eventData = null;
        $cache = Redis::connection();
        $eventData = $cache->get("Set_Market_".$eventId);
        $market = json_decode($eventData);
        $runners = $matchOddData = [];
        if ( $market != null ) {
            foreach ($market as $marketData) {

                if( !isset($marketData->marketId) ){
                    $marketData = json_decode($marketData);
                }
                if( $marketData != null ){
                    $marketId = $marketData->marketId;
                    $suspended = $marketData->suspended;
                    $ballrunning = $marketData->ball_running;
                    $matchOddData = json_decode($marketData->runners, true);

                    if( $marketData->status == 1 && $marketData->game_over == 0){
                        if ($matchOddData != null) {
                            $runners = [];
                            foreach ($matchOddData as $data) {

                            $min_stack=$data->min_stack;
                            $max_stack=$data->max_stack;
                            $max_profit_limit=$data->max_profit_limit;
                            
                            $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                            if($marketSettingStatus == 1){
                                // $Setting = $cache->get('MATCHODD_SETTING');
                                // $Setting = json_decode($Setting);

                                
                            if($sportId==1) {
                                $eventLimitDataJson1 = $cache->get("FOOTBALL_EVENT_SETTING");
                                $Setting = json_decode($eventLimitDataJson1);
                                }
                                if($sportId==2) {
                                $eventLimitDataJson1 = $cache->get("TENNIS_EVENT_SETTING" );
                                $Setting = json_decode($eventLimitDataJson1);
                                }
                                if($sportId==4) {
                                $eventLimitDataJson1 = $cache->get("CRICKET_EVENT_SETTING");
                                $Setting = json_decode($eventLimitDataJson1);
                                }
                                
                                if(!empty($Setting)){
                                
                                $min_stack = $Setting->Min_stack;
                                $max_stack = $Setting->Max_stack;
                                $max_profit_limit = $Setting->Max_profit_limit;
                            }
                            }

                            $userkey="Admin_Market_".$data->marketId;
                            if (!empty($userkey)) {
                                $Setting = $cache->get("Admin_Market_".$data->marketId);
                                $Setting = json_decode($Setting);
                                $min_stack=$Setting->min_stack;
                                $max_stack=$Setting->max_stack;
                                $max_profit_limit=$Setting->max_profit_limit;
                            }
                                $runners[] = [
                                    'sportId' => $sportId,
                                    'marketId' => $marketId,
                                    'eventId' => $eventId,
                                    'sec_id' => $data['secId'],
                                    'runner' => $data['runner'],
                                    'mType' => 'set_market',
                                    'suspended' => $suspended,
                                    'ballrunning' => $ballrunning,
                                    'minStack' => $min_stack,
                                    'maxStack' => $max_stack,
                                    'maxProfitLimit' => $max_profit_limit,
                                    'backprice' =>0,
                                    'backsize' => '',
                                    'layprice' => 0,
                                    'laysize' => ''
                                ];
                            }
                        }
                        $items[] = [
                            'marketName' => $marketData->title,
                            'sportId' => $sportId,
                            'market_id' => $marketId,
                            'event_id' => $eventId,
                            'suspended' => $suspended,
                            'ballrunning' => $ballrunning,
                            'info' => $marketData->info,
                            'runners' => $runners,
                        ];
                        $this->marketIdsArr[] =  $marketId ;
                    }
                }

            }
        }

        return $items;
    }
    //Event: getDataManualMatchOdd
    public function getDataManualMatchOddMulti($eventId, $sportId)
    {
        $items = null;
        $cache = Redis::connection();
        $eventData = $cache->get("Bookmaker_".$eventId);
        $market = json_decode($eventData);
        $runners = [];
        if ($market != null) {


            foreach ($market as $marketData) {
                $marketId = $marketData->marketId;
                $suspended = $marketData->suspended;
                $ballrunning = $marketData->ball_running;
                $matchOddData = json_decode($marketData->runners, true);

                $min_stack=$marketData->min_stack;
                $max_stack=$marketData->max_stack;
                $max_profit_limit=$marketData->max_profit_limit;

                $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                if($marketSettingStatus == 1){
                    $Setting = $cache->get('BOOKMAKER_SETTING');
                        if(!empty($Setting)){
                        $Setting = json_decode($Setting);
                        $min_stack = $Setting->Min_stack;
                        $max_stack = $Setting->Max_stack;
                        $max_profit_limit = $Setting->Max_profit_limit;
                       }
                }

                $userkey="Admin_Market_".$marketData->marketId;
                            
                            if (!empty($userkey)) {//if ($cache->exists($userkey)) {
                                    $Setting = $cache->get("Admin_Market_".$marketData->marketId);
                                if (!empty($Setting)) {
                                    $Setting = json_decode($Setting);
                                    $min_stack=$Setting->min_stack;
                                    $max_stack=$Setting->max_stack;
                                    $max_profit_limit=$Setting->max_profit_limit;
                                }
                            }

                if( $marketData->status == 1 && $marketData->game_over == 0){
                    if ($matchOddData != null) {
                        $runners = [];
                        foreach ($matchOddData as $data) {

                            $runners[] = [
                                'sportId' => $sportId,
                                'marketId' => $marketId,
                                'eventId' => $eventId,
                                'sec_id' => $data['secId'],
                                'runner' => $data['runner'],
                                'mType' => 'bookmaker',
                                'suspended' => $suspended,
                                'ballrunning' => $ballrunning,
                                'minStack' => $min_stack,
                                'maxStack' => $max_stack,
                                'maxProfitLimit' => $max_profit_limit,
                                'backprice' =>0,
                                'backsize' => '',
                                'layprice' => 0,
                                'laysize' => ''
                            ];
                        }
                    }
                    $items[] = [
                        'marketName' => $marketData->title,
                        'sportId' => $sportId,
                        'market_id' => $marketId,
                        'event_id' => $eventId,
                        'suspended' => $suspended,
                        'ballrunning' => $ballrunning,
                        'info' => $marketData->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $marketId ;
                }
            }
        }

        return $items;
    }

    // Event: getDataWinner
    public function getDataWinner($eventId)
    {
        $marketListArr = null; $cache = Redis::connection();
        $eventData = $cache->get("Winner_".$eventId);
        $eventData = json_decode($eventData); $runnerData = [];

        if (!empty($eventData) && isset($eventData->marketId)) {
            // print_r($eventData);
            $marketId = $eventData->marketId;
            $eventId = $eventData->eid;
            $sportId = $eventData->sid;
            $suspended = $eventData->suspended;
            $ballrunning = $eventData->ball_running;
            if( $eventData->status == 1 && $eventData->game_over == 0){
                $runnerData = json_decode($eventData->runners);
                if ($runnerData != null) {
                    $i = 0; $runnersArr = [];
                    foreach ($runnerData as $runners) {
                        $runnerName = $runners->runner;
                        $selectionId = $runners->secId;

                        $min_stack=$eventData->min_stack;
                        $max_stack=$eventData->max_stack;
                        $max_profit_limit=$eventData->max_profit_limit;

                        $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                            $Setting = $cache->get('WINNER_SETTING');
                                if(!empty($Setting)){
                                $Setting = json_decode($Setting);
                                $min_stack = $Setting->Min_stack;
                                $max_stack = $Setting->Max_stack;
                                $max_profit_limit = $Setting->Max_profit_limit;
                            }
                        }
                        $userkey="Admin_Market_".$eventData->marketId;
                        if (!empty($userkey)) {
                            $Setting = $cache->get("Admin_Market_".$eventData->marketId);
                            if (!empty($Setting)) {
                                $Setting = json_decode($Setting);
                                $min_stack=$Setting->min_stack;
                                $max_stack=$Setting->max_stack;
                                $max_profit_limit=$Setting->max_profit_limit;
                            }
                        }

                        $runnersArr[] = [
                            'slug' => 'cricket',
                            'sportId' => $sportId,
                            'marketId' => $marketId,
                            'eventId' => $eventId,
                            'suspended' => $suspended,
                            'ballrunning' => $ballrunning,
                            'selectionId' => $selectionId,
                            'runnerName' => $runnerName,
                            'minStack' => $min_stack,
                            'maxStack' => $max_stack,
                            'maxProfitLimit' => $max_profit_limit,
                            'mType' => 'winner',
                            'backprice' => '-',
                            'backsize' => '',
                            'layprice' => '-',
                            'laysize' => ''
                        ];
                        $i++;
                    }
                }

                $marketListArr = [
                    'sportId' => $sportId,
                    'slug' => 'cricket',
                    'mType' => 'winner',
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'info'=> $eventData->info,
                    'suspended' => $suspended,
                    'ballrunning' => $ballrunning,
                    'time' => '',
                    'marketName' => 'Winner',
                    'matched' => '',
                    'runners' => $runnersArr,
                    'status'=> $eventData->status,
                    'game_over' => $eventData->game_over
                ];

                $this->marketIdsArr[] = $marketId;
            }
        }

        return $marketListArr;
    }

    // Event: getDataMatchOdd
    public function getDataMatchOdd($event)
    {

        $marketListArr = $eventData = null; $cache = Redis::connection();
        $eventData= $cache->get("Match_Odd_".$event->eventId);
        $eventData = json_decode($eventData);

        if (!empty($eventData) && isset($eventData->marketId)) {
            $slug = ['1' => 'football', '2' => 'tennis', '4' => 'cricket','998917' => 'volleyball', '7522' => 'basketball', '3503' => 'dart', '16' => 'boxing', '22' => 'Table tennis', '13' => 'badminton', '14' => 'kabaddi'];
            $marketId = $eventData->marketId;
            $eventId = $eventData->eid;
            $sportId = $eventData->sid;
            $suspended = $eventData->suspended;
            $ballrunning = $eventData->ball_running;


            $min_stack=$eventData->min_stack;
            $max_stack=$eventData->max_stack;
            $max_profit_limit=$eventData->max_profit_limit;

            $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                if($marketSettingStatus == 1){
                //$Setting = $cache->get('MATCHODD_SETTING');
                //$Setting = json_decode($Setting);

            if($sportId==1) {
                $eventLimitDataJson1 = $cache->get("FOOTBALL_EVENT_SETTING");
                $Setting = json_decode($eventLimitDataJson1);
                }
                if($sportId==2) {
                $eventLimitDataJson1 = $cache->get("TENNIS_EVENT_SETTING" );
                $Setting = json_decode($eventLimitDataJson1);
                }
                if($sportId==4) {
                $eventLimitDataJson1 = $cache->get("CRICKET_EVENT_SETTING");
                $Setting = json_decode($eventLimitDataJson1);
                }
                
    

                if(!empty($Setting)){
                $min_stack = $Setting->Min_stack;
                $max_stack = $Setting->Max_stack;
                $max_profit_limit = $Setting->Max_profit_limit;
                }
            }
            $userkey="Admin_Market_".$eventData->marketId;
                    
            if (!empty($userkey)) {//if ($cache->exists($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$eventData->marketId);
                     if (!empty($Setting)) {
                        $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
            }



            if( $eventData->status == 1 && $eventData->game_over == 0){
                $runnerData = json_decode($eventData->runners);
                if ($runnerData != null) {
                    $i = 0; $runnersArr = [];
                    foreach ($runnerData as $runners) {
                        $runnerName = $runners->runner;
                        $selectionId = $runners->secId;
                        $runnersArr[] = [
                            'slug' => $slug[$sportId],
                            'sportId' => $sportId,
                            'marketId' => $marketId,
                            'eventId' => $eventId,
                            'suspended' => $suspended,
                            'ballrunning' => $ballrunning,
                            'selectionId' => $selectionId,
                            'runnerName' => $runnerName,
                            'minStack' => $min_stack,
                            'maxStack' => $max_stack,
                            'maxProfitLimit' => $max_profit_limit,
                            'mType' => 'match_odd',
                            'backprice' => '-',
                            'backsize' => '',
                            'layprice' => '-',
                            'laysize' => ''
                        ];
                        $i++;
                    }
                }

                $marketListArr = [
                    'sportId' => $sportId,
                    'slug' => $slug[$sportId],
                    'mType' => 'match_odd',
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'info'=> $eventData->info,
                    'suspended' => $suspended,
                    'ballrunning' => $ballrunning,
                    'time' => $event->time,
                    'marketName' => 'Match Odds',
                    'matched' => '',
                    'runners' => $runnersArr,
                    'status'=> $eventData->status,
                    'game_over' => $eventData->game_over
                ];

                $this->marketIdsArr[] = $marketId;
            }
        }

        return $marketListArr;
    }

    //Event: Completed match
    public function getCompletedMatch($event)
    {
        $marketListArr = null;
        $cache = Redis::connection();
        $eventData = $cache->get("Completed_Match_".$event->eventId);
        $eventData = json_decode($eventData);

        if (!empty($eventData) && isset($eventData->marketId)) {
            $slug = ['1' => 'football', '2' => 'tennis', '4' => 'cricket'];
            $marketId = $eventData->marketId;
            $eventId = $eventData->eid;
            $time = $event->time;
            $sportId = $eventData->sid;
            $suspended = $eventData->suspended;
            $ballrunning = $eventData->ball_running;

            $min_stack = $eventData->min_stack;
            $max_stack = $eventData->max_stack;
            $max_profit_limit = $eventData->max_profit_limit;

            $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                if($marketSettingStatus == 1){
                // $Setting = $cache->get('MATCHODD_SETTING');
                // $Setting = json_decode($Setting);


            if($sportId==1) {
                $eventLimitDataJson1 = $cache->get("FOOTBALL_EVENT_SETTING");
                $Setting = json_decode($eventLimitDataJson1);
                }
                if($sportId==2) {
                $eventLimitDataJson1 = $cache->get("TENNIS_EVENT_SETTING" );
                $Setting = json_decode($eventLimitDataJson1);
                }
                if($sportId==4) {
                $eventLimitDataJson1 = $cache->get("CRICKET_EVENT_SETTING");
                $Setting = json_decode($eventLimitDataJson1);
                }
                
                if(!empty($Setting)){                
                $min_stack = $Setting->Min_stack;
                $max_stack = $Setting->Max_stack;
                $max_profit_limit = $Setting->Max_profit_limit;
                }
            }
            $userkey = "Admin_Market_".$eventData->marketId;

                    //print_r($userkey); echo ". ."; print_r($Setting); die('one');
                    
                    if (!empty($userkey)) {//if ($cache->exists($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$eventData->marketId);
                     if (!empty($Setting)) {
                         $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
            }

            $runnerData = json_decode($eventData->runners);

            if( $eventData->status == 1 && $eventData->game_over == 0){
                if ($runnerData != null) {
                    $i = 0;
                    foreach ($runnerData as $runners) {
                        $runnersArr[] = [
                            'slug' => $slug[$sportId],
                            'sportId' => $sportId,
                            'marketId' => $marketId,
                            'eventId' => $eventId,
                            'suspended' => $suspended,
                            'ballrunning' => $ballrunning,
                            'selectionId' => $runners->secId,
                            'runnerName' => $runners->runner,
                            'minStack' => $min_stack,
                            'maxStack' =>  $max_stack,
                            'maxProfitLimit' => $max_profit_limit,
                            'mType' => 'completed_match',
                            'backprice' => '-',
                            'backsize' => '',
                            'layprice' => '-',
                            'laysize' => ''
                        ];

                        $i++;
                    }

                }

                $marketListArr = [
                    'sportId' => $sportId,
                    'slug' => $slug[$sportId],
                    'mType' => 'completed_match',
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'info'=> $eventData->info,
                    'suspended' => $suspended,
                    'ballrunning' => $ballrunning,
                    'time' => $time,
                    'marketName' => 'Completed Match',
                    'matched' => '',
                    'runners' => $runnersArr,
                    'game_over' => $eventData->game_over
                ];

                $this->marketIdsArr[] = $marketId;
            }
        }
        return $marketListArr;
    }

    //Event: Completed match
    public function getTiedMatch($event)
    {
        $marketListArr = null;
        $cache = Redis::connection();
        $eventData = $cache->get("Tied_Match_".$event->eventId);
        $eventData = json_decode($eventData);

        if (!empty($eventData) && isset($eventData->marketId)) {
            $slug = ['1' => 'football', '2' => 'tennis', '4' => 'cricket'];
            $marketId = $eventData->marketId;
            $eventId = $eventData->eid;
            $time = $event->time;
            $sportId = $eventData->sid;
            $suspended = $eventData->suspended;
            $ballrunning = $eventData->ball_running;
            if( $eventData->status == 1 && $eventData->game_over == 0){
                $runnerData = json_decode($eventData->runners);
                if ($runnerData != null) {
                    $runnersArr = [];
                    //print_r($runnerData);die('stop');
                    foreach ($runnerData as $runners) {

                    $min_stack=$eventData->min_stack;
                    $max_stack=$eventData->max_stack;
                    $max_profit_limit=$eventData->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                        // $Setting = $cache->get('MATCHODD_SETTING');
                        // $Setting = json_decode($Setting);

                        
                    if($sportId==1) {
                        $eventLimitDataJson1 = $cache->get("FOOTBALL_EVENT_SETTING");
                        $Setting = json_decode($eventLimitDataJson1);
                        }
                        if($sportId==2) {
                        $eventLimitDataJson1 = $cache->get("TENNIS_EVENT_SETTING" );
                        $Setting = json_decode($eventLimitDataJson1);
                        }
                        if($sportId==4) {
                        $eventLimitDataJson1 = $cache->get("CRICKET_EVENT_SETTING");
                        $Setting = json_decode($eventLimitDataJson1);
                        }
                        
                        if(!empty($Setting)){
                        $min_stack = $Setting->Min_stack;
                        $max_stack = $Setting->Max_stack;
                        $max_profit_limit = $Setting->Max_profit_limit;
                        }
                    }
                    $userkey="Admin_Market_".$eventData->marketId;
                    if (!empty($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$eventData->marketId);
                     if (!empty($Setting)) {
                         $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
                    }
                        $runnersArr[] = [
                            'slug' => $slug[$sportId],
                            'sportId' => $sportId,
                            'marketId' => $marketId,
                            'eventId' => $eventId,
                            'suspended' => $suspended,
                            'ballrunning' => $ballrunning,
                            'selectionId' => $runners->secId,
                            'runnerName' => $runners->runner,
                            'minStack' => $min_stack,
                            'maxStack' => $max_stack,
                            'maxProfitLimit' => $max_profit_limit,
                            'mType' => 'tied_match',
                            'backprice' => '-',
                            'backsize' => '',
                            'layprice' => '-',
                            'laysize' => ''
                        ];
                    }
                }
                $marketListArr = [
                    'sportId' => $sportId,
                    'slug' => $slug[$sportId],
                    'mType' => 'tied_match',
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'info'=> $eventData->info,
                    'suspended' => $suspended,
                    'ballrunning' => $ballrunning,
                    'time' => $time,
                    'marketName' => 'Tied Match',
                    'matched' => '',
                    'runners' => $runnersArr,
                    'game_over' => $eventData->game_over
                ];

                $this->marketIdsArr[] = $marketId;
            }
        }
        return $marketListArr;
    }

    //Event: getDataFancy
    public function getDataFancy($eventId)
    {
        $items = null;
        $cache = Redis::connection();
        $eventData = $cache->get("Fancy_".$eventId);
        $marketList = json_decode($eventData);
        if ($marketList != null) {
            $suspended = $ballRunning = 0;
            foreach ($marketList as $market) {
                if($market->status == 1 && $market->game_over == 0){
                    if ( isset($market->suspended) ) { $suspended = $market->suspended; }
                    if ( isset($market->ball_running) ) { $ballRunning = $market->ball_running; }
                    if( in_array($market->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                    $min_stack=$market->min_stack;
                    $max_stack=$market->max_stack;
                    $max_profit_limit=$market->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                        $Setting = $cache->get('SESSION_SETTING');
                        if (!empty($Setting)) {
                        $Setting = json_decode($Setting);
                        $min_stack = $Setting->MFY_MIN_STAKE_DEFAULT;
                        $max_stack = $Setting->MFY_MAX_STAKE_DEFAULT;
                        $max_profit_limit = $Setting->MFY_MAX_PROFIT_LIMIT_DEFAULT;
                        $max_odd_limit = (!empty($Setting->MFY_MAX_ODD_LIMIT_DEFAULT))? $Setting->MFY_MAX_ODD_LIMIT_DEFAULT :0; 
                        $bet_delay = $Setting->BETDELAY;
                        }
                    }
                    $userkey="Admin_Market_".$market->marketId;
                   if (!empty($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$market->marketId);
                           $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                    }

                    $items[] = [
                        'marketId' => $market->marketId,
                        'eventId' => $market->eid,
                        'title' => $market->title,
                        'info' => $market->info,
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'mType' => 'fancy',
                        'sportId' => $market->sid,
                        'slug' => 'cricket',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        'no' => 0, 'no_rate' => 0,
                        'yes' => 0, 'yes_rate' => 0,
                    ];

                    $this->marketIdsArr[] = $market->marketId ;
                }
            }

        }
        return $items;

    }

    //Event: getDataManualSessionFancy
    public function getDataManualSessionFancy($eventId)
    {
        $cache = Redis::connection();
        $eventData = $cache->get("Fancy_2_".$eventId);
        $marketList = json_decode($eventData);

        $items = null;
        if ($marketList != null) {

            $suspended = $ballRunning = 0;
            foreach ($marketList as $data) {
                if( $data->status == 1 && $data->game_over == 0){
                    $suspended = $ballRunning = 0;
                    if ( isset($data->suspended) ) { $suspended = $data->suspended; }
                    if ( isset($data->ball_running) ) { $ballRunning = $data->ball_running; }
                    if( in_array($data->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                    $min_stack=$data->min_stack;
                    $max_stack=$data->max_stack;
                    $max_profit_limit=$data->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                        $Setting = $cache->get('SESSION_SETTING');
                        if (!empty($Setting)) {
                        $Setting = json_decode($Setting);
                        $min_stack = $Setting->MFY_MIN_STAKE_DEFAULT;
                        $max_stack = $Setting->MFY_MAX_STAKE_DEFAULT;
                        $max_profit_limit = $Setting->MFY_MAX_PROFIT_LIMIT_DEFAULT;
                        $max_odd_limit = (!empty($Setting->MFY_MAX_ODD_LIMIT_DEFAULT))? $Setting->MFY_MAX_ODD_LIMIT_DEFAULT :0; 
                        $bet_delay = (!empty($Setting->BETDELAY))? $Setting->BETDELAY :0;
                        }
                    }
                    $userkey="Admin_Market_".$data->marketId;
                   if (!empty($userkey)) {//$cache->exists($userkey)
                     $Setting = $cache->get("Admin_Market_".$data->marketId);
                     if (!empty($Setting)) {
                         $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
                    }

                    $items[] = [
                        'id' => $data->id,
                        'eventId' => $data->eid,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'mType' => 'fancy2',
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'sportId' => $data->sid,
                        'slug' => 'cricket',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        'info' => $data->info,
                        'no' => 0,
                        'no_rate' => 0,
                        'yes' => 0,
                        'yes_rate' => 0,
                    ];

                    $this->marketIdsArr[] =  $data->marketId ;
                }
            }
        }

        return $items;
    }

    //Event: getDataMeterFancy
    public function getDataMeterFancy($eventId)
    {
        $cache = Redis::connection();
        $eventData = $cache->get("Meter_".$eventId);
        $marketList = json_decode($eventData);

        $items = null;
        if ($marketList != null) {
            $suspended = $ballRunning = 0;
            foreach ($marketList as $data) {
                if( $data->status == 1 && $data->game_over == 0){
                    $suspended = $ballRunning = 0;
                    if ( isset($data->suspended) ) { $suspended = $data->suspended; }
                    if ( isset($data->ball_running) ) { $ballRunning = $data->ball_running; }
                    if( in_array($data->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                    $min_stack=$data->min_stack;
                    $max_stack=$data->max_stack;
                    $max_profit_limit=$data->max_profit_limit;
                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                        $Setting = $cache->get('METER_SETTING');
                        if(!empty($Setting)){
                        $Setting = json_decode($Setting);
                        $min_stack = $Setting->Min_stack;
                        $max_stack = $Setting->Max_stack;
                        $max_profit_limit = $Setting->Max_profit_limit;
                        }
                    }
                    $userkey="Admin_Market_".$data->marketId;
                    if (!empty($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$data->marketId);
                     if (!empty($Setting)) {
                         $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
                    }

                    $items[] = [
                        'id' => $data->id,
                        'eventId' => $data->eid,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'mType' => 'meter',
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'sportId' => $data->sid,
                        'slug' => 'cricket',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        'info' => $data->info,
                        'no' => 0,
                        'no_rate' => 0,
                        'yes' => 0,
                        'yes_rate' => 0,
                    ];

                    $this->marketIdsArr[] =  $data->marketId ;
                }
            }
        }

        return $items;
    }

    //khado
    public function getDataKhado($eventId)
    {
        $cache = Redis::connection();
        $eventData = $cache->get("Khado_".$eventId);
        $marketList = json_decode($eventData);
        $items = null;
        if ($marketList != null) {
            $suspended = $ballRunning = 0;
            foreach ($marketList as $data) {
                if( $data->status  ==1 && $data->game_over == 0){
                    if (isset($data->suspended)) { $suspended = $data->suspended; }
                    if (isset($data->ball_running)) { $ballRunning = $data->ball_running; }
                    if( in_array($data->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                    $min_stack=$data->min_stack;
                    $max_stack=$data->max_stack;
                    $max_profit_limit=$data->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                        $Setting = $cache->get('KHADO_SETTING');
                        if(!empty($Setting)){
                        $Setting = json_decode($Setting);
                        $min_stack = $Setting->Min_stack;
                        $max_stack = $Setting->Max_stack;
                        $max_profit_limit = $Setting->Max_profit_limit;
                        }
                    }
                    $userkey="Admin_Market_".$data->marketId;
                    
                   if (!empty($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$data->marketId);
                     if (!empty($Setting)) {
                         $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
                    }

                    $items[] = [
                        'id' => $data->id,
                        'eventId' => $data->eid,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'mType' => 'khado',
                        'sportId' => $data->sid,
                        'slug' => 'cricket',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        'info' => $data->info,
                        'no' => 0, 'no_rate' => 0,
                        'yes' => 0, 'yes_rate' =>0,
                        'difference' => $data->diff,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }
            }
        }
        return $items;
    }

    //Event: getDataManualSessionFancy
    public function getDataBallbyball($eventId)
    {
        $cache = Redis::connection();
        $eventData=$cache->get("Ballbyball_".$eventId);
        $marketList = json_decode($eventData);
        $items = null;
        if ($marketList != null) {
            $suspended = $ballRunning = 0;

            //echo "<pre>";  print_r($marketList); exit;
            foreach ($marketList as $data) {
                if($data->status  ==1 && $data->game_over == 0){
                    if ( isset($data->suspended) ) { $suspended = $data->suspended; }
                    if ( isset($data->ball_running) ) { $ballRunning = $data->ball_running; }
                    if( in_array($data->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                    $min_stack=$data->min_stack;
                    $max_stack=$data->max_stack;
                    $max_profit_limit=$data->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                        if($marketSettingStatus == 1){
                        $Setting = $cache->get('BALLBYBALL_SETTING');
                        if(!empty($Setting)){
                        $Setting = json_decode($Setting);
                        $min_stack = $Setting->Min_stack;
                        $max_stack = $Setting->Max_stack;
                        $max_profit_limit = $Setting->Max_profit_limit;
                        }
                    }
                    $userkey="Admin_Market_".$data->marketId;
                   if (!empty($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$data->marketId);
                     if (!empty($Setting)) {
                         $Setting = json_decode($Setting);
                            $min_stack=$Setting->min_stack;
                            $max_stack=$Setting->max_stack;
                            $max_profit_limit=$Setting->max_profit_limit;
                     }
                    }

                    $items[] = [
                        'id' => $data->id,
                        'eventId' => $data->eid,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'over' => $data->over,
                        'ball' => $data->ball,
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'mType' => 'ballbyball',
                        'sportId' => $data->sid,
                        'slug' => 'cricket',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        'info' => $data->info,
                        'no' => 0, 'no_rate' => 0,
                        'yes' => 0, 'yes_rate' => 0,
                        'on_edit'=> $data->editOn
                    ];
                    $this->marketIdsArr[] =  $data->marketId ;
                }
            }
        }
        return $items;
    }

    //Event: get Profit Loss Match Odds
    /*public function getProfitLossMatchOdds($userId, $marketId, $eventId, $selId)
    {
        $book = 0;
        if (null != $userId && $marketId != null && $eventId != null && $selId != null) {
            $bookData = DB::table('tbl_book_matchodd')->select('book')
                ->where([ ['uid', $userId] , ['eid', $eventId], ['mid', $marketId], ['secId', $selId] ])
                ->first();
            if( $bookData != null ){
                $book = $bookData->book;
            }
        }
        return $book;
    }*/

    //Event: get Profit Loss Book maker
    /*public function getProfitLossBookmaker($userId, $marketId, $eventId, $selId)
    {
        $book = 0;
        if (null != $userId && $marketId != null && $eventId != null && $selId != null) {
            $bookData = DB::table('tbl_book_bookmaker')->select('book')
                ->where([ ['uid', $userId] , ['eid', $eventId], ['mid', $marketId], ['secId', $selId] ])
                ->first();
            if( $bookData != null ){
                $book = $bookData->book;
            }
        }
        return $book;
    }*/

}
