<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Model\SportList;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;

class HomeListController extends Controller
{
    public function list(Request $request)
    {
        
        try{
            // $slider[] = array('image' => 'http://3.248.221.146/DevBetexClient/public/slider/dash-slider_01.jpg');
            //$slider[] = array('image' => 'http://54.154.79.223/DevBetexClient/public/slider/slider-one.jpg');
            
            $slider[] = array('image' => 'https://api.mpcexch.com/slider/slider-three.jpg');
            $slider[] = array('image' => 'https://api.mpcexch.com/slider/slider-two.jpg');
            
        $response = response()->json(['status'=>0,'message'=>'Bad request !!!','data'=>null]);
        $pagination = []; $filters = [];
        //$uid = Auth::user()->id;

        //$sportData = DB::table('tbl_sport')->get();
        //$sportData = json_decode($sportData, True);

        $cache = Redis::connection();
        
        $sportList = $cache->get("SportList");
        $sportList = json_decode($sportList,16);

           $url = '';
           $serverUrl = DB::table('tbl_common_setting')->select('value')->where([['status',1],['key_name','SERVER_URL']])->first();

           if(!empty($serverUrl)){
             $url = $serverUrl->value.'/'.'dashboard_images';
           }
           
           //$unblockSport = $this->checkUnBlockSportList($uid);

            if(!empty($sportList)){

            foreach ($sportList as $value) {
             // print_r($value);die();
                if($value['status']==1 && $value['sport_class']=='sport'){

                // if(!in_array($value['sportId'], $unblockSport )){

                 $sportArr['sport'][] = [
                        'sport_id' => $value['sportId'],
                        'sport_name' => $value['name'],
                        'sport_slug' => strtolower($value['slug']),
                        'img' => $url.'/'.$value['img'],
                        'icon' => $url.'/'.$value['icon'],
                        'icon_class' => $value['icon_class'],
                        'is_new' => $value['is_block'],
                 ];

                  //}
                 }else  if($value['status']==1 && $value['sport_class']=='event'){

                 //if(!in_array($value['sportId'], $unblockSport )){

                 $sportArr['event'][] = [
                        'sport_id' => $value['sportId'],
                        'sport_name' => $value['name'],
                        'sport_slug' => strtolower($value['slug']),
                        'img' => $url.'/'.$value['img'],
                        'icon' => $url.'/'.$value['icon'],
                        'icon_class' => $value['icon_class'],
                        'is_new' => $value['is_block'],
                 ];

                  //}
                 }else  if($value['status']==1 && $value['sport_class']=='casino'){

                 //if(!in_array($value['sportId'], $unblockSport )){

                 $sportArr['casino'][] = [
                        'sport_id' => $value['sportId'],
                        'sport_name' => $value['name'],
                        'sport_slug' => strtolower($value['slug']),
                        'img' => $url.'/'.$value['img'],
                        'icon' => $url.'/'.$value['icon'],
                        'icon_class' => $value['icon_class'],
                        'is_new' => $value['is_block'],
                 ];

                  //}
                 }
          
               }

        	 /*  foreach ($sportList['sport'] as $value) {
              
                if($value['is_block'] == 0 && $value['status']==1){

                 if(!in_array($value['sportId'], $unblockSport )){

                 $sportArr['sport'][] = [
                        'sport_id' => $value['sportId'],
                        'sport_name' => $value['name'],
                        'sport_slug' => strtolower($value['slug']),
                        'img' => $url.'/'.$value['img'],
                        'icon' => $url.'/'.$value['icon'],
                        'icon_class' => $value['icon_class'],
                        'is_new' => $value['is_block']
                 ];

                  }
                 }
          
        	   }


                  foreach ($sportList['event'] as $value) {
              
                if($value['is_block'] == 0 && $value['status']==1){

                 if(!in_array($value['sportId'], $unblockSport )){

                 $sportArr['event'][] = [
                        'sport_id' => $value['sportId'],
                        'sport_name' => $value['name'],
                        'sport_slug' => strtolower($value['slug']),
                        'img' => $url.'/'.$value['img'],
                        'icon' => $url.'/'.$value['icon'],
                        'icon_class' => $value['icon_class'],
                        'is_new' => $value['is_block']
                 ];

                  }
                 }
          
               }


                    foreach ($sportList['casino'] as $value) {
              
                if($value['is_block'] == 0 && $value['status']==1){

                 if(!in_array($value['sportId'], $unblockSport )){

                 $sportArr['casino'][] = [
                        'sport_id' => $value['sportId'],
                        'sport_name' => $value['name'],
                        'sport_slug' => strtolower($value['slug']),
                        'img' => $url.'/'.$value['img'],
                        'icon' => $url.'/'.$value['icon'],
                        'icon_class' => $value['icon_class'],
                        'is_new' => $value['is_block']
                 ];

                  }
                 }
          
               }*/
               $settingData = DB::table('tbl_common_setting')->select('key_name', 'value')
                     ->where('status', 1)
                     ->get();

            if ($settingData != null) {
                $operator = 0;
                foreach ($settingData as $data) {

                    if (trim($data->key_name) == 'GLOBAL_COMMENTARY') {
                        $sportArr['commentry'] = $data->value;;
                    }

                    if (trim($data->key_name) == 'WHATSAPP_NO') {
                        $sportArr['whatsapp'] = json_decode($data->value);
                    }
                }

            }

            $globalCommentary_List = null;
                 $globalCommentaryList = DB::table('tbl_global_commentary')->select('id', 'commentary')->orderBy('id', 'DESC')->limit(6)->get();
                 $cnt = 0;
                 foreach ($globalCommentaryList as $dataList) {
                     if ($cnt != 0) {
                         $globalCommentary_List[] = $dataList;
                     }
                     $cnt++;
                 }

            $sportArr['globalCommentaryList'] = $globalCommentary_List;

        	$response = response()->json([ "status" => 1 , 'code'=>200, 'data' => $sportArr,"slider"=>$slider,"message" => "Data Found !" ] );
        }else{

        	$response = response()->json([ "status" => 0 , 'code'=>404, 'data' => null,"slider"=>$slider,"message" => "Data Not Found !" ] );
        }

       return $response;

    }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    public function checkUnBlockSportList($uId)
    {
       try{

        $sportId =[];
        $user = DB::table('tbl_user_sport_status')->select('sid')->where('uid',$uId)->get();

       

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $sportId[] = $value->sid; 
            }

        }
         //print_r($sportId); die('iuieor');
        return $sportId;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }



}
