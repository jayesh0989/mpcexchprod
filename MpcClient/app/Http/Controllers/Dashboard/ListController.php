<?php

namespace App\Http\Controllers\Dashboard;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\SportList;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;


class ListController extends Controller
{

    public function list(Request $request)
    {

     
        
     try{
        $response = response()->json(['status'=>0,'message'=>'Bad request !!!','data'=>null]);
        $pagination = []; $filters = [];
        $uid = Auth::user()->id;

       // $sportData = DB::table('tbl_sport')->get();
        //$sportData = json_decode($sportData, True);

        $cache = Redis::connection();
         if(isset($cache)){
         $sportData=$cache->get("SportList");
         $sportData = json_decode($sportData, True);
        }
           $url = '';
           $serverUrl = DB::table('tbl_common_setting')->select('value')->where([['status',1],['key_name','SERVER_URL']])->first();

           if(!empty($serverUrl)){
             $url = $serverUrl->value.'/'.'dashboard_images';
           }
           
           $unblockSport = $this->checkUnBlockSportList($uid);

            if(!empty($sportData)){

        	   foreach ($sportData as $sportData) {

                if($sportData['is_block'] == 0 && $sportData['status']==1){

                 if(!in_array($sportData['sportId'], $unblockSport )){
                     $sportname= $sportData['name'];
        	     $sportArr[] = [
                        'sport_id' => $sportData['sportId'],
                        'sport_name' => $sportname,
                        'sport_slug' => strtolower($sportData['slug']),
                        'img' => $url.'/'.$sportData['img'],
                        'icon' => $sportData['icon'],
                        'icon_class' => $sportData['icon_class'],
                        'is_new' => $sportData['is_block'],
                        'sport_class' => $sportData['sport_class']

        	     ];

                  }
                 }
        	   }
            
            $slider[] = array('image' => 'https://api.mpcexch.com/slider/b01.jpg');
            $slider[] = array('image' => 'https://api.mpcexch.com/slider/b02.jpg');
            $slider[] = array('image' => 'https://api.mpcexch.com/slider/dash-slider_01.jpg');

        	$response = response()->json([ "status" => 1 , 'code'=>200, 'data' => $sportArr,"slider"=>$slider,"message" => "Data Found !" ] );
        }else{

        	$response = response()->json([ "status" => 0 , 'code'=>404, 'data' => null,"message" => "Data Not Found !" ] );
        }

       return $response;

    }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }



    }


    public function getBlockStatusAdmin($eventIdOrSportId,$type)
    {
        if( $type == 'event' ) {
            $eventId = $eventIdOrSportId;
            $redis = Redis::connection();
            $blockEventIdsJson = $redis->get('Block_Event_Market');
            $blockEventIds = json_decode($blockEventIdsJson);
            if (!empty($blockEventIds) && in_array($eventId, $blockEventIds)) {
                return 1;
            } else {
                return 0;
            }
        }
    }


    public function checkUnBlockSportList($uId){
       try{

        $sportId =[];
        $user = DB::table('tbl_user_sport_status')->select('sid')->where('uid',$uId)->get();

       

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $sportId[] = $value->sid; 
            }

        }
         //print_r($sportId); die('iuieor');
        return $sportId;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }



}
