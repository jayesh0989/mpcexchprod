<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class DeleteController extends Controller
{
    public function deleteData(){
       $del_market_expose = DB::connection('mongodb')->table('tbl_user_market_expose')->where('status',2)->delete();
       $del_acces_req = DB::connection('mongodb')->table('access_request')->delete();

        $lastday = Carbon::now()->subDays(5)->format('Y-m-d h:i:s');
	     	$tbl_market = DB::connection('mongodb')->table('tbl_market_result')->where('created_on', '<', $lastday)->delete();
    
         $del_acces_req_1 = DB::connection('mysql')->table('access_request')->delete();

        $lastday1 = Carbon::now()->subDays(7)->format('Y-m-d h:i:s');

        DB::connection('mysql')->table('tbl_user_activity_log')->where('created_on', '<', $lastday1)->delete();

        $response = [ "status" => 1 , "code" => 200 , "message" => "Data Deleted !! " ];
        return $response; 
    }
}
