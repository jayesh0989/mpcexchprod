<?php

namespace App\Http\Controllers\Event;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;



class ElectionController extends Controller
{
   private $bookMarketArr = [];
   public function election(Request $request){
        
     try{
         $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bad request!"];
         $key=$this->haskKey();
         $requestId=$request->tnp;
         $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
         $hashKey = $request->header('hash');
         $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
         if($hashAuth==1) {

             $response = ['status' => 0, 'message' => 'Bad request !!!', 'data' => null];
             $eventArr = [];

             if (null != $request->route('id')) {
                 $uid = Auth::user()->id;
                 $eventId = $request->route('id');
                 $sportId = 6;
                 $a = true;
                 $updatedOn = $this->updatedOn($uid);

                 $cache = Redis::connection();
                 $eventDataCache = $cache->get("Event_" . $eventId);
                 $eventData = json_decode($eventDataCache);

                 $eventDataRunner = json_decode($eventData->runners, 16);

                 $rnr1 = '';
                 $rnr2 = '';
                 if ($eventDataRunner != null) {
                     $rnr1 = $eventDataRunner[0]['runner'];
                     $rnr2 = $eventDataRunner[1]['runner'];

                     if (isset($eventDataRunner[2]['runner'])) {
                         $rnr3 = $eventDataRunner[2]['runner'];
                     }
                 }
                 if ($eventData != null) {
                     $sportId = $eventData->sportId;
                     $title = $eventData->name;
                     $marketId = null;
                     $this->bookMarketArr = $this->getBookMarketArr($uid, $eventId);
                     $matchodd2Data = $this->getDataManualMatchOddElection($eventId, $sportId);
                     $fancy2Data = $this->getDataFancy2($uid, $eventId);
                     $fancy3Data = $this->getSingleDataManualSessionFancy($uid, $eventId, $sportId);


                     $eventArr = [
                         'title' => $title,
                         'rnr1' => $rnr1,
                         'rnr2' => $rnr2,
                         'sport' => 'Election',
                         'eventId' => $eventId,
                         'sportId' => $sportId,
                         'bookmaker' => $matchodd2Data,
                         'fancy2' => $fancy2Data,
                         'fancy3' => $fancy3Data
                     ];

                     if ((null == $matchodd2Data && null == $fancy2Data && null == $fancy3Data) || ($matchodd2Data == '' && $fancy2Data == '' && $fancy3Data == '')) {
                         $this->marketIdsArr = null;

                     }

                     $response = ["status" => 1, 'code' => 200, "updatedOn" => strtotime($updatedOn), "data" => ["items" => $eventArr, 'marketIdsArr' => $this->marketIdsArr, "count" => 0], 'message' => "Data Found !!!"];
                 } else {
                     $response = ["status" => 1, 'code' => 200, "updatedOn" => null, "data" => null, "message" => "This event is closed !!"];
                 }
             }
             return $response;
         }else{
             return $response;
         }
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }



    //Event: getSingleDataManualSessionFancy
    public function getSingleDataManualSessionFancy($uid, $eventId , $sportId)
    {
        
        $items = null;
        $cache = Redis::connection();
        if( isset($cache) ){
         $eventData=$cache->get("Fancy_3_".$eventId);
         $marketList = json_decode($eventData);                      
        }
       
        if ($marketList != null) {
            $dataVal = []; $info = '';
            foreach ($marketList as $data) {

               if($data->game_over == 0 && $data->status == 1){
                    if ( isset($data->suspended) ) { $suspended = $data->suspended; }
                    if ( isset($data->ball_running) ) { $ballRunning = $data->ball_running; }
                    if( in_array($data->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                /*$minStack = $data->min_stack;
                $maxStack = $data->max_stack;
                $maxProfitLimit = $data->max_profit_limit;*/

                $min_stack=$data->min_stack;
                $max_stack=$data->max_stack;
                $max_profit_limit=$data->max_profit_limit;
                $userkey="Admin_Market_".$data->marketId;
                if (!empty($userkey)) {
                    $Setting = $cache->get("Admin_Market_".$data->marketId);
                    if (!empty($Setting)) {
                        $Setting = json_decode($Setting);
                        $min_stack=$Setting->min_stack;
                        $max_stack=$Setting->max_stack;
                        $max_profit_limit=$Setting->max_profit_limit;
                    }
                }

                $info = $data->info;                
                $items[] = [
                    'id' => $data->id,
                    'eventId' => $data->eid,
                    'marketId' => $data->marketId,
                    'title' => $data->title,
                     'mType' => 'fancy3',
                    'suspended' => $suspended,
                    'ballrunning' => $ballRunning,
                    'sportId' => $sportId,
                    'slug' => 'election',
                    'is_book' => $isBook,
                    'minStack' => $min_stack,
                    'maxStack' => $max_stack,
                    'maxProfitLimit' => $max_profit_limit,
                    'info' => $info,
                    'no' =>0,
                    'no_rate' => 0,
                    'yes' => 0,
                    'yes_rate' => 0,
                ];

               
                $this->marketIdsArr[] =  $data->marketId ;
           }
       }

        }

        return $items;
}

  //Event: getDataManualMatchOdd
 public function getDataManualMatchOddElection($eventId , $sportId){
        
        $items = null;
         $cache = Redis::connection();
        if( isset($cache) ){
          $eventData=$cache->get("Bookmaker_".$eventId);
          $market = json_decode($eventData);                 
        }
  
        $runners = []; $info = '';
        if ($market != null) {
            foreach ($market as $marketData) {
                $eventName = $marketData->title;
                $marketId = $marketData->marketId;
             //   $minStack = $marketData->min_stack;
             //   $maxStack = $marketData->max_stack;
             //   $maxProfitLimit = $marketData->max_profit_limit;
                $suspended = $marketData->suspended;
                $ballrunning = $marketData->ball_running;
                $info = $marketData->info;

                $min_stack=$marketData->min_stack;
                $max_stack=$marketData->max_stack;
                $max_profit_limit=$marketData->max_profit_limit;
                $userkey="Admin_Market_".$marketData->marketId;
                if (!empty($userkey)) {
                    $Setting = $cache->get("Admin_Market_".$marketData->marketId);
                    if (!empty($Setting)) {
                        $Setting = json_decode($Setting);
                        $min_stack=$Setting->min_stack;
                        $max_stack=$Setting->max_stack;
                        $max_profit_limit=$Setting->max_profit_limit;
                    }
                }
            
        
                $matchOddData = json_decode($marketData->runners, true);
                if($marketData->game_over == 0 && $marketData->status == 1){
    
                if ($matchOddData != null) {

                    $runners = [];
                    foreach ($matchOddData as $data) {

                        $runners[] = [
                            'sportId' => $sportId,
                            'marketId' => $marketId,
                            'eventId' => $eventId,
                            'sec_id' => $data['secId'],
                            'runner' => $data['runner'],
                            'is_profitloss' => 0,
                            'suspended' => $marketData->suspended,
                            'ballrunning' => $marketData->ball_running,
                            'minStack' => $min_stack,
                            'maxStack' => $max_stack,
                            'maxProfitLimit' => $max_profit_limit,
                            'mType' => 'bookmaker',
                            'backprice' => 0,
                            'backsize' => '',
                            'layprice' => 0,
                            'laysize' => ''
                        ];
                    }
                }

                $items[] = [
                    'marketName' => $eventName,
                    'sportId' => $sportId,
                    'market_id' => $marketId,
                    'event_id' => $eventId,
                    'suspended' => $suspended,
                    'ballrunning' => $ballrunning,
                    'info' => $info,
                    'slug' => 'election',
                    'runners' => $runners,
                ];

                 $this->marketIdsArr[] = $marketId ;
            }
        }
    }

        return $items;    
}



//Event: getDataFancy
public function getDataFancy2($uid, $eventId){
     
        $items = null;    
        $cache = Redis::connection();

        if( isset($cache) ){
          $eventData=$cache->get("Fancy_2_".$eventId);
          $marketList = json_decode($eventData);       
        }
        
        if ($marketList != null) {
          
            foreach ($marketList as $market) {

                if($market->game_over == 0 && $market->status == 1){
                    $suspended = $ballRunning = 0;
                   if ( isset($market->suspended) ) { $suspended = $market->suspended; }
                   if ( isset($market->ball_running) ) { $ballRunning = $market->ball_running; }
                   if( in_array($market->marketId,$this->bookMarketArr) ){ $isBook = '1'; }else{ $isBook = '0'; }

                /*$minStack = $market->min_stack;
                $maxStack = $market->max_stack;
                $maxProfitLimit = $market->max_profit_limit;*/
                $min_stack=$market->min_stack;
                $max_stack=$market->max_stack;
                $max_profit_limit=$market->max_profit_limit;
                $userkey="Admin_Market_".$market->marketId;
                if (!empty($userkey)) {
                    $Setting = $cache->get("Admin_Market_".$market->marketId);
                    if (!empty($Setting)) {
                        $Setting = json_decode($Setting);
                        $min_stack=$Setting->min_stack;
                        $max_stack=$Setting->max_stack;
                        $max_profit_limit=$Setting->max_profit_limit;
                    }
                }
                
                $betDelay = 0;
               

                $items[] = [
                    'marketId' => $market->marketId,
                    'eventId' => $market->eid,
                    'title' => $market->title,
                    'info' => $market->info,
                    'suspended' => $suspended,
                    'ballrunning' => $ballRunning,
                     'mType' => 'fancy2',
                    'sportId' => 6,
                    'slug' => 'election',
                    'is_book' => $isBook,
                    'minStack' => $min_stack,
                    'maxStack' => $max_stack,
                    'maxProfitLimit' => $max_profit_limit,
                    'betDelay' => $betDelay,
                    'no' =>0,
                    'no_rate' =>0,
                    'yes' =>0,
                    'yes_rate' =>0,
                ];

                 $this->marketIdsArr[] = $market->marketId ;
            }
         }

        }


        return $items;
}



 //getBookMarketArr
    public function getBookMarketArr($uid,$eventId)
    {
        $marketsOnBet = DB::connection('mongodb')->table('tbl_user_market_expose')
            ->where([['eid',(int)$eventId],['uid',$uid],['status',1],['sid','!=',99]])->distinct()->select('mid')->get();
        $marketIds = [];
        if( !empty($marketsOnBet) ){
            foreach ( $marketsOnBet as $key => $market ){
                $marketIds[] = $market;
            }
        }
        return $marketIds;
    }

//get updatedOn function
public function updatedOn($uid){
      
        $user = DB::table('tbl_user_info')->select('updated_on')->where('uid',$uid)->first();

        if(!empty($user) && $user != null){

          return $user->updated_on;

        }else{

           return $user->updated_on;
        }

}


}