<?php

namespace App\Http\Controllers\Event;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;


class InplayTodayTomorrow extends Controller
{

    public function inplayTodayTomorrow(Request $request)
    {

       try{
           $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bad request!"];
           $key=$this->haskKey();
           $requestId=$request->tnp;
           $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
           $hashKey = $request->header('hash');
           $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
           if($hashAuth==1) {


               $dataArr = $cricketInplay = $tennisInplay = $footballInplay = $horseracingInplay=
               $voleyballInplay = $basketballInplay = $dartInplay = $tabletenniesInplay
                   = $badmintonInplay = $kabaddiInplay = $boxingInplay= [];
               $cricketToday = $tennisToday = $footballToday = $horseracingToday =
               $voleyballToday = $basketballToday = $dartToday = $tabletenniesToday
                   = $badmintonToday = $kabaddiToday = $boxingToday= [];
               $cricketTomorrow = $tennisTomorrow = $footballTomorrow = $horseracingTomorrow =
               $voleyballTomorrow = $basketballTomorrow = $dartTomorrow = $tabletenniesTomorrow
                   = $badmintonTomorrow = $kabaddiTomorrow = $boxingTomorrow= [];

               $uid = Auth::user()->id;
               $cache = Redis::connection();
               $sportList = $cache->get("SportList");
               $sportList = json_decode($sportList);
               if (!empty($sportList)) {
                   foreach ($sportList as $sport_List) {
                       if ($sport_List->is_block != 1) {
                           $sportId = $sport_List->sportId;
                           $sport_name = $sport_List->name;
                           $event_Data = $cache->get("Event_List_" . $sportId);
                           $eventData1 = json_decode($event_Data);
                           if(isset($eventData1->eventData)) {
                               foreach ($eventData1->eventData as $key => $value) {
                                   $value->sportname = $sport_List->slug;
                                   $eventData[] = $value;
                               }
                           }
                       }
                   }
               }
              
               $count2 = 0;
               $count1 = 0;
               $count3 = 0;
               if (!empty($eventData) && $eventData != null) {

                   $unblockEvents = $this->checkUnBlockList($uid, $eventData);
                   $unblockSport = $this->checkUnBlockSportList($uid);
                   

                   $i = 0;
                   foreach ($eventData as $event) {
                    $unblockbyAdmin = $this->getBlockStatusAdmin( $event->eventId, 'event');
                         if($unblockbyAdmin==0){
                        $eventId = $event->eventId;
                        $runners = json_decode($event->runners, true);
                            if (!empty($runners)) {
                        $runner1 = $runners[0]['runner'];
                        $runner2 = $runners[1]['runner'];
                        $today = date('Y-m-d');
                        $tomorrow = date('Y-m-d', strtotime($today . ' +1 day'));
                        $eventDate = date('Y-m-d', ($event->time));
                        }
                        $eventData1 = $cache->get("Fancy_3_" . $eventId);
                        $eventData1 = json_decode($eventData1, 16);
                        $count1 = 0;
                        if (isset($eventData1)) {
                            if ($eventData1 != null || $eventData1 != 0 || $eventData1 != ' ') {
                                $count1 = count($eventData1);
                            } else {
                                $count1 = 0;
                            }
                        }

                        //fancy 2 count
                        $eventData2 = $cache->get("Fancy_2_" . $eventId);
                        $eventData2 = json_decode($eventData2, 16);
                        $count2 = 0;
                        if (isset($eventData2)) {
                            if ($eventData2 != null || $eventData2 != 0 || $eventData2 != ' ') {
                                $count2 = count($eventData2);
                            } else {
                                $count2 = 0;
                            }
                        }

                        //fancy count
                        $eventData3 = $cache->get("Fancy_" . $eventId);
                        $eventData3 = json_decode($eventData3, 16);
                        //print_r($eventId);
                        $count3 = 0;
                        if (isset($eventData3)) {
                            if ($eventData3 != null || $eventData3 != 0 || $eventData3 != ' ') {
                                $count3 = count($eventData3);
                            } else {
                                $count3 = 0;
                            }
                        }

                        $fancycount = $count1 + $count2 + $count3;

                        if ($fancycount > 0) {
                            $isFancy = 1;
                        } else {
                            $isFancy = 0;
                        }

                        //In play List
                        if ($event->type == 'IN_PLAY' && $event->sportId == 4 && $event->is_block != 1) {

                            if (!in_array($eventId, $unblockEvents)) {

                                $var = [
                                    'slug' => 'cricket',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0
                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $cricketInplay[] = $var;
                            }

                        } else if ($event->type == 'IN_PLAY' && $event->sportId == 2 && $event->is_block != 1) {

                            if (!in_array($eventId, $unblockEvents)) {

                                $var = [
                                    'slug' => 'tennis',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $tennisInplay[] = $var;
                            }

                        } else if ($event->type == 'IN_PLAY' && $event->sportId == 1 && $event->is_block != 1) {

                            if (!in_array($eventId, $unblockEvents)) {

                                $var = [
                                    'slug' => 'football',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $footballInplay[] = $var;
                            }
                        }else if (($event->type == 'IN_PLAY' && $event->is_block != 1) && ($event->sportId == 998917 || $event->sportId == 7522 || $event->sportId == 3503 || $event->sportId == 22 || $event->sportId == 13 || $event->sportId == 14 || $event->sportId == 16 )) {

                            if (!in_array($eventId, $unblockEvents)) {
                                $sport_name = strtolower($event->sportname);
                                $var = [
                                    'slug' => $sport_name,
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }

                                if ($event->type == 'IN_PLAY' && $event->sportId == 998917 && $event->is_block != 1) {
                                    $voleyballInplay[] = $var;
                                }

                                if ($event->type == 'IN_PLAY' && $event->sportId == 7522  && $event->is_block != 1) {
                                    $basketballInplay[] = $var;
                                }

                                if ($event->type == 'IN_PLAY' && $event->sportId == 3503  && $event->is_block != 1) {
                                    $dartInplay[] = $var;
                                }

                                if ($event->type == 'IN_PLAY' && $event->sportId == 22  && $event->is_block != 1) {
                                    $tabletenniesInplay[] = $var;
                                }

                                if ($event->type == 'IN_PLAY' && $event->sportId == 13  && $event->is_block != 1) {
                                    $badmintonInplay[] = $var;
                                }

                                if ($event->type == 'IN_PLAY' && $event->sportId == 14  && $event->is_block != 1) {
                                    $kabaddiInplay[] = $var;
                                }

                                if ($event->type == 'IN_PLAY' && $event->sportId == 16  && $event->is_block != 1) {
                                    $boxingInplay[] = $var;
                                }

                            }

                        } else {
                            // Do nothing
                        }


                        //Upcoming List Today
                        if ($event->type == 'UPCOMING' && $event->sportId == 4 && $today == $eventDate && $event->is_block != 1) {

                            if (!in_array($eventId, $unblockEvents)) {


                                $var = [
                                    'slug' => 'cricket',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $cricketToday[] = $var;
                            }

                        } elseif ($event->type == 'UPCOMING' && $event->sportId == 2 && $today == $eventDate && $event->is_block != 1) {

                            if (!in_array($eventId, $unblockEvents)) {


                                $var = [
                                    'slug' => 'tennis',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = isset($odds->odds)?$odds->odds : [];

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }

                                }
                                $tennisToday[] = $var;
                            }

                        } else if ($event->type == 'UPCOMING' && $event->sportId == 1 && $today == $eventDate && $event->is_block != 1) {
                            if (!in_array($eventId, $unblockEvents)) {

                                $var = [
                                    'slug' => 'football',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $footballToday[] = $var;
                            }
                        }else if (($event->type == 'UPCOMING' && $event->is_block != 1 && $today == $eventDate) && ($event->sportId == 998917 || $event->sportId == 7522 || $event->sportId == 3503 || $event->sportId == 22 || $event->sportId == 13 || $event->sportId == 14 || $event->sportId == 16 )) {

                            if (!in_array($eventId, $unblockEvents)) {
                                $sport_name = strtolower($event->sportname);
                                $var = [
                                    'slug' => $sport_name,
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 998917 && $event->is_block != 1) {
                                    $voleyballToday[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 7522  && $event->is_block != 1) {
                                    $basketballToday[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 3503  && $event->is_block != 1) {
                                    $dartToday[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 22  && $event->is_block != 1) {
                                    $tabletenniesToday[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 13  && $event->is_block != 1) {
                                    $badmintonToday[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 14  && $event->is_block != 1) {
                                    $kabaddiToday[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 16  && $event->is_block != 1) {
                                    $boxingToday[] = $var;
                                }

                            }

                        } else {
                            // Do nothing
                        }

                        //Upcoming List Tomorrow
                        if ($event->type == 'UPCOMING' && $event->sportId == 4 && $tomorrow == $eventDate && $event->is_block != 1) {
                            if (!in_array($eventId, $unblockEvents)) {

                                $var = [
                                    'slug' => 'cricket',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $cricketTomorrow[] = $var;
                            }

                        } else if ($event->type == 'UPCOMING' && $event->sportId == 2 && $tomorrow == $eventDate && $event->is_block != 1) {
                            if (!in_array($eventId, $unblockEvents)) {

                                $var = [
                                    'slug' => 'tennis',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $tennisTomorrow[] = $var;
                            }

                        } else if ($event->type == 'UPCOMING' && $event->sportId == 1 && $tomorrow == $eventDate && $event->is_block != 1) {
                            if (!in_array($eventId, $unblockEvents)) {

                                $var = [
                                    'slug' => 'football',
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }
                                $footballTomorrow[] = $var;
                            }
                        }else if (($event->type == 'UPCOMING' && $tomorrow == $eventDate && $event->is_block != 1) && ($event->sportId == 998917 || $event->sportId == 7522 || $event->sportId == 3503 || $event->sportId == 22 || $event->sportId == 13 || $event->sportId == 14 || $event->sportId == 16 )) {

                            if (!in_array($eventId, $unblockEvents)) {
                                $sport_name = strtolower($event->sportname);
                                $var = [
                                    'slug' => $sport_name,
                                    'eventId' => $eventId,
                                    'title' => $event->name,
                                    'league' => $event->league,
                                    'time' => $event->time * 1000,
                                    'runner1' => $runner1,
                                    'runner2' => $runner2,
                                    'is_fancy' => $isFancy,
                                    'is_tv'=>isset($event->isTv) ? $event->isTv : 0

                                ];
                                if (isset($request->platform) && $request->platform == 'desktop') {
                                    $var['odds'] = [];

                                    $eventDat = $cache->get("Match_Odd_" . $eventId);
                                    $eventDat = json_decode($eventDat);
                                    if (!empty($eventDat)) {

                                        $marketId = $eventDat->marketId;
                                        $matchOdd_odds = $cache->get($marketId);

                                        $odds = json_decode($matchOdd_odds);
                                        $Match_odds = $odds->odds;

                                        foreach ($Match_odds as $key => $value) {

                                            $var['odds'][] = [

                                                'backPrice1' => $value->backPrice1,
                                                'layPrice1' => $value->layPrice1
                                            ];
                                        }
                                    }
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 998917 && $event->is_block != 1) {
                                    $voleyballTomorrow[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 7522  && $event->is_block != 1) {
                                    $basketballTomorrow[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 3503  && $event->is_block != 1) {
                                    $dartTomorrow[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 22  && $event->is_block != 1) {
                                    $tabletenniesTomorrow[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 13  && $event->is_block != 1) {
                                    $badmintonTomorrow[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 14  && $event->is_block != 1) {
                                    $kabaddiTomorrow[] = $var;
                                }

                                if ($event->type == 'UPCOMING' && $event->sportId == 16  && $event->is_block != 1) {
                                    $boxingTomorrow[] = $var;
                                }

                            }

                        } else {
                            // Do nothing
                        }


                        }
                    }
                   $i++;

               }

                   if (!empty($unblockSport)  ) {
                   if (in_array(1, $unblockSport)) {
                       $footballInplay = $footballToday = $footballTomorrow = [];
                   }
                   if (in_array(2, $unblockSport)) {
                       $tennisInplay = $tennisToday = $tennisTomorrow = [];
                   }
                   if (in_array(4, $unblockSport)) {
                       $cricketInplay = $cricketToday = $cricketTomorrow = [];
                   }

                   if (in_array(998917, $unblockSport)) {
                       $voleyballInplay = $voleyballToday = $voleyballTomorrow = [];
                   }

                   if (in_array(7522, $unblockSport)) {
                       $basketballInplay = $basketballToday = $basketballTomorrow = [];
                   }
                   if (in_array(3503, $unblockSport)) {
                       $dartInplay = $dartToday = $dartTomorrow = [];
                   }
                   if (in_array(22, $unblockSport)) {
                       $tabletenniesInplay = $tabletenniesToday = $tabletenniesTomorrow = [];
                   }
                   if (in_array(13, $unblockSport)) {
                       $badmintonInplay = $badmintonToday = $badmintonTomorrow = [];
                   }
                   if (in_array(14, $unblockSport)) {
                       $kabaddiInplay = $kabaddiToday = $kabaddiTomorrow = [];
                   }
                   if (in_array(16, $unblockSport)) {
                       $boxingInplay = $boxingToday = $boxingTomorrow = [];
                   }
               }

               $dataArr['inplay'] = [
                   [
                       'title' => 'Cricket',
                       'list' => $cricketInplay,
                       'slug' => 'cricket'
                   ],
                   [
                       'title' => 'Tennis',
                       'list' => $tennisInplay,
                       'slug' => 'tennis'
                   ],
                   [
                       'title' => 'Football',
                       'list' => $footballInplay,
                       'slug' => 'football'
                   ],
                   [
                       'title' => 'Volleyball',
                       'list' => $voleyballInplay,
                       'slug' => 'volleyball'
                   ],
                   [
                       'title' => 'Basketball',
                       'list' => $basketballInplay,
                       'slug' => 'basketball'
                   ],
                   [
                       'title' => 'Dart',
                       'list' => $dartInplay,
                       'slug' => 'dart'
                   ],
                   [
                       'title' => 'Table tennis',
                       'list' => $tabletenniesInplay,
                       'slug' => 'table_tennis'
                   ],
                   [
                       'title' => 'Badminton',
                       'list' => $badmintonInplay,
                       'slug' => 'badminton'
                   ],
                   [
                       'title' => 'Kabaddi',
                       'list' => $kabaddiInplay,
                       'slug' => 'kabaddi'
                   ],
                   [
                       'title' => 'Boxing',
                       'list' => $boxingInplay,
                       'slug' => 'boxing'
                   ],

               ];
                $time = array_column($cricketToday, 'time');
                        array_multisort($time, SORT_ASC, $cricketToday);
                $time = array_column($tennisToday, 'time');
                                array_multisort($time, SORT_ASC, $tennisToday);
                $time = array_column($footballToday, 'time');
                                array_multisort($time, SORT_ASC, $footballToday);
                $time = array_column($voleyballToday, 'time');
                                array_multisort($time, SORT_ASC, $voleyballToday);
                $time = array_column($basketballToday, 'time');
                                array_multisort($time, SORT_ASC, $basketballToday);
                $time = array_column($dartToday, 'time');
                                array_multisort($time, SORT_ASC, $dartToday);
                $time = array_column($tabletenniesToday, 'time');
                                array_multisort($time, SORT_ASC, $tabletenniesToday);
                $time = array_column($badmintonToday, 'time');
                                array_multisort($time, SORT_ASC, $badmintonToday);
                $time = array_column($kabaddiToday, 'time');
                                array_multisort($time, SORT_ASC, $kabaddiToday);
                $time = array_column($boxingToday, 'time');
                                array_multisort($time, SORT_ASC, $boxingToday);
               $dataArr['today'] = [
                   [
                       'title' => 'Cricket',
                       'list' => $cricketToday,
                       'slug' => 'cricket'
                   ],
                   [
                       'title' => 'Tennis',
                       'list' => $tennisToday,
                       'slug' => 'tennis'
                   ],
                   [
                       'title' => 'Football',
                       'list' => $footballToday,
                       'slug' => 'football'
                   ],
                   [
                       'title' => 'Volleyball',
                       'list' => $voleyballToday,
                       'slug' => 'volleyball'
                   ],
                   [
                       'title' => 'Basketball',
                       'list' => $basketballToday,
                       'slug' => 'basketball'
                   ],
                   [
                       'title' => 'Dart',
                       'list' => $dartToday,
                       'slug' => 'dart'
                   ],
                   [
                       'title' => 'Table tennies',
                       'list' => $tabletenniesToday,
                       'slug' => 'table-tennies'
                   ],
                   [
                       'title' => 'Badminton',
                       'list' => $badmintonToday,
                       'slug' => 'badminton'
                   ],
                   [
                       'title' => 'Kabaddi',
                       'list' => $kabaddiToday,
                       'slug' => 'kabaddi'
                   ],
                   [
                       'title' => 'Boxing',
                       'list' => $boxingToday,
                       'slug' => 'boxing'
                   ],

               ];

               $time = array_column($cricketTomorrow, 'time');
                        array_multisort($time, SORT_ASC, $cricketTomorrow);
                $time = array_column($tennisTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $tennisTomorrow);
                $time = array_column($footballTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $footballTomorrow);
                $time = array_column($voleyballTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $voleyballTomorrow);
                $time = array_column($basketballTomorrow, 'time');
                                 array_multisort($time, SORT_ASC, $basketballTomorrow);
                $time = array_column($dartTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $dartTomorrow);
                $time = array_column($tabletenniesTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $tabletenniesTomorrow);
                $time = array_column($badmintonTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $badmintonTomorrow);
                $time = array_column($kabaddiTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $kabaddiTomorrow);
                $time = array_column($boxingTomorrow, 'time');
                                array_multisort($time, SORT_ASC, $boxingTomorrow);

               $dataArr['tomorrow'] = [
                   [
                       'title' => 'Cricket',
                       'list' => $cricketTomorrow,
                       'slug' => 'cricket'
                   ],
                   [
                       'title' => 'Tennis',
                       'list' => $tennisTomorrow,
                       'slug' => 'tennis'
                   ],
                   [
                       'title' => 'Football',
                       'list' => $footballTomorrow,
                       'slug' => 'football'
                   ],
                   [
                       'title' => 'Volleyball',
                       'list' => $voleyballTomorrow,
                       'slug' => 'volleyball'
                   ],
                   [
                       'title' => 'Basketball',
                       'list' => $basketballTomorrow,
                       'slug' => 'basketball'
                   ],
                   [
                       'title' => 'Dart',
                       'list' => $dartTomorrow,
                       'slug' => 'dart'
                   ],
                   [
                       'title' => 'Table tennies',
                       'list' => $tabletenniesTomorrow,
                       'slug' => 'table-tennies'
                   ],
                   [
                       'title' => 'Badminton',
                       'list' => $badmintonTomorrow,
                       'slug' => 'badminton'
                   ],
                   [
                       'title' => 'Kabaddi',
                       'list' => $kabaddiTomorrow,
                       'slug' => 'kabaddi'
                   ],
                   [
                       'title' => 'Boxing',
                       'list' => $boxingTomorrow,
                       'slug' => 'boxing'
                   ],

               ];
               $response = ["status" => 1, 'code' => 200, "data" => $dataArr, "message" => 'Data Found!!'];

               return $response;

           }else{
               return $response;
           }
     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    
  }

    //check block events
public function checkUnBlockList($uId,$eventData)
    {
        $eventID =[];
        $user = DB::table('tbl_user_event_status')->select('eid')->where('uid',$uId)->get();

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $eventID[] = $value->eid; 
            }

         }
         
        return $eventID;
        
}


//check block sport 
public function checkUnBlockSportList($uId){

         $sportId =[];
        $user = DB::table('tbl_user_sport_status')->select('sid')->where('uid',$uId)->get();

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $sportId[] = $value->sid; 
            }

        }
         
        return $sportId;

}

public function getBlockStatusAdmin($eventIdOrSportId,$type)
  {
    if($type=='event') {
        $redis             = Redis::connection();
        $blockEventIdsJson = $redis->get('Block_Event_Market');
        $blockEventIds     = json_decode($blockEventIdsJson);
        if(!empty($blockEventIds)&&in_array($eventIdOrSportId,$blockEventIds)){ return 1; }else{ return 0; }
    }
  }

}


















