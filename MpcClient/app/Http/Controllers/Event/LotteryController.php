<?php

namespace App\Http\Controllers\Event;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;



class LotteryController extends Controller
{

    public function lottery(Request $request)
    {
     
     try{

         $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bad request!"];
         $key=$this->haskKey();
         $requestId=$request->tnp;
         $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
         $hashKey = $request->header('hash');
         $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
         if($hashAuth==1) {

             $eventArr = [];

             if (null != $request->route('id')) {

                 $uid = Auth::user()->id;
                 $eventId = $request->route('id');
                 $sportId = 11;
                 $updatedOn = $this->updatedOn($uid);

                 $cache = Redis::connection();
                 $eventData = $cache->get("Event_" . $eventId);
                 $eventData = json_decode($eventData);

                 if ($eventData->status == 1 && $eventData->game_over == 0) {

                     if ($eventData != null) {
                         $title = $eventData->name;
                         $lotteryData = $this->getDataLottery($uid, $eventId);
                         $eventArr = [
                             'title' => $title,
                             'sport' => 'Cricket Casino',
                             'event_id' => $eventId,
                             'sport_id' => $sportId,
                             'lottery' => $lotteryData,
                         ];

                         $response = ["status" => 1, 'code' => 200, "updatedOn" => strtotime($updatedOn), "data" => ["items" => $eventArr, "count" => 0], 'message' => 'Data Found !!'];
                     } else {
                         $response = ["status" => 1, 'code' => 200, "updatedOn" => null, "data" => null, "message" => "This event is closed !!"];
                     }
                 }
             }
             return $response;

         }else{
             return $response;
         }
}catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
}

 // getDataLottery function
public function getDataLottery($uid, $eventId){
      
        $cache = Redis::connection();
        $marketList=$cache->get("Casino_".$eventId);
        $marketList = json_decode($marketList);
     
        $items = [];
        if ($marketList != null) {
            foreach ($marketList as $data) {

               if($data->status == 1 && $data->game_over == 0){

                $minStack = $data->min_stack;
                $maxStack = $data->max_stack;
                $maxProfitLimit = $data->max_profit_limit;
    
                $items[] = [
                    'id' => $data->id,
                    'sportId' => 11,
                    'mType' => 'cricket_casino',
                    'event_id' => $data->eid,
                    'market_id' => $data->marketId,
                    'title' => $data->title,
                    'minStack' => $minStack,
                    'maxStack' => $maxStack,
                    'maxProfitLimit' => $maxProfitLimit,
                    'is_book' => $this->isBookOn($uid,$data->marketId, 'cricket_casino'),
                    'number' => 0,
                    'rate' => $data->rate,
                ];

            }
         }
        }

        return $items;
}



//Event: isBookOn
public function isBookOn($uid,$marketId, $sessionType){

        $findBet = DB::connection('mongodb')->table("tbl_bet_pending_4")
                      ->where([['result','PENDING'],['uid',$uid],['mid',$marketId],['mType',$sessionType],['status',1]])
                      ->get();
         
        if ($findBet->isEmpty()) {
            return '0';
        }
        return '1';
}

//check database function
public function updatedOn($uid){
      
        $user = DB::table('tbl_user_info')->select('updated_on')->where('uid',$uid)->first();
        if(!empty($user) && $user != null){
          return $user->updated_on;
        }else{
           return $user->updated_on;
        }
}

}