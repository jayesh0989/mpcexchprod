<?php
namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use stdClass;


class DataCleanController extends Controller
{

    public function redisClean(Request $request){
        $cache = Redis::connection();
        $cache->flushall();
        echo "Done";
    }

    public function PendingResult_Old(){
        try {
            $marketIds = null;
            $url = 'https://fawk.app/api/exchange/odds/market/result';
            // Current date and time
            $datetime = date("Y-m-d H:i:s");
            // echo "current date-->".$datetime; echo "<br>";
            // Convert datetime to Unix timestamp
            $timestamp = strtotime($datetime);
            // Subtract time from datetime
            $time = $timestamp - (1 * 60 * 30);
            // Date and time after subtraction
            $datetime = date("Y-m-d H:i:s", $time);
            // echo "convert date-->".$datetime; exit;
            $query = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')
                ->where([['result', 'PENDING'], ['status', 1],['sid',99]])->where([['created_on', '<=', $datetime]]);
            $betList = $query->distinct()->select('mid')->get();
            //  echo "<pre>"; print_r($betList);exit;
            if (!empty($betList)) {

                $settingData = DB::table('tbl_common_setting')->select('key_name', 'value')
                    ->where('key_name','TEENPATTI_OPERATORID')
                    ->first();
              echo "operatorId-->".  $operatorId =$settingData->value;
              echo "<br>";

                // print_r($betList); exit;
                foreach ($betList as $data) {

                    if (isset($data)) {

                        $marketId = $data;
                        $transactionCheck = DB::connection('mysql3')->table('tbl_transaction_client')->select(['mid'])
                            ->where([['mid', $marketId]])->first();
                        if ($transactionCheck != null) {
                            // do nothing
                        }else {
                            $resultCheck = DB::connection('mongodb')->table('tbl_teenpatti_result')->select(['user_data'])
                                ->where([['mid', $marketId]])->first();
                            if ($resultCheck != null) {
                                // do nothing
                            } else {
                                $marketIds[] = $marketId;
                            }
                        }
                    }
                }

               // print_r($marketIds); exit;
                if ($marketIds != null && !empty($marketIds)) {
                  // echo $marketIds=json_encode($marketIds); exit;
                    $post = [
                        'operatorId' => $operatorId,
                        'markets' => $marketIds
                    ];

                    // print_r($post); exit;
                    $post = json_encode($post);
                    //  print_r($post); exit;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    // print_r($response);
                    echo "done.";
                    //exit;
                }
            }


            $this->TeenpattiExposeUpdate();

            echo "<pre>"; print_r($marketIds); exit;


        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


    public function PendingResult(){
        try {
            $marketIds = null;
            $url = 'https://fawk.app/api/exchange/odds/market/resultJson';
            // Current date and time
            $datetime = date("Y-m-d H:i:s");
            // echo "current date-->".$datetime; echo "<br>";
            // Convert datetime to Unix timestamp
            $timestamp = strtotime($datetime);
            // Subtract time from datetime
            $time = $timestamp - (1 * 60 * 30);
            // Date and time after subtraction
            $datetime = date("Y-m-d H:i:s", $time);
            // echo "convert date-->".$datetime; exit;
            $query = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')
                ->where([['result', 'PENDING'], ['status', 1],['sid',99]])->where([['created_on', '<=', $datetime]]);
            $betList = $query->distinct()->select('mid')->get();
            //  echo "<pre>"; print_r($betList);exit;
            if (!empty($betList)) {

                $settingData = DB::table('tbl_common_setting')->select('key_name', 'value')
                    ->where('key_name','TEENPATTI_OPERATORID')
                    ->first();
                echo "operatorId-->".  $operatorId =$settingData->value;
                echo "<br>";

                // print_r($betList); exit;

                $updatedOn = date('Y-m-d H:i:s');
                $year = date('Y', strtotime($updatedOn));
                $month = date('m', strtotime($updatedOn));
                $currentMonth=$month.$year;
                $tableName='tbl_transaction_client_'.$currentMonth;
                foreach ($betList as $data) {

                    if (isset($data)) {

                        $marketId = $data;
                        $transactionCheck = DB::connection('mysql3')->table($tableName)->select(['mid'])
                            ->where([['mid', $marketId]])->first();
                        if ($transactionCheck != null) {
                            // do nothing
                        }else {
                            $resultCheck = DB::connection('mongodb')->table('tbl_teenpatti_result')->select(['user_data'])
                                ->where([['mid', $marketId]])->first();
                            if ($resultCheck != null) {
                                // do nothing
                            } else {
                                $marketIds[] = $marketId;
                            }
                        }
                    }
                }

                // print_r($marketIds); exit;
               /* if ($marketIds != null && !empty($marketIds)) {
                    // echo $marketIds=json_encode($marketIds); exit;
                    $post = [
                        'operatorId' => $operatorId,
                        'markets' => $marketIds
                    ];

                    // print_r($post); exit;
                    $post = json_encode($post);
                    //  print_r($post); exit;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    // print_r($response);
                    echo "done.";
                    //exit;
                }*/


                if(!empty($marketIds)){
                    foreach ($marketIds as $marketIdData) {
                      //  echo $marketIdData;      echo "<br>";
                            $post = [
                                'operatorId' => $operatorId,
                                'markets' => [$marketIdData]
                            ];

                            // print_r($post); exit;
                            $post = json_encode($post);
                              print_r($post);// exit;
                            echo "<br>";echo "<br>";
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $response = curl_exec($ch);
                            curl_close($ch);


                            // print_r($post); exit;
                            $response2=null;
                            if(!empty($response)){
                                $data = json_decode($response,true);
                                if( isset($data) && !empty($data['result'][0])) {

                                   $data1=   (object)$data['result'][0];
                                  // print_r($data); exit;
                                     $post = json_encode($data1);
                                   //  print_r($post); exit;
                                    // echo "<br>";echo "<br>";
                                    $url = 'https://api.mpcexch.com/api/poker/results';
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $url);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $response2 = curl_exec($ch);
                                    curl_close($ch);

                                    print_r($response2);
                                    echo "done.";
                                    exit;
                                }else{

                                   // echo $marketIdData; echo "<br>";
                                    $List = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->select('*')->where([['mid', $marketIdData], ['result', 'PENDING'], ['status', 1],['sid',99]])->get();
                                  //  echo "<pre>"; print_r($List); exit;
                                    if (!empty($List)) {
                                        foreach ($List as $betlist) {
                                          //  $id = $betlist->id;
                                            $betlist=(object)$betlist;
                                            //echo "<pre>"; print_r($betlist); exit;
                                            $userId = $betlist->uid;
                                            //exit;
                                            echo     $marketId = $betlist->mid;

                                            // Update User Event Expose
                                            DB::connection('mongodb')->table('tbl_user_market_expose')
                                                ->where([['uid', $userId], ['mid', $marketId]])->update(['status' => 2]);

                                            DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->where([['uid', $userId], ['mid', $marketId]])->update(['result' => 'CANCELED']);

                                            $balExpose = $expose = 0;
                                            $userMarketExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select('expose')
                                                ->where([['uid', $userId], ['status', 1]])->get();
                                            if ($userMarketExpose->isNotEmpty()) {
                                                foreach ($userMarketExpose as $data) {
                                                    $data = (object)$data;
                                                    if ((int)$data->expose > 0) {
                                                        $expose = $expose + (int)$data->expose;
                                                    }
                                                }
                                                $balExpose = round($expose);
                                            }

                                            $updated_on = date('Y-m-d H:i:s');
                                            $updateData = [
                                                'expose' => $balExpose,
                                                'updated_on' => $updated_on
                                            ];

                                            DB::connection('mysql')->table('tbl_user_info')->where('uid', $userId)->update($updateData);

                                      }
                                    }


                                }
                            }



                    }
                }
            }


          //  $this->TeenpattiExposeUpdate();

           // echo "<pre>"; print_r($marketIds); exit;


        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function actionGameResult()
    {

        try {
            $data = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);

          //  return $data;
           // print_r($result);
            if (isset($data) && isset($data['result']) && isset($data['result'][0]) && isset($data['runners'])
                && isset($data['roundId'])) {
                $userArr = $userIds = $betArr = [];
                $roundId = $data['roundId'];
                $result = $data['result'][0];

                return $result;
            }

            return 1;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    public function TeenpattiExposeUpdate()
    {

        try {

            $datetime = date("Y-m-d H:i:s");
            // echo "current date-->".$datetime; echo "<br>";
            // Convert datetime to Unix timestamp
            $timestamp = strtotime($datetime);
            // Subtract time from datetime
            $time = $timestamp - (1 * 60 * 300);
            // Date and time after subtraction
             $datetime = date("Y-m-d H:i:s", $time);

            $List = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->select('*')->where([['created_on', '<', $datetime], ['result', 'PENDING'], ['status', 1],['sid',99]])->orderBy('id', 'asc')->get();
            //echo "<pre>"; print_r($List); exit;
            if (!empty($List)) {
                foreach ($List as $betlist) {
                    $id = $betlist->id;
                    $userId = $betlist->uid;
                    //exit;
                    $marketId = $betlist->mid;
                    $eventId = $betlist->eid;
                    // Update User Event Expose
                    DB::connection('mongodb')->table('tbl_user_market_expose')
                        ->where([['uid', $userId], ['mid', $marketId]])->update(['status' => 2]);

                    DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->where([['uid', $userId], ['mid', $marketId]])->update(['result' => 'CANCELED']);

                    $balExpose = $expose = 0;
                    $userMarketExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select('expose')
                        ->where([['uid', $userId], ['status', 1]])->get();
                    if ($userMarketExpose->isNotEmpty()) {
                        foreach ($userMarketExpose as $data) {
                            $data = (object)$data;
                            if ((int)$data->expose > 0) {
                                $expose = $expose + (int)$data->expose;
                            }
                        }
                        $balExpose = round($expose);
                    }

                    $updated_on = date('Y-m-d H:i:s');
                    $updateData = [
                        'expose' => $balExpose,
                        'updated_on' => $updated_on
                    ];

                    DB::connection('mysql2')->table('tbl_user_info')->where('uid', $userId)->update($updateData);
                }
            }


            echo "Done.";
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


}