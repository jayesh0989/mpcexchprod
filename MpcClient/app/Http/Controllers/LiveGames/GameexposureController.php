<?php

namespace App\Http\Controllers\LiveGames;
use App\Http\Controllers\Controller;
use App\Model\Teenpatti;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;


class GameexposureController extends Controller
{

    /**
     * Action Bet Place
     */
    public function activityLog($message)
    {
        Log::channel('activity-log')->info($message);
    }
    public function actionBetPlaceConfirm(Request $request)
    {

        try {
            $response = [ "errorDescription" => "General", "errorCode" => 2 ];
            $requestData = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);
            //Production server
            $key='a6fbf9fc-8c67-46ef-9f05-1b8fc51f6e48';
            //Developmret server
           // $key = "3c6fe53a-4a56-4bda-aba3-215fc896f1fc";
            $generatehashkey = base64_encode(hash_hmac("sha256", file_get_contents('php://input'), $key, true));
            $hashKey = $request->header('hash');
          // if($hashKey == $hashKey) {
             if($generatehashkey == $hashKey) {
                 /* if($generatehashkey == $hashKey) {
                      $array=array('Hash'=>"Key validate",'self_key'=>$generatehashkey,'OperatorKey'=>$hashKey);
                      $this->activityLog( json_encode($array) );
                  }else{
                      $array=array('Hash'=>"Key not validate",'self_key'=>$generatehashkey,'OperatorKey'=>$hashKey);
                      $this->activityLog( json_encode($array) );
                  }*/

                 $setting = DB::table('tbl_common_setting')->select(['value'])
                     ->where([['key_name', 'LIVE_GAME_2_STATUS'], ['status', 1]])->first();

                 if ($setting != null && trim($setting->value) != 1) {
                     $response = ["errorDescription" => "UserBlocked", "errorCode" => 6];
                     return json_encode($response);  exit;
                 }

                 // print_r($requestData);
                 $array = array('api' => 'confirm bet', 'request' => $requestData);
                 $this->activityLog(json_encode($array));
                 //   exit;


                 if (isset($requestData) && isset($requestData['token'])) {


                     $access_token = '';
                     $operatorId = '';
                     if (isset($request->token) && isset($request->operatorId)) {
                         $access_token = $request->token;
                         $operatorId = $request->operatorId;
                     } else {
                         if (isset($requestData) && isset($requestData['token']) && isset($requestData['operatorId'])) {
                             $access_token = $requestData['token'];
                             $operatorId = $request->operatorId;
                         }
                     }


                     if (isset($requestData['requestId'])) {
                         $checkRequest = DB::table('access_request')->select('requestId')
                             ->where('requestId', $requestData['requestId'])->first();
                         if ($checkRequest != null) {
                             $responseData = [
                                 "operatorId" => $operatorId,
                                 "errorCode" => 2,
                                 "errorDescription" => "General"
                             ];

                             $array = array('api' => 'Confirm bet -Request log', 'request' => $responseData);
                             $this->activityLog(json_encode($array));
                             return json_encode($responseData);  exit;
                         }

                         $resultArr = [
                             'requestId' => $requestData['requestId'],
                         ];

                         DB::table('access_request')->insert($resultArr);
                     }

                     $betInfoData = $requestData;

                     $calculateExpose = isset($requestData['marketExposure']) ? $requestData['marketExposure'] : 0;
                     if ($calculateExpose < 0) {
                         $calculateExpose =  (-1) * round($calculateExpose);
                     }

                     $authCheck = DB::table('oauth_access_tokens_livegames')->select(['user_id'])->where([['id', $access_token]])->first();
                     // echo "test"; print_r($authCheck); exit;
                     if ($authCheck != null) {
                         // $uid =$request->userId;
                         $uid = $authCheck->user_id;
                         $pId = 1;
                         $betInfo = $betInfoData;
                         $gameId = $betInfo['eventId'];

                         if( $this->getEventBetAllowAndBlockStatusAdmin($gameId) != 0 ){
                             $message = 'Bet not allowed for this event! Plz contact administrator!!';
                             $response = [
                                 "operatorId" => $operatorId,
                                 "errorCode" => 6,
                                 "errorDescription" => "UserBetLocked",
                                 "message" => $message
                             ];
                             $array = array('api' => 'bet place log', 'request' => $response, 'userId' => $uid);
                             $this->activityLog(json_encode($array));
                             return json_encode($response);
                             exit;
                         }

                         $userStatusCheck = DB::table('tbl_user_block_status')->select(['type'])->where([['uid', $uid]])->first();
                         if ($userStatusCheck != null) {
                             $message = 'You are blocked by parent! Plz contact administrator!!';
                             if ($userStatusCheck->type != 1) {
                                 $message = 'Your bet is locked by parent! Plz contact administrator!!';
                             }
                             // $response = [ "status" => 3, "message" => $message ];
                             $response = [
                                 "operatorId" => $operatorId,
                                 "errorCode" => 6,
                                 "errorDescription" => "UserBlocked",
                                 "message" => $message
                             ];
                             $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                             $this->activityLog(json_encode($array));
                             return json_encode($response);
                             exit;
                         }

                         $user = DB::table('tbl_user as u')
                             ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                             ->select(['u.systemId', 'parentId', 'name', 'username', 'balance', 'pl_balance', 'expose', 'role', 'pName'])
                             ->where([['is_login', 1], ['role', 4], ['u.id', $uid]])->first();
                         if ($user != null) {

                             // check profit loss data
                             $profitLossClient = DB::table('tbl_user_profit_loss')
                                 ->where([['clientId', $uid],['userId', 1]])->first();
                             if( $profitLossClient == null ){
                                 $message = 'Bet can not place! Plz contact admin!!';
                                 $response = [
                                     "operatorId" => $operatorId,
                                     "errorCode" => 6,
                                     "errorDescription" => "UserBlocked",
                                     "message" => $message
                                 ];

                                 $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                 $this->activityLog(json_encode($array));
                                 return json_encode($response);
                                 exit;
                             }

                             $parentId = $user->parentId;
                             $systemId = $user->systemId;
                             $mainBalance = round($user->balance);
                             $plBalance = round($user->pl_balance);
                             $exposeBalance = round($user->expose);
                             $balance = round($mainBalance + $plBalance - $exposeBalance);

                             // Check Bet Time
                          /*   $checkLastBet = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->select('id', 'created_on', 'requestId')
                                 ->where([['uid', $uid], ['mid', $betInfo['marketId']], ['round', $betInfo['marketId']]])->orderBy('id', 'desc')->first();
                             if ($checkLastBet != null) {
                                 $updated_on = date('Y-m-d H:i:s');
                                 $betTimeDiff = (strtotime($updated_on) - strtotime($checkLastBet->created_on));
                                 if ($betTimeDiff < 2) {
                                     $message = 'Bet can not place! Please try again after SomeTime !!';
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 6,
                                         "errorDescription" => "UserBlocked",
                                         "message" => $message
                                     ];

                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));

                                     return json_encode($response);
                                     exit;
                                 }

                             }*/

                             $gameData = DB::table('tbl_live_game')
                                 ->select(['sportId', 'eventId', 'name', 'mType', 'is_block', 'min_stake', 'max_stake', 'max_profit_limit', 'suspend'])
                                 ->where([['status', 1], ['is_block', 0], ['eventId', $gameId]])->first();
                             if ($gameData != null) {
                                 if ($gameData != null && $gameData->suspend != 0) {
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 6,
                                         "errorDescription" => "UserBlocked",
                                         "message" => "Suspended ! Bet Not Allowed!"
                                     ];

                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response);  exit;

                                 } elseif ($gameData != null && ((int)$gameData->min_stake > (int)$betInfo['size'])) {

                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 2,
                                         "errorDescription" => "General",
                                         "message" => "Min Stack is " . $gameData->min_stake . "! Bet Not Allowed!"
                                     ];

                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));

                                     return json_encode($response);  exit;

                                 } elseif ($gameData != null && ((int)$gameData->max_stake < (int)$betInfo['size'])) {
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 2,
                                         "errorDescription" => "General",
                                         "message" => "Max Stack is " . $gameData->max_stake . "! Bet Not Allowed!"
                                     ];

                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response);  exit;
                                 }


                                 $gameData = DB::table('tbl_sport')
                                     ->select('is_block')
                                     ->where([['status', 1], ['is_block', 1], ['sportId', $gameData->sportId]])->first();
                                 if ($gameData != null) {
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 6,
                                         "errorDescription" => "UserBlocked",
                                         "message" => "Sport Block ! Bet Not Allowed!"
                                     ];

                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response);  exit;
                                 }

                                 //echo "2";  print_r($gameData); exit;
                                 $checkEventStatus = DB::table('tbl_user_event_status')->select(['uid'])
                                     ->where([['uid', $parentId], ['eid', $gameId]])->first();

                                 if ($checkEventStatus != null) {
                                     $message = 'This event is block by parent!';
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 6,
                                         "errorDescription" => "UserBlocked",
                                         "message" => $message
                                     ];
                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response);
                                     exit;
                                 }

                                 $checkSportStatus = DB::table('tbl_user_sport_status')->select(['uid'])
                                     ->where([['uid', $uid], ['sid', 999]])->first();

                                 if ($checkSportStatus != null) {
                                     $message = 'This sport is block by parent!';
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 6,
                                         "errorDescription" => "UserBlocked",
                                         "message" => $message
                                     ];
                                     $array = array('api' => 'Confirm bet responce log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response);
                                     exit;
                                 }


                                 $userexposeBalance = 0;
                                /* $userAvailableBalance = $this->checkAvailableBalance($uid, $betInfo['marketId'], $calculateExpose);
                                 // echo "2";  print_r($userAvailableBalance); exit;
                                 $exposeBalance = $userAvailableBalance['expose'];
                                 $userexposeBalance = $exposeBalance;// + $betInfo['size'];

                                 if (($userexposeBalance > $userAvailableBalance['balance'])) {
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 3,
                                         "errorDescription" => "InsufficientFunds"
                                     ];

                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response);  exit;
                                 }*/



                                 $userAvailableBalance = $this->checkAvailableBalance($uid, $betInfo['marketId'], $calculateExpose);
                                 // echo "2";  print_r($userAvailableBalance); exit;
                                 $array = array('api' => 'Confirm bet log - userAvailableBalance', 'userAvailableBalance' => $userAvailableBalance, 'userId' => $uid);
                                 $this->activityLog(json_encode($array));
                                 $exposeBalance = (int)$userAvailableBalance['expose'];

                                 if (($exposeBalance > $userAvailableBalance['balance'])) {
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 3,
                                         "errorDescription" => "InsufficientFunds"
                                     ];

                                     $array = array('api' => 'Confirm bet log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response); exit;
                                 }

                                 //$balance = round($mainBalance + $plBalance - $exposeBalance);
                                 $responseData = [
                                     "operatorId" => $operatorId,
                                     "userId" => $uid,
                                     "token" => $access_token,
                                     "currency" => "INR",
                                     "balance" => $balance,
                                     "nickName" => $user->username,
                                     "errorCode" => 1,
                                     "errorDescription" => "Success"
                                 ];

                                 $array = array('api' => 'Confirm bet-Responce log', 'request' => $responseData, 'userId' => $uid);
                                 $this->activityLog(json_encode($array));
                                 return json_encode($responseData);  exit;
                             } else {
                                 $responseData = [
                                     "operatorId" => $operatorId,
                                     "errorCode" => 2,
                                     "errorDescription" => "General"
                                 ];
                                 return json_encode($response);  exit;
                             }
                         }

                     } else {
                         $responseData = [
                             "operatorId" => $operatorId,
                             "errorCode" => 4,
                             "errorDescription" => "TokenNotFound"
                         ];

                         $array = array('api' => 'Confirm bet -Token Not Found log', 'request' => $responseData);
                         $this->activityLog(json_encode($array));
                         return json_encode($responseData);  exit;
                     }
                 } else {
                     return json_encode($response);
                 }
             }else{
                 $response = [ "errorDescription" => "HashNotMatched ", "errorCode" => 2 ];
                 $array = array('api' => 'Confirm bet', 'request' => $response);
                 $this->activityLog(json_encode($array));
                 return json_encode($response);
             }
        } catch (\Throwable $t) {
            $response['errorDescription'] = "Something Wrong! Server Error 500 !!";
            return json_encode($response);
        }

    }


    public function actionBetPlace(Request $request)
    {

        $response = [ "errorDescription" => "General", "errorCode" => 2 ];
        try {
            $requestData = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);
            // $this->saveResponse($requestData,'auth');
            //$str = json_encode($requestData);
            //Production server
            $key='a6fbf9fc-8c67-46ef-9f05-1b8fc51f6e48';
            //Developmret server
            //$key = "3c6fe53a-4a56-4bda-aba3-215fc896f1fc";
            $generatehashkey = base64_encode(hash_hmac("sha256", file_get_contents('php://input'), $key, true));
            $hashKey = $request->header('hash');

            //if($hashKey == $hashKey) {
            if($generatehashkey == $hashKey) {
                 $setting = DB::table('tbl_common_setting')->select(['value'])
                     ->where([['key_name', 'LIVE_GAME_2_STATUS'], ['status', 1]])->first();

                 if ($setting != null && trim($setting->value) != 1) {
                     $response = ["errorDescription" => "UserBlocked", "errorCode" => 6];
                     return json_encode($response);
                 }

                 // print_r($requestData);
                 $array = array('api' => 'bet place', 'request' => $requestData);
                 $this->activityLog(json_encode($array));
                 //   exit;

                 $access_token = '';
                 $operatorId = '';

                 if (isset($request->token) && isset($request->operatorId)) {
                     $access_token = $request->token;
                     $operatorId = $request->operatorId;
                 } else {
                     if (isset($requestData) && isset($requestData['token']) && isset($requestData['operatorId'])) {
                         $access_token = $requestData['token'];
                         $operatorId = $request->operatorId;
                     }
                 }


              if (isset($requestData['requestId'])) {
                     $checkRequest = DB::table('access_request')->select('requestId')
                         ->where('requestId', $requestData['requestId'])->first();
                     if ($checkRequest != null) {
                         $responseData = [
                             "operatorId" => $operatorId,
                             "errorCode" => 2,
                             "errorDescription" => "General"
                         ];
                         return json_encode($responseData);
                     }

                     $resultArr = [
                         'requestId' => $requestData['requestId'],
                     ];

                     DB::table('access_request')->insert($resultArr);
                 }

                 if (isset($requestData) && isset($access_token)) {


                     $betInfoData = $requestData;
                     $calculateExpose = isset($requestData['marketExposure']) ? $requestData['marketExposure'] : 0;
                     if ($calculateExpose < 0) {
                         $calculateExpose =  (-1) * round($calculateExpose);
                     }

                     $authCheck = DB::table('oauth_access_tokens_livegames')->select(['user_id'])->where([['id', $access_token]])->first();
                     // echo "test"; print_r($authCheck); exit;
                     if ($authCheck != null) {
                         // $uid =$request->userId;
                         $uid = $authCheck->user_id;
                         $pId = 1;
                         $betInfo = $betInfoData['bet'];
                         //  echo "21";  print_r($betInfo); exit;
                         $gameId = $betInfo['eventId'];

                         if( $this->getEventBetAllowAndBlockStatusAdmin($gameId) != 0 ){
                             $message = 'Bet not allowed for this event! Plz contact administrator!!';
                             $response = [
                                 "operatorId" => $operatorId,
                                 "errorCode" => 6,
                                 "errorDescription" => "UserBetLocked",
                                 "message" => $message
                             ];
                             $array = array('api' => 'bet place log', 'request' => $response, 'userId' => $uid);
                             $this->activityLog(json_encode($array));
                             return json_encode($response);
                             exit;
                         }

                         if (isset($betInfo['id']) && $betInfo['id'] != 0) {


                             $userStatusCheck = DB::table('tbl_user_block_status')->select(['type'])->where([['uid', $uid]])->first();
                             if ($userStatusCheck != null) {
                                 $message = 'UserBlocked';
                                 if ($userStatusCheck->type != 1) {
                                     $message = 'UserBlocked';
                                 }

                                 // $response = [ "status" => 3, "message" => $message ];
                                 $response = [
                                     "operatorId" => $operatorId,
                                     "errorCode" => 6,
                                     "errorDescription" => "UserBlocked"
                                 ];
                                 $array = array('api' => 'bet place log', 'request' => $response, 'userId' => $uid);
                                 $this->activityLog(json_encode($array));
                                 return json_encode($response);
                                 exit;
                             }

                             $user = DB::table('tbl_user as u')
                                 ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                                 ->select(['u.systemId', 'parentId', 'name', 'username', 'balance', 'pl_balance', 'expose', 'role', 'pName'])
                                 ->where([['is_login', 1], ['role', 4], ['u.id', $uid]])->first();

                             if ($user != null) {

                                 // check profit loss data
                                 $profitLossClient = DB::table('tbl_user_profit_loss')
                                     ->where([['clientId', $uid],['userId', 1]])->first();
                                 if( $profitLossClient == null ){
                                     $message = 'Bet can not place! Plz contact admin!!';
                                     $response = [
                                         "operatorId" => $operatorId,
                                         "errorCode" => 6,
                                         "errorDescription" => "UserBlocked",
                                         "message" => $message
                                     ];

                                     $array = array('api' => 'bet place-same time bet place issue log', 'request' => $response, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     return json_encode($response);
                                     exit;
                                 }

                                 $parentId = $user->parentId;
                                 $systemId = $user->systemId;
                                 $mainBalance = round($user->balance);
                                 $plBalance = round($user->pl_balance);
                                 $exposeBalance = round($user->expose);
                                 $balance = round($mainBalance + $plBalance - $exposeBalance);

                                 // Check Bet Time
                                 $checkLastBet = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->select('id', 'created_on', 'requestId')
                                     ->where([['uid', $uid], ['mid', (int)$betInfo['marketId']], ['round', (int)$betInfo['marketId']]])->orderBy('id', 'desc')->first();


                                 if ($checkLastBet != null) {
                                     $checkLastBet=(object)$checkLastBet;
                                     $updated_on = date('Y-m-d H:i:s');
                                     $betTimeDiff = (strtotime($updated_on) - strtotime($checkLastBet->created_on));
                                     if ($betTimeDiff < 2) {
                                         $message = 'Bet can not place! Please try again after SomeTime !!';
                                         $response = [
                                             "operatorId" => $operatorId,
                                             "errorCode" => 6,
                                             "errorDescription" => "UserBlocked",
                                             "message" => $message
                                         ];

                                         $array = array('api' => 'bet place-same time bet place issue log', 'request' => $response, 'userId' => $uid);
                                         $this->activityLog(json_encode($array));
                                         return json_encode($response);
                                         exit;
                                     }

                                 }

                                 $gameData = DB::table('tbl_live_game')
                                     ->select(['sportId', 'eventId', 'name', 'mType', 'is_block', 'min_stake', 'max_stake', 'max_profit_limit', 'suspend'])
                                     ->where([['status', 1], ['is_block', 0], ['eventId', $gameId]])->first();

                                 if ($gameData != null) {


                                     if ($gameData != null && $gameData->suspend != 0) {

                                         $response = [
                                             "operatorId" => $operatorId,
                                             "errorCode" => 6,
                                             "errorDescription" => "UserBlocked",
                                             "message" => "Suspended ! Bet Not Allowed!"
                                         ];

                                         $array = array('api' => 'bet place-Suspended log', 'request' => $response, 'userId' => $uid);
                                         $this->activityLog(json_encode($array));
                                         return json_encode($response);   exit;

                                     } elseif ($gameData != null && ((int)$gameData->min_stake > (int)$betInfo['size'])) {

                                         $msg = "Min Stack is " . $gameData->min_stake . "! Bet Not Allowed!";
                                         $response = ["errorDescription" => "General", "errorCode" => 2, 'message' => $msg];
                                         return json_encode($response);   exit;

                                     } elseif ($gameData != null && ((int)$gameData->max_stake < (int)$betInfo['size'])) {
                                         $msg = "Max Stack is " . $gameData->max_stake . "! Bet Not Allowed!";
                                         $response = ["errorDescription" => "General", "errorCode" => 2, 'message' => $msg];
                                         return json_encode($response);   exit;
                                     }

                                     //  echo "2";  print_r($gameData); exit;
                                     $checkEventStatus = DB::table('tbl_user_event_status')->select(['uid'])
                                         ->where([['uid', $uid], ['eid', $gameId]])->first();

                                     if ($checkEventStatus != null) {
                                         $message = 'This event is block by parent!';
                                         // $response = [ "status" => 3, "message" => $message ];
                                         $response = [
                                             "operatorId" => $operatorId,
                                             "errorCode" => 6,
                                             "errorDescription" => "UserBlocked",
                                             "message" => $message
                                         ];
                                         $array = array('api' => 'bet place-Event block log', 'request' => $response, 'userId' => $uid);
                                         $this->activityLog(json_encode($array));
                                         return json_encode($response);
                                         exit;
                                     }

                                     $checkSportStatus = DB::table('tbl_user_sport_status')->select(['uid'])
                                         ->where([['uid', $parentId], ['sid', 999]])->first();

                                     if ($checkSportStatus != null) {
                                         $message = 'This sport is block by parent!';
                                         //$response = [ "status" => 3, "message" => $message ];
                                         $response = [
                                             "operatorId" => $operatorId,
                                             "errorCode" => 6,
                                             "errorDescription" => "UserBlocked",
                                             "message" => $message
                                         ];
                                         $array = array('api' => 'bet place-Event block log', 'request' => $response, 'userId' => $uid);
                                         $this->activityLog(json_encode($array));
                                         return json_encode($response);
                                         exit;
                                     }


                                     $userAvailableBalance = $this->checkAvailableBalance($uid, $betInfo['marketId'], $calculateExpose);
                                     // echo "2";  print_r($userAvailableBalance); exit;
                                     $array = array('api' => 'bet place - userAvailableBalance', 'userAvailableBalance' => $userAvailableBalance, 'userId' => $uid);
                                     $this->activityLog(json_encode($array));
                                     $exposeBalance = $userAvailableBalance['expose'];

                                     if (($userAvailableBalance['expose'] > $userAvailableBalance['balance'])) {
                                         $response = [
                                             "operatorId" => $operatorId,
                                             "errorCode" => 3,
                                             "errorDescription" => "InsufficientFunds"
                                         ];

                                         $array = array('api' => 'bet place log', 'request' => $response, 'userId' => $uid);
                                         $this->activityLog(json_encode($array));
                                         return json_encode($response); exit;
                                     }

                                     $winamt = $betInfo['size'];// isset( $betInfo['pnl'] ) ? $betInfo['pnl'] : 0;
                                     $lossamt = isset($betInfo['size']) ? $betInfo['size'] : 0;
                                     $bType = 'LAY';
                                     if ($betInfo['side'] == 1) {
                                         $bType = 'BACK';
                                     }

                                     $updated_on = date('Y-m-d H:i:s');

                                     $betInsertData = [
                                         'requestId' => $betInfoData['requestId'],
                                         'systemId' => (int)$systemId,
                                         'orderId' => isset($betInfo['id']) ? $betInfo['id'] : 123,
                                         'uid' => (int)$uid,
                                         'sid' => 999,
                                         'eid' => (int)$gameId,
                                         'mid' => $betInfo['marketId'],
                                         'round' => $betInfo['marketId'],
                                         'secId' => 0,
                                         'price' => $betInfo['price'],
                                         'size' => (int)$betInfo['size'],
                                         'bType' => $bType,
                                         'rate' => $betInfo['size'],
                                         'win' => round($winamt, 2),
                                         'loss' => round($lossamt, 2),
                                         'mType' => $gameData->mType,
                                         'runner' =>  $betInfo['runnerName'],
                                         'market' => $gameData->name . ' - ' . ucfirst(isset($requestData['marketName']) ? $requestData['marketName'] : ''),
                                         'event' => $gameData->name,
                                         'sport' => 'Live Game',
                                         'ip_address' => $this->getClientIp(),
                                         'client' => $user->username,
                                         'master' => $user->pName,
                                         'result' => 'PENDING',
                                         'description' => $gameData->name . ' > ' . $betInfo['runnerName'],
                                         'status' => 1,
                                         'created_on' => $updated_on,
                                         'updated_on' => $updated_on,
                                         'diff' => 0,
                                         'is_match' => 1,
                                         'ccr' => 0
                                     ];

                                     $betId = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->insertGetId($betInsertData);
                                    // if( $betId != null ){
                                     //echo "3";  print_r($userAvailableBalance); exit;
                                    // if ($model->save()) {
                                         $this->updateUserExpose($uid, $gameId, $betInfo['marketId'], $gameData->mType, $calculateExpose,$systemId);

                                         // Update user balance
                                         $exposeBalance2=$exposeBalance;
                                           if ($exposeBalance > 0) {
                                               $expose = 0;
                                               $userMarketExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select('expose')
                                                   ->where([['uid',$uid],['status',1],['mid','!=',(int)$betInfo['marketId']]])->get();
                                               if( $userMarketExpose->isNotEmpty() ){
                                                   foreach ( $userMarketExpose as $data ){
                                                       $data = (object) $data;
                                                       if( (int)$data->expose > 0 ){
                                                           $expose =$expose+(int)$data->expose;
                                                       }
                                                   }

                                                   $balExpose = round($expose);
                                                   $exposeBalance = $exposeBalance - $balExpose;
                                               }
                                           }

                                         DB::table('tbl_user_info')->where('uid', $uid)->update(['expose' => $exposeBalance2, 'updated_on' => $updated_on]);

                                         $balance = round($mainBalance + $plBalance - $exposeBalance);
                                         $responseData = [
                                             "operatorId" => $operatorId,
                                             "userId" => $uid,
                                             "token" => $access_token,
                                             "currency" => "INR",
                                             "balance" => $balance,
                                             "nickName" => $user->username,
                                             "errorCode" => 1,
                                             "errorDescription" => "Success"
                                         ];

                                         $array = array('api' => 'bet place - responce log', 'request' => $responseData, 'userId' => $uid);
                                         $this->activityLog(json_encode($array));
                                         return json_encode($responseData);   exit;

                                   //  }


                                 } else {
                                     $response = ["errorDescription" => "General", "errorCode" => 2];
                                     return json_encode($response);   exit;
                                 }
                             }
                         }else{
                             $responseData = [
                                 "operatorId" => $operatorId,
                                 "errorCode" => 2,
                                 "errorDescription" => "General"
                             ];
                             return json_encode($responseData);   exit;
                         }
                     } else {
                         $responseData = [
                             "operatorId" => $operatorId,
                             "errorCode" => 4,
                             "errorDescription" => "TokenNotFound"
                         ];
                         return json_encode($responseData);
                     }
                 } else {
                     return json_encode($response);
                 }

             }else{
                  $response = [ "errorDescription" => "HashNotMatched ", "errorCode" => 2 ];
                  $array = array('api' => 'place bet', 'request' => $response);
                  $this->activityLog(json_encode($array));
                  return json_encode($response);
              }
        } catch ( \Throwable $t) {
            return json_encode($response);
        }

    }

    // getEventBetAllowAndBlockStatusAdmin
    public function getEventBetAllowAndBlockStatusAdmin($eventId)
    {
        $redis = Redis::connection();
        $blockEventIdsJson = $redis->get('Block_Event_Market');
        $blockEventIds = json_decode($blockEventIdsJson);
        if (!empty($blockEventIds) && in_array($eventId, $blockEventIds)) {
            return 1;
        }

        $betAllowEventIdsJson = $redis->get('Bet_Allowed_Events');
        $betAllowEventIds = json_decode($betAllowEventIdsJson);
        if (!empty($betAllowEventIds) && in_array($eventId, $betAllowEventIds)) {
            return 1;
        }

        $sportDataJson = $redis->get("Sport_" . 999);
        $sportData = json_decode($sportDataJson);
        if( !empty( $sportData ) && $sportData->is_block != 0){
            return 1;
        }
        return 0;
    }

    //checkAvailableBalance
    public function actionGetBalance(Request $request)
    {
        $response = [ "errorDescription" => "General", "errorCode" => 2 ];
        try {
            $requestData = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);
            $access_token ='';
            $operatorId='';
            $str = json_encode($requestData);
            //Production server
            $key='a6fbf9fc-8c67-46ef-9f05-1b8fc51f6e48';
            //Developmret server
            //$key = "3c6fe53a-4a56-4bda-aba3-215fc896f1fc";
            $generatehashkey = base64_encode(hash_hmac("sha256", file_get_contents('php://input'), $key, true));
            $hashKey = $request->header('hash');
            //if($hashKey == $hashKey) {
            if($generatehashkey == $hashKey) {


                if (isset($request->token) && isset($request->operatorId)) {
                    $access_token = $request->token;
                    $operatorId = $request->operatorId;
                } else {
                    if (isset($requestData) && isset($requestData['token']) && isset($requestData['operatorId'])) {
                        $access_token = $requestData['token'];
                        $operatorId = $request->operatorId;
                    }
                }


                if (isset($requestData) && isset($access_token)) {
                    // echo "test";
                    //print_r($requestData); exit;
                    if (isset($requestData['requestId'])) {

                        $checkRequest = DB::table('access_request')->select('requestId')
                            ->where('requestId', $requestData['requestId'])->first();
                        if ($checkRequest != null) {
                            $responseData = [
                                "operatorId" => $operatorId,
                                "errorCode" => 2,
                                "errorDescription" => "General"
                            ];
                            return json_encode($responseData);
                        }

                        $resultArr = ['requestId' => $requestData['requestId']];
                        DB::table('access_request')->insert($resultArr);
                    }

                    $array = array('api' => 'getbalance', 'request' => $requestData);
                    $this->activityLog(json_encode($array));

                    $authCheck = DB::table('oauth_access_tokens_livegames')->select(['user_id'])->where([['id', $access_token]])->first();
                    // echo "test"; print_r($authCheck); exit;
                    if ($authCheck != null) {
                        $uid = $authCheck->user_id;
                        $pId = 1;
                        $user = DB::table('tbl_user as u')
                            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                            ->select(['u.systemId', 'parentId', 'name', 'username', 'balance', 'pl_balance', 'expose', 'role', 'pName'])
                            ->where([['is_login', 1], ['role', 4], ['u.id', $uid]])->first();

                        if ($user != null) {

                            $mainBalance = round($user->balance);
                            $plBalance = round($user->pl_balance);
                            $exposeBalance = round($user->expose);
                            if ($exposeBalance > 0) {
                              /*  $userExpose = DB::table('tbl_user_market_expose')
                                    ->where([['uid', $uid], ['status', 1], ['sid', 999]])->sum("expose");
                                if (isset($userExpose) && $userExpose != null && $userExpose != '') {
                                    $balExpose = round($userExpose);
                                    $exposeBalance = $exposeBalance - $balExpose;
                                }*/

                                $expose = 0;
                                $userMarketExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select('expose')
                                    ->where([['uid',$uid],['status',1],['sid',999]])->get();
                                if( $userMarketExpose->isNotEmpty() ){
                                    foreach ( $userMarketExpose as $data ){
                                        $data = (object) $data;
                                        if( (int)$data->expose > 0 ){
                                            $expose =$expose+(int)$data->expose;
                                        }
                                    }
                                    $balExpose = round($expose);
                                    $exposeBalance = $exposeBalance - $balExpose;
                                }
                            }

                            $balance = round($mainBalance + $plBalance - $exposeBalance);
                            $responseData = [
                                "operatorId" => $operatorId,
                                "userId" => $uid,
                                "token" => $access_token,
                                "currency" => "INR",
                                "balance" => $balance,
                                "nickName" => $user->username,
                                "errorCode" => 1,
                                "errorDescription" => "Success"
                            ];

                            $this->activityLog(json_encode($responseData));
                            return response()->json($responseData);

                        } else {
                            $responseData = [
                                "errorCode" => 5,
                                "errorDescription" => "UserNotFound"
                            ];
                            return json_encode($responseData);
                        }
                    } else {
                        $responseData = [
                            "operatorId" => $operatorId,
                            "errorCode" => 4,
                            "errorDescription" => "TokenNotFound"
                        ];
                        return json_encode($responseData);
                    }
                }
            }else{
                $response = [ "errorDescription" => "HashNotMatched ", "errorCode" => 2 ];
                $array = array('api' => 'Getbalance', 'request' => $response);
                $this->activityLog(json_encode($array));
                return json_encode($response);
            }
        } catch ( \Throwable $t) {
            return json_encode($response);
        }

    }


    //checkAvailableBalance
    public function checkAvailableBalance($uid, $marketId, $calculateExpose)
    {
        $user = DB::table('tbl_user_info')->select(['balance','expose','pl_balance'])
            ->where([['uid',$uid]])->first();

        if ($user != null) {
            $balance = round( (int)$user->balance );
            $expose = round( (int)$user->expose );
            $profitLoss = round( (int)$user->pl_balance );

            if( $profitLoss < 0 ){
                $profitLoss = (-1)*$profitLoss;
                $mywallet = ( $balance - $profitLoss );
            }else{
                $mywallet = ( $balance + $profitLoss );
            }

            //Old Expose
          /*  $userExpose = DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['status',1],['mid','!=',(string)$marketId]])->sum("expose");
            //echo "test";  print_r($userExpose); exit;
            if ( isset( $userExpose ) && $userExpose != null && $userExpose != '') {
                $balExpose = round( $userExpose );
            }else{
                $balExpose = 0;
            }*/
            $balExpose =$expose= 0;
            $userMarketExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select('expose')
                ->where([['uid',$uid],['status',1],['mid','!=',(int)$marketId]])->get();
            if( $userMarketExpose->isNotEmpty() ){
                foreach ( $userMarketExpose as $data ){
                    $data = (object) $data;
                    if( (int)$data->expose > 0 ){
                        $expose =$expose+(int)$data->expose;

                    }
                }

                $balExpose = round($expose);
            }


            $newExpose = (int)$balExpose + (int)$calculateExpose;
            return $data = [ 'balance' => $mywallet, 'expose' => $newExpose , 'calExp' => $calculateExpose ];

        }
        return $data = [ 'balance' => 0, 'expose' => 0 , 'calExp' => 0 ];

    }

    /**
     * update User Expose
     */
    public function updateUserExpose( $uid, $eid, $mid, $mType, $expose,$systemId )
    {
        $userExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select(['id'])
            ->where([['uid',(int)$uid],['mid',$mid],['status',1]])->first();
        if($expose < 0){
            $expose = (-1)*$expose;
        }
        if( $userExpose != null ){
            $updateData = [
                'expose' => $expose,
                'profit' => 0
            ];

            DB::connection('mongodb')->table('tbl_user_market_expose')
                ->where([['uid',(int)$uid],['mid',$mid],['status',1]])->update($updateData);

        }else{

            $insertData = [
                'uid' => (int)$uid,
                'systemId' => $systemId,
                'eid' => $eid,
                'mid' => $mid,
                'sid' => 999,
                'mType' => $mType,
                'expose' =>$expose,
                'profit' => 0,
                'secId' => 0,
                'status' => 1
            ];

            $array = array('api' => 'bet place - user-expose', 'insertData' => $insertData, 'userId' => $uid);
            $this->activityLog(json_encode($array));
            DB::connection('mongodb')->table('tbl_user_market_expose')->insert($insertData);
        }

    }


    /**
     * saveResponse
     */
    public function saveResponse($data,$type)
    {
        $filePath = '/var/www/html/pokerlog/'.$type.'.txt';

        $fp = fopen($filePath, 'a');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }

    /**
     * getClientIp
     */
    public function getClientIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
