<?php

namespace App\Http\Controllers\Market;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class MarketController extends Controller
{
    
    public function marketList(Request $request)
    {
             
      try{

        $response = ['status'=>0,'message'=>'Bad request !!!','data'=>null];

        if( null != Auth::user()->id){
            
            $uid = Auth::user()->id;

            $updatedOn = strtotime(Auth::user()->updated_at->toDateTimeString());

            $eventArr = $marketIdsArr = [];
           
             $betListEvent=[];
             $betListData = DB::connection('mongodb')->table('tbl_bet_pending_1')->select('eid')
                  ->where([['uid',$uid],['is_match',1],['status',1],['result','PENDING']])
                   ->get();
                   foreach ($betListData as $value) {
                    $value = (object) $value;
                        $betListEvent[] = $value->eid; 
                   }

             $betListData2 = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('eid')
                  ->where([['uid',$uid],['is_match',1],['status',1],['result','PENDING']])
                   ->get();
                    foreach ($betListData2 as $value) {
                        $value = (object) $value;
                        $betListEvent[] = $value->eid; 
                   }
            
           $betListEvents=array_unique($betListEvent);
            
            if( $betListEvents != null ){
                
               $cache = Redis::connection();
                foreach ( $betListEvents as $eventId  ) {

                    $event = $cache->get("Event_" . $eventId);
                    $event = json_decode($event);
                    if (!empty($event)) {
                        $sportId = $event->sportId;
                        $title = $event->name;

                        if ($sportId == '1' && $event->game_over != 1 && $event->is_block != 1) {

                            $bookmaker = $this->getDataBookmaker($uid, $eventId);
                            $matchodd = $this->getDataMatchodd($uid, $eventId);
                            $fancy3 = $this->getDataSingleFancy($uid, $eventId);
                            $fancy2 = $this->getDataFancy2($uid, $eventId);
                            $fancy1 = $this->getDataFancy($uid, $eventId);

                            if ($bookmaker != null || $matchodd != null || $fancy3 != null || $fancy2 != null) {
                                $eventArr[] = [
                                    'title' => $title,
                                    'sport' => 'Football',
                                    'event_id' => $eventId,
                                    'sport_id' => $sportId,
                                    'slug' => 'football',
                                    'bookmaker' => $bookmaker,
                                    'matchodd' => $matchodd,
                                    'fancy3' => $fancy3,
                                    'fancy2' => $fancy2,
                                    'fancy1' => $fancy1
                                ];
                            }
                        } elseif ($sportId == '2' && $event->game_over != 1 && $event->is_block != 1) {

                            $bookmaker = $this->getDataBookmaker($uid, $eventId);
                            $matchodd = $this->getDataMatchodd($uid, $eventId);
                            $fancy2 = $this->getDataFancy2($uid, $eventId);
                            $fancy3 = $this->getDataSingleFancy($uid, $eventId);
                            $fancy1 = $this->getDataFancy($uid, $eventId);

                            if ($bookmaker != null || $matchodd != null || $fancy3 != null || $fancy2 != null) {

                                $eventArr[] = [
                                    'title' => $title,
                                    'sport' => 'Tennis',
                                    'event_id' => $eventId,
                                    'sport_id' => $sportId,
                                    'slug' => 'tennis',
                                    'bookmaker' => $bookmaker,
                                    'matchodd' => $matchodd,
                                    'fancy3' => $fancy3,
                                    'fancy2' => $fancy2,
                                    'fancy1' => $fancy1

                                ];
                            }
                        } elseif ($sportId == '4' && $event->game_over != 1 && $event->is_block != 1) {

                            $bookmaker = $this->getDataBookmaker($uid, $eventId);
                            $matchodd = $this->getDataMatchodd($uid, $eventId);
                            $fancy3 = $this->getDataSingleFancy($uid, $eventId);
                            $fancy2 = $this->getDataFancy2($uid, $eventId);
                            $fancy1 = $this->getDataFancy($uid, $eventId);


                            if ($bookmaker != null || $matchodd != null || $fancy3 != null || $fancy2 != null) {

                                $eventArr[] = [
                                    'title' => $title,
                                    'sport' => 'Cricket',
                                    'event_id' => $eventId,
                                    'sport_id' => $sportId,
                                    'slug' => 'cricket',
                                    'bookmaker' => $bookmaker,
                                    'matchodd' => $matchodd,
                                    'fancy3' => $fancy3,
                                    'fancy2' => $fancy2,
                                    'fancy1' => $fancy1

                                ];
                            }
                        }
                    }
                }
            }

            if( $eventArr != [] ){
                $marketIdsArr = $this->marketIdsArr;
            }else{
              $this->marketIdsArr=[];
            }

            $response = [ "status" => 1 ,'code'=>200, "updatedOn" => $updatedOn, "data" => [ "items" => $eventArr , 'marketIdsArr' => $this->marketIdsArr,'message' => 'Data Found !!!' ] ];
        }
        
        return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
        
    }


      //Event: getDataMatchOdd
    public function getDataMatchodd($uid,$eventId)
    {
       
               $items = null;         
               $cache = Redis::connection();
                $eventData=$cache->get("Match_Odd_".$eventId); 
                $market = json_decode($eventData);
                                 
        if($market != null){
             $slug = ['1' => 'football', '2' => 'tennis', '4' => 'cricket'];
      
              $marketId = $market->marketId;

              $betListData = DB::connection('mongodb')->table('tbl_bet_pending_1')->select('eid')
                  ->where([['uid',$uid],['mid',$marketId],['is_match',1],['status',1],['result','PENDING']])
                   ->first();
            if(!empty($betListData)){

                $eventId  =$market->eid;
                $sportId = $market->sid;
                $minStack = $market->min_stack;
                $maxStack = $market->max_stack;
                $maxProfitLimit = $market->max_profit_limit;
             
                $runners1 = $market->runners;
                $matchOddData = json_decode($runners1,16);
                     
                $runners = [];
                foreach( $matchOddData as $data ){
                    
                    $profitLoss = 0;//$this->getProfitLossMatchOdds($uid,$marketId,$eventId,$data['secId'],'match_odd');
                    $runners[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'slug' => $slug[$sportId],
                         'mType' => 'match_odd',
                        'eventId' => $eventId,
                        'selectionId' => $data['secId'],
                        'runnerName' => $data['runner'],
                        'profit_loss' => $profitLoss,
                        'suspended' => $market->suspended,
                        'ballRunning' => $market->ball_running,
                         'minStack' => $minStack,
                        'maxStack' => $maxStack,
                        'maxProfitLimit' => $maxProfitLimit,
                        'minStack' => $minStack,
                       'maxStack' => $maxStack,
                       'maxProfitLimit' => $maxProfitLimit,
                        'backprice' =>0, 
                        'backsize' => '',
                        'layprice' => 0,
                        'laysize' => ''
                    ];
                }
                
                $items[] = [
                    'marketName' => $market->title,
                    'sportId' =>  $sportId,
                    'slug' => $slug[$sportId],
                    'event_title' => $market->title,
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'info'=> $market->info,
                    'mType' => 'match_odd',
                    //'suspended' => $market->suspended,
                    //'ballRunning' => $market->ball_running,
                    'mType' => 'match_odd',
                   
                    'runners' => $runners,
                ];
                
                $this->marketIdsArr[] =  $marketId;
                 
            }
          
        }
    
        return $items;

   }


  //Event: getDataBookmaker
  public function getDataBookmaker($uid,$eventId){
       
               $items = null;         
               $cache = Redis::connection();
                $eventData=$cache->get("Bookmaker_".$eventId); 
                $marketData = json_decode($eventData);
                                 
        if($marketData != null){
            
            $betListDataArray = DB::connection('mongodb')->table('tbl_bet_pending_1')->select('eid','mid')
                  ->where([['uid',$uid],['is_match',1],['status',1],['result','PENDING']])
                   ->get()->toArray();
            // dd($betListDataArray);
          foreach ($marketData as $key => $market) {

              $marketId = $market->mid;

              // $betListData = DB::connection('mongodb')->table('tbl_bet_pending_1')->select('eid')
              //     ->where([['uid',$uid],['mid',$marketId],['is_match',1],['status',1],['result','PENDING']])
              //      ->first();
            // if(!empty($betListData)){
            if(!empty($betListDataArray) && in_array($marketId, array_column($betListDataArray, 'mid'))){

                $eventId  =$market->eid;
                $sportId = $market->sid;
                $minStack = $market->min_stack;
                $maxStack = $market->max_stack;
                $maxProfitLimit = $market->max_profit_limit;
                $info = $market->info;
             
                $runners1 = $market->runners;
                $matchOddData = json_decode($runners1,16);
           
                
                $runners = [];
                foreach( $matchOddData as $data ){
                    
                    $profitLoss = 0;//$this->getProfitLossMatchOdds($uid,$marketId,$eventId,$data['secId'],'bookmaker');
                    $runners[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'sec_id' => $data['secId'],
                        'runner' => $data['runner'],
                        'mType' => 'bookmaker',
                        'profitloss' => round($profitLoss,0),
                        'suspended' => $market->suspended,
                        'ballRunning' => $market->ball_running,
                        'minStack' => $minStack,
                        'maxStack' => $maxStack,
                        'maxProfitLimit' => $maxProfitLimit,
                        'backprice' =>0, 
                        'backsize' => '',
                        'layprice' => 0,
                         'laysize' => ''
                    ];
                }
                
                $items[] = [
                    'marketName' => $market->title,
                    'sportId' =>  $sportId,
                    'event_title' => $market->title,
                    'market_id' => $marketId,
                    'event_id' => $eventId,
                    'mType' => 'bookmaker',
                    //'suspended' => $market->suspended,
                    //'ballRunning' => $market->ball_running,
                    'minStack' => $minStack,
                    'maxStack' => $maxStack,
                    'maxProfitLimit' => $maxProfitLimit,
                    'info' => $info,
                    'runners' => $runners,
                ];
                
                $this->marketIdsArr[] = $marketId;
          
                 
            }
          }
        }
     
        return $items;
    }

    //Event: getDataBookmaker
    public function getDataFancy($uid,$eventId)
    {
      
        $items = null;
         
               $cache = Redis::connection();
                $eventData=$cache->get("Fancy_".$eventId); 
                $marketData = json_decode($eventData);
                             
        if($marketData != null){
             $betListDataArray = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('eid','mid')
                  ->where([['uid',$uid],['is_match',1],['status',1],['result','PENDING']])
                   ->get()->toArray();
          foreach ($marketData as $key => $market) {

              $marketId = $market->mid;

              // $betListData = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('eid')
              //     ->where([['uid',$uid],['mid',$marketId],['is_match',1],['status',1],['result','PENDING']])
              //      ->first();

            if(!empty($betListDataArray) && in_array($marketId, array_column($betListDataArray, 'mid'))){

                $eventId  =$market->eid;
                $sportId = $market->sid;
                $minStack = $market->min_stack;
                $maxStack = $market->max_stack;
                $maxProfitLimit = $market->max_profit_limit;
                 $info = $market->info;
            
                
                $items[] = [
                    'title' => $market->title,
                    'sportId' =>  $sportId,
                    'event_title' => $market->title,
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'suspended' => $market->suspended,
                    'ballRunning' => $market->ball_running,
                    'minStack' => $minStack,
                    'maxStack' => $maxStack,
                    'info' => $info,
                    'mType' => 'fancy',
                    'maxProfitLimit' => $maxProfitLimit,
                    'is_book' => '1',//$this->isBookOn($uid,$marketId,'fancy'),
                    'no' => 0,
                    'no_rate' => 0,
                    'yes' => 0,
                    'yes_rate' => 0
             
                ];
                
                $this->marketIdsArr[] =  $marketId;
          
                 
            }
          }
        }
     
        return $items;

    }


 //Event: getDataBookmaker
    public function getDataSingleFancy($uid,$eventId)
    {
    
        $items = null;
         
               $cache = Redis::connection();
                $eventData=$cache->get("Fancy_3_".$eventId); //All event market for fancy
                $marketData = json_decode($eventData);
                        
                //  print_r($eventId);  die('popopo');         
        if($marketData != null){
             
             $betListDataArray = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('eid','mid')
                  ->where([['uid',$uid],['is_match',1],['status',1],['result','PENDING']])
                   ->get()->toArray();

          foreach ($marketData as $key => $market) {

              $marketId = $market->mid;

              // $betListData = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('eid')
              //     ->where([['uid',$uid],['mid',$marketId],['is_match',1],['status',1],['result','PENDING']])
              //      ->first();

            
                if(!empty($betListDataArray) && in_array($marketId, array_column($betListDataArray, 'mid'))){

                $eventId  =$market->eid;
                $sportId = $market->sid;
                $minStack = $market->min_stack;
                $maxStack = $market->max_stack;
                $maxProfitLimit = $market->max_profit_limit;
                 $info = $market->info;
                         
             
                
                $items[] = [
                    'title' => $market->title,
                    'sportId' =>  $sportId,
                    'event_title' => $market->title,
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'suspended' => $market->suspended,
                    'ballRunning' => $market->ball_running,
                    'minStack' => $minStack,
                    'maxStack' => $maxStack,
                     'info' => $info,
                    'mType' => 'fancy3',
                     'maxProfitLimit' => $maxProfitLimit,
                    'is_book' => '1',//$this->isBookOn($uid,$marketId,'fancy3'),
                    'no' => 0,
                    'no_rate' => 0,
                    'yes' => 0,
                    'yes_rate' => 0
                  
                ];
                
                $this->marketIdsArr[] = $marketId;
          
                 
            }
          }
        }
     
 
        return $items;

   }


//Event: getDataBookmaker
    public function getDataFancy2($uid,$eventId)
    {
      
        $items = null;
         
               $cache = Redis::connection();
                $eventData=$cache->get("Fancy_2_".$eventId); 
                $marketData = json_decode($eventData);
                                 
        if($marketData != null){
             
             $betListDataArray = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('eid','mid')
                  ->where([['uid',$uid],['is_match',1],['status',1],['result','PENDING']])
                   ->get()->toArray();

          foreach ($marketData as $key => $market) {

              $marketId = $market->mid;

              // $betListData = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('eid')
              //     ->where([['uid',$uid],['mid',$marketId],['is_match',1],['status',1],['result','PENDING']])
              //      ->first();

            //if(!empty($betListData)){
              if(!empty($betListDataArray) && in_array($marketId, array_column($betListDataArray, 'mid'))){

                $eventId  =$market->eid;
                $sportId = $market->sid;
                $minStack = $market->min_stack;
                $maxStack = $market->max_stack;
                $maxProfitLimit = $market->max_profit_limit;
                  $info = $market->info;
            
                
                $items[] = [
                    'title' => $market->title,
                    'sportId' =>  $sportId,
                    'event_title' => $market->title,
                    'marketId' => $marketId,
                    'eventId' => $eventId,
                    'suspended' => $market->suspended,
                    'ballRunning' => $market->ball_running,
                    'minStack' => $minStack,
                     'mType' => 'fancy2',
                    'maxStack' => $maxStack,
                    'maxProfitLimit' => $maxProfitLimit,
                     'info' => $info,
                    'is_book' => '1',//$this->isBookOn($uid,$marketId,'fancy2'),
                    'no' => 0,
                    'no_rate' => 0,
                    'yes' => 0,
                    'yes_rate' => 0
                   
                ];
                
                $this->marketIdsArr[] = $marketId;
          
                 
            }
          }
        }
     
 
        return $items;

    }

    //Event: get Profit Loss Match Odds
    public function getProfitLossMatchOdds($uid,$marketId, $eventId, $selId, $sessionType)
    {

        $total = 0;
        // IF RUNNER WIN
        if (null != $uid && $marketId != null && $eventId != null && $selId != null) {

            $where = [ ['uid', $uid] , ['eid', $eventId] ,['mid', $marketId] , ['mType', $sessionType],['secId', $selId] ];
   
          $userBetExpose = DB::table('tbl_user_market_expose_2')
                  ->where($where)
                   ->first();
                  
            if($userBetExpose == null || $userBetExpose == ''){
                $total = 0;
            }else{
              $total = $userBetExpose->expose;
            }
        

        }

        return $total;

    }

//Event: isBookOn
    public function isBookOn($uid,$marketId, $sessionType)
    {
 
        if($sessionType=='match_odd' || $sessionType=='bookmaker'){
            $table='tbl_bet_pending_1';
        }else{
           $table='tbl_bet_pending_2';
        }

        $findBet = DB::connection('mongodb')->table($table)
            ->where([['result','Pending'],['uid',$uid],['mid',$marketId],['mType',$sessionType],['status',1]])
            ->get();

        if ($findBet != null) {
            return '1';
        }
        return '0';

    }


  }