<?php

namespace App\Http\Controllers\Market;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;


class MarketProfitLossController extends Controller
{

    /**
     * mergeTable
     */
    public function mergeTable($startDate,$endDate){

        $tbl = 'tbl_transaction_client';
        date_default_timezone_set('Asia/Kolkata');


        if( isset($startDate) && isset($endDate) ){
            $sd = explode('-',trim($startDate));
            $ed = explode('-',trim($endDate));
            $td = explode('-',trim(Carbon::now()->format('Y-m-d')));
            if( count($td) == 3 && count($sd) == 3 && count($ed) == 3){
                $sm = $sd[1]; $em = $ed[1]; $tm = $td[1];
                $sy = $sd[0]; $ey = $sd[0]; $ty = $td[0];
                $smy = $sm.$sy; $emy = $em.$ey; $tmy = $tm.$ty;

                if( $smy == $emy && $smy == $tmy ){
                    $tbl1 = $tbl.'_'.$tmy;
                    $tbl2 = null;
                }elseif($smy != $emy && $emy == $tmy){
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$tmy;
                }else{
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = null;//$tbl.'_'.$emy;
                }
            }else{
                $tbl = $this->currentTable($tbl);
                $tbl1 = $tbl;
                $tbl2 = null;
            }
        }else{
            $tbl = $this->currentTable($tbl);
            $tbl1 = $tbl;
            $tbl2 = null;
        }

        return ['tbl1' => $tbl1,'tbl2' => $tbl2];
    }

    public function marketProfitLoss(Request $request)
    {

        try{

          $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];

          $userId = Auth::user()->id;

          if(isset($request->eid) && $request->eid != ' '){

              $endDate = date('Y-m-d H:i:s');
              $startDate = date('2021-02-01 00:00:01');
           /* $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            $tableName='tbl_transaction_client_'.$currentMonth;

             $Query = DB::connection('mysql3')->table($tableName)->select('eid','mid','mType','result','description','type','created_on','updated_on','amount','balance')
                          ->where([['userId',$userId],['status',1],['eid',$request->eid]])
                          ->orderBy('created_on','DESC')
                          ->get();

            if(!$Query->isEmpty()){*/

              $tbls = $this->mergeTable($startDate,$endDate);
              $tbl1 = $tbls['tbl1'];
              if( strpos($tbl1,'_012021') > 0 ){
                  $tbl1 = str_replace('_012021','_022021',$tbl1);
              }
              $tbl2 = $tbls['tbl2'];

              $query = DB::connection('mysql3')->table($tbl1)
                  ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
                  ->where([['userId',$userId],['status',1],['eid',$request->eid]]);
              $eventData1 = $query->get();
              $listArr1 = $eventData1;
              $listArr = [];
              if( $tbl2 != null ){
                  $query2 = DB::connection('mysql3')->table($tbl2)
                      ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
                      ->where([['userId',$userId],['status',1],['eid',$request->eid]]);
                  $eventData2 = $query2->whereBetween('created_on',[$startDate, $endDate])->get();

                  $listArr2 = $eventData2;

                  if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                      if($listArr2) {
                          foreach ( $listArr2 as $data ){
                              $listArr[] = $data;
                          }
                      }
                      if($listArr1) {
                          foreach ( $listArr1 as $data ){
                              $listArr[] = $data;
                          }
                      }
                  }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                      $listArr = $listArr2;
                  }else{
                      $listArr = $listArr1;
                  }
              }else{
                  $listArr = $listArr1;
              }
              if(!$listArr->isEmpty()){

              $win = 0; $loss = 0; $profitLossSum=0;
              foreach ($listArr as  $acQuery) {
                 $win = 0; $loss = 0;

                if($acQuery->type == 'DEBIT'){

                   $trans_amount = $acQuery->amount;
                   if(isset($acQuery->amount)){ $loss = $acQuery->amount; }else{ $loss = 0;}

                }else{
                  $trans_amount = 0;
                  if(isset($acQuery->amount)){ $win = $acQuery->amount; }else{ $win = 0;}
                }
                   
                $profitLoss = $win - $loss ;
                $profitLossSum += $profitLoss;

                if ($acQuery->mType == 'casino') {
                  $desc = explode(">", $acQuery->description);
                  $description = $desc[0].'>'.$desc[1].'>'.$desc[2];
                }
                
                $arr[] = [
                          
                          'eventId'  => $acQuery->eid,
                          'marketId'  => $acQuery->mid,
                          'mType'  => $acQuery->mType,
                          'winner'  => $acQuery->result,
                          'description'  => isset($description)?$description:$acQuery->description,
                          'settled_time'  => $acQuery->updated_on,
                          'profitLoss'  => round($profitLoss)    
             
                ];
              
              }

               $response = [ "status" => 1 ,'code'=> 200, "data" =>[ 'items' => $arr ,'totalAmount' => round($profitLossSum),'message'=> 'Data Found !!' ]];
            }else{

               $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Not Found !!' ];
            }
        }else{

           $response = [ "status" => 1 ,'code'=> 401, "data" => null ,'message'=> 'Data Not Found !!' ];
        }



      return $response;

    }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

  }

  public function marketSportsList(Request $request)
    {

     
      $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
      try{

                 $userId = Auth::user()->id;
                  $cache = Redis::connection();

          if( isset( $request->start_date ) && isset( $request->end_date )
              && ( $request->start_date != '' ) && ( $request->end_date != '' )
              && ( $request->start_date != null ) && ( $request->end_date != null ) ){

              $startDate = date('Y-m-d', strtotime($request->start_date));
              $startDate =$startDate." 00:00:01";

              $endDate = date('Y-m-d', strtotime($request->end_date));
              $endDate =$endDate." 23:59:59";

              $start_date = strtotime($request->start_date); 
              $end_date = strtotime($request->end_date); 

          }else{

              $start = new \DateTime('now +1 day');
              $endDate =  $start->format('yy-m-d h:i:s');

              $end = new \DateTime('now -5 day');
              $startDate = $end->format('yy-m-d h:i:s');

              $start_date = strtotime($request->start_date); 
              $end_date = strtotime($request->end_date); 
          }

          //$days = (int)((strtotime($endDate) - strtotime($startDate))/86400);
          $days = abs(($end_date - $start_date)/60/60/24); 

          if( $days > 31 ){
              $response = [ "status" => 0 ,'code'=> 200, "data" => null ,'message'=> 'Select Max 30 day for search result!' ];
              return $response;
          }

                $sportList=$cache->get("SportList");
                $sportList = json_decode($sportList);


                if(!empty($sportList)) {
                    foreach ($sportList as $sport_List) {

                      $pl  = $this->getProfitLossSport($sport_List->sportId, $userId, null,$startDate,$endDate);

                      $totalProfitLoss[] = $pl;

                      if(isset($totalProfitLoss)){ $profitLossSum = array_sum($totalProfitLoss); }else{ $profitLossSum = 0 ;}
                      
                        $sport_name[] = [
                                          'sportName'   => strtolower($sport_List->name),
                                          'sportId'     => $sport_List->sportId,
                                          'profitLoss'  => $pl
                                        ];
      
                     }
                     $response = [ "status" => 1 ,'code'=> 200, "data" =>['item'=> $sport_name ,'totalAmount'=>$profitLossSum ],'message'=> 'Data Found !!' ];
                }else{
                  $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Not Found !!' ];
                }
          return $response;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

     }





    public function getProfitLoss($sid, $userId, $eid,$startDate,$endDate)
    {
        if($eid != null){
            $where = ([['userId',$userId],['status',1],['eid',$eid],['sid',$sid]]);
        }else{
            $where = ([['userId',$userId],['status',1],['sid',$sid]]);
        }

        /* $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            $tableName='tbl_transaction_client_'.$currentMonth;

        $Query = DB::connection('mysql3')->table($tableName)->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')->where($where);
        $Query = $Query->whereBetween('created_on',[$startDate, $endDate])->get();
        $profitLoss=[];

        foreach ($Query as  $acQuery) {
            $win = 0; $loss = 0;
            if($acQuery->type == 'DEBIT'){

                $trans_amount = $acQuery->amount;
                if(isset($acQuery->amount)){ $loss = $acQuery->amount; }else{ $loss = 0;}

            }else{
                $trans_amount = 0;
                if(isset($acQuery->amount)){ $win = $acQuery->amount; }else{ $win = 0;}
            }

            $profitLoss[] = $win - $loss ;


        }*/

          $tbls = $this->mergeTable($startDate,$endDate);
        $tbl1 = $tbls['tbl1'];
        if( strpos($tbl1,'_012021') > 0 ){
            $tbl1 = str_replace('_012021','_022021',$tbl1);
        }
        $tbl2 = $tbls['tbl2'];

        $query = DB::connection('mysql3')->table($tbl1)
            ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
            ->where($where);
        $eventData1 = $query->whereBetween('created_on',[$startDate, $endDate])->get();
        $listArr1 = $eventData1;
        $listArr = [];
        if( $tbl2 != null ){
            $query2 = DB::connection('mysql3')->table($tbl2)
                ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
                ->where($where);
            $eventData2 = $query2->whereBetween('created_on',[$startDate, $endDate])->get();

            $listArr2 = $eventData2;

            if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                if($listArr2) {
                    foreach ( $listArr2 as $data ){
                        $listArr[] = $data;
                    }
                }
                if($listArr1) {
                    foreach ( $listArr1 as $data ){
                        $listArr[] = $data;
                    }
                }
            }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                $listArr = $listArr2;
            }else{
                $listArr = $listArr1;
            }
        }else{
            $listArr = $listArr1;
        }

        if(!empty($listArr)){
            $profitLoss = [];
            foreach ($listArr as  $acQuery) {
                $win = 0; $loss = 0;
                if($acQuery->type == 'DEBIT'){
                    //$trans_amount = $acQuery->amount;
                    if(isset($acQuery->amount)){ $loss = $acQuery->amount; }else{ $loss = 0;}

                }else{
                    //$trans_amount = 0;
                    if(isset($acQuery->amount)){ $win = $acQuery->amount; }else{ $win = 0;}
                }
                $profitLoss[] = $win - $loss ;
            }
        }

        if(isset($profitLoss) && $profitLoss != '' && $profitLoss != null){
            $profitLoss = array_sum($profitLoss);
            return $profitLoss;

        }else{
            return 0;
        }

    }

    public function getProfitLossLiveGame($sid, $userId, $eid, $startDate, $endDate)
    {

        if($eid != null){
            $where = ([['userId',$userId],['status',1],['eid',$eid]]);
        }else{
            $where = ([['userId',$userId],['status',1],['sid',$sid]]);
        }

      /*  $Query = DB::table('tbl_transaction_client')->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
            ->where($where)
            ->get();*/
//             $updatedOn = date('Y-m-d H:i:s');
//            $year = date('Y', strtotime($updatedOn));
//            $month = date('m', strtotime($updatedOn));
//            $currentMonth=$month.$year;
//            $tableName='tbl_transaction_client_'.$currentMonth;

        $tbls = $this->mergeTable($startDate,$endDate);
        $tbl1 = $tbls['tbl1'];
        if( strpos($tbl1,'_012021') > 0 ){
            $tbl1 = str_replace('_012021','_022021',$tbl1);
        }
        $tbl2 = $tbls['tbl2'];

        $query = DB::connection('mysql3')->table($tbl1)
            ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
            ->where($where);
       $eventData1 = $query->whereBetween('created_on',[$startDate, $endDate])->get();
        $listArr1 = $eventData1;
        $listArr = [];
        if( $tbl2 != null ){
            $query2 = DB::connection('mysql3')->table($tbl2)
                ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
                ->where($where);
            $eventData2 = $query2->whereBetween('created_on',[$startDate, $endDate])->get();
            $listArr2 = $eventData2;

            if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                if($listArr2) {
                    foreach ( $listArr2 as $data ){
                        $listArr[] = $data;
                    }
                }
                if($listArr1) {
                    foreach ( $listArr1 as $data ){
                        $listArr[] = $data;
                    }
                }
            }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                $listArr = $listArr2;
            }else{
                $listArr = $listArr1;
            }
        }else{
            $listArr = $listArr1;
        }

        if(!empty($listArr)){
            $profitLoss = [];
            foreach ($listArr as  $acQuery) {
                $win = 0; $loss = 0;
                if($acQuery->type == 'DEBIT'){
                    //$trans_amount = $acQuery->amount;
                    if(isset($acQuery->amount)){ $loss = $acQuery->amount; }else{ $loss = 0;}

                }else{
                    //$trans_amount = 0;
                    if(isset($acQuery->amount)){ $win = $acQuery->amount; }else{ $win = 0;}
                }
                $profitLoss[] = $win - $loss ;
            }
        }

        if(isset($profitLoss) && $profitLoss != '' && $profitLoss != null){
            $profitLoss = array_sum($profitLoss);
            return round($profitLoss);

        }else{
            return 0;
        }

    }

    public function getProfitLossSport($sid, $userId, $eid,$startDate,$endDate)
    {

        if($eid != null){
            $where = ([['userId',$userId],['status',1],['eid',$eid]]);
        }else{
            $where = ([['userId',$userId],['status',1],['sid',$sid]]);
        }

        // $Query = DB::table('tbl_transaction_client')->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
        //      ->where($where)
        //    ->get();

//         $updatedOn = date('Y-m-d H:i:s');
//            $year = date('Y', strtotime($updatedOn));
//            $month = date('m', strtotime($updatedOn));
//            $currentMonth=$month.$year;
//            $tableName='tbl_transaction_client_'.$currentMonth;

        $tbls = $this->mergeTable($startDate,$endDate);
        $tbl1 = $tbls['tbl1'];
        if( strpos($tbl1,'_012021') > 0 ){
            $tbl1 = str_replace('_012021','_022021',$tbl1);
        }
        $tbl2 = $tbls['tbl2'];

        $query = DB::connection('mysql3')->table($tbl1)
            ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
            ->where($where);
        $eventData1 = $query->whereBetween('created_on',[$startDate, $endDate])->get();
        $listArr1 = $eventData1;
        $listArr = [];
        if( $tbl2 != null ){
            $query2 = DB::connection('mysql3')->table($tbl2)
                ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
                ->where($where);
            $eventData2 = $query2->whereBetween('created_on',[$startDate, $endDate])->get();

            $listArr2 = $eventData2;

            if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                if($listArr2) {
                    foreach ( $listArr2 as $data ){
                        $listArr[] = $data;
                    }
                }
                if($listArr1) {
                    foreach ( $listArr1 as $data ){
                        $listArr[] = $data;
                    }
                }
            }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                $listArr = $listArr2;
            }else{
                $listArr = $listArr1;
            }
        }else{
            $listArr = $listArr1;
        }

        if(!empty($listArr)){
            $profitLoss = [];
            foreach ($listArr as  $acQuery) {
                $win = 0; $loss = 0;
                if($acQuery->type == 'DEBIT'){
                    //$trans_amount = $acQuery->amount;
                    if(isset($acQuery->amount)){ $loss = $acQuery->amount; }else{ $loss = 0;}

                }else{
                    //$trans_amount = 0;
                    if(isset($acQuery->amount)){ $win = $acQuery->amount; }else{ $win = 0;}
                }
                $profitLoss[] = $win - $loss ;
            }
        }


        if(isset($profitLoss) && $profitLoss != '' && $profitLoss != null){
            $profitLoss = array_sum($profitLoss);
            return round($profitLoss);

        }else{
            return 0;
        }

    }


       public function marketBetList(Request $request)
        {

        try{
           $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
           $uid = Auth::user()->id;

           $where = ([['mType',$request->mType],['uid',$uid],['mid',$request->market_id],['status',1]]);

           $mTypeArr = ['TEST_TEENPATTI','LUDO','TEENPATTI','POKER','ANDAR_BAHAR','7UPDOWN','MATKA','SIX_POKER','TEENPATTI20','32CASINO','HILOW','DRAGON_TIGER'];


           if(isset($request->market_id) && isset($request->mType) && !in_array($request->mType, $mTypeArr)){

              $betList = DB::connection('mongodb')->table('tbl_bet_history')->select('_id','betId','mType','rate','is_match','price','size','win','loss','bType','result','description','diff','market','created_on','updated_on')
                  ->where($where)
                  ->orderBy('created_on' ,'DESC')
                  ->get();
           
                  $bets=[];
                   if(!$betList->isEmpty()){
                         foreach ($betList as $value) {
                            $value = (object) $value;
                           if($value->mType == 'ballbyball' ){
                              $market = $value->market;
                              $space = ' ';
                              $ball = strstr($market,$space,true);        
                              $value->ball= $ball;
                            }
                            if($value->mType == 'khado'){
                               $difference = $value->diff - $value->price ;        
                              $value->difference= $difference;
                            }
                          
                             $bets[] =$value;
                        }
                      
                    }  

              $response = [ "status" => 1 ,'code'=> 200, "data" => $bets ,'message'=> 'Data Not Found !!' ];

          }elseif(in_array($request->mType, $mTypeArr)){

               if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                      
                        $startDate = date('Y-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                     
                        $endDate = date('Y-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
               }else{
              
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('Y-m-d h:i:s');

                $end = new \DateTime('now -5 day');
                $startDate = $end->format('Y-m-d h:i:s');  
              }


               if(isset($request->event_id)) {

                   $where = ([['mType', $request->mType], ['uid', $uid], ['status', 1], ['eid', (int)$request->event_id]]);
                      $query = DB::connection('mongodb')->table('tbl_bet_history_teenpatti')
                              ->select('_id','mType','rate','is_match','price','size','win','loss','bType','result','description','created_on','updated_on')
                              ->where($where)
                              ->orderBy('created_on' ,'DESC');
               }else{
                  // $where = ([['mType',$request->mType],['uid',$uid],['status',1],['mid',$request->market_id]]);
                      $query = DB::connection('mongodb')->table('tbl_bet_history_teenpatti')
                                ->select('_id','mType','rate','is_match','price','size','win','loss','bType','result','description','created_on','updated_on')
                                ->where([['uid',$uid],['status',1],['mid',(int)$request->market_id]])
                                ->orWhere([['uid',$uid],['status',1],['mid',$request->market_id]])
                                // ->where($where)
                                ->orderBy('created_on' ,'DESC');
               }

              if(isset($request->isFirst) && $request->isFirst==1){
                  $betList = $query->limit(10)->get();

              }else{                  
                $betList = $query->whereBetween('created_on',[$startDate, $endDate])->get();
              }

              //print_r($betList);die();

                  if($betList->isEmpty()){
                    
        //  echo $request->event_id;
                      if(isset($request->event_id)) {
                          $marketId = explode("-", $request->event_id);
                      }else{
                          $marketId = explode("-", $request->market_id);
                      }
                //$array1=explode("-",$request->event_id);
                //echo $array1[0];
                 $BetList = DB::connection('mongodb')->table('tbl_casino_transaction')
                      ->select('*')
                      ->where([['uid',$uid],['status',1],['round',$marketId[0]]])
                     // ->whereIn('result',['WIN','LOSS'])
                      ->get();
                            //->whereIn('result',['WIN','LOSS'])
                            //print_r($BetList);die();

                  $bets=[];
                   if(!$BetList->isEmpty()){
                         foreach ($BetList as $list) {
                  
                          $list = (object) $list;
                          /*    if($list->bType == 1){
                                $bType = "back";
                             }elseif($list->bType == 2){
                                $bType = "lay";
                             }else{
                                $bType = $list->bType;
                             }(*/
                          $game_code = explode("_", $list->game_code);
                          $gameCode = $game_code[1];
                          
                          $bets[] = [
                                '_id'       => isset($list->_id)?$list->_id:'N/A',
                               // 'description' => isset($list->game_code)?$list->game_code.' > round #'.$list->round:'N/A',
                                'description' => isset($gameCode)?$gameCode.' > round #'.$gameCode:'N/A',
                                'bType'       => isset($list->type)?$list->type:'N/A',
                                'price'       => isset($list->amount)?$list->amount:'N/A',
                                'size'        => isset($list->size)?$list->amount:'N/A',
                                'updated_on'  => isset($list->updated_on)?$list->updated_on:'N/A',
                                'created_on'  => isset($list->created_on)?$list->created_on:'N/A',
                                'result'      => isset($list->type)?$list->type:'N/A',
                                'win'         => isset($list->amount)?$list->amount:'N/A',
                                'loss'        => isset($list->amount)?$list->amount:'N/A',
                                'rate'        => 'N/A',
                                'mType'       => 'Casino'
                              ];
                        }
                      
                    }  
 

             $response = [ "status" => 1 ,'code'=> 200, "data" => $bets ,'message'=> 'Data Not Found !!' ]; 
                  }else{

                     $bets=[];
                   if(!$betList->isEmpty()){
                         foreach ($betList as $list) {
                          $list = (object) $list;
                              if($list->bType == 1){
                                $bType = "back";
                             }elseif($list->bType == 2){
                                $bType = "lay";
                             }else{
                                $bType = $list->bType;
                             }

                             $bets[] = [

                                '_id' => $list->_id,
                                'description' => $list->description,
                                'bType' => $bType,
                                 'price' => $list->price,
                                 'size' => $list->size,
                                 'updated_on'  =>  $list->updated_on,
                                 'created_on'  =>  $list->created_on,
                                 'result' => $list->result,
                                 'win'  =>$list->win,
                                 'loss' => $list->loss,
                                 'rate' => $list->rate,
                                 'mType' => isset($list->mType)?$list->mType:null
                          ];
                        }
                      
                    }  
 

             $response = [ "status" => 1 ,'code'=> 200, "data" => $bets ,'message'=> 'Data Not Found !!' ];
          }
        }
        elseif($request->type=='casino'){

            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                      
                        $startDate = date('Y-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                     
                        $endDate = date('Y-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
               }else{
              
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('Y-m-d h:i:s');

                $end = new \DateTime('now -5 day');
                $startDate = $end->format('Y-m-d h:i:s');  
              }

               $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            $tableName='tbl_transaction_client_'.$currentMonth;

            $query = DB::connection('mysql3')->table($tableName)
            ->select('*')
            ->where([['userId',$uid],['status',1],['sid',9999]])
            ->where('amount','!=',0);
        
            $bets=[];

            if($request->isFirst == 1){
                $BetList = $query->limit(10)->orderBy('created_on','desc')->get();
            }else{
                $BetList = $query->whereBetween('created_on',[$startDate, $endDate])->orderBy('created_on','desc')->get();
            }

         if(!$BetList->isEmpty()){
             
               foreach ($BetList as $list) {
        
                //$list = (object) $list;
                /*    if($list->bType == 1){
                      $bType = "back";
                   }elseif($list->bType == 2){
                      $bType = "lay";
                   }else{
                      $bType = $list->bType;
                   }(*/
               // $game_code = explode("_", $list->game_code);
                //$gameCode = $game_code[1];
                
                $bets[] = [
                      'id'       => isset($list->id)?$list->id:'N/A',
                      'description' => $list->description,//isset($gameCode)?$gameCode.' > round #'.$gameCode:'N/A',
                      'bType'       => isset($list->type)?$list->type:'N/A',
                      'price'       => isset($list->amount)?$list->amount:'N/A',
                      //'size'        => isset($list->amount)?$list->amount:'N/A',
                      'updated_on'  => isset($list->updated_on)?$list->updated_on:'N/A',
                      'created_on'  => isset($list->created_on)?$list->created_on:'N/A',
                      'result'      => isset($list->result)?$list->result:'N/A',
                      //'win'         => 0,
                      //'loss'        => 0,
                      //'rate'        => 'N/A',
                      'mType'       => 'Casino'
                    ];
              }
            
          }  
          $response = [ "status" => 1 ,'code'=> 200, "data" => $bets ,'message'=> 'Data Found !!' ]; 
          }
          else{

             $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Not Found !!' ];
          }


    return $response;

  }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

      }

    public function getEventList(Request $request) {
        $response =  [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
        try{
            $eventArr       = null;
            $profitLossSum  = 0;
            $sportDataCache = '';
            $cache          = Redis::connection();
            $userId         = Auth::user()->id;
            if( isset( $request->start_date ) && isset( $request->end_date ) && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ) {

                $startDate  = date('Y-m-d', strtotime($request->start_date));
                $startDate  = $startDate." 00:00:01";

                $endDate    = date('Y-m-d', strtotime($request->end_date));
                $endDate    = $endDate." 23:59:59";

                $start_date = strtotime($request->start_date);
                $end_date   = strtotime($request->end_date);

            }else{

                $start      = new \DateTime('now +1 day');
                $endDate    =  $start->format('Y-m-d h:i:s');

                $end        = new \DateTime('now -5 day');
                $startDate  = $end->format('Y-m-d h:i:s');

                $start_date = strtotime($request->start_date);
                $end_date   = strtotime($request->end_date);
            }

            //$days = (int)((strtotime($endDate) - strtotime($startDate))/86400);
            $days = abs(($end_date - $start_date)/60/60/24);

            if( $days > 31 ){
                $response = [ "status" => 0 ,'code'=> 200, "data" => null ,'message'=> 'Select Max 30 day for search result!' ];
                return $response;
            }

            if(!empty($request->sid) && $request->sid != 99 && $request->sid != 999) {

                if(isset($request->isFirst) && $request->isFirst == 1){
                    $end        = new \DateTime('now -1 day');
                    $startDate  = $end->format('yy-m-d h:i:s');
                }

            /*     $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            $tableName='tbl_transaction_client_'.$currentMonth;

                $query = DB::connection('mysql3')->table($tableName)
                    ->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
                    ->where([['sid',$request->sid],['userId',$userId],['status',1]])
                    ->orderBy('created_on','DESC');

                if(isset($request->isFirst) && $request->isFirst == 1){
                    $eventData1 = $query->limit(10)->get();
                }else{
                    $eventData1 = $query->whereBetween('created_on',[$startDate, $endDate])->get();
                }*/
                $tbls = $this->mergeTable($startDate,$endDate);
                $tbl1 = $tbls['tbl1'];
                if( strpos($tbl1,'_012021') > 0 ){
                    $tbl1 = str_replace('_012021','_022021',$tbl1);
                }
                $tbl2 = $tbls['tbl2'];
                $query = DB::connection('mysql3')->table($tbl1)
                    ->where([['sid',$request->sid],['userId',$userId],['status',1]]);
                $eventData1 = $query->whereBetween('created_on',[$startDate, $endDate])->get();
                $listArr1 = $eventData1;
                $listArr = [];
                if( $tbl2 != null ){
                    $query2 = DB::connection('mysql3')->table($tbl2)
                        ->where([['sid',$request->sid],['userId',$userId],['status',1]]);
                    $eventData2 = $query2->whereBetween('created_on',[$startDate, $endDate])->get();

                    $listArr2 = $eventData2;

                    if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                        if($listArr2) {
                            foreach ( $listArr2 as $data ){
                                $listArr[] = $data;
                            }
                        }
                        if($listArr1) {
                            foreach ( $listArr1 as $data ){
                                $listArr[] = $data;
                            }
                        }
                    }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                        $listArr = $listArr2;
                    }else{
                        $listArr = $listArr1;
                    }
                }else{
                    $listArr = $listArr1;
                }


                $profitLossSum  = 0;
                $eventArr       = null;
                $eidArr         = [];
//print_r($eventData1); exit;
                foreach ($listArr as $value) {
                    //$eidArr[] = $value->eid;
                    $totalArr[] = $value;
                    if(!in_array($value->eid, $eidArr)){

                        $des          = explode('>', $value->description);
                        $description  = $des[1];
                        $pl           = $this->getProfitLoss($value->sid, $userId, $value->eid,$startDate,$endDate);

                        // if($pl != 0){

                        $totalProfitLoss[] = $pl;
                        if(isset($totalProfitLoss)){ $profitLossSum = array_sum($totalProfitLoss); }

                        $sportDataCache = $cache->get('Sport_'.$request->sid);
                        $sportDataCache = json_decode($sportDataCache);

                        $eventArr[] = [

                            'eventId'     => $value->eid,
                            'eventName'   => $description,
                            //'slug'       => $value->slug,
                            'mType'       => $value->mType,
                            //'time'  => strtotime($value->created_on),
                            'profitLoss'  => round($pl),
                            'settled_date'=> $value->created_on,
                            'sport_name'  =>$sportDataCache->name

                        ];
                        $eidArr[] = $value->eid;
                        // }
                    }

                }

                $response = [ "status" => 1 ,'code'=> 200, "data" =>['item'=> $eventArr,'totalAmount'=>round($profitLossSum) ],'message'=> 'Data Found !!' ];
            }
            elseif($request->sid == 99)
            {

                $teenpattiGame = DB::connection('mysql')->table('tbl_live_game')->select('eventId','name','slug','mType','created_on')
                    ->where([['sportId',$request->sid],['status',1]])
                    ->get();

                if(!$teenpattiGame->isEmpty()){

                    $sportDataCache = $cache->get('Sport_'.$request->sid);
                    $sportDataCache = json_decode($sportDataCache);

                    foreach ($teenpattiGame as $value) {

                        $pl                 = $this->getProfitLossLiveGame(null, $userId, $value->eventId,$startDate, $endDate);
                        $totalProfitLoss[]  = $pl;

                        if(isset($totalProfitLoss)){ $profitLossSum = array_sum($totalProfitLoss); }else{ $profitLossSum = 0 ;}

                        $eventArr[] = [
                            'eventId'     => $value->eventId,
                            'eventName'   => $value->name,
                            'slug'        => $value->slug,
                            'mType'       => $value->mType,
                            'profitLoss'  => $pl,
                            'settled_date'=> $value->created_on,
                            'sport_name'  =>$sportDataCache->name

                        ];
                    }
                }

                $response = [ "status" => 1 ,'code'=> 200, "data" =>['item'=> $eventArr , 'totalAmount'=>$profitLossSum ],'message'=> 'Data Found !!' ];
            }
            else
            {
                $teenpattiGame = DB::connection('mysql')->table('tbl_live_game')
                    ->select('eventId','name','slug','mType','created_on')
                    ->where([['sportId',$request->sid],['status',1]])
                    ->get();

                if(!$teenpattiGame->isEmpty()){

                    $sportDataCache = $cache->get('Sport_'.$request->sid);
                    $sportDataCache = json_decode($sportDataCache);

                    foreach ($teenpattiGame as $value) {

                        $pl  = $this->getProfitLossLiveGame(null, $userId, $value->eventId, $startDate, $endDate);
                        $totalProfitLoss[] = $pl;
                        if(isset($totalProfitLoss)){ $profitLossSum = array_sum($totalProfitLoss); }else{ $profitLossSum = 0 ;}
                        $eventArr[] = [

                            'eventId' => $value->eventId,
                            'eventName'  => $value->name,
                            'slug'       => $value->slug,
                            'mType'       => $value->mType,
                            'profitLoss'  => $pl,
                            'settled_date'  => $value->created_on,
                            'sport_name'=>$sportDataCache->name

                        ];
                    }
                }

                $response = [ "status" => 1 ,'code'=> 200, "data" =>['item'=> $eventArr , 'totalAmount'=>$profitLossSum ],'message'=> 'Data Found !!' ];
            }
            return $response;
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

  /*
  public function getEventList20-01-21(Request $request)
    {

  
      $response =  [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
      try{
        $eventArr=null;
        $profitLossSum = 0;
        $sportDataCache = '';
      $cache = Redis::connection();
         $userId = Auth::user()->id;
          if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                  
                        $startDate = date('Y-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                     
                        $endDate = date('Y-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";

              $start_date = strtotime($request->start_date); 
              $end_date = strtotime($request->end_date);
                    
            }else{
                
                 $start = new \DateTime('now +1 day');
                $endDate =  $start->format('Y-m-d h:i:s');

                $end = new \DateTime('now -5 day');
                $startDate = $end->format('Y-m-d h:i:s');  

                $start_date = strtotime($request->start_date); 
                $end_date = strtotime($request->end_date); 
            }

          //$days = (int)((strtotime($endDate) - strtotime($startDate))/86400);
            $days = abs(($end_date - $start_date)/60/60/24); 

          if( $days > 31 ){
              $response = [ "status" => 0 ,'code'=> 200, "data" => null ,'message'=> 'Select Max 30 day for search result!' ];
              return $response;
          }
       
           if(!empty($request->sid) && $request->sid != 99 && $request->sid != 999) {
                      
                   if(isset($request->isFirst) && $request->isFirst == 1){
                     
                      $end = new \DateTime('now -1 day');
                      $startDate = $end->format('yy-m-d h:i:s');
                   }
                

                    $query = DB::connection('mysql3')->table('tbl_transaction_client')->select('eid','mid','sid','mType','result','description','type','created_on','updated_on','amount','balance')
                          ->where([['sid',$request->sid],['userId',$userId],['status',1]])
                           ->orderBy('created_on','DESC');
                        

                  if(isset($request->isFirst) && $request->isFirst == 1){
                       $eventData1 = $query->limit(10)->get();
                  }else{
                      $eventData1 = $query->whereBetween('created_on',[$startDate, $endDate])->get();
                  }
                 
                    $profitLossSum = 0;
                    $eventArr=null; 
                    $eidArr=[];
                       foreach ($eventData1 as $value) {

                        //$eidArr[] = $value->eid;
                        $totalArr[] = $value;
                        if(!in_array($value->eid, $eidArr)){

                        $des = explode('>', $value->description);
                        $description = $des[1];
                      

                          $pl  = $this->getProfitLoss(null, $userId, $value->eid);
                          if($pl != 0){
                              $totalProfitLoss[] = $pl;
                              if(isset($totalProfitLoss)){ $profitLossSum = array_sum($totalProfitLoss); }

     $sportDataCache= $cache->get('Sport_'.$request->sid);
                            $sportDataCache = json_decode($sportDataCache);


                              $eventArr[] = [

                                  'eventId' => $value->eid,
                                  'eventName'  => $description,
                                  //'slug'       => $value->slug,
                                  'mType'       => $value->mType,
                                  //'time'  => strtotime($value->created_on),
                                  'profitLoss'  => $pl,
                                  'settled_date'  => $value->created_on,
                                  'sport_name'=>$sportDataCache->name

                              ];
                              $eidArr[] = $value->eid;
                          }
                        }

                       }

                        $response = [ "status" => 1 ,'code'=> 200, "data" =>['item'=> $eventArr,'totalAmount'=>$profitLossSum ],'message'=> 'Data Found !!' ];
                                                             
                   
                  }elseif($request->sid == 99){
                         
                        $teenpattiGame = DB::connection('mysql')->table('tbl_live_game')->select('eventId','name','slug','mType','created_on')
                          ->where([['sportId',$request->sid],['status',1]])
                          ->get();

                          if(!$teenpattiGame->isEmpty()){

                            $sportDataCache= $cache->get('Sport_'.$request->sid);
                            $sportDataCache = json_decode($sportDataCache);
                            foreach ($teenpattiGame as $value) {

                               $pl  = $this->getProfitLossLiveGame(null, $userId, $value->eventId,$startDate, $endDate);
                               $totalProfitLoss[] = $pl;
                               if(isset($totalProfitLoss)){ $profitLossSum = array_sum($totalProfitLoss); }else{ $profitLossSum = 0 ;}
                                 $eventArr[] = [

                                              'eventId' => $value->eventId,
                                              'eventName'  => $value->name,
                                              'slug'       => $value->slug,
                                              'mType'       => $value->mType,
                                              'profitLoss'  => $pl,
                                              'settled_date'  => $value->created_on,
                                              'sport_name'=>$sportDataCache->name

                                           ];
                                }

                            }

                         $response = [ "status" => 1 ,'code'=> 200, "data" =>['item'=> $eventArr , 'totalAmount'=>$profitLossSum ],'message'=> 'Data Found !!' ];
                    
                 }else{
                     
                        $teenpattiGame = DB::connection('mysql')->table('tbl_live_game')->select('eventId','name','slug','mType','created_on')
                          ->where([['sportId',$request->sid],['status',1]])
                          ->get();

                          if(!$teenpattiGame->isEmpty()){
                            $sportDataCache= $cache->get('Sport_'.$request->sid);
                            $sportDataCache = json_decode($sportDataCache);
                            foreach ($teenpattiGame as $value) {

                               $pl  = $this->getProfitLossLiveGame(null, $userId, $value->eventId, $startDate, $endDate);
                               $totalProfitLoss[] = $pl;
                               if(isset($totalProfitLoss)){ $profitLossSum = array_sum($totalProfitLoss); }else{ $profitLossSum = 0 ;}
                                 $eventArr[] = [

                                              'eventId' => $value->eventId,
                                              'eventName'  => $value->name,
                                              'slug'       => $value->slug,
                                              'mType'       => $value->mType,
                                              'profitLoss'  => $pl,
                                              'settled_date'  => $value->created_on,
                                              'sport_name'=>$sportDataCache->name

                                           ];
                                }

                            }

                         $response = [ "status" => 1 ,'code'=> 200, "data" =>['item'=> $eventArr , 'totalAmount'=>$profitLossSum ],'message'=> 'Data Found !!' ];
                    
                  }

          return $response;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

     }
  */
}