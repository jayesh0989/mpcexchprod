<?php

namespace App\Http\Controllers\Market;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;


class MarketResultController extends Controller
{

    public function marketResult(Request $request)
    {

        try {

            $response = ["status" => 0, "code" => 400, "message" => "Bad request!"];
            $userId = Auth::user()->id;
            $items = [];


            if (json_last_error() == JSON_ERROR_NONE) {

                if (isset($request->start_date) && isset($request->end_date)
                    && ($request->start_date != '') && ($request->end_date != '')
                    && ($request->start_date != null) && ($request->end_date != null)) {

                    $startDate = date('Y-m-d', strtotime($request->start_date));
                    $startDate = $startDate . " 00:00:01";
                    $endDate = date('Y-m-d', strtotime($request->end_date));
                    $endDate = $endDate . " 23:59:59";
                } else {
                    $start = new \DateTime('now +1 day');
                    $endDate = $start->format('yy-m-d h:i:s');
                    $end = new \DateTime('now -5 day');
                    $startDate = $end->format('yy-m-d h:i:s');
                }

            }

            if (isset($request->type) && $request->type != '') {
                $type = $request->type;
                $where = ['mType' => $type, 'userId' => $userId, 'status' => 1];
            } else {
                $where = ['userId' => $userId, 'status' => 1];
            }

            if (isset($request->type) && $request->type != ' ') {
                if ($request->type == 'cricket') {
                    $portId = 4;
                } elseif ($request->type == 'football') {
                    $portId = 1;
                } elseif ($request->type == 'tennis') {
                    $portId = 2;
                } elseif ($request->type == 'jackpot') {
                    $portId = 10;
                } elseif ($request->type == 'cricket_casino') {
                    $portId = 11;
                } elseif ($request->type == 'election') {
                    $portId = 6;
                } else {
                    $portId = 99;
                }

                $marketList = [];
                $marketList2 = [];

                $updatedOn = date('Y-m-d H:i:s');
                $year = date('Y', strtotime($updatedOn));
                $month = date('m', strtotime($updatedOn));
                $currentMonth = $month . $year;
                $tableName = 'tbl_transaction_client_' . $currentMonth;

                $query = DB::connection('mysql3')->table($tableName)->select('eid', 'result', 'description', 'updated_on', 'mType', 'type', 'mid')
                    ->where([['userId', $userId], ['status', 1], ['sid', $portId]])
                    ->where('result', '!=', 'null')
                    ->orderBy('created_on', 'DESC');


                if (isset($request->isFirst) && $request->isFirst == 1) {
                    $marketList = $query->limit(10)->get();
                } else {
                    $marketList = $query->whereBetween('created_on', [$startDate, $endDate])->get();
                }

            } else {

                $updatedOn = date('Y-m-d H:i:s');
                $year = date('Y', strtotime($updatedOn));
                $month = date('m', strtotime($updatedOn));
                $currentMonth = $month . $year;
                $tableName = 'tbl_transaction_client_' . $currentMonth;

                $query = DB::connection('mysql3')->table($tableName)->select('eid', 'result', 'description', 'updated_on', 'mType', 'type', 'mid')
                    ->where([['userId', $userId], ['status', 1]])
                    ->where('result', '!=', 'null')
                    ->orderBy('created_on', 'DESC');

                if (isset($request->isFirst) && $request->isFirst == 1) {
                    $marketList = $query->limit(10)->get();
                } else {
                    $marketList = $query->whereBetween('created_on', [$startDate, $endDate])->get();
                }

            }


            if (!$marketList->isEmpty()) {
                $casinoArray = $arr = [];
                foreach ($marketList as $marketList3) {

                    if (isset($marketList3->mType) && $marketList3->mType == 'casino') {


                        if ($marketList3->mType == 'CREDIT') {
                            if (strpos($marketList3->mid, 'B') > 0) {
                                $marketId = str_replace('-B', '', $marketList3->mid);
                            } else {
                                $marketId = str_replace('-W', '', $marketList3->mid);
                            }
                            $casinoArray[] = $marketId;
                        }


                    }
                }

                // echo "<pre>"; print_r($marketList); die();
                foreach ($marketList as $marketList4) {
                    $marketList4 = (object)$marketList4;
                    if (isset($marketList4->mType) && $marketList4->mType == 'casino') {
                        $desc = explode(">", $marketList4->description);
                        $description = $desc[0] . '>' . $desc[1] . '>' . $desc[2];

                        $result = $marketList4->type;
                        if($marketList4->type == 'CREDIT'){
                            $result = 'WIN';
                        }else{
                          $result = 'LOSS';
                        }
                    }
                    if (isset($marketList4->mType) && $marketList4->mType == 'casino') {
                        if (strpos($marketList4->mid, 'B') > 0) {
                            $marketId = str_replace('-B', '', $marketList4->mid);
                        } else {
                            $marketId = str_replace('-W', '', $marketList4->mid);
                        }
                        if ($marketList4->type == 'DEBIT' && !in_array($marketId, $casinoArray)) {


                        } else {
                           
                            $arr [] = [

                                'eventId' => $marketList4->eid,
                                'description' => isset($description) ? $description : $marketList4->description,
                                'result' => isset($result) ? $result : $marketList4->result,
                                'date' => $marketList4->updated_on
                            ];
                        }

                    } else {
                         
                        if (isset($marketList4->eid)) {
                            $arr [] = [

                                'eventId' => $marketList4->eid,
                                'description' => isset($description) ? $description : $marketList4->description,
                                'result' => isset($result) ? $result : $marketList4->result,
                                'date' => $marketList4->updated_on
                            ];
                        }
                    }


                }

                $response = ["status" => 1, 'code' => 200, "data" => $arr, 'message' => 'Data Found !!'];

            } else {
                $response = ["status" => 1, 'code' => 200, "data" => null, 'message' => 'Data Not Found !!'];
            }


            return $response;

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }
}