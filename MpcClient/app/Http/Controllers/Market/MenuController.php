<?php

namespace App\Http\Controllers\Dashboard;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;


class MenuController extends Controller
{

  
    public function frontMenuList(Request $request)
    {
      
    	  $response = response()->json(['status'=>0,'code'=>400,'message'=>'Bad request !!!','data'=>null]);
      try{
    	//echo "else";
         /* $cache = Redis::connection();
          $sportList=$cache->get("SportList");
          $sportList = json_decode($sportList);
          */
           $sportList = DB::table('tbl_sport')->select('*')->get();
           $sportList = json_decode($sportList, True);
   
           $url = '';
           $serverUrl = DB::table('tbl_common_setting')->select('value')->where([['status',1],['key_name','SERVER_URL']])->first();

           if(!empty($serverUrl)){
             $url = $serverUrl->value.'/'.'dashboard_images';
           }
           
          if(!empty($sportList)){
             foreach ($sportList as  $list) {
            
             $menu[] = [
         
              'name' => $list['name'],
              'slug'  => strtolower($list['slug']),
              'sportId'  => $list['sportId'],
              'status' => $list['status'],
              'icon'  => $url.'/'.$list['img'],
              'icon_class' => $list['icon_class'],
              'sport_class' => $list['sport_class'],
              'list'  => $this->getEventList($list['sportId']),
               ];

           }

             $response = response()->json(['status'=>1,'code'=>200,'data'=>$menu,'message'=>'List found !!']);

        }else{

      	     $response = response()->json(['status'=>1,'code'=>200,'message'=>'List not found !!','data'=>null]);
          }
     

      return $response;

    }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
 
 }


   public function getEventList($sportId)
    {
       try{

         $list=[];
        $cache = Redis::connection();
        $event_Data = $cache->get("Event_List_" . $sportId);
        $event_Data1 = json_decode($event_Data);
           $event_Data=[];
           if(!empty($event_Data1)) {
               foreach ($event_Data1->eventData as $key => $value) {
                   $event_Data[] = $value;
               }
               $event_Data=  array_reverse($event_Data);
           }
        //print_r($event_Data->eventData);die('ahdaj');
        if(!empty($event_Data)){
        foreach ($event_Data as $event) {
          # code...
          if($event->game_over != 1 && $event->is_block != 1){
          $runners = json_decode($event->runners);

           $runner1 = 0;
          if(isset($runners[0])){
          $runner1 = $runners[0]->runner;
          }
          $runner2 = 0;
          if(isset($runners[1])){
          $runner2 = $runners[1]->runner;
          }
          $runner3 = 0;
          if(isset($runners[3])){
            $runner3 = $runners[3]->runner;
          }

         

          $list[] = [

              'eventId' => $event->eventId,
              'name'    => $event->name,
                'type' => $event->type,
              'runner1' => $runner1,
              'runner2' => $runner2,
              'runner3' => $runner3
          ];
        }
        }
      }
    
     return $list;
   }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

}


}