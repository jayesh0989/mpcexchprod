<?php

namespace App\Http\Controllers\Market;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;


class ProfitLossFancyController extends Controller
{

    public function profitLossFancy(Request $request)
    {

      //die('profit');
       $response = response()->json(['status'=>0,'code'=>400,'message'=>'Bad request !!!','data'=>null]);

       try{

        if ($request['market_id']) {
          
            $userId = Auth::user()->id;
     
            $marketId = $request['market_id'];
            $eventId = $request['event_id'];
            $sessionType = $request['session_type'];
            $betList = null;

            if ($sessionType == 'fancy' || $sessionType == 'fancy2'  || $sessionType == 'fancy3') {
                $where = [ ['mid', $marketId], ['uid', $userId],['status',1]];
                $betListData = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('runner', 'rate', 'price', 'size', 'win', 'loss', 'bType', 'mType')
                    ->where($where)
                    ->orderBy('_id', 'DESC')
                    ->get();
             
                if ($betListData != null) {
                    $betList = $betListData;
                }
            }

            if ($sessionType == 'khado' || $sessionType == 'ballbyball') {
                $where = [ ['mid', $marketId], ['uid', $userId],['status',1]];
                $betListData = DB::connection('mongodb')->table('tbl_bet_pending_3')->select('runner', 'rate', 'price', 'size', 'win', 'loss', 'bType', 'mType')
                    ->where($where)
                    ->orderBy('_id', 'DESC')
                    ->get();

                if ($betListData != null) {
                    $betList = $betListData;
                }
            }
       

            if ($sessionType == 'fancy' || $sessionType == 'fancy2' || $sessionType == 'ballbyball') {
                 $profitLossData = $this->getProfitLossFancy($userId, $eventId, $marketId, $sessionType);
            }

            if ($sessionType == 'fancy3') {
                $profitLossData = $this->getProfitLossSingleFancyBook($userId, $eventId, $marketId, $sessionType);
            }

            if ($sessionType == 'khado') {
                $profitLossData = $this->getProfitLossKhado($userId, $eventId, $marketId, $sessionType);
            }

            $response = response()->json(['status' => 1, 'code'=> 200, 'data' =>[ 'profit_loss' => $profitLossData, 'betList' => $betList], 'message'=>'Data Found !!']);

        }

        return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
  
    }


    // getProfitLossFancy 
    public function getProfitLossFancy($userId,$eventId, $marketId, $sessionType)
    {
      try{

        $table='tbl_bet_pending_2';
        if($sessionType=='ballbyball'){
           $table='tbl_bet_pending_3';
        }

        $where = [ ['result', 'PENDING'],['mid', $marketId] , ['uid', $userId], ['mType',$sessionType], ['status',1] ];
         $betList = DB::connection('mongodb')->table($table)->select('price','win','loss','bType')
                  ->where($where)
                   ->get();

        $dataReturn = null;
        $result = $newbetresult = $betresult = [];
        if( $betList != null ){
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $betresult[]= array('price'=>$bet->price,'bType'=>$bet->bType,'loss'=>$bet->loss,'win'=>$bet->win);
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1;
            $max = $max+1;
            $betarray = []; $bet_type = '';
            $win=0; $loss=0; $count= $min;
            $totalbetcount=count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bet_type = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount=0;
                for($i= 0;$i<$minval;$i++){
                    if($bet_type == 'no'){
                        $top= $top+$value['win'];
                        $profitcount++;
                        $newresult[]= array('count'=>$count,'price'=>$value['price'],'bType'=>$value['bType'],'totalbetcount'=>$totalbetcount,'expose'=>$value['win']);
                    }else{
                        $bottom=$bottom + $value['loss'];
                        $losscount++;
                        $newresult[]= array('count'=>$count,'price'=>$value['price'],'bType'=>$value['bType'],'totalbetcount'=>$totalbetcount,'expose'=>-$value['loss']);

                    }
                    $count++;
                }

                for($i= 0;$i<=$maxval;$i++){
                    if($bet_type=='no'){
                        $newresult[]= array('count'=>$count,'price'=>$value['price'],'bType'=>$value['bType'],'totalbetcount'=>$totalbetcount,'expose'=>-$value['loss']);
                        $bottom=$bottom+ $value['loss'];
                        $losscount++;
                    }else{
                        $top= $top+$value['win'];
                        $profitcount++;
                        $newresult[]= array('count'=>$count,'price'=>$value['price'],'bType'=>$value['bType'],'totalbetcount'=>$totalbetcount,'expose'=>$value['win']);
                    }

                    $count++;
                }
                $result[]= array('count'=>$value['price'],'bType'=>$value['bType'],'profit'=>$top,'loss'=>$bottom,'profitcount'=>$profitcount,'losscount'=>$losscount,'newarray'=>$newresult);
            }

            $newbetarray=[]; $newbetresult=[];
            $totalmaxcount= $max-$min;
            if($totalmaxcount>0){
                $minstart=$min;
                for($i=0;$i<$totalmaxcount;$i++){

                    $newbetarray1=[]; $finalexpose=0;
                    for($x=0;$x<$totalbetcount;$x++){
                        // echo "<pre>"; print_r($result[$x]['newarray']);echo "<br>";echo "<br>"; exit;

                        $expose=$result[$x]['newarray'][$i]['expose'];
                        $finalexpose=$finalexpose+$expose;

                        $newbetarray1[]=array('bet_price'=>$result[$x]['count'],'bType'=>$result[$x]['bType'],'expose'=>$expose);
                    }
                    //$newbetresult[] = $finalexpose;
                    $dataReturn[] = [
                        'price' => $minstart,
                        'profitLoss' => round($finalexpose),
                    ];

                    $minstart++;
                    $newbetarray[]=array('exposearray'=>$newbetarray1,'finalexpose'=>$finalexpose);
                }
            }

        }

        if( $dataReturn != null ){

            $dataReturnNew = [];

            $i = $start = $startPl = $end = 0;

            foreach ( $dataReturn as $index => $d) {

                if ($index == 0) {
                    $dataReturnNew[] = [ 'price' => $d['price'] . ' or less' , 'profitLoss' => $d['profitLoss'] ];

                } else {
                    if ($startPl != $d['profitLoss']) {
                        if ($end != 0) {

                            if( $start == $end ){
                                $priceVal = $start;
                            }else{
                                $priceVal = $start . ' - ' . $end;
                            }

                            $dataReturnNew[] = [ 'price' => $priceVal , 'profitLoss' => $startPl ];
                        }

                        $start = $d['price'];
                        $end = $d['price'];

                    } else {
                        $end = $d['price'];
                    }
                    if ( $index == (count($dataReturn) - 1)) {
                        $dataReturnNew[] = [ 'price' => $start . ' or more' , 'profitLoss' => $d['profitLoss'] ];
                    }

                }

                $startPl = $d['profitLoss'];
                $i++;

            }

            $dataReturn = $dataReturnNew;

        }

        return $dataReturn;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }




    // getProfitLossFancy
    public function getProfitLossKhado($userId,$eventId, $marketId, $sessionType)
    {
        try {
            $where = [['result', 'PENDING'], ['mid', $marketId],  ['uid', $userId], ['status', 1]];
            $betList = DB::connection('mongodb')->table('tbl_bet_pending_3')->select('price', 'win', 'loss', 'bType', 'diff')
                ->where($where)
                ->get();
            $dataReturn = null;
            $result = $newbetresult = $betresult = [];
            if ($betList != null) {
                $min = $max = 0;

                foreach ($betList as $index => $bet) {
                    $betresult[] = array('price' => $bet->price, 'bet_type' => $bet->bType, 'loss' => $bet->loss, 'win' => $bet->win, 'difference' => $bet->diff);
                    if ($index == 0) {
                        $min = $bet->price;
                        $max = $bet->diff;
                    }
                    if ($min > $bet->price)
                        $min = $bet->price;
                    if ($max < $bet->diff)
                        $max = $bet->diff;
                }

                $min = $min - 20;
                if ($min < 0) {
                    $min = 0;
                }
                // $max = $max+1;
                $max = $max + 20;
                $betarray = [];
                $bet_type = '';
                $win = 0;
                $loss = 0;
                $count = $min;
                $lossVal1 = 0;
                $totalbetcount = count($betresult);
                for ($i = $min; $i <= $max; $i++) {
                    $winVal = 0;
                    $currentVal = (int)$i;
                    $where = [['diff', '>', $currentVal], ['price', '<=', $currentVal], ['mid', $marketId], ['uid', $userId], ['result', 'PENDING'], ['status', 1],  ['mType', $sessionType]];
                    $betList1 = DB::connection('mongodb')->table('tbl_bet_pending_3')
                        ->where($where)->sum("win");
                    if (!empty($betList1)) {
                        $winVal = $betList1;
                    }

                    $sqlQuery = "SELECT SUM(loss) as lossVal FROM tbl_bet_pending_3 where (diff <= $currentVal OR price > $currentVal) and (status=1 and result='PENDING' and mid='" . $marketId . "' and uid=" . $userId . " and mType='" . $sessionType . "')";
                    $betList3 = DB::connection('mongodb')->select($sqlQuery);
                    $lossVal1 = 0;
                    if (!empty($betList3)) {
                        // $lossVal1 = $betList3;
                        $lossVal1 = $betList3[0]->lossVal;
                    }

                    $total = $winVal - $lossVal1;
                    $dataReturn[] = [
                        'price' => $i,
                        'profitLoss' => round($total, 0)
                    ];
                }

            }
            return $dataReturn;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


     //Event: get single fancy Profit Loss book
   public function getProfitLossSingleFancyBook($userId,$eventId, $marketId, $sessionType)
   {
       try {
           $yes_sum = 0;
           $yes_loss = 0;
           $no_loss = 0;
           $where = [ ['result', 'PENDING'], ['mid', $marketId], ['uid', $userId], ['status', 1], ['bType', 'back']];
           $betListYesWin = DB::connection('mongodb')->table('tbl_bet_pending_2')->where($where)->sum('win');
           $betListYesLoss = DB::connection('mongodb')->table('tbl_bet_pending_2')->where($where)->sum('loss');
           $yes_sum = $betListYesWin;
           $yes_loss = $betListYesLoss;


           $where = [ ['result', 'PENDING'], ['mid', $marketId], ['uid', $userId], ['status', 1], ['bType', 'lay']];
           $betListNoWin = DB::connection('mongodb')->table('tbl_bet_pending_2')->where($where)->sum('win');
           $betListNoLoss = DB::connection('mongodb')->table('tbl_bet_pending_2')->where($where)->sum('loss');
           $no_win = $betListNoWin;
           $no_loss = $betListNoLoss;

           $yes_profitloss = round($yes_sum - $no_loss);
           $no_profitloss = round($no_win - $yes_loss);

           $betList = null;
           $where = [ ['mid', $marketId], ['uid', $userId], ['status', 1]];
           $betListData = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('runner', 'bType', 'price', 'win', 'size', 'rate', 'loss')
               ->where($where)
               ->get();
           if ($betListData != null) {
               $betList = $betListData;
           }

           $response = ['yes_profitloss' => $yes_profitloss, 'no_profitloss' => $no_profitloss];
           return $response;
       } catch (\Exception $e) {
           $response = $this->errorLog($e);
           return response()->json($response, 501);
       }
   }




    public function getBetList(Request $request)
    {
        $response = response()->json(['status' => 0, 'code' => 400, 'message' => 'Bad request !!!', 'data' => null]);
        try {

            if ($request['market_id']) {
                $userId = Auth::user()->id;
                $marketId = $request['market_id'];
                $eventId = $request['event_id'];
                $sessionType = $request['session_type'];
                $betList = null;

                if($sessionType == 'USDINR' || $sessionType == 'GOLD'|| $sessionType == 'SILVER'|| $sessionType == 'EURINR'|| $sessionType == 'GBPINR'|| $sessionType == 'ALUMINIUM' || $sessionType == 'COPPER' || $sessionType == 'CRUDEOIL' || $sessionType == 'ZINC' || $sessionType == 'binary' || $sessionType == 'NIFTY' || $sessionType == 'BANKNIFTY'){
                    $where = [ ['mid', $marketId], ['uid', $userId], ['status', 1]];
                    $betListData = DB::connection('mongodb')->table('tbl_bet_pending_5')->select('runner', 'rate', 'price', 'size', 'win', 'loss', 'bType', 'mType')
                        ->where($where)
                        ->orderBy('_id', 'DESC')
                        ->get();

                    if ($betListData != null) {
                        $betList = $betListData;
                    }
                }


                if ($sessionType == 'fancy' || $sessionType == 'fancy2' || $sessionType == 'fancy3' || $sessionType == 'oddeven') {
                    $where = [ ['mid', $marketId], ['uid', $userId], ['status', 1]];
                    $betListData = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('runner', 'rate', 'price', 'size', 'win', 'loss', 'bType', 'mType')
                        ->where($where)
                        ->orderBy('_id', 'DESC')
                        ->get();

                    if ($betListData != null) {
                        $betList = $betListData;
                    }
                }

                if ($sessionType == 'ballbyball') {
                    $where = [ ['mid', $marketId], ['uid', $userId], ['status', 1]];
                    $betListData = DB::connection('mongodb')->table('tbl_bet_pending_3')->select('runner', 'rate', 'price', 'size', 'win', 'loss', 'bType', 'mType')
                        ->where($where)
                        ->orderBy('_id', 'DESC')
                        ->get();

                    if ($betListData != null) {
                        $betList = $betListData;
                    }
                }

                if ($sessionType == 'khado') {
                    $where = [ ['mid', $marketId], ['uid', $userId], ['status', 1]];
                    $betListData = DB::connection('mongodb')->table('tbl_bet_pending_3')->select('runner', 'rate', 'price','diff', 'size', 'win', 'loss', 'bType', 'mType')
                        ->where($where)
                        ->orderBy('_id', 'DESC')
                        ->get();

                    if ($betListData != null) {
                        $betList = $betListData;
                    }
                }

                if ($sessionType == 'cricket_casino') {
                    $where = [ ['mid', $marketId], ['uid', $userId], ['status', 1]];
                    $betListData = DB::connection('mongodb')->table('tbl_bet_pending_4')->select('*')
                        ->where($where)
                        ->orderBy('_id', 'DESC')
                        ->get();

                    if ($betListData != null) {
                        $betList = $betListData;
                    }
                }

                if ($sessionType == 'jackpot') {
                    $where = [ ['mid', $marketId], ['uid', $userId], ['status', 1]];
                    $betListData = DB::connection('mongodb')->table('tbl_bet_pending_4')->select('*')
                        ->where($where)
                        ->orderBy('_id', 'DESC')
                        ->get();

                    if ($betListData != null) {
                        $betList = $betListData;
                    }
                }

                if($betList != null) {
                    $response = response()->json(['status' => 1, 'code' => 200, 'data' => ['betList' => $betList], 'message' => 'Data Found !!']);
                }else{
                    $response = response()->json(['status' => 1, 'code' => 200, 'data' => ['betList' => null], 'message' => 'Data Not Found !!']);
                }
            }

            return $response;

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


}