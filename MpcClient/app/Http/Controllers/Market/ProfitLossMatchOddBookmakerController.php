<?php

namespace App\Http\Controllers\Market;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class ProfitLossMatchOddBookmakerController extends Controller
{

    public function ProfitLossMatchOddBookmaker(Request $request)
    {
   
        try{

        $response = ['status'=>0,'code'=>400,'message'=>'Bad request !!!','data'=>null];

        $userId = Auth::id();
        // $marketId = $request->market_id;
        $eventId = $request->event_id;
//        $sessionType ='';
//        if(isset($request->session_type)){
//            $sessionType = $request->session_type;
//        }


        $runnerSetMarketArr = $runnerGoalsArr = $runnerWinnerArr = $runnerMatchoddArr = $runnerBookmakerArr = $runnerCompletedMatchArr = $runnerTiedMatchArr= $runnerVirtualArr = [];

        $total = 0;
        // IF RUNNER WIN
        if(!empty($eventId) ){

            $cache = Redis::connection();
            $eventData = $cache->get("Bookmaker_".$eventId);

            if(!empty($eventData)) {
                $market = json_decode($eventData);
                if($market != null || $market != '') {
                    foreach ($market as $market) {
                      $eventDatarunner = json_decode($market->runners, 16);
                      foreach ($eventDatarunner as $value) {
                          $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'bookmaker');
                          $runnerBookmakerArr [] = [
                              'marketId' => $market->marketId,
                              'runner' => $value['runner'],
                              'secId' => $value['secId'],
                              'profit_loss' => $profit_loss ? $profit_loss : 0
                          ];
                      }

                    }
                }
            }

            $eventDataVirtual=$cache->get("Virtual_".$eventId);

            if(!empty($eventDataVirtual)) {
                    $market = json_decode($eventDataVirtual);

                  if($market != null || $market != '') {
                      foreach ($market as $market) {
                          $eventDatarunner = json_decode($market->runners, 16);
                          
                          foreach ($eventDatarunner as $value) {
                              $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'virtual_cricket');
                              $runnerVirtualArr [] = [
                                  'marketId' => $market->marketId,
                                  'runner' => $value['runner'],
                                  'secId' => $value['secId'],
                                  'profit_loss' => $profit_loss ? $profit_loss : 0

                              ];

                          }

                      }
                  }
                }

            $eventMatchodd=$cache->get("Match_Odd_".$eventId);

            if(!empty($eventMatchodd)) {
                      $eventMatchodd = json_decode($eventMatchodd);
                       
                     $market = $eventMatchodd;

                        if(isset($market->runners)){
                          $eventMatchoddRunner = json_decode($market->runners, 16);


                          foreach ($eventMatchoddRunner as $value) {
                              $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'match_odd');
                              $runnerMatchoddArr [] = [
                                  'marketId' => $market->marketId,
                                  'runner' => $value['runner'],
                                  'secId' => $value['secId'],
                                  'profit_loss' => $profit_loss ? $profit_loss : 0

                              ];

                          }
                        }
                     
                      
                  }

            // winner
            $eventWinner = $cache->get("Winner_".$eventId);

            if(!empty($eventWinner)) {
                $eventWinner = json_decode($eventWinner);
                $market = $eventWinner;

                if(isset($market->runners)){
                    $eventWinnerRunner = json_decode($market->runners, 16);
                    foreach ($eventWinnerRunner as $value) {
                        $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'winner');
                        $runnerWinnerArr [] = [
                            'marketId' => $market->marketId,
                            'runner' => $value['runner'],
                            'secId' => $value['secId'],
                            'profit_loss' => $profit_loss ? $profit_loss : 0
                        ];

                    }
                }
            }

            // goals
            $eventGoalsArr = $cache->get("Goals_".$eventId);
            if(!empty($eventGoalsArr)) {
                $marketArr = json_decode($eventGoalsArr);
                if( $marketArr != null || $marketArr != '') {
                    foreach ( $marketArr as $market ) {
                    	if(!isset($market->runners)){
                    		$market = json_decode($market);
                    	}
                    	if( $market != null ){
                            $eventDatarunner = json_decode($market->runners, 16);
                            foreach ($eventDatarunner as $value) {
                                $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'goals');
                                $runnerGoalsArr[] = [
                                    'marketId' => $market->marketId,
                                    'runner' => $value['runner'],
                                    'secId' => $value['secId'],
                                    'profit_loss' => $profit_loss ? $profit_loss : 0
                                ];
                            }
                        }

                    }
                }
            }

            // set_market
            $eventSetMarketArr = $cache->get("Set_Market_".$eventId);
            if(!empty($eventSetMarketArr)) {
                $marketArr = json_decode($eventSetMarketArr);
                if( $marketArr != null || $marketArr != '') {
                    foreach ( $marketArr as $market ) {
                        if( !isset($market->runners) ){
                            $market = json_decode($market);
                        }

                        if( $market != null ){
                            $eventDatarunner = json_decode($market->runners, 16);
                            foreach ($eventDatarunner as $value) {
                                $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'set_market');
                                $runnerSetMarketArr[] = [
                                    'marketId' => $market->marketId,
                                    'runner' => $value['runner'],
                                    'secId' => $value['secId'],
                                    'profit_loss' => $profit_loss ? $profit_loss : 0
                                ];
                            }
                        }

                    }
                }
            }

                  $eventMatchodd=$cache->get("Completed_Match_".$eventId);

                  if(!empty($eventMatchodd)) {
                      $eventMatchodd = json_decode($eventMatchodd);
                        
                     $market = $eventMatchodd;
if(isset($market->runners)){
                          $eventMatchoddRunner = json_decode($market->runners, 16);
 
                          foreach ($eventMatchoddRunner as $value) {
                              $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'completed_match');
                              $runnerCompletedMatchArr [] = [
                                  'marketId' => $market->marketId,
                                  'runner' => $value['runner'],
                                  'secId' => $value['secId'],
                                  'profit_loss' => $profit_loss ? $profit_loss : 0

                              ];
}
                          }
                      
                  }
                  $eventMatchodd=$cache->get("Tied_Match_".$eventId);

                  if(!empty($eventMatchodd)) {
                      $eventMatchodd = json_decode($eventMatchodd);
                        //print_r($eventMatchodd->runners); die('qwqwqwq');
                     $market = $eventMatchodd;
if(isset($market->runners)){
                          $eventMatchoddRunner = json_decode($market->runners, 16);

                          foreach ($eventMatchoddRunner as $value) {
                              $profit_loss = $this->getProfitLossMatchOdds($userId, $market->marketId, $eventId, $value['secId'], 'tied_match');
                              $runnerTiedMatchArr [] = [
                                  'marketId' => $market->marketId,
                                  'runner' => $value['runner'],
                                  'secId' => $value['secId'],
                                  'profit_loss' => $profit_loss ? $profit_loss : 0

                              ];

                          }
                   }
                      
                  }
               $response = [
                   'status' => 1,
                   'code' => 200,
                   'data' => [
                       'bookmaker' => $runnerBookmakerArr,
                       'match_odd' => $runnerMatchoddArr,
                       'goals' => $runnerGoalsArr,
                       'set_market' => $runnerSetMarketArr,
                       'winner' => $runnerWinnerArr,
                       'completed_match' => $runnerCompletedMatchArr,
                       'tied_match' => $runnerTiedMatchArr,
                       'virtual_cricket' => $runnerVirtualArr
                   ],
                   'message'=>'Data Found !!!'
               ];
             

        }else{


         $runnerBookmakerArr = []; $runnerMatchoddArr = [];
        if ( isset($request->bookmaker)  && isset($request->match_odd) ) {
           
           if ( isset($request->bookmaker) ) {

                foreach ($request->bookmaker as  $market_id) {

                     $cache = Redis::connection();
                    $eventData=$cache->get("Market_".$market_id);
                    $market = json_decode($eventData);

                    $eventDatarunner = json_decode($market->runners, 16);
                     foreach ($eventDatarunner as $value) {
                             $profit_loss = $this->getProfitLossMatchOdds($userId, $market->mid, $market->eid, $value['secId'], 'bookmaker');
                              $runnerBookmakerArr [] = [
                                  'marketId' => $market_id,
                                  'runner' => $value['runner'],
                                  'secId' => $value['secId'],
                                  'profit_loss' => $profit_loss ? $profit_loss : 0

                              ];

                          }

                   
                }

              }
           if ( isset($request->virtual_cricket) ) {

                foreach ($request->virtual_cricket as  $market_id) {

                     $cache = Redis::connection();
                    $eventData=$cache->get("Market_".$market_id);
                    $market = json_decode($eventData);

                    $eventDatarunner = json_decode($market->runners, 16);
                     foreach ($eventDatarunner as $value) {
                             $profit_loss = $this->getProfitLossMatchOdds($userId, $market->mid, $market->eid, $value['secId'], 'virtual_cricket');
                              $runnerBookmakerArr [] = [
                                  'marketId' => $market_id,
                                  'runner' => $value['runner'],
                                  'secId' => $value['secId'],
                                  'profit_loss' => $profit_loss ? $profit_loss : 0

                              ];

                          }

                   
                }

              }
           if ( isset($request->match_odd) ) {
                 foreach ($request->match_odd as  $market_id) {

                     $cache = Redis::connection();
                    $eventData=$cache->get("Market_".$market_id);
                    $market = json_decode($eventData);

                    $eventDatarunner = json_decode($market->runners, 16);
                     foreach ($eventDatarunner as $value) {
                             $profit_loss = $this->getProfitLossMatchOdds($userId, $market->mid, $market->eid, $value['secId'], 'match_odd');
                              $runnerBookmakerArr [] = [
                                  'marketId' => $market_id,
                                  'runner' => $value['runner'],
                                  'secId' => $value['secId'],
                                  'profit_loss' => $profit_loss ? $profit_loss : 0

                              ];

                          }

                   
                }
              }
                 
           $response = ['status'=>1,'code'=>200,'data'=>$runnerBookmakerArr,'message'=>'Data Found !!!'];

        }

      }
        
       return $response;   

       }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }


   //Event: get Profit Loss Match Odds
    public function getProfitLossMatchOdds($userId,$marketId, $eventId, $selId, $sessionType)
    {

        $total = 0;
        // IF RUNNER WIN
        if (null != $userId && $marketId != null && $eventId != null && $selId != null) {
            $where = [ ['uid', $userId] , ['eid', $eventId] ,['mid', $marketId] ,['secId', $selId] ];

            if($sessionType == 'bookmaker' || $sessionType == 'virtual_cricket'){

              $userBetExpose = DB::table('tbl_book_bookmaker_view')->select('book')
                      ->where($where)
                       ->first();
              if(!empty($userBetExpose)){
                  $total = round($userBetExpose->book);
              }
            }else{

              $userBetExpose = DB::table('tbl_book_matchodd_view')->select('book')
                      ->where($where)
                       ->first();
              if(!empty($userBetExpose)){
                  $total = round($userBetExpose->book);
              }
            }

        }
        return $total;
    }


}