<?php

namespace App\Http\Controllers\Poker;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{

    //action Auth
    public function actionAppUserAuth(Request $request)
    {

        try{

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
//print_r($requestData); exit;
            $response = [ 'status' => 0 , 'message' => 'UnAuthorized Access !!!' , 'data' => ['suspend' => true] ];
            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['key_name','LIVE_GAME_STATUS'],['status',1]])->first();

            if( $setting != null && trim($setting->value) != 1 ){
                $response['errorDescription'] = "This sport is block by parent!";
                return response()->json($response);
            }

            if (isset($request['token'])) {

                $access_token = $request['token'];
                if($access_token=='3aG8xehr9ish22f7dCPsnV2u1u5STJxpdFp01s9LMcpj4BLA0G'){


                    $event = [];
                    $event['min_stack'] = 500;
                    $event['max_stack'] = 10000;
                    $event['max_profit_all_limit'] = 100000;
                    $eventData = null;

                    $data = [
                        "userId" => 1,
                        "username" =>  'admin [ admin ] ',
                        "balance" => 0,
                        "expose" => 0,
                        "eventData" => $eventData
                    ];

                    $response = ['status' => 1, 'message' => 'Success !!', 'data' => $data];

                }else {

                    $token_parts = explode('.', $access_token);
                    $token_header = $token_parts[1];
                    $token_header_json = base64_decode($token_header);
                    $token_header_array = json_decode($token_header_json, true);
                    $user_token = $token_header_array['jti'];

                    $authCheck = DB::table('oauth_access_tokens')->select('user_id')->where('id', $user_token)->first();

                    if ($authCheck != null) {
                        $uid = $authCheck->user_id;
                        $exposeBalance = 0;
                        $user = DB::table('tbl_user as u')
                            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                            ->select(['name', 'username', 'balance', 'pl_balance', 'expose', 'role'])
                            ->where([['is_login', 1], ['u.id', $uid], ['status', 1]])->first();

                        if ($user != null) {


                            /* $event = DB::table('tbl_user as u')
                                 ->select(['min_stack','max_stack','max_profit_limit'])
                                 ->where([['event_id',56767 ],['status',1]])->first();*/

                            $event = [];
                            $event['min_stack'] = 500;
                            $event['max_stack'] = 10000;
                            $event['max_profit_all_limit'] = 100000;
                            $eventData = null;
                            if ($event != null) {
                                $eventData = $event;
                            }


                            if ($user->expose != 0 || $user->expose != null) {
                                $exposeBalance = $user->expose;
                                $balance = round($user->balance + $user->pl_balance - $user->expose);
                            } else {
                                $exposeBalance = 0;
                                $balance = round($user->balance + $user->pl_balance);
                            }

                            $data = [
                                "userId" => $uid,
                                "username" => $user->name . ' [ ' . $user->name . ' ] ',
                                "balance" => $balance,
                                "expose" => $exposeBalance,
                                "eventData" => $eventData
                            ];

                            $response = ['status' => 1, 'message' => 'Success !!', 'data' => $data];
                        }

                    }
                }

            }

            return response()->json($response);

        }catch (\Exception $e) {
            $response = [
                "Error"=> 4,
                "result" => null,
                "message"=> "Something Wrong! Server Error 500 !!",
            ];

            return response()->json($response);

        }catch (\Exception $e) {

            $response = [
                "Error"=> 4,
                "result" => null,
                "message"=> "Something Wrong! Database Error 500 !!",
            ];
            return response()->json($response);

        }catch (\Exception $e) {
            $response = [
                "Error"=> 4,
                "result" => null,
                "message"=> "Something Wrong! Server Error 500 !!",
            ];
            return response()->json($response);
        }


    }
    /**
     * Action Login
     */
    public function actionLogin(Request $request)
    {
        try {

            //print_r($request->input());
            $response = [ "errorCode" => 1, "errorDescription" => "Something Wrong!" ];
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            // $this->saveResponse($requestData,'auth');
            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['key_name','LIVE_GAME_STATUS'],['status',1]])->first();

            if( $setting != null && trim($setting->value) != 1 ){
                $response['errorDescription'] = "This sport is block by parent!";
                return response()->json($response);
            }

            $access_token ='';
            $operatorId='';
            if(isset($request->token) && isset($request->operatorId)){
                $access_token=$request->token;
                $operatorId=$request->operatorId;
            }else{
                if( isset($requestData) && isset($requestData['token']) && isset($requestData['operatorId']) ){
                    $access_token = $requestData['token'];
                    $operatorId=$request->operatorId;
                }
            }



            if( isset($access_token) && !empty($access_token)){


                if($access_token=='3aG8xehr9ish22f7dCPsnV2u1u5STJxpdFp01s9LMcpj4BLA0G'){
                    $responseData = [
                        "operatorId" => $operatorId,
                        "userId" => 1,
                        "username" => 'admin [ admin ] ',
                        "playerTokenAtLaunch" => $access_token,
                        "token" => $access_token,
                        "balance" => 0,
                        "currency" => "INR",
                        "language" => "en",
                        "timestamp" => time(),
                        "clientIP" => ["1"],
                        "VIP" => "3",
                        "errorCode" => 0,
                        "errorDescription" => "ok"
                    ];


                    return response()->json($responseData);
                }
              //  $access_token = $requestData['token'];
                $token_parts = explode('.', $access_token);
                $token_header = $token_parts[1];
                $token_header_json = base64_decode($token_header);
                $token_header_array = json_decode($token_header_json, true);
                $user_token = $token_header_array['jti'];



                $authCheck = DB::table('oauth_access_tokens')->select('user_id')->where('id',$user_token)->first();

                if( $authCheck != null ){
                    $uid = $authCheck->user_id;

                    $userStatusCheck = DB::table('tbl_user_block_status')->select(['type'])
                        ->where([['uid',$uid],['type',1]])->first();
                    if( $userStatusCheck != null ){
                        $message = 'You are blocked by parent! Plz contact administrator!!';
                        $response = [ "status" => 3, "message" => $message ];
                        return response()->json($response);exit;
                    }

                    $user = DB::table('tbl_user as u')
                        ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                        ->select(['name','username','balance','pl_balance','expose','role'])
                        ->where([['is_login',1 ],['u.id', $uid ]])->first();

                    if( $user != null ){
                        $balance = 0;
                        if( $user->role == 4 ){
                            $balance = round(($user->balance)+($user->pl_balance)-($user->expose));
                        }

                        $responseData = [
                            "operatorId" => $operatorId,
                            "userId" => $uid,
                            "username" => $user->username,
                            "playerTokenAtLaunch" => $access_token,
                            "token" => $access_token,
                            "balance" => $balance,
                            "currency" => "INR",
                            "language" => "en",
                            "timestamp" => time(),
                            "clientIP" => ["1"],
                            "VIP" => "3",
                            "errorCode" => 0,
                            "errorDescription" => "ok"
                        ];


                        return response()->json($responseData);

                    }else{
                        $response['errorDescription'] = "User Not Found!";
                        return response()->json($response);
                    }

                }else{
                    return response()->json($response);
                }

            }else{
                return response()->json($response);
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }  

    }

    /**
     * saveResponse
     */
    public function saveResponse($data,$type)
    {
        $filePath = '/var/www/html/pokerlog/'.$type.'.txt';

        $fp = fopen($filePath, 'a');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }

}
