<?php

namespace App\Http\Controllers\Poker;
use App\Http\Controllers\Controller;
use App\Model\Teenpatti;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class ExposureController extends Controller
{

    /**
     * Action Bet Place
     */
    public function actionBetPlace()
    {

       try {
            $response = [ "status" => 1, "message" => "Something wrong! Bet Not Saved!"];
            $requestData = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);
            // $this->saveResponse($requestData,'auth');

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['key_name','LIVE_GAME_STATUS'],['status',1]])->first();

            if( $setting != null && trim($setting->value) != 1 ){
                $response['errorDescription'] = "This sport is block by parent!";
                return json_encode($response);
            }

            if( isset($requestData) && isset($requestData['token']) && isset($requestData['betInfo']) ){

                $betInfo = $requestData['betInfo'];
                $access_token = $requestData['token'];
                $token_parts = explode('.', $access_token);
                $token_header = $token_parts[1];
                $token_header_json = base64_decode($token_header);

                $token_header_array = json_decode($token_header_json, true);
                $user_token = $token_header_array['jti'];
             //  $user_token ='59347b14375e5b0c7584790c6ca8d0bc4b0e773ca0683e1abc5c1e3aaaee137511dc4af7b1a3467b';// $token_header_array['jti'];

                $calculateExpose = isset( $requestData['calculateExposure'] ) ? $requestData['calculateExposure'] : 0;

                if( $calculateExpose < 0 ){
                    $calculateExpose = (-1)*round( $calculateExpose );
                }

                $authCheck = DB::table('oauth_access_tokens')->select(['user_id'])->where([['id',$user_token]])->first();

                if( $authCheck != null ) {


                    $uid = $authCheck->user_id; $pId = 1;
                    $gameId = $betInfo['gameId'];

                    if( $this->getEventBetAllowAndBlockStatusAdmin($gameId) != 0 ){
                        $message = 'Bet not allowed for this event! Plz contact administrator!!';
                        $response = [ "status" => 3, "message" => $message ];
                        return json_encode($response);exit;
                    }

                    $userStatusCheck = DB::table('tbl_user_block_status')->select(['type'])->where([['uid',$uid]])->first();
                    if( $userStatusCheck != null ){
                        $message = 'You are blocked by parent! Plz contact administrator!!';
                        if( $userStatusCheck->type != 1 ){
                            $message = 'Your bet is locked by parent! Plz contact administrator!!';
                        }
                        $response = [ "status" => 3, "message" => $message ];
                        return json_encode($response);exit;
                    }

                    $user = DB::table('tbl_user as u')
                        ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                        ->select(['u.systemId','parentId','name','username','balance','pl_balance','expose','role','pName'])
                        ->where([['is_login',1],['role',4],['u.id',$uid]])->first();

                    if( $user != null ){

                        // check profit loss data
                        $profitLossClient = DB::table('tbl_user_profit_loss')
                            ->where([['clientId', $uid],['userId', 1]])->first();
                        if( $profitLossClient == null ){
                            $response = [
                                "status" => 3,
                                "message" => "Bet can not place! Plz contact admin!!",
                            ];
                            return json_encode($response);
                        }

                        $parentId = $user->parentId;
                        $systemId = $user->systemId;
                        $mainBalance = round($user->balance);
                        $plBalance = round($user->pl_balance);
                        $exposeBalance = round($user->expose);
                        $balance = round($mainBalance + $plBalance - $exposeBalance);

                        $gameData = DB::table('tbl_live_game')
                            ->select(['sportId','eventId','name','mType','is_block','min_stake','max_stake','max_profit_limit','suspend'])
                            ->where([['status',1],['is_block',0],['eventId',$gameId]])->first();

                        if( $gameData != null ){

                            if ($gameData != null && $gameData->suspend != 0) {
                                $response = [
                                    "status" => 3,
                                    "message" => "Suspended ! Bet Not Allowed!",
                                ];

                                return json_encode($response);

                            } elseif ($gameData != null && ((int)$gameData->min_stake > (int)$betInfo['reqStake'])) {

                                $response = [
                                    "status" => 3,
                                    "message" => "Min Stack is " . $gameData->min_stake . "! Bet Not Allowed!",
                                ];

                                return json_encode($response);

                            } elseif ($gameData != null && ((int)$gameData->max_stake < (int)$betInfo['reqStake'])) {

                                $response = [
                                    "status" => 3,
                                    "message" => "Max Stack is " . $gameData->max_stake . "! Bet Not Allowed!",
                                ];

                                return json_encode($response);
                            }

                          //  echo "2";  print_r($gameData); exit;
                            $checkEventStatus = DB::table('tbl_user_event_status')->select(['uid'])
                                ->where([['uid',$uid],['eid',$gameId]])->first();

                            if( $checkEventStatus != null ){
                                $message = 'This event is block by parent!';
                                $response = [ "status" => 3, "message" => $message ];
                                return json_encode($response);exit;
                            }

                            $checkSportStatus = DB::table('tbl_user_sport_status')->select(['uid'])
                                ->where([['uid',$uid],['sid',99]])->first();

                            if( $checkSportStatus != null ){
                                $message = 'This sport is block by parent!';
                                $response = [ "status" => 3, "message" => $message ];
                                return json_encode($response);exit;
                            }

                            $redis = Redis::connection();
                            $sportDataJson = $redis->get("Sport_" . 9999);
                            $sportData = json_decode($sportDataJson);
                            if( !empty( $sportData ) && $sportData->is_block != 0){
                                $message = 'This sport is block by parent!';
                                $response = [ "status" => 3, "message" => $message ];
                                return json_encode($response);exit;
                            }

                            $userAvailableBalance = $this->checkAvailableBalance($uid,$betInfo['marketId'],$calculateExpose);
                           // echo "2";  print_r($userAvailableBalance); exit;
                            $exposeBalance = $userAvailableBalance['expose'];

                            if (($userAvailableBalance['expose'] > $userAvailableBalance['balance'])) {
                                $response = [
                                    "status" => 3,
                                    "message" => "Insufficient fund !! Bet Not Allowed!",
                                ];
                                return json_encode($response);
                            }


                            // Check Bet Time
                           /* $checkLastBet = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->select('_id','created_on')
                                ->where([['uid', $uid],['mid', $betInfo['marketId']], ['round',$betInfo['roundId']]])->first();
                            print_r($checkLastBet); exit;
                            if( $checkLastBet != null ){

                                print_r($userAvailableBalance); exit;
                                $updated_on = date('Y-m-d H:i:s');
                                $betTimeDiff = (strtotime($updated_on)-strtotime($checkLastBet->created_on) );
                                if( $betTimeDiff < 2 ){
                                    $message = 'Bet can not place! Please try again after SomeTime !!';
                                    $response = [
                                        "status" => 3,
                                        "message" => $message,
                                    ];
                                    return json_encode($response);exit;
                                }
                            }*/



                            $winamt= isset( $betInfo['pnl'] ) ? $betInfo['pnl'] : 0;
                            $lossamt=  isset( $betInfo['reqStake'] ) ? $betInfo['reqStake'] : 0;

                           /* $model = new Teenpatti();
                            $model->systemId = $systemId;
                            $model->orderId = isset( $betInfo['orderId'] ) ? $betInfo['orderId'] : 123;
                            $model->uid = $uid;
                            $model->sid = 99;
                            $model->eid = $gameId;
                            $model->mid = $betInfo['marketId'];
                            $model->round = $betInfo['roundId'];
                            $model->secId = $betInfo['runnerId'];
                            $model->price = $betInfo['requestedOdds'];
                            $model->size = $betInfo['reqStake'];
                            $model->rate = $betInfo['requestedOdds'];
                            $model->win = round($winamt,2);
                            $model->loss = round($lossamt,2);
                            $model->client = $user->username;
                            $model->master = $user->pName;
                            $model->runner = $betInfo['runnerName'];
                            $model->market = $gameData->name.' - '.ucfirst( isset( $requestData['marketType'] ) ? $requestData['marketType'] : '' );
                            $model->event = $gameData->name;
                            $model->sport = 'Live Game';
                            $model->bType = $betInfo['isBack'] == 1 ? 'BACK' : 'LAY';
                            $model->mType = $gameData->mType;
                            $model->result = 'PENDING';
                            $model->description = $gameData->name . ' > ' . $betInfo['runnerName'];
                            $model->is_match = 1;
                            $model->ip_address = $this->getClientIp();
                            $model->status = 1;
                            $updated_on = date('Y-m-d H:i:s');
                            $model->created_on = $updated_on;
                            $model->updated_on = $updated_on;*/

                            $updated_on = date('Y-m-d H:i:s');

                            $betInsertData = [
                                'requestId' => 0,
                                'systemId' => (int)$systemId,
                                'orderId' => isset( $betInfo['orderId'] ) ? $betInfo['orderId'] : 123,
                                'uid' => (int)$uid,
                                'sid' => 99,
                                'eid' => (int)$gameId,
                                'mid' => $betInfo['marketId'],
                                'round' => $betInfo['roundId'],
                                'secId' => $betInfo['runnerId'],
                                'price' => $betInfo['requestedOdds'],
                                'size' => (int)$betInfo['reqStake'],
                                'bType' => $betInfo['isBack'] == 1 ? 'BACK' : 'LAY',
                                'rate' => $betInfo['requestedOdds'],
                                'win' => (double)round($winamt, 2),
                                'loss' => (double)round($lossamt, 2),
                                'mType' => $gameData->mType,
                               'runner' =>  $betInfo['runnerName'],
                                'market' => $gameData->name.' - '.ucfirst( isset( $requestData['marketType'] ) ? $requestData['marketType'] : '' ),
                                'event' => $gameData->name,
                                'sport' => 'Live Game',
                                'ip_address' => $this->getClientIp(),
                                'client' => $user->username,
                                'master' => $user->pName,
                                'result' => 'PENDING',
                                'description' => $gameData->name . ' > ' . $betInfo['runnerName'],
                                'status' => 1,
                                'created_on' => $updated_on,
                                'updated_on' => $updated_on,
                                'diff' => 0,
                                'is_match' => 1,
                                'ccr' => 0
                            ];
                           // print_r($betInsertData); exit;
                            $betId = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->insertGetId($betInsertData);
                            if( $betId != null ){

                            //if ($model->save()) {

                                //update expose  //updateUserExpose( $uid, $eid, $mid, $mType, $expose )
                                $this->updateUserExpose( $uid,$gameId,$betInfo['marketId'],$gameData->mType,$calculateExpose,$systemId );

                                // Update user balance

                                DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);

                                $balance = round($mainBalance + $plBalance - $exposeBalance);
                                $response = [
                                    "status" => 0,
                                    "message" => "Exposure insert Successfully...!",
                                    "wallet" => $balance,
                                ];
                                return json_encode($response);

                            }


                        }else{
                            $response['message'] = "Wrong data! Bet Not Allowed!";
                            return json_encode($response);
                        }


                    }



                }


            }else{
                return json_encode($response);
            }


        } catch ( \Throwable $t) {
            $response['errorDescription'] = "Something Wrong! Server Error 500 !!";
            return json_encode($response);
        }

    }

    // getEventBetAllowAndBlockStatusAdmin
    public function getEventBetAllowAndBlockStatusAdmin($eventId)
    {
        $redis = Redis::connection();
        $blockEventIdsJson = $redis->get('Block_Event_Market');
        $blockEventIds = json_decode($blockEventIdsJson);
        if (!empty($blockEventIds) && in_array($eventId, $blockEventIds)) {
            return 1;
        }

        $betAllowEventIdsJson = $redis->get('Bet_Allowed_Events');
        $betAllowEventIds = json_decode($betAllowEventIdsJson);
        if (!empty($betAllowEventIds) && in_array($eventId, $betAllowEventIds)) {
            return 1;
        }

        $sportDataJson = $redis->get("Sport_" . 99);
        $sportData = json_decode($sportDataJson);
        if( !empty( $sportData ) && $sportData->is_block != 0){
            return 1;
        }
        return 0;
    }

    //checkAvailableBalance
    public function checkAvailableBalance($uid,$marketId,$calculateExpose)
    {
        $user = DB::table('tbl_user_info')->select(['balance','expose','pl_balance'])
            ->where([['uid',$uid]])->first();

        if ($user != null) {
            $balance = round( (int)$user->balance );
            $expose = round( (int)$user->expose );
            $profitLoss = round( (int)$user->pl_balance );

            if( $profitLoss < 0 ){
                $profitLoss = (-1)*$profitLoss;
                $mywallet = ( $balance - $profitLoss );
            }else{
                $mywallet = ( $balance + $profitLoss );
            }

            //Old Expose
           /* $userExpose = DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['status',1],['mid','!=',$marketId]])->sum("expose");
            //echo "test";  print_r($userExpose); exit;
            if ( isset( $userExpose ) && $userExpose != null && $userExpose != '') {
                $balExpose = round( $userExpose );
            }else{
                $balExpose = 0;
            }*/

            $balExpose =$expose= 0;
            $userMarketExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select('expose')
                ->where([['uid',$uid],['status',1],['mid','!=',$marketId]])->get();
            if( $userMarketExpose->isNotEmpty() ){
                foreach ( $userMarketExpose as $data ){
                    $data = (object) $data;
                    if( (int)$data->expose > 0 ){
                        $expose =$expose+(int)$data->expose;

                    }
                }

                $balExpose = round($expose);
            }

            $newExpose = (int)$balExpose + (int)$calculateExpose;
            return $data = [ 'balance' => $mywallet, 'expose' => $newExpose , 'calExp' => $calculateExpose ];

        }
        return $data = [ 'balance' => 0, 'expose' => 0 , 'calExp' => 0 ];

    }

    /**
     * update User Expose
     */

    public function updateUserExpose( $uid, $eid, $mid, $mType, $expose,$systemId )
    {
        $userExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select(['id'])
            ->where([['uid',$uid],['mid',$mid],['status',1]])->first();
        if($expose < 0){
            $expose = (-1)*$expose;
        }
        if( $userExpose != null ){
            $updateData = [
                'expose' => $expose,
                'profit' => 0
            ];

            DB::connection('mongodb')->table('tbl_user_market_expose')
                ->where([['uid',$uid],['mid',$mid],['status',1]])->update($updateData);

        }else{

            $insertData = [
                'uid' => $uid,
                'systemId' => $systemId,
                'eid' => $eid,
                'mid' => $mid,
                'sid' => 99,
                'mType' => $mType,
                'expose' =>$expose,
                'profit' => 0,
                'secId' => 0,
                'status' => 1
            ];

            DB::connection('mongodb')->table('tbl_user_market_expose')->insert($insertData);
        }

    }
    /*public function updateUserExpose( $uid, $eid, $mid, $mType, $expose )
    {
        $userExpose = DB::table('tbl_user_market_expose')->select(['id'])
            ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->first();

        if( $userExpose != null ){
            $updateData = [
                'expose' => $expose,
                'profit' => 0
            ];

            DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->update($updateData);

        }else{
            $addData = [
                'uid' => $uid,
                'eid' => $eid,
                'mid' => $mid,
                'sid'=>99,
                'mType' => $mType,
                'expose' => $expose,
                'profit' => 0,
                'status' => 1,
            ];


            DB::table('tbl_user_market_expose')->insert($addData);
        }

    }*/




    /**
     * update User Profit
     */
    public function updateUserProfit( $uid, $eid, $mid, $mType, $profit )
    {
        $userExpose = DB::table('tbl_user_market_expose')->select(['id'])
            ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->first();

        if( $userExpose != null ){
            $updateData = [
                'profit' => $profit,
            ];

            DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->update($updateData);

        }else{
            $addData = [
                'uid' => $uid,
                'eid' => $eid,
                'mid' => $mid,
                'mType' => $mType,
                'expose' => 0,
                'profit' => $profit,
                'status' => 1,
            ];

            DB::table('tbl_user_market_expose')->insert($addData);
        }

    }


    /**
     * saveResponse
     */
    public function saveResponse($data,$type)
    {
        $filePath = '/var/www/html/pokerlog/'.$type.'.txt';

        $fp = fopen($filePath, 'a');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }

    /**
     * getClientIp
     */
    public function getClientIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
