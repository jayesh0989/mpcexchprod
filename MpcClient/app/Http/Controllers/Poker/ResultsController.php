<?php

namespace App\Http\Controllers\Poker;
use App\Http\Controllers\Controller;
use App\Model\TeenpattiResult;
use Illuminate\Support\Facades\DB;


class ResultsController extends Controller
{

    /**
     * Action Game Result
     */
    public function actionGameResult()
    {

        try{

        $data = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset($data) && isset( $data['result'] ) && isset($data['result'][0]) && isset( $data['runners'] )
            && isset( $data['roundId'] ) ){
           // echo "2";  print_r($data); exit;
            $userArr = $userIds = $betArr = [];
            $roundId = $data['roundId'];
            $result = $data['result'][0];

            $eventId = $result['gameId'];
            $marketId = $result['marketId'];
            $winner = $result['winnerId'];

            $betvoid = false;

            if( isset( $data['betvoid'] ) && $data['betvoid'] != false ){
                $betvoid = true;
            }

            $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            $tableName='tbl_transaction_client_'.$currentMonth;
          $transactionCheck = DB::connection('mysql3')->table($tableName)->select(['mid'])
                ->where([['mid',$marketId],['eid',$eventId]])->first();
               if($transactionCheck != null){
                       // do nothing
               }else{


           $resultCheck = DB::connection('mysql3')->table('tbl_teenpatti_result')->select(['user_data'])
                ->where([['mid',$marketId],['eid',$eventId]])->first();

            if( $resultCheck != null){
                // do nothing
           }else{

                foreach ( $data['result'] as $res ){

                    $userArr[] = [
                        'event_id' => $eventId,
                        'market_id' => $marketId,
                        'user_id' => $res['userId'],
                        'pl' => $res['downpl']
                    ];

                    $betArr[] = [
                        'user_id' => $res['userId'],
                        'bets' => $res['orders']
                    ];

                    $userIds[] = $this->updatedUserResultData($res['userId'],$marketId,$res['downpl']);
                }

                $description = 'Teen Patti ';

                if( isset($data['market']) ){

                    $marketData = $data['market'];

                    $description = '';
                    foreach ( $marketData['marketRunner'] as $marketRunner ){
                        $cards = ' , ';
                        if( isset( $marketRunner['cards'] ) && $marketRunner['cards'] != null ){
                            $cards = json_encode($marketRunner['cards']);
                            $cards = '( '.$cards.' ) ';
                        }

                        $description .= $marketRunner['name'].' : '.$marketRunner['status'].$cards;
                    }

                    if( isset( $marketData['indexCard'] ) && $marketData['indexCard'] != null ){
                        $indexCard = json_encode($marketData['indexCard']);
                        $description .= ' IndexCard( '.$indexCard.' )';
                    }

                }

                $mType = '';
                $gameData = DB::connection('mysql')->table('tbl_live_game')
                    ->select(['sportId', 'eventId', 'name', 'mType', 'is_block', 'min_stake', 'max_stake', 'max_profit_limit', 'suspend'])
                    ->where([['status', 1], ['eventId', $eventId]])->first();

                if ($gameData != null) {
                    $mType = $gameData->mType;
                    $marketName = $gameData->name;
                }

                $model = new TeenpattiResult();
               // $model->setConnection('mysql3'); // non-static method
                $model->eid = $eventId;
                $model->mid = $marketId;
                $model->round = $roundId;
                $model->game_over = 0;
                $model->winner = $winner;
                $model->description = $description;
                $model->result = json_encode($data['runners']);
                $model->user_data = json_encode($userArr);
                $model->bet_data = json_encode($betArr);
                $model->result_data = '';//json_encode($data)
                $model->status = 1;
                $model->betvoid = $betvoid == true ? 1 : 0;

                $array = array('api' => 'Live-game1-profit-loss', 'request' => $data);
                $this->activityLog(json_encode($array));

                if( $model->save() ){

                    $checkBets = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->select(['id'])
                        ->where([['eid', (int)$eventId], ['mid', $marketId], ['result', 'PENDING'], ['status', 1]])->first();

                // echo "test";  print_r($checkBets); exit;
                    if ($checkBets != null) {
                            $this->gameOverResult($eventId, $marketId, $betArr);
                            $this->transactionResult($roundId, $eventId, $marketId, $userArr, $mType, $marketName, $description);
                        /* add account stattement market */
                        //$insertArr = ['market_id' => $marketId, 'sid'=>99, 'mType'=>'livegame'];
                       // DB::connection('mysql3')->table('tbl_gameover_market')->insert($insertArr);
                    }

                    DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')
                        ->where([['mid', $marketId], ['result', 'PENDING'], ['status', 1]])
                        ->update(['result' => 'CANCELED']);

                    DB::connection('mongodb')->table('tbl_user_market_expose')
                        ->where([['mid', $marketId]])->update(['status' => 2]);


                    $userList = [];
                    $userBetList = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->where([['status', 1], ['mid', $marketId]])->distinct()->select('uid')->get();
                    if (!empty($userBetList)) {
                        $userList = $userBetList;
                    }

                    if (!empty($userList)) {
                        foreach ($userList as $user) {
                            // $user=(object)$user;
                            $userId = $user;
                            // print_r($user); exit;
                            $expose = 0;
                            $userExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->where([['uid', $userId], ['status', 1]])->sum('expose');
                            if ($userExpose != null) {
                                $expose = round($userExpose, 2);
                            }

                            $updated_on = date('Y-m-d H:i:s');
                            $updateData = [
                                'expose' => $expose,
                                'updated_on' => $updated_on
                            ];

                            DB::connection('mysql')->table('tbl_user_info')->where('uid', $userId)->update($updateData);
                        }
                    }

                    $response = [
                        "Error" => 0,
                        "result" => $userIds,
                        "message" => "user pl updated!",//"Result Saved Successfully...!",
                    ];
                }else{
                    $response = [
                        "Error"=> 1,
                        "result" => null,
                        "message"=> "Something Wrong!!!",
                    ];
                }

                return json_encode($response);

            }
        }


        }
     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }  

    }

    public function transactionResult($roundId, $eventId, $marketId, $userData, $mType, $marketName, $resultData)
    {

        if ($userData != null) {

            //$userData = json_decode($userData, 16);
            $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            foreach ($userData as $users) {
                $uid = $users['user_id'];
                $amount = $users['pl'];
                if ($amount != 0) {
                    $this->updateTransactionHistory($uid, $roundId, $eventId, $marketId, $amount, $mType, $marketName, $resultData,$currentMonth);
                }
            }

            $this->updateTransactionParentHistory($marketId);
        }

    }


    // Update Transaction Parent History
    public function updateTransactionParentHistory($marketId)
    {
        try {
            $conn = DB::connection('mysql');
            $conn3 = DB::connection('mysql3');
            $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            $tableName='tbl_transaction_client_'.$currentMonth;
            $clientTransData = $conn3->table($tableName)
                ->where([['status', 1],['mid',$marketId]])->get();

            if ( $clientTransData->isNotEmpty() ) {
                $newUserArr = [];
                foreach ($clientTransData as $transData){
                    $userId = $transData->userId;
                    $type = $transData->type;
                    $amount = $transData->amount;
                    $sid = $transData->sid;
                    $eid = $transData->eid;
                    $mid = $transData->mid;
                    $result = $transData->result;
                    $eType = $transData->eType;
                    $mType = $transData->mType;
                    $description = $transData->description;

                    if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }

                    $parentUserData = $conn->table('tbl_user_profit_loss as pl')
                        ->select(['pl.userId as userId','pl.parentId as parentId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
                        ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
                        ->where([['pl.clientId',$userId],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

                    $childId = $userId;

                    foreach ( $parentUserData as $user ){
                        $cUser = $conn->table('tbl_user')->select(['systemId','parentId','role'])
                            ->where([['id',$user->userId]])->first();
                        $systemId = $cUser->systemId;
                        $parentId = $cUser->parentId;
                        $role = $cUser->role;
                        $tAmount = ( $amount*$user->apl )/100;
                        $pTAmount = ( $amount*(100-$user->gpl) )/100;
                        $cTAmount = ( $amount*($user->gpl-$user->apl) )/100;

                        if( isset($newUserArr[$user->userId]) ){

                            if( $pType != 'CREDIT' ){
                                $tAmount = (-1)*$tAmount;
                                $pTAmount = (-1)*$pTAmount;
                                $cTAmount = (-1)*$cTAmount;
                            }

                            $tAmount = $newUserArr[$user->userId]['tAmount']+$tAmount;
                            $pTAmount = $newUserArr[$user->userId]['pTAmount']+$pTAmount;
                            $cTAmount = $newUserArr[$user->userId]['cTAmount']+$cTAmount;

                            $newUserArr[$user->userId]['tAmount'] = $tAmount;
                            $newUserArr[$user->userId]['pTAmount'] = $pTAmount;
                            $newUserArr[$user->userId]['cTAmount'] = $cTAmount;

                        }else{

                            if( $pType != 'CREDIT' ){
                                $tAmount = (-1)*$tAmount;
                                $pTAmount = (-1)*$pTAmount;
                                $cTAmount = (-1)*$cTAmount;
                            }

                            $newUserArr[$user->userId] = [
                                'role' => $role,
                                'systemId' => $systemId,
                                'clientId' => $userId,
                                'userId' => $user->userId,
                                'childId' => $childId,
                                'parentId' => $parentId,
                                'tAmount' => $tAmount,
                                'pTAmount' => $pTAmount,
                                'cTAmount' => $cTAmount,
                                'sid' => $sid,
                                'eid' => $eid,
                                'mid' => $mid,
                                'result' => $result,
                                'eType' => $eType,
                                'mType' => $mType,
                                'description' => $description,
                            ];
                        }
                        $childId = $user->userId;
                    }

                }

                if( $newUserArr != null ){
                    foreach ( $newUserArr as $userData ){
                        if( $userData['tAmount'] < 0 ){
                            $pType = 'DEBIT';
                            $amount = (-1)*round($userData['tAmount'],2);
                            $pAmount = (-1)*round($userData['pTAmount'],2);
                            $cAmount = (-1)*round($userData['cTAmount'],2);
                        }else{
                            $pType = 'CREDIT';
                            $amount = round($userData['tAmount'],2);
                            $pAmount = round($userData['pTAmount'],2);
                            $cAmount = round($userData['cTAmount'],2);
                        }

                        $this->userSummaryUpdate($userData['userId'],$amount,$pAmount,$pType, 0, 0);

                        $ubalance = 0;
                        $SummaryData1 = DB::connection('mysql')->table('tbl_settlement_summary')->select('ownPl', 'ownComm')
                            ->where('uid', $userData['userId'])->first();
                        if (!empty($SummaryData1)) {
                            $updated_on = date('Y-m-d H:i:s');
                            $ubalance = 0;
                            $ubalance = $SummaryData1->ownPl + $SummaryData1->ownComm;
                            DB::connection('mysql')->table('tbl_user_info')->where([['uid', $userData['userId']]])
                                ->update(['pl_balance' => $ubalance, 'updated_on' => $updated_on]);
                        }

                        $currentBalance = $ubalance;

                        if(empty($currentBalance) && $currentBalance==''){
                            $currentBalance=0;
                        }
                      //  $balance = $this->getCurrentBalance($userData['userId'], $amount, $pType,0,0,'PARENT');

                        $resultArr = [
                            'systemId' => $userData['systemId'],
                            'clientId' => $userData['clientId'],
                            'userId' => $userData['userId'],
                            'childId' => $userData['childId'],
                            'parentId' => $userData['parentId'],
                            'sid' => $userData['sid'],
                            'eid' => $userData['eid'],
                            'mid' => $userData['mid'],
                            'result' => $userData['result'],
                            'eType' => $userData['eType'],
                            'mType' => $userData['mType'],
                            'type' => $pType,
                            'amount' => $amount,
                            'p_amount' => $pAmount,
                            'c_amount' => $cAmount,
                            'balance' => $currentBalance,
                            'description' => $userData['description'],
                            'remark' => 'Transactional Entry',
                            'status' => 1,
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s')
                        ];

                        $updatedOn = date('Y-m-d H:i:s');
                        $year = date('Y', strtotime($updatedOn));
                        $month = date('m', strtotime($updatedOn));
                        $currentMonth=$month.$year;
                        if( $userData['role'] == 1 ){
                            //$tbl = 'tbl_transaction_admin';
                            $tbl='tbl_transaction_admin_'.$currentMonth;
                        }else{
                            //$tbl = 'tbl_transaction_parent';
                            $tbl='tbl_transaction_parent_'.$currentMonth;
                        }

                        if( $conn3->table($tbl)->insert($resultArr) ){
                            //return ['status' => 1];
                        }

                    }
                }
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Update Transaction History
    public function updateTransactionHistory($uid, $roundId, $eventId, $marketId, $amount, $mType, $marketName, $resultData,$currentMonth)
    {
        $updated_on = date('Y-m-d H:i:s');
        if ($amount > 0) {
            $type = 'CREDIT';
        } else {
            $amount = (-1) * $amount;
            $type = 'DEBIT';
        }

        $summaryData = [];

        $cUser = DB::connection('mysql')->table('tbl_user')->select(['parentId', 'systemId'])->where([['id', $uid]])->first();
        $cUserInfo = DB::connection('mysql')->table('tbl_user_info')->select(['balance', 'pl_balance'])->where([['uid', $uid]])->first();

        $systemId = $cUser->systemId;
        $parentId = $cUser->parentId;
        $description = $this->setDescription($marketName, $roundId);

        $summaryData['uid'] = $uid;
        $summaryData['ownPl'] = $amount;
        $summaryData['type'] = $type;

        $this->userSummaryUpdate($uid, $amount, 0, $type, 0, 0);

        $balance = $this->getCurrentBalance($uid, $amount, $type, $cUserInfo->balance, $cUserInfo->pl_balance, 'CLIENT');
        if(empty($balance) && $balance==''){
            $balance=0;
        }

        $resultArr = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => 99,
            'eid' => $eventId,
            'mid' => $marketId,
            'round' => $roundId,
            'result' => $resultData,
            'eType' => 0,
            'mType' => $mType,
            'type' => $type,
            'amount' => round($amount, 2),
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Transactional Entry',
            'status' => 1,
            'created_on' => $updated_on,
            'updated_on' => $updated_on
        ];
        $updatedOn = date('Y-m-d H:i:s');
        $year = date('Y', strtotime($updatedOn));
        $month = date('m', strtotime($updatedOn));
        $currentMonth=$month.$year;
        $tableName='tbl_transaction_client_'.$currentMonth;
        DB::connection('mysql3')->table($tableName)->insert($resultArr);

       /* if ($type == 'CREDIT') {
            $pType = 'DEBIT';
        } else {
            $pType = 'CREDIT';
        }

        $parentUserData = DB::connection('mysql')->table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId', 'pl.actual_profit_loss as apl', 'pl.profit_loss as gpl', 'ui.balance as balance', 'ui.pl_balance as pl_balance'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId', $uid], ['pl.actual_profit_loss', '!=', 0]])->orderBy('pl.userId', 'desc')->get();

        $childId = $uid;

        foreach ($parentUserData as $user) {

            $summaryData = [];

            $cUser = DB::connection('mysql')->table('tbl_user')->select(['parentId', 'role', 'systemId'])->where([['id', $user->userId]])->first();
            $systemId = $cUser->systemId;
            if ($user->userId == 1) {
                $cUser1 = DB::connection('mysql')->table('tbl_user')->select(['parentId', 'role', 'systemId'])->where([['id', $childId]])->first();
                $systemId = $cUser1->systemId;
            }

            $parentId = $cUser->parentId;

            $transactionAmount = ($amount * $user->apl) / 100;
            $pTransactionAmount = ($amount * (100 - $user->gpl)) / 100;
            $cTransactionAmount = ($amount * ($user->gpl - $user->apl)) / 100;


            $ubalance = 0;
            $this->userSummaryUpdate($user->userId, $transactionAmount, $pTransactionAmount, $pType, 0, 0);
            $SummaryData1 = DB::connection('mysql')->table('tbl_settlement_summary')->select('ownPl', 'ownComm')
                ->where('uid', $user->userId)->first();
            if (!empty($SummaryData1)) {
                $updated_on = date('Y-m-d H:i:s');
                $ubalance = 0;
                $ubalance = $SummaryData1->ownPl + $SummaryData1->ownComm;

                DB::connection('mysql')->table('tbl_user_info')->where([['uid', $user->userId]])
                    ->update(['pl_balance' => $ubalance, 'updated_on' => $updated_on]);
            }


            $currentBalance = $ubalance;

            if(empty($currentBalance) && $currentBalance==''){
                $currentBalance=0;
            }

            $resultArr = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $user->userId,
                'childId' => $childId,
                'parentId' => $parentId,
                'sid' => 99,
                'eid' => $eventId,
                'mid' => $marketId,
                'round' => $roundId,
                'result' => $resultData,
                'eType' => 0,
                'mType' => $mType,
                'type' => $pType,
                'amount' => round($transactionAmount, 2),
                'p_amount' => round($pTransactionAmount, 2),
                'c_amount' => round($cTransactionAmount, 2),
                'balance' => $currentBalance,
                'description' => $description,
                'remark' => 'Transactional Entry',
                'status' => 1,
                'created_on' => $updated_on,
                'updated_on' => $updated_on
            ];

            if ($cUser->role == 1) {
                DB::connection('mysql3')->table('tbl_transaction_admin')->insert($resultArr);
                $tableName='tbl_transaction_admin_market_'.$currentMonth;
                //  $this->userAccountUpdate($user->userId, $systemId, $roundId, $eventId, $marketId,99, $transactionAmount,$currentBalance, $pType,0,$resultData, $mType, $description,0,$tableName);

            } else {
                DB::connection('mysql3')->table('tbl_transaction_parent')->insert($resultArr);
                $tableName='tbl_transaction_parent_market_'.$currentMonth;
                //  $this->userAccountUpdate($user->userId, $systemId, $roundId, $eventId, $marketId,99, $transactionAmount,$currentBalance, $pType,0,$resultData, $mType, $description,0,$tableName);

            }

            //}
            $childId = $user->userId;

        }*/

    }



    // get Current Balance New
    public function getCurrentBalance($uid, $amount, $type, $balance, $plBalance, $role)
    {
        $balance=0;
        $conn = DB::connection('mysql');
        if ($type != 'CREDIT') {
            $amount = (-1) * $amount;
        }
        $updated_on = date('Y-m-d H:i:s');
        $updateArr = [
            'pl_balance' => $conn->raw('pl_balance + ' . $amount),
            'updated_on' => $updated_on
        ];

        if ($conn->table('tbl_user_info')
            ->where([['uid', $uid]])->update($updateArr)) {
            $cUserInfo = $conn->table('tbl_user_info')
                ->select(['balance', 'pl_balance', 'updated_on'])->where([['uid', $uid]])->first();
            $balance = $cUserInfo->balance;
            $plBalance = $cUserInfo->pl_balance;

            if ($role != 'CLIENT') {
                return round(($balance), 2);
            } else {
                return round(($balance + $plBalance), 2);
            }
        } else {
            $updated_on = date('Y-m-d H:i:s');
            $cUserInfo = $conn->table('tbl_user_info')
                ->select(['balance', 'pl_balance', 'updated_on'])->where([['uid', $uid]])->first();
            $betTimeDiff = (strtotime($updated_on) - strtotime($cUserInfo->updated_on));
            if ($betTimeDiff == 0) {
                usleep(1000);
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance', 'pl_balance', 'updated_on'])->where([['uid', $uid]])->first();
            }

            $balance = $cUserInfo->balance;
            $newPlBalance = $cUserInfo->pl_balance;

            $updateArr = [
                'pl_balance' => $newPlBalance,
                'updated_on' => $updated_on
            ];

            if ($conn->table('tbl_user_info')
                ->where([['uid', $uid]])->update($updateArr)) {
                if ($role != 'CLIENT') {
                    return round(($balance), 2);
                } else {
                    return round(($balance + $newPlBalance), 2);
                }
            }
        }
    }
    //user Summary Update New
    public function userSummaryUpdate($uid, $ownPl, $parentPl, $type, $ownComm, $parentComm)
    {
        $conn = DB::connection('mysql');
        $userSummary = $conn->table('tbl_settlement_summary')
            ->where([['status', 1], ['uid', $uid]])->first();
        if ($userSummary != null) {

            $updatedOn = date('Y-m-d H:i:s');
            if (isset($type) && $type != 'CREDIT') {
                $ownPl = (-1) * round($ownPl, 2);
                $parentPl = (-1) * round($parentPl, 2);
            }
            //$ownComm = round($ownComm,2);
            //$parentComm = (-1)*round($parentComm,2);

            $updateArr = [
                'ownPl' => $conn->raw('ownPl + ' . $ownPl),
                //'ownComm' => $conn->raw('ownComm + ' . $ownComm),
                'parentPl' => $conn->raw('parentPl + ' . $parentPl),
                //'parentComm' => $conn->raw('parentComm + ' . $parentComm),
                'updated_on' => $updatedOn
            ];

            if ($conn->table('tbl_settlement_summary')
                ->where([['status', 1], ['uid', $uid]])->update($updateArr)) {
                $updateArr1 = ['uid' => $uid, 'updated_on' => $updatedOn,
                    'ownPl' => $ownPl, 'parentPl' => $parentPl];

                //  $array = array('function' => 'settelement', 'Query' => $updateArr1);
                //  $this->activityLog(json_encode($array));
            } else {
                $userSummary = $conn->table('tbl_settlement_summary')
                    ->where([['status', 1], ['uid', $uid]])->first();
                $updatedOn = date('Y-m-d H:i:s');
                $betTimeDiff = (strtotime($updatedOn) - strtotime($userSummary->updated_on));
                if ($betTimeDiff == 0) {
                    usleep(1000);
                    $userSummary = $conn->table('tbl_settlement_summary')
                        ->where([['status', 1], ['uid', $uid]])->first();
                }
                $updateArr = [
                    'ownPl' => $userSummary->ownPl + $ownPl,
                    //'ownComm' => $userSummary->ownComm+$ownComm,
                    'parentPl' => $userSummary->parentPl + $parentPl,
                    //'parentComm' => $userSummary->parentComm+$parentComm,
                    'updated_on' => date('Y-m-d H:i:s')
                ];
                if ($conn->table('tbl_settlement_summary')
                    ->where([['status', 1], ['uid', $uid]])->update($updateArr)) {
                    $updateArr1 = ['uid' => $uid, 'updated_on' => $updatedOn,
                        'ownPl' => $ownPl, 'parentPl' => $parentPl];

                    //  $array = array('function' => 'settelement', 'Query' => $updateArr1);
                    //   $this->activityLog(json_encode($array));
                }
            }

        } else {
            $updatedOn = date('Y-m-d H:i:s');
            if (isset($type) && $type != 'CREDIT') {
                $ownPl = (-1) * round($ownPl, 2);
                $parentPl = (-1) * round($parentPl, 2);
            }
            $ownComm = 0;
            $parentComm = 0;
            $insertArr = ['uid' => $uid, 'updated_on' => $updatedOn,
                'ownPl' => $ownPl, 'ownComm' => $ownComm,
                'parentPl' => $parentPl, 'parentComm' => $parentComm];

            $conn->table('tbl_settlement_summary')->insert($insertArr);

        }
    }


    // Function to get the Description
    public function setDescription($marketName, $roundId)
    {
        $description = $marketName . ' > Round #' . $roundId;
        return $description;
    }
    //Game Over Result TeenPatti
    public function gameOverResult($eventId, $marketId, $betData)
    {

        if ($betData != null) {

           // $betData = json_decode($betData, 16);

            foreach ($betData as $bets) {

                $uid = $bets['user_id'];

                foreach ($bets['bets'] as $bet) {

                    $status = 'CANCELED';
                    $updateArr = ['result' => $status];
                    $orderId = trim($bet['orderId']);

                    if (isset($bet['status']) && $bet['status'] == 'LOST') {
                        $status = 'LOSS';
                        $loss = (-1) * ((double)$bet['downPl']);
                        $updateArr = ['result' => $status, 'loss' => round((double)$loss, 2)];
                    }

                    if (isset($bet['status']) && $bet['status'] == 'WON') {
                        $status = 'WIN';
                        $win = (double)$bet['downPl'];
                        $updateArr = ['result' => $status, 'win' => round((double)$win, 2)];
                    }

                    // Update User Event Expose
                    DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')
                        ->where([['mid', $marketId], ['orderId', $orderId], ['result', 'PENDING']])
                        ->update($updateArr);

                    //echo "tes4334t"; exit;

                }


            }

        }

    }


    //updated User Result Data
    public function updatedUserResultData($uid,$marketId,$pl)
    {
      try{

        $wallet = $newExpose = $newPl = 0;

        $user = DB::table('tbl_user_info')->select(['balance','expose','pl_balance'])
            ->where([['uid',$uid]])->first();

        if( $user != null ){

            if( $marketId != false ){
                $newPl = round($user->pl_balance)+round($pl);

                $expose = DB::connection('mongodb')->table('tbl_user_market_expose')->select(['expose'])
                    ->where([['uid',(int)$uid],['mid',$marketId]])->first();
                //echo "test111";  print_r($expose); exit;
                if( $expose != null ){
                    $expose = (object)$expose;
                    $newExpose = round($user->expose)-round($expose->expose);
                }
                $wallet = round($user->balance)+round($newPl)-round($newExpose);
            }else{
                $wallet = round($user->balance)+round($user->pl_balance)-round($user->expose);
            }

        }

        return ['wallet' => $wallet , 'exposure' => 0 , 'userId' => $uid ];

     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }  


    }

}
