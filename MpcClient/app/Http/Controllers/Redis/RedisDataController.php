<?php

namespace App\Http\Controllers\Redis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use DB;

class RedisDataController extends Controller
{
    public function __construct(){
        $this->redis = Redis::connection();
    }

    //for common markets
    public function storeGameSportdata(){
        try{
            $sportList = DB::table('tbl_sport')->select('*')->where([['status',1],['is_block',0]])->get();
            $sportList = json_decode($sportList);
            if(!empty( $sportList)) {
                $sportData = json_encode($sportList);
                $this->redis->set('SportList', $sportData);
                foreach ($sportList as $sport_List) {
                    $sportId = $sport_List->sportId;
                    $sportDT = json_encode($sport_List);
                    $this->redis->set('Sport_' . $sportId, $sportDT);
                }
            }
            $startTime = time();
            $endTime = time() + 60;
            while ($endTime > time()) {
                usleep(500000);
                $gameInfo = [];
                #to fetch game sport data
                //Production api
                $url = "http://api3.dreamexch9.com/api/get-game-sportdata";
                //Development api
                // $url = "https://api-devsession.dbm9.com/api/get-game-sportdata";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $result = curl_exec($ch);
                $data = json_decode($result);

                #to store sports list data
                $sportListData = isset($data->sportList) ? $data->sportList : [];

                #to store event data
                $gameList = isset($data->gameList) ? $data->gameList : [];
                $arr_event_id_list = [];
                $arr_market_list   = [];
                foreach ($gameList as $key1 => $game) {
                    #for game events list
                    $sportId = $game->sportId;
                    $sport_name = $game->sport_name;
                    $sportwiseeventList = [];

                    foreach ($game->eventList as $key2 => $value) {
                        $event_data = isset($value->eventData) ? $value->eventData : null;
                        $eid = $event_data->eventId;
                        $arr_event_id_list[] = $eid;
                        $sportwiseeventList[] = $event_data;

                        #for event related data
                        $this->redis->set("Event_" . $eid, json_encode($event_data));
                        $eventSetting = isset($value->eventSetting) ? $value->eventSetting : null;
                        $this->redis->set("Event_limit_".$eid,json_encode($eventSetting));
                        if($sportId ==7) {
                            if (!empty($value->marketList->binary)) {
                            $this->redis->set("Binary_" . $eid, json_encode($value->marketList->binary));
                                $data = $value->marketList->binary;
                                foreach ($data as $key => $marketArr) {
                                    $mid = isset($marketArr->marketId) ? $marketArr->marketId : null;
                                    $this->redis->set("Binary_market_" . $marketArr->marketId, json_encode($marketArr));
                                    $this->redis->set("Market_" . $mid, json_encode($marketArr));
                                    $status = isset($marketArr->status) ? $marketArr->status : null;
                                    if( $status == 1){ $arr_market_list[] = $mid; }
                                }
                            }else{
                                $this->redis->set("Binary_" . $eid,0);
                            }
                        }

                        #to store market related data
                        if($sportId ==1 || $sportId == 2 || $sportId==4){
                            if(!empty($value->marketList->match_odd)){
                                $this->redis->set("Match_Odd_" . $eid, json_encode($value->marketList->match_odd));
                                $mid = isset($value->marketList->match_odd->marketId) ? $value->marketList->match_odd->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode(($value->marketList->match_odd)));
                                $status = isset($value->marketList->match_odd->status) ? $value->marketList->match_odd->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }else{
                                $this->redis->set("Match_Odd_" . $eid, 0);
                            }

                            if(!empty($value->marketList->score)){
                                $this->redis->set("EventScore_" . $eid, json_encode($value->marketList->score));
                            }

                            if(!empty($value->marketList->completed_match)){
                                $this->redis->set("Completed_Match_" . $eid, json_encode($value->marketList->completed_match));
                                $mid = isset($value->marketList->completed_match->marketId) ? $value->marketList->completed_match->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($value->marketList->completed_match));
                                $status = isset($value->marketList->completed_match->status) ? $value->marketList->completed_match->status : null;
                                if( $status == 1){ $arr_market_list[] = $mid; }
                            }else{
                                $this->redis->set("Completed_Match_" . $eid,0);
                            }
                            if(!empty($value->marketList->tied_match)){
                                $this->redis->set("Tied_Match_" . $eid, json_encode($value->marketList->tied_match));
                                $mid = isset($value->marketList->tied_match->marketId) ? $value->marketList->tied_match->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($value->marketList->tied_match));
                                $status = isset($value->marketList->tied_match->status) ? $value->marketList->tied_match->status : null;
                                if( $status == 1){ $arr_market_list[] = $mid; }
                            }else{
                                $this->redis->set("Tied_Match_" . $eid,0);
                            }
                        }

                        if (!empty($value->marketList->bookmaker)) {
                        $this->redis->set("Bookmaker_" . $eid, json_encode($value->marketList->bookmaker));
                            $data = $value->marketList->bookmaker;
                            foreach ($data as $key => $bookmaker) {
                                $mid = isset($bookmaker->marketId) ? $bookmaker->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($bookmaker));
                                $status = isset($bookmaker->status) ? $bookmaker->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        }else{
                              $this->redis->set("Bookmaker_" . $eid, 0);
                        }

                        // fancy3 for other market
                        if (!empty($value->marketList->other_market)) {
                        $fancy3 = $this->redis->set("Fancy_3_" . $eid, json_encode($value->marketList->other_market));
                            $data = $value->marketList->other_market;
                            foreach ($data as $key => $other_market) {
                                $mid = isset($other_market->marketId) ? $other_market->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($other_market));
                                $status = isset($other_market->status) ? $other_market->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        }else{
                              $this->redis->set("Fancy_3_" . $eid, 0);
                        }

                        // fancy2 for session
                        if (!empty($value->marketList->session)) {
                        $fancy2 = $this->redis->set("Fancy_2_" . $eid, json_encode($value->marketList->session));
                            $data = $value->marketList->session;
                            foreach ($data as $key => $session) {
                                $mid = isset($session->marketId) ? $session->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($session));
                                $status = isset($session->status) ? $session->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        }else{
                              $this->redis->set("Fancy_2_" . $eid, 0);
                        }

                        if($sportId ==11) {
                            if (!empty($value->marketList->casino)) {
                            $casino = $this->redis->set("Casino_" . $eid, json_encode($value->marketList->casino));
                                $data = $value->marketList->casino;
                                foreach ($data as $key => $casino) {
                                    $mid = isset($casino->marketId) ? $casino->marketId : null;
                                    $this->redis->set("Market_" . $mid, json_encode($casino));
                                    $status = isset($casino->status) ? $casino->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                                }
                            }else{
                                $this->redis->set("Casino_" . $eid,0);
                            }
                        }

                        if($sportId ==1) {
                            if (!empty($value->marketList->goals)) {
                            $goals = $this->redis->set("Goals_" . $eid, json_encode($value->marketList->goals));
                                $data = $value->marketList->goals;
                                foreach ($data as $key => $goals) {
                                    $mid = isset($goals->marketId) ? $goals->marketId : null;
                                    $this->redis->set("Market_" . $mid, json_encode($goals));
                                    $status = isset($goals->status) ? $goals->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                                }
                            }else{
                                $this->redis->set("Goals_" . $eid,0);
                            }
                        }

                        if($sportId ==2) {
                            if (!empty($value->marketList->set_market)) {
                            $set_market = $this->redis->set("Set_Market_" . $eid, json_encode($value->marketList->set_market));
                                $data = $value->marketList->set_market;
                                foreach ($data as $key => $set_market) {
                                    $mid = isset($set_market->marketId) ? $set_market->marketId : null;
                                    $this->redis->set("Market_" . $mid, json_encode($set_market));
                                    $status = isset($set_market->status) ? $set_market->status : null;
                                    if( $status == 1){ $arr_market_list[] = $mid; }
                                }
                            }else{
                                $this->redis->set("Set_Market_" . $eid,0);
                            }
                        }
                    }//end event list
                    $eventDataArray = array('sportId' => $sportId, 'sport_name' => $sport_name, 'eventData' => $sportwiseeventList);
                    $this->redis->set("Event_List_" . $sportId, json_encode($eventDataArray));
                }//end game list

                $arr_event_id_list_old = json_decode($this->redis->get("eventIdList"));
                if(!empty($arr_event_id_list_old)) {
                    foreach ($arr_event_id_list_old as $key => $value) {
                        // dd($value);
                        if (!in_array($value, $arr_event_id_list)) {
                            $this->redis->set("Event_" . $value, 0);
                        }
                    }
                }
                $this->redis->set("eventIdList", json_encode($arr_event_id_list));
                $this->redis->set("commonMarketList", json_encode(array_values(array_filter($arr_market_list))));
               // return response()->json(["status" => 1, "code" => 200, "message" => "Data Saved.",], 200);
            }
        }
        catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }
    
    public function getGameSportdata(Request $request){
        try{

            if( isset($request->id) && isset($request->username) ) {
              echo "token-->".  $token = md5( '1-' . $request->id . '-' . $request->username . '-BETEX567'); //$request->bearerToken();
            }

            $allKeys =  $this->redis->keys('*');
            
            $sportList = DB::table('tbl_sport')->select('*')->where([['status',1],['is_block',0]])->get();
            $sportList = json_decode($sportList);
            if(!empty( $sportList)) {
                $sportData = json_encode($sportList);
                $this->redis->set('SportList', $sportData);
                foreach ($sportList as $sport_List) {
                    $sportId = $sport_List->sportId;
                    $sportDT = json_encode($sport_List);
                    $this->redis->set('Sport_' . $sportId, $sportDT);
                }
            }
            //Production api
            $url = "http://api3.dreamexch9.com/api/get-game-sportdata";
            //Development api
            // $url = "https://api-devsession.dbm9.com/api/get-game-sportdata";
            $ch = curl_init();  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_URL, $url); 
            $result = curl_exec($ch);
            $data = json_decode($result);
            return response()->json([ "status" => 1 , "code" => 200 , "message" => "Data Found",'data' => $data],200);
        }
        catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function getMarketList(){
        try {
            $items        = null;
            $marketIdsArr = $eid = [];
            $eventListArr = $this->redis->get("eventIdList");
            $eventListArr = json_decode($eventListArr);
            $commonMarketList = $this->redis->get("commonMarketList");
            $marketIdsArr = json_decode($commonMarketList);
            //Production api
            $url = "http://52.48.184.39:8180/dream/get_odds";
            //Development api
            // $url = "http://52.48.184.39:6162/dream/dev/get_odds";
            if(!empty($marketIdsArr)) {
                $startTime = time();
                $endTime = time() + 60;
                while ($endTime > time()) {
                    usleep(300000);
                    $eventListArr = $this->redis->get("eventIdList");
                    $eventListArr = json_decode($eventListArr);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($marketIdsArr));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $server_output = curl_exec($ch);
                    $result = json_decode($server_output);
                    curl_close($ch);

                    if(!empty($result->data->items)) {

                        foreach ($result->data->items as $key => $obj) {
                            $marketId = isset($obj->market_id) ? $obj->market_id : null;
                            $this->redis->set($marketId, json_encode($obj));
                        }
                    }
                }
            }
            return response()->json([ "status" => 1 , "code" => 200 , "message" => "Data Saved!",'markets'=>$marketIdsArr, 'eventID'=>$eventListArr],200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }
    //end for common markets

    //for sport cricket
    public function storeGameCricketdata(){
        try{
            $startTime = time();
            $endTime = time() + 60;
            while ($endTime > time()) {
            usleep(500000);
            $gameInfo = [];
            #to fetch game sport data
            //Production api
            $url = "http://api3.dreamexch9.com/api/get-game-cricketdata";
            //Development api
            //$url = "https://api-devsession.dbm9.com/api/get-game-cricketdata";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);
            $data = json_decode($result);
            #to store event data
            $gameList = isset($data->gameList) ? $data->gameList : [];
            $arr_event_id_list = [];
            $arr_market_list   = [];
            if(!empty($gameList)) {
                foreach ($gameList as $key1 => $game) {
                    #for game events list
                    $sportId = $game->sportId;
                    $sport_name = $game->sport_name;
                    $sportwiseeventList = [];

                    foreach ($game->eventList as $key2 => $value) {
                        $event_data = isset($value->eventData) ? $value->eventData : null;
                        $eid = $event_data->eventId;
                        $arr_event_id_list[] = $eid;
                        $sportwiseeventList[] = $event_data;

                        #for event related data
                        $this->redis->set("Event_" . $eid, json_encode($event_data));
                        $eventSetting = isset($value->eventSetting) ? $value->eventSetting : null;
                        $this->redis->set("Event_limit_" . $eid, json_encode($eventSetting));

                        #to store market related data
                        if (!empty($value->marketList->match_odd)) {
                            $this->redis->set("Match_Odd_" . $eid, json_encode($value->marketList->match_odd));
                            $mid = isset($value->marketList->match_odd->marketId) ? $value->marketList->match_odd->marketId : null;
                            $this->redis->set("Market_" . $mid, json_encode(($value->marketList->match_odd)));
                            $status = isset($value->marketList->match_odd->status) ? $value->marketList->match_odd->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; } 
                        } else {
                            $this->redis->set("Match_Odd_" . $eid, 0);
                        }

                        if(!empty($value->marketList->score)){
                            $this->redis->set("EventScore_" . $eid, json_encode($value->marketList->score));
                        }
                        
                        if (!empty($value->marketList->completed_match)) {
                            $this->redis->set("Completed_Match_" . $eid, json_encode($value->marketList->completed_match));
                            $mid = isset($value->marketList->completed_match->marketId) ? $value->marketList->completed_match->marketId : null;
                            $this->redis->set("Market_" . $mid, json_encode($value->marketList->completed_match));
                            $status = isset($value->marketList->completed_match->status) ? $value->marketList->completed_match->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                        } else {
                            $this->redis->set("Completed_Match_" . $eid, 0);
                        }
                        if (!empty($value->marketList->tied_match)) {
                            $this->redis->set("Tied_Match_" . $eid, json_encode($value->marketList->tied_match));
                            $mid = isset($value->marketList->tied_match->marketId) ? $value->marketList->tied_match->marketId : null;
                            $this->redis->set("Market_" . $mid, json_encode($value->marketList->tied_match));
                            $status = isset($value->marketList->tied_match->status) ? $value->marketList->tied_match->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                        } else {
                            $this->redis->set("Tied_Match_" . $eid, 0);
                        }

                        if (!empty($value->marketList->bookmaker)) {
                            $this->redis->set("Bookmaker_" . $eid, json_encode($value->marketList->bookmaker));
                            $data = $value->marketList->bookmaker;
                            foreach ($data as $key => $bookmaker) {
                                $mid = isset($bookmaker->marketId) ? $bookmaker->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($bookmaker));
                                $status = isset($bookmaker->status) ? $bookmaker->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Bookmaker_" . $eid, 0);
                        }

                        // fancy3 for other market
                        if (!empty($value->marketList->other_market)) {
                            $fancy3 = $this->redis->set("Fancy_3_" . $eid, json_encode($value->marketList->other_market));
                            $data = $value->marketList->other_market;
                            foreach ($data as $key => $other_market) {
                                $mid = isset($other_market->marketId) ? $other_market->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($other_market));
                                $status = isset($other_market->status) ? $other_market->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Fancy_3_" . $eid, 0);
                        }

                        // fancy2 for session
                        if (!empty($value->marketList->session)) {
                            $fancy2 = $this->redis->set("Fancy_2_" . $eid, json_encode($value->marketList->session));
                            $data = $value->marketList->session;
                            foreach ($data as $key => $session) {
                                $mid = isset($session->marketId) ? $session->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($session));
                                $status = isset($session->status) ? $session->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Fancy_2_" . $eid, 0);
                        }

                        if (!empty($value->marketList->khado)) {
                            $khado = $this->redis->set("Khado_" . $eid, json_encode($value->marketList->khado));
                            $data = $value->marketList->khado;
                            foreach ($data as $key => $khado) {
                                $mid = isset($khado->marketId) ? $khado->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($khado));
                                $status = isset($khado->status) ? $khado->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Khado_" . $eid, 0);
                        }

                        if (!empty($value->marketList->fancy)) {
                            $fancy = $this->redis->set("Fancy_" . $eid, json_encode($value->marketList->fancy));
                            $data = $value->marketList->fancy;
                            foreach ($data as $key => $fancy) {
                                $mid = isset($fancy->marketId) ? $fancy->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($fancy));
                                $status = isset($fancy->status) ? $fancy->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Fancy_" . $eid, 0);
                        }

                        if (!empty($value->marketList->ballbyball)) {
                            $ballbyball = $this->redis->set("Ballbyball_" . $eid, json_encode($value->marketList->ballbyball));
                            $data = $value->marketList->ballbyball;
                            foreach ($data as $key => $ballbyball) {
                                $mid = isset($ballbyball->marketId) ? $ballbyball->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($ballbyball));
                                $status = isset($ballbyball->status) ? $ballbyball->status : null;
                                if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Ballbyball_" . $eid, 0);
                        }

                        if (!empty($value->marketList->meter)) {
                            $fancy3 = $this->redis->set("Meter_" . $eid, json_encode($value->marketList->meter));
                            $data = $value->marketList->meter;
                            foreach ($data as $key => $meter) {
                                $mid = isset($meter->marketId) ? $meter->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($meter));
                                $status = isset($meter->status) ? $meter->status : null;
                                if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Meter_" . $eid, 0);
                        }

                        if (!empty($value->marketList->winner)) {
                            $winner = $this->redis->set("Winner_" . $eid, json_encode($value->marketList->winner));
                            $data = $value->marketList->winner;
                            $mid = isset($value->marketList->winner->marketId)?$value->marketList->winner->marketId:null;
                            $this->redis->set("Market_" . $mid, json_encode($data));
                            $status = isset($value->marketList->winner->status) ? $value->marketList->winner->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                        } else {
                            $this->redis->set("Winner_" . $eid, 0);
                        }

                        if (!empty($value->marketList->virtual_cricket)) {
                            $vc_market = $this->redis->set("Virtual_" . $eid, json_encode($value->marketList->virtual_cricket));
                            $data = $value->marketList->virtual_cricket;
                            foreach ($data as $key => $vc_market) {
                                $mid = isset($vc_market->marketId) ? $vc_market->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($vc_market));
                                $status = isset($vc_market->status) ? $vc_market->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        }else{
                            $this->redis->set("Virtual_" . $eid, 0);
                        }

                        if (!empty($value->marketList->odd_even)) {
                            $odd_even = $this->redis->set("Odd_Even_" . $eid, json_encode($value->marketList->odd_even));
                            $data = $value->marketList->odd_even;
                            foreach ($data as $key => $odd_even) {
                                $mid = isset($odd_even->marketId) ? $odd_even->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($odd_even));
                                $status = isset($odd_even->status) ? $odd_even->status : null;
                            if( $status == 1){ $arr_market_list[] = $mid; }
                            }
                        } else {
                            $this->redis->set("Odd_Even_" . $eid, 0);
                        }
                    }//end event list
                }
                $eventDataArray = array('sportId' => $sportId, 'sport_name' => $sport_name, 'eventData' => $sportwiseeventList);
                $this->redis->set("Event_List_" . $sportId, json_encode($eventDataArray));
            }//end game list
            $arr_event_id_list_old = json_decode($this->redis->get("cricketEventIdList"));
            if(!empty($arr_event_id_list_old)) {
                foreach ($arr_event_id_list_old as $key => $value) {
                    if (!in_array($value, $arr_event_id_list)) {
                        $this->redis->set("Event_" . $value, 0);
                    }
                }
            }
            $this->redis->set("cricketMarketList", json_encode(array_values(array_filter($arr_market_list))));
            $this->redis->set("cricketEventIdList", json_encode($arr_event_id_list));
            // return response()->json(["status" => 1, "code" => 200, "message" => "Data Saved.",], 200);

            }
            echo "1";
        }//try
        catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function getCricketMarketList(){
        try {
            $items         = null;
            $marketIdsArr  = $eid = [];
            $marketListArr = $this->redis->get("cricketMarketList");
            $marketIdsArr  = json_decode($marketListArr);
            //Production api
            $url = "http://52.48.184.39:8180/dream/get_odds";
            //Development api
            // $url = "http://52.48.184.39:6162/dream/dev/get_odds";
            if(!empty($marketIdsArr)) {
                $startTime = time();
                $endTime = time() + 60;
                while ($endTime > time()) {
                    usleep(250000);
                    $marketListArr = $this->redis->get("cricketMarketList");
                    $marketIdsArr  = json_decode($marketListArr);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($marketIdsArr));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $server_output = curl_exec($ch);
                    $result = json_decode($server_output);
                    curl_close($ch);

                    foreach ($result->data->items as $key => $obj) {
                        $marketId = isset($obj->market_id) ? $obj->market_id : null;
                        $this->redis->set($marketId, json_encode($obj));
                    }
                }
            }
            return response()->json([ "status" => 1 , "code" => 200 , "message" => "Data Saved!",'markets'=>$marketIdsArr],200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }
    //end for sport cricket
    
    //for sport jackpot
    public function storeGameJackpotdata(){
        try{
            $startTime = time();
            $endTime = time() + 60;
            //  while ($endTime > time()) {
            //  usleep(700000);
                $gameInfo = [];
                #to fetch game sport data
                //Production api
                $url = "http://api3.dreamexch9.com/api/get-game-jackpotdata";
                //Development :
                // $url = "https://api-devsession.dbm9.com/api/get-game-jackpotdata";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $result = curl_exec($ch);
                $data = json_decode($result);

                #to store event data
                $gameList = isset($data->gameList) ? $data->gameList : [];
                $arr_event_id_list = [];
                $arr_market_list   = [];
                foreach ($gameList as $key1 => $game) {
                    #for game events list
                    $sportId = $game->sportId;
                    $sport_name = $game->sport_name;
                    $sportwiseeventList = [];

                    foreach ($game->eventList as $key2 => $value) {
                        $event_data = isset($value->eventData) ? $value->eventData : null;
                        $eid = $event_data->eventId;
                        $arr_event_id_list[] = $eid;
                        $sportwiseeventList[] = $event_data;

                        #for event related data
                        $this->redis->set("Event_" . $eid, json_encode($event_data));
                        $eventSetting = isset($value->eventSetting) ? $value->eventSetting : null;
                        $this->redis->set("Event_limit_".$eid,json_encode($eventSetting));

                        // fancy3 for other market
                        if (!empty($value->marketList->other_market)) {
                            $fancy3 = $this->redis->set("Fancy_3_" . $eid, json_encode($value->marketList->other_market));
                            $data = $value->marketList->other_market;
                            foreach ($data as $key => $other_market) {
                                $mid = isset($other_market->marketId) ? $other_market->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($other_market));
                                $arr_market_list[] = $mid;
                            }
                        }else{
                            $this->redis->set("Fancy_3_" . $eid, 0);
                        }

                        // fancy2 for session
                        if (!empty($value->marketList->session)) {
                            $fancy2 = $this->redis->set("Fancy_2_" . $eid, json_encode($value->marketList->session));
                            $data = $value->marketList->session;
                            foreach ($data as $key => $session) {
                                $mid = isset($session->marketId) ? $session->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($session));
                                $arr_market_list[] = $mid;
                            }
                        }else{
                            $this->redis->set("Fancy_2_" . $eid, 0);
                        }

                        if (!empty($value->marketList->khado)) {
                            $khado = $this->redis->set("Khado_" . $eid, json_encode($value->marketList->khado));
                            $data = $value->marketList->khado;
                            foreach ($data as $key => $khado) {
                                $mid = isset($khado->marketId) ? $khado->marketId : null;
                                $this->redis->set("Market_" . $mid, json_encode($khado));
                                $arr_market_list[] = $mid;
                            }
                        }else{
                            $this->redis->set("Khado_" . $eid, 0);
                        }

                            if (!empty($value->marketList->jackpot->jkgametype)) {
                                $data = isset($value->marketList->jackpot->jkgametype) ? $value->marketList->jackpot->jkgametype : Null;
                                $this->redis->set("Jackpot_type", json_encode($data));
                            }

                            if (!empty($value->marketList->jackpot->data)) {
                                $data = $value->marketList->jackpot->data;

                                foreach ($data as $data1) {
                                    if (isset($data1->jackpot_data)) {
                                       $this->redis->set("Jackpot_data_" . $data1->jackpot_data[0]->type . "_" . $eid, json_encode($data1->jackpot_data));
                                    }
                                    if (isset($data1->market_data)) {
                                        $this->redis->set("Jackpot_all_data_" . $eid, json_encode($data1->market_data));
                                        foreach ($data1->market_data as $key => $marketData) {
                                            $mid = isset($marketData->marketId) ? $marketData->marketId : null;
                                            $this->redis->set("Jackpot_market_" . $marketData->type_id . "_" . $eid . "_" . $mid, json_encode($marketData));
                                            $this->redis->set("Jackpot_market_setting_" . $marketData->type_id . "_" . $eid . "_" . $mid, json_encode($marketData->settingData));

                                            $this->redis->set("Market_" . $mid, json_encode($marketData->settingData));
                                            $arr_market_list[] = $mid;
                                            
                                            $this->redis->set("Jackpot_" . $marketData->type_id . "_" . $mid, json_encode($marketData->playerlist));
                                        }
                                    }else{
                                        $this->redis->set("Jackpot_all_data_" . $eid,0);
                                    }
                                }
                            }
                    }//end event list
                    $eventDataArray = array('sportId' => $sportId, 'sport_name' => $sport_name, 'eventData' => $sportwiseeventList);
                    $this->redis->set("Event_List_" . $sportId, json_encode($eventDataArray));
                }//end game list
                $arr_event_id_list_old = json_decode($this->redis->get("jackpoteventIdList"));
                if(!empty($arr_event_id_list_old)){
                   foreach ($arr_event_id_list_old as $key => $value) {
                       if(!in_array($value, $arr_event_id_list)){
                           $this->redis->set("Event_" . $value,0);
                       }
                   }
                }
            $this->redis->set("jackpoteventIdList", json_encode($arr_event_id_list));
            $this->redis->set("jackpotMarketList", json_encode(array_values(array_filter($arr_market_list))));
            echo "1";
        // }
        }
        catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }//storeGameJackpotdata

    public function getMarketListJackpot(){
        try {
            $items              = null;
            $marketIdsArr       = $eid = [];
            $eventListArr       = $this->redis->get("jackpoteventIdList");
            $eventListArr       = json_decode($eventListArr);
            $jackpotMarketList  = $this->redis->get("jackpotMarketList");
            $marketIdsArr       = json_decode($jackpotMarketList);

            if(!empty($eventListArr)){
                //Production api
                $url = "http://52.48.184.39:8180/dream/get_odds";
                //Development api
                // $url = "http://52.48.184.39:6162/dream/dev/get_odds";
                if(!empty($marketIdsArr)) {
                    $startTime = time();
                    $endTime = time() + 60;
                    while ($endTime > time()) {
                        usleep(300000);
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($marketIdsArr));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $server_output = curl_exec($ch);
                        $result = json_decode($server_output);
                        curl_close($ch);
                        if(!empty($result)){
                        foreach ($result->data->items as $key => $obj) {
                            $marketId = isset($obj->market_id) ? $obj->market_id : null;
                            $this->redis->set($marketId, json_encode($obj));
                        }
                    }
                    }
                }
            }//end if
        return response()->json([ "status" => 1 , "code" => 200 , "message" => "Data Saved!",'markets'=>$marketIdsArr, 'eventID'=>$eventListArr],200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }
    //end for sport jackpot
}