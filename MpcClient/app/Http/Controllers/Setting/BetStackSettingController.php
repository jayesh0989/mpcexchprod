<?php

namespace App\Http\Controllers\Setting;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\BetStackFunction;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class BetStackSettingController extends Controller
{

    public function setBetOptions(Request $request)
    {

      try{
        $response  = ['status' => 0, 'code'=>400,"message" => "Bad request !!" ,'data'=>null];
       $uid = Auth::user()->id;
       if(isset($request->bet_option) ) {
   
         	 $values=array(
                    	'bet_option' =>$request->bet_option
                       );

             BetStackFunction::where('uid',$uid)->update($values);


          $response  = ['status' => 1, 'code'=>200,'data'=>null,"message" => "Settings saved successfully!"];
          return $response;

    	 }else{

            $response = ['status' => 1, 'code'=>200,'data'=>null,"message" => "Settings not saved successfully!"];
          return $response;
        }

     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }



}