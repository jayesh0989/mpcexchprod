<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    /**
     * List
     */
    public function list()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $data = DB::connection('mongodb')
                ->collection('category')->get();
            //$data = Category::all();
            if( $data != null ){
                $response = [ "status" => 1 ,'code'=> 200, "data" => $data ];
            }else{
                $response = [ "status" => 1 ,'code'=> 200, "data" => null ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Add
     */
    public function add(Request $request)
    {
        try{
            //dd($request->input());
            request()->validate([
                'name' => 'required',
                'slug' => 'required',
            ]);

            if( Category::create($request->all()) ){
                $response = [ "status" => 1 ,'code'=> 200, "message" => 'Added Success!' ];
            }else{
                $response = [ "status" => 0 ,'error' => [ 'message' => 'Something Wrong!'] ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Update
     */
    public function update(Request $request, Category $cat )
    {
        try{
            //dd($request->input());
            request()->validate([
                'name' => 'required',
                'slug' => 'required',
            ]);

            $check = DB::connection('mongodb')->collection('category')
                ->where('_id', trim($request->id))->first();
            if( $check != null ){
                $updateArr = [
                    'name' => trim($request->name),
                    'slug' => trim($request->slug),
                    'status' => $request->status
                ];

                if( DB::connection('mongodb')->collection('category')
                    ->where('_id', trim($request->id))->update($updateArr) ){
                    $response = [ "status" => 1 ,'code'=> 200, "message" => 'Updated Success!' ];
                }else{
                    $response = [ "status" => 0 ,'error' => [ 'message' => 'Something Wrong!'] ];
                }
            }else{
                $response = [ "status" => 0 ,'error' => [ 'message' => 'Something Wrong! Data not found!'] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
