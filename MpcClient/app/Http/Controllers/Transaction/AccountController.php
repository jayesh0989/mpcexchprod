<?php

namespace App\Http\Controllers\Transaction;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;



class AccountController extends Controller
{

    /**
     * mergeTable
     */
    public function mergeTable($startDate,$endDate){

        $tbl = 'tbl_transaction_client';

        if( isset($startDate) && isset($endDate) ){
            $sd = explode('-',trim($startDate));
            $ed = explode('-',trim($endDate));
            $td = explode('-',trim(Carbon::now()->format('Y-m-d')));
            if( count($td) == 3 && count($sd) == 3 && count($ed) == 3){
                $sm = $sd[1]; $em = $ed[1]; $tm = $td[1];
                $sy = $sd[0]; $ey = $sd[0]; $ty = $td[0];
                $smy = $sm.$sy; $emy = $em.$ey; $tmy = $tm.$ty;

                if( $smy == $emy && $smy == $tmy ){
                    $tbl1 = $tbl.'_'.$tmy;
                    $tbl2 = null;
                }elseif($smy != $emy && $emy == $tmy){
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 =  $tbl.'_'.$tmy;
                }else{
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = null;//$tbl.'_'.$emy;
                }
            }else{
                $tbl = $this->currentTable($tbl);
                $tbl1 = $tbl;
                $tbl2 = null;
            }
        }else{
            $tbl = $this->currentTable($tbl);
            $tbl1 = $tbl;
            $tbl2 = null;
        }

        return ['tbl1' => $tbl1,'tbl2' => $tbl2];
    }

    public function accountStatement(Request $request)
    {

     try{
        
         $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
         $userId = Auth::user()->id;

            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                  
                        $startDate = date('Y-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                        
                        $endDate = date('Y-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
              
            }else{
                
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('Y-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('Y-m-d h:i:s');  
                
            }

          //  echo "startdate-->".$startDate."--enddate-->".$endDate;

            $limit =10; $offset=0;
           if(isset($request->page)){
               $pageid=$request->page- 1;
               $offset= $limit * $pageid;
           }

//           $updatedOn = date('Y-m-d H:i:s');
//           $year = date('Y', strtotime($updatedOn));
//           $month = date('m', strtotime($updatedOn));
//           $currentMonth=$month.$year;
//           $tableName='tbl_transaction_client_'.$currentMonth;

           $tbls = $this->mergeTable($startDate,$endDate);
           $tbl1 = $tbls['tbl1'];
         if( strpos($tbl1,'_012021') > 0 ){
             $tbl1 = str_replace('_012021','_022021',$tbl1);
         }
           $tbl2 = $tbls['tbl2'];
           $accountQuery = DB::connection('mysql3')->table($tbl1)->select('id','sid','description','userId','type','created_on','amount','balance','mid','mType','status')
                          ->where([['userId',$userId],['amount','!=',0]])//9968
                          ->orderBy('id','DESC');
              
              if(isset($request->isFirst) && $request->isFirst == 1 ){
                  $accountQuery = $accountQuery->limit($limit)->offset($offset)->get();
              }else{
                $accountQuery = $accountQuery->whereBetween('created_on',[$startDate, $endDate])->limit($limit)->offset($offset)->get();
                //$accountQuery = $accountQuery->whereBetween('created_on',[$startDate, $endDate])->get();

              }

         $listArr1 = $accountQuery;
         $listArr =[];
         $listArr2= array();
         if( $tbl2 != null ){
             $accountQuery2 = DB::connection('mysql3')->table($tbl2)->select('id','sid','description','userId','type','created_on','amount','balance','mid','mType','status')
                 ->where([['userId',$userId],['amount','!=',0]])//9968
                 ->orderBy('id','DESC');

             if(isset($request->isFirst) && $request->isFirst == 1 ){
                 $accountQuery2 = $accountQuery2->limit($limit)->offset($offset)->get();
             }else{
                 $accountQuery2 = $accountQuery2->whereBetween('created_on',[$startDate, $endDate])->limit($limit)->offset($offset)->get();
             }

             $listArr2 = $accountQuery2;
            // print_r($listArr2); exit;
             if( $listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                 if($listArr2) {
                     foreach ( $listArr2 as $data ){
                         $listArr[] = $data;
                     }
                 }
                 if($listArr1) {
                     foreach ( $listArr1 as $data ){
                         $listArr[] = $data;
                     }
                 }
             }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
               //  $listArr = $listArr2;
             }
         }else{
                 $listArr= $listArr1;
         }

//         if($accountQuery->isEmpty()){
//
//          $updatedOn = date('Y-m-d H:i:s');
//            $year = date('Y', strtotime($updatedOn));
//            $month = date('m', strtotime($updatedOn));
//            $currentMonth=$month.$year;
//            $tableName='tbl_transaction_client_'.$currentMonth;
//
//             $accountQuery = DB::connection('mysql3')->table($tableName)->select('id','sid','description','userId','type','created_on','amount','balance','mid','mType','status')
//                 ->where([['userId',$userId],['amount','!=',0]])
//                 ->orderBy('id','DESC');
//
//             if(isset($request->isFirst) && $request->isFirst == 1 ){
//                 $accountQuery = $accountQuery->limit($limit)->offset($offset)->get();
//             }else{
//                 $accountQuery = $accountQuery->whereBetween('created_on',[$startDate, $endDate])->limit($limit)->offset($offset)->get();
//             }
//
//         }

            if(!empty($listArr)){
                $arr = [];
            	foreach ($listArr as  $acQuery) {


                         $description = $acQuery->description;

                     if(strpos($acQuery->description,'>')){
                       $desc = explode(">", $acQuery->description);
                       //print_r($desc); die();
                       $description = $desc[0].'>'.$desc[1];
                       if(isset($desc[3])){
                       $description = $desc[0].'>'.$desc[1].'>'.$desc[3];
                       }
                     }
                     if($acQuery->mType == 'cricket_casino'){
                       $description = $acQuery->description;
                     }

                      if($acQuery->mType == 'casino'){
                         $desc = explode(">", $acQuery->description);
                        $description = $desc[0].'>'.$desc[1].'>'.$desc[2];
              
                     }

                    if($acQuery->mType == 'USDINR' || $acQuery->mType == 'GOLD'|| $acQuery->mType == 'SILVER'|| $acQuery->mType == 'EURINR'|| $acQuery->mType == 'GBPINR'|| $acQuery->mType == 'ALUMINIUM' || $acQuery->mType == 'COPPER' || $acQuery->mType == 'CRUDEOIL' || $acQuery->mType == 'ZINC' || $acQuery->mType == 'BANKNIFTY' || $acQuery->mType == 'NIFTY'){
                        $description = $acQuery->description;
                    }
                 
            		$arr[] = [
                          
                          'id'  => $acQuery->id,
                          'created_on'  => $acQuery->created_on,
                          'description'  => $description,
                          'type'  => $acQuery->type,
                          'amount'  => $acQuery->amount,
                          'balance'  => $acQuery->balance,
                          'marketId'  => empty($acQuery->mid) ? null : $acQuery->mid,
                          'sid'  => $acQuery->sid,
                          'status' => $acQuery->status      
             
            		];
            	
            	}

            	 $response = [ "status" => 1 ,'code'=> 200, "data" => $arr ,'message'=> 'Data Found !!','offset'=>$offset ];
            }else{

            	 $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Not Found !!' ];
            }



      return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

   public function accountBetList(Request $request){
    $response =  response()->json([ "status" => 0 , "code" => 400 , "message" => "Bad request!" ]);
    try{
        $listArr=[];
        $userId = Auth::user()->id;

        if( isset( $request->start_date ) && isset( $request->end_date ) && ( $request->start_date != '' ) && ( $request->end_date != '' ) && ( $request->start_date != null ) && ( $request->end_date != null ) )
        {

          $startDate  = date('Y-m-d', strtotime($request->start_date));
          $startDate  = $startDate." 00:00:01";
          $endDate    = date('Y-m-d', strtotime($request->end_date));
          $endDate    = $endDate." 23:59:59";
        }else{
          $start      = new \DateTime('now +1 day');
          $endDate    = $start->format('Y-m-d h:i:s');
          $end        = new \DateTime('now -5 day');
          $startDate  = $end->format('Y-m-d h:i:s'); 
        }

        if(!empty($request->type))
        {
          if ($request->type =='casino')
          {

          // $marketId= explode("-",$request->market_id);
              if( strpos($request->market_id,'B') > 0 ){
                  $marketId = str_replace('-B','',$request->market_id);
              }else{
                  $marketId = str_replace('-W','',$request->market_id);
              }

         // $userId=9959;
            $BetList = DB::connection('mongodb')->table('tbl_casino_transaction')
                      ->select('*')
                      ->where([['uid',$userId],['status',1],['round',$marketId],['amount','!=',0]])
                     // ->whereIn('result',['WIN','LOSS'])
                      ->get();

                   //   print_r($BetList ); exit;
          }
          elseif($request->type =='livegame1' || $request->type =='livegame2' || $request->type =='teenpatti')
          {

            
            $BetList =  DB::connection('mongodb')->table('tbl_bet_history_teenpatti')
                          ->select('id','rate','win','loss','description','bType','mType','created_on','updated_on','price','size','result','diff','market')
                          ->where([['uid',$userId],['status',1],['mid',(int)$request->market_id],['amount','!=',0]])
                          ->orWhere([['uid',$userId],['status',1],['mid',$request->market_id]])
                          ->whereIn('result',['WIN','LOSS'])
                          ->get();
          }
      
          if(isset($BetList) && !$BetList->isEmpty())
          {
            
            if($request->type =='casino'){
                foreach ($BetList as $list) {
                  $list = (object) $list;

                  if(isset($list->type) && $list->type == 'CREDIT'){
                    $profitLoss = isset($list->amount)?$list->amount:'N/A';
                  }
                  else{
                    $profitLoss = isset($list->amount)?$list->amount:'N/A';
                  }

                  if(isset($list->type) && $list->type == 1){
                    $bType = "back";
                  }
                  elseif(isset($list->type) && $list->type == 2){
                    $bType = "lay";
                  }
                  else{
                    $bType = isset($list->type)?$list->type:'N/A';
                  }

                  $game_code = explode("_", $list->game_code);
                  $gameCode = $game_code[1];

                  $listArr[] = [
                                  'betId'       => isset($list->_id)?$list->_id:'N/A',
                                  //'description' => isset($list->game_code)?$list->game_code.' > round #'.$list->round:'N/A',
                                  'description' => isset($gameCode)?$gameCode.' > round #'.$list->round:'N/A',
                                  'bType'       => isset($list->type)?$list->type:'N/A',
                                  'price'       => isset($list->amount)?$list->amount:'N/A',
                                  'size'        => isset($list->amount)?$list->amount:'N/A',
                                  'profitLoss'  => isset($profitLoss)?$profitLoss:'N/A',
                                  'date'        => isset($list->updated_on)?$list->updated_on:'N/A',
                                  'result'      => isset($bType)?$bType:'N/A',
                                  'win'         => isset($list->amount)?$list->amount:'N/A',
                                  'loss'        => isset($list->amount)?$list->amount:'N/A',
                                  'rate'        => 'N/A',
                                  'mType'       => 'Casino'
                                ];
              }
            }
            else{

              foreach ($BetList as $list) {
                $list = (object) $list;
                if(isset($list->result) && $list->result == 'WIN'){
                  $profitLoss = isset($list->win)?$list->win:'N/A';
                }
                else{
                  $profitLoss = isset($list->loss)?$list->loss:'N/A';
                }

                if(isset($list->bType) && $list->bType == 1){
                  $bType = "back";
                }
                elseif(isset($list->bType) && $list->bType == 2){
                  $bType = "lay";
                }
                else{
                  $bType = isset($list->bType)?$list->bType:'N/A';
                }

                $listArr[] = [
                                'betId'       => isset($list->_id)?$list->_id:'N/A',
                                'description' => isset($list->description)?$list->description:'N/A',
                                'bType'       => isset($list->bType)?$list->bType:'N/A',
                                'price'       => isset($list->price)?$list->price:'N/A',
                                'size'        => isset($list->size)?$list->size:'N/A',
                                'profitLoss'  => isset($profitLoss)?$profitLoss:'N/A',
                                'date'        => isset($list->updated_on)?$list->updated_on:'N/A',
                                'result'      => isset($list->result)?$list->result:'N/A',
                                'win'         => isset($list->win)?$list->win:'N/A',
                                'loss'        => isset($list->loss)?$list->loss:'N/A',
                                'rate'        => isset($list->rate)?$list->rate:'N/A',
                                'mType'       => isset($list->mType)?$list->mType:'N/A'
                              ];
              }
          }
            $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => $listArr ,'message'=> 'Data Found !!' ]);
          }
          else
          {
            $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Found !!' ]);
          }

        }
        elseif( !empty($request->market_id) && empty($request->type))
        {
        
          $BetList =  DB::connection('mongodb')->table('tbl_bet_history')
                      ->select('betId','rate','win','loss','description','bType','mType','created_on','updated_on','price','size','result','diff','market')
                      ->where([['uid',$userId],['status',1],['mid',$request->market_id]])
                      ->whereIn('result',['WIN','LOSS'])
                      ->get();
          if(!$BetList->isEmpty() ){

            foreach ($BetList as $list) {
              $list = (object) $list;
              if($list->result == 'WIN'){
                $profitLoss = $list->win;
              }else{
                $profitLoss = $list->loss;
              }

              $listArr = [
                            'betId'       => isset($list->_id)?$list->_id:'N/A',
                            'description' => isset($list->description)?$list->description:'N/A',
                            'bType'       => isset($list->bType)?$list->bType:'N/A',
                            'price'       => isset($list->price)?$list->price:'N/A',
                            'size'        => isset($list->size)?$list->size:'N/A',
                            'profitLoss'  => isset($profitLoss)?$profitLoss:'N/A',
                            'date'        => isset($list->updated_on)?$list->updated_on:'N/A',
                            'result'      => isset($list->result)?$list->result:'N/A',
                            'win'         => isset($list->win)?$list->win:'N/A',
                            'loss'        => isset($list->loss)?$list->loss:'N/A',
                            'rate'        => isset($list->rate)?$list->rate:'N/A',
                            'mType'       => isset($list->mType)?$list->mType:'N/A'
                        ];
            
              if($list->mType == 'khado'){
                $listArr['difference'] = $list->diff - $list->price;
              }

              if($list->mType == 'ballbyball'){
                $market           = $list->market;
                $space            = ' ';
                $ball             = strstr($market,$space,true);        
                $listArr['ball']  = $ball;
              }
              $lists[] = $listArr;
            }

            $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => $lists ,'message'=> 'Data Found !!' ]);
          }else{
            $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Found !!' ]);
          }

        }
        
        return $response;
    }catch (\Exception $e) {
      $response = $this->errorLog($e);
      return response()->json($response, 501);
    }
  }

 }