<?php

namespace App\Http\Controllers\UserProfile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;

class UserProfileController extends Controller
{
    public function userProfile(){
    	$response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];

    	if (Auth::check()) {
        $uid = Auth::user()->id;

    	//$userProfile = 
    	$userProfile = $this->getBalanceValUpdate($uid);


    	return response()->json([ "status" => 1 ,'code'=>200, "data" =>$userProfile ,"message" => "Data Found!" ]);
    	}
    	else{
    		return $response;
    	}
    }

    public function getBalanceValUpdate($uid)
       {

            $user = DB::table('tbl_user_info')->select('balance','pl_balance','expose','updated_on')
                        ->where('uid',$uid)
                        ->first();

            $profile = DB::table('tbl_user')->select('name','username','created_at')->where('id',$uid)->first();

            if( $user != null ){
                 if($user->expose!=0 || $user->expose!=null || $user->expose!=''){
                     $user_balance = $user->balance-$user->expose+$user->pl_balance;
                 }else{
                     $user_balance = $user->balance+$user->pl_balance;
                 }
               

                //$updatedTime = (strtotime($user->updated_on)+0);

                return ["name" => $profile->name,"username" => $profile->username,  "balance" => round($user_balance) , "expose" => round( $user->expose) , "mywallet" => round($user->balance) , "joining_date" => $profile->created_at ];
            }

        return [ "balance" => 0 , "expose" => 0 , "mywallet" => 0 , "updated_time" => 0];
  }
}
