<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class OauthAccessToken extends Model
{
    public $timestamps=false;
    protected $table = 'oauth_access_tokens';
    
}
