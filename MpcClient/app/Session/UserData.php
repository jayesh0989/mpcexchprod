<?php

namespace App\Session;

use App\Models\System\Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserData extends Model
{
    public static function getAllUserList()
    {
        $rs = DB::table('user')->where('status',1)->orderBy('id', 'DESC')->limit(10)->get();
        return $rs;
    }

    /**
     * @inheritDoc
     */
    public function getQueueableRelations()
    {
        // TODO: Implement getQueueableRelations() method.
    }
}



