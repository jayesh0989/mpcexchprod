<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>mpc-activity-log</title>
    <style>
        a{
            color: #000;
        }
        a:hover{
            color:#533201;
        }
        h2,center{
            color:#533201;
        }
    </style>
</head>
<body style="background-color:#729B93">

<div class="container-fluid mt-4">
    <center><h2>MPCEXCH</h2></center><hr>
<div class="row">

<div class="col-lg-2"></div>
<div class="col-lg-6">
    <h2>View Activity Log</h2>
    <!--form method="get" action="{{route('activityLog')}}" >
        {{ csrf_field() }}
        <input type="date" name="date">
        <button class="btn btn-secondary" formtarget="_blank">Date</button>
    </form--> 
</div>
</div>
<br>
<div class="row">
<div class="col-lg-2"></div>
    <div class="col-lg-8">

            <table class="table table-bordered table-hover">
            <thead>
                <tr>
                <th scope="col">Id</th>
                <th scope="col">Date</th>
                <th scope="col">Delete</th>
                </tr>
            </thead>
            
            @foreach($ActivityFile as $key => $file)
            <tbody>
                <tr>
                @if($file != '.gitignore')
                <th scope="row">{{$key+1}}</th>
                <td><a href="{{url('api/view-activity-log/'.$file)}}" target="_blank">{{$file}}</a></td>
                <td><a href="{{url('api/delete-activity/'.$file)}}" onclick="return confirm('do you really want to delete this file?')"><button class="btn btn-sm btn-danger">Delete</button></a></td>
                @endif
                </tr>
            </tbody>
            @endforeach
            </table>

            {{ $ActivityFile->links() }}
    </div>
</div>

</div>


<script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</script>
</body>
</html>