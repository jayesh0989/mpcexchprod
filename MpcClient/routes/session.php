<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/upload', 'Session\UserController@upload');


Route::get('/userlist/{id}','Session\UserController@getUser');

Route::get('/userlist','Session\UserController@getAllUser');


Route::post('/login','UserController@login', function(Request $request) {
$productname = $request->input();

});

Route::post('/createCustomer','UserController@createCustomer', function(Request $request) {
$productname = $request->input();

});

//Route::group(['prefix' => 'api','namespace' => 'Api', 'middleware' => 'api'], function () {
//Route::group(['namespace' => 'Api\V1', 'prefix' => 'api', 'as' => 'api'], function () {
    Route::post('/getAllCustomerVendor','Api\V1\UserController@getAllCustomerVendor', function(Request $request) {
    });
//});
/*Route::post('/getAllCustomerVendor','Api\UserController@getAllCustomerVendor', function(Request $request) {
$productname = $request->input();
});*/
Route::post('/createTransaction','UserController@createTransaction', function(Request $request) {
$productname = $request->input();

});
Route::post('/updateProfile','UserController@updateProfile', function(Request $request) {
$productname = $request->input();

});


