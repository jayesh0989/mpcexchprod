<?php

namespace App\Http\Controllers\AppUser;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\UserInfo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ActionController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }
    /**
     * User User Data
     * Action - Post
     * Created at APR 2020 by dream
     */
    public function updateUserData()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
        return $response;exit;
        $userList = DB::table('tbl_user')->where('role',4)
            ->select(['id','parentId'])->get();
        if( $userList->isNotEmpty() ){
            foreach ($userList as $user){
                $pUser = DB::table('tbl_user')->select('username')
                    ->where('id',$user->parentId)->first();
                if( $pUser != null ){
                    $pName = $pUser->username;
                    DB::table('tbl_user_info')->where('uid',$user->id)->update(['pName' => $pName]);
                }
            }
        }
    }
    /**
     * Clear User Data
     * Action - Post
     * Created at DEC 2020
     */
    public function clearUserData()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
        return $response;exit;
        try {

            DB::table('tbl_bet_pending_1')->delete();
            DB::table('tbl_bet_pending_2')->delete();
            DB::table('tbl_bet_pending_3')->delete();
            DB::table('tbl_bet_pending_4')->delete();
            DB::table('tbl_bet_pending_teenpatti')->delete();
            DB::table('tbl_bet_history')->delete();
            DB::table('tbl_bet_history_teenpatti')->delete();
            DB::table('tbl_book_bookmaker')->delete();
            DB::table('tbl_book_matchodd')->delete();
            DB::table('tbl_user_market_expose')->delete();
            DB::table('tbl_user_market_expose_2')->delete();
            DB::table('tbl_transaction_admin')->where([['eType','!=',1]])->delete();
            DB::table('tbl_transaction_client')->where([['eType','!=',1]])->delete();
            DB::table('tbl_transaction_parent')->where([['eType','!=',1]])->delete();
            DB::table('tbl_transaction_history')->where([['eType','!=',1]])->delete();
            DB::table('tbl_teenpatti_result')->delete();
            DB::table('tbl_user_info')->where([['id','!=',0]])->update(['pl_balance'=>0,'expose'=>0]);
            DB::table('tbl_settlement_summary')->where([['id','!=',0]])
                ->update(['ownPl'=>0,'ownCash'=>0, 'ownComm'=>0,
                    'parentCash'=>0,'parentComm'=>0,'parentPl'=>0,'lastTransId'=>0]);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function clearUserDataNew()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
        try {
            $connMongodb = DB::connection('mongodb');
            $conn3 = DB::connection('mysql3');
            $conn = DB::connection('mysql');

            $connMongodb->table('tbl_bet_pending_1')->delete();
            $connMongodb->table('tbl_bet_pending_2')->delete();
            $connMongodb->table('tbl_bet_pending_3')->delete();
            $connMongodb->table('tbl_bet_pending_4')->delete();
            $connMongodb->table('tbl_bet_pending_5')->delete();
            $connMongodb->table('tbl_bet_pending_teenpatti')->delete();
            $connMongodb->table('tbl_bet_history')->delete();
            $connMongodb->table('tbl_bet_history_teenpatti')->delete();
            $connMongodb->table('tbl_user_market_expose')->delete();
            $connMongodb->table('tbl_market_result')->delete();
            $connMongodb->table('tbl_casino_transaction')->delete();
            $connMongodb->table('tbl_market_data')->delete();

            $transTbl = $this->currentTable('tbl_transaction_admin');
            $conn3->table($transTbl)->where([['eType','!=',1]])->delete();
            $transTbl = $this->currentTable('tbl_transaction_client');
            $conn3->table($transTbl)->where([['eType','!=',1]])->delete();
            $transTbl = $this->currentTable('tbl_transaction_parent');
            $conn3->table($transTbl)->where([['eType','!=',1]])->delete();

            $conn->table('tbl_book_bookmaker')->delete();
            $conn->table('tbl_book_matchodd')->delete();
            $conn->table('tbl_user_info')->where([['id','!=',0]])->update(['pl_balance'=>0,'expose'=>0]);
            $conn->table('tbl_settlement_summary')->where([['id','!=',0]])
                ->update(['ownPl'=>0,'ownCash'=>0, 'ownComm'=>0,
                    'parentCash'=>0,'parentComm'=>0,'parentPl'=>0,'lastTransId'=>0]);

            $response = ['status' => 1, 'message' => 'Data clean Successfully!'];
            return $response;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    /**
     * Check User Data
     * Action - Post
     * Created at DEC 2020
     */
    public function checkUserData()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try {
            $pUser = Auth::user(); // current user id
            $pUserInfo = UserInfo::where('uid',$pUser->id)->first();

            if( $pUser->role == 1 && $pUserInfo->pl == 0){
                $pl = 100;
            }else{
                $pl = $pUserInfo->pl;
            }

            $data = [
                'balance' => $pUserInfo->balance,
                'pl' => $pl,
                'username' => $pUser->name,
            ];

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Check User Data Both
     * Action - Post
     * Created at DEC 2020
     */
    public function checkUserDataBoth($uid)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try {
            $cUser = User::where('id',$uid)->first();
            $pUser = User::where('id',$cUser->parentId)->first();
            $pUserInfo = UserInfo::where('uid',$cUser->parentId)->first();

            if( $pUser->role == 1 && $pUserInfo->pl == 0){
                $pl = 100;
            }else{
                $pl = $pUserInfo->pl;
            }

            $pData = [
                'balance' => $pUserInfo->balance,
                'pl' => $pl,
                'username' => $pUser->username,
            ];

            $cUserInfo = DB::table('tbl_user_info')->where([['uid',$uid]])->first();
            if( $cUserInfo != null ){
                if( $cUser->role == 4 ){
                    if( $cUserInfo->pl_balance < 0 ){
                        $available = $cUserInfo->balance+$cUserInfo->pl_balance-$cUserInfo->expose;
                    }else{
                        $available = $cUserInfo->balance-$cUserInfo->expose;
                    }
                }else{
                    $available = $cUserInfo->balance;
                }

                $cData = [
                    'balance' => $available,
                    'pl' => $cUserInfo->pl,
                    'username' => $cUser->username,
                ];

                $data = [ 'pData' => $pData , 'cData' => $cData ];

                if( $data != null ){
                    $response = [ 'status' => 1, 'data' => $data ];
                }
            }else{
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This user not created properly!' ] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * doDelete
     * Action - Post
     * Created at DEC 2020
     */
    public function doDelete($uid)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try {

            $user = DB::table('tbl_user')->where([['id',$uid],['status','!=',2]])->first();

            if( $user != null ){
                $cUser = Auth::user(); // current user id
                if( $cUser->id == $user->parentId ){
                    if( $user->role == 4 ){
                        $userInfo = DB::table('tbl_user_info')->where([['uid',$uid]])->first();
                        if( $userInfo != null && ( round($userInfo->balance) >= '-1' && round($userInfo->balance) <= '1' )
                            && ( round($userInfo->pl_balance) >= '-1' && round($userInfo->pl_balance) <= '1' )
                            && ( round($userInfo->expose) >= '-1' && round($userInfo->expose) <= '1' ) ){
                            if( DB::table('tbl_user')->where([['id',$uid]])->update(['status' => 2]) ){
                                DB::table('oauth_access_tokens')->where([['user_id',$uid]])->delete();
                                DB::table('tbl_settlement_summary')->where([['uid',$uid]])->update(['status' => 2]);
                                $response = [ 'status' => 1 , "success" => [ "message" => "User Deleted successfully!" ] ];
                            }else{
                                $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! User can not deleted !" ] ];
                            }
                        }else{
                            $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! P&L Or Balance OrExposer is pending !" ] ];
                        }
                    }elseif( $user->role == 5 || $user->role == 6 ){
                        if( DB::table('tbl_user')->where([['id',$uid]])->update(['status' => 2]) ){
                            DB::table('oauth_access_tokens')->where([['user_id',$uid]])->delete();
                            // DB::table('tbl_settlement_summary')->where([['uid',$uid]])->update(['status' => 2]);
                            $response = [ 'status' => 1 , "success" => [ "message" => "User Deleted successfully!" ] ];
                        }else{
                            $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! User can not deleted !" ] ];
                        }
                    }else{
                        $childUser = DB::table('tbl_user')->where([['parentId',$uid],['status','!=',2]])->first();

                        if( $childUser != null ){
                            $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! Still Child User is Active !" ] ];
                        }else{
                            $userInfo = DB::table('tbl_user_info')->where([['uid',$uid]])->first();
                            if( $userInfo != null && $userInfo->pl_balance == '0' && $userInfo->balance == '0'){
                                if( DB::table('tbl_user')->where([['id',$uid]])->update(['status' => 2]) ){
                                    DB::table('oauth_access_tokens')->where([['user_id',$uid]])->delete();
                                    DB::table('tbl_settlement_summary')->where([['uid',$uid]])->update(['status' => 2]);
                                    $response = [ 'status' => 1 , "success" => [ "message" => "User deleted successfully!" ] ];
                                }else{
                                    $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! User can not deleted !" ] ];
                                }
                            }else{
                                $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! P&L Or Balance is pending !" ] ];
                            }
                        }

                    }

                }else{
                    $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! Only parent can delete!" ] ];
                }
            }else{
                $response = [ 'status' => 1 , "success" => [ "message" => "This user already deleted !" ] ];
            }
            if( $response['status'] == 1 ){
                $data['title'] = 'Delete User';
                $data['description'] = 'User ( '. $user->username .' ) deleted';
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Admin Reset Activity Password
     * Action - Post
     * Created at DEC 2020
     */
    public function resetActivityPassword(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        try{

            $data = $request->input();

            $user = Auth::user();
            $password1 = md5( $data['password1'] );
            $password2 = md5( $data['password2'] );
            $checkOldPass = DB::table('tbl_common_setting')->select(['value'])
                ->where([['key_name','ADMIN_ACTIVITY_PASSWORD']])->first();

            if( $checkOldPass == null || ( $checkOldPass->value != $password1 ) ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Old Password did not match ! Plz try again !!' ] ];
                return response()->json($response, 200);
            }

            if( $password1 == $password2 ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Old Password and New Password must be a different !!' ] ];
                return response()->json($response, 200);
            }

            if( $user->roleName == 'ADMIN' ){
                DB::table('tbl_common_setting')
                    ->where([['key_name','ADMIN_ACTIVITY_PASSWORD']])
                    ->update(['value' => $password2 ]);
                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Reset Activity Password successfully!'
                    ]
                ];
                $data['title'] = 'Reset Activity Password';
                $data['description'] = 'Reset Activity Password';
                $this->addNotification($data);
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Reset Password
     * Action - Post
     * Created at DEC 2020
     */
    public function resetPassword(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        try{
            $cUser = Auth::user();
            $data = $request->input();
            if( isset($data['uid']) && isset($data['password']) && strlen($data['password']) >= 6 ){

                if( ( $data['uid'] == 1 && $cUser->id != 1 ) || in_array( $cUser->roleName ,['SA','SV'] ) ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Plz contact administrator !' ] ];
                    return response()->json($response); exit;
                }

                // $uppercase = preg_match('@[A-Z]@', $data['password']);
                // $number    = preg_match('@[0-9]@', $data['password']);

                // if( !$uppercase || !$number ) {
                //     $response = [
                //         'status' => 0,
                //         'error' => [ 'message' => 'Password must like this format. example : Xyz$@123' ]
                //     ];
                //     return response()->json($response);
                // }

                $user = User::find($data['uid']);
                $user->password = bcrypt( $data['password'] );

                if( $user->save() ){
                    DB::table('oauth_access_tokens')
                        ->where([['user_id',$user->id]])->delete();
                    $response = [
                        'status' => 1,
                        'success' => [
                            'message' => 'Reset Password successfully!'
                        ]
                    ];
                    $data['title'] = 'Reset Password';
                    $data['description'] = 'User ( '. $user->username .' ) reset password';
                    $this->addNotification($data);
                }

                return response()->json($response, 200);
            }else{
                $response = [
                    'status' => 0,
                    'error' => [ 'message' => 'Invalid Data !!' ]
                ];
                return response()->json($response);
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Deposit Balance
     * Action - Post
     * Created at DEC 2020
     */
    public function depositBalance(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];

        try{
            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                return $response; exit;
            }
            $data = $request->input();
            if( isset($data['uid']) && isset($data['balance'])
                && $data['uid'] != null && $data['balance'] != null){
                $regExp = "#[?!%^&*=><()'\"+-]+#";
                if( isset($data['remark']) && preg_match($regExp,trim($data['remark'])) ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! ?!%^&*=><()\'" are not allowed ' ] ];
                    return $response;
                }

                $cUser = Auth::user();
                $user = User::where('id',$data['uid'])->first();
                if( $user != null ){
                    $parentId = $user->parentId;
                    if( $parentId != $cUser->id && $cUser->roleName != 'ADMIN' ){
                        return $response; exit;
                    }
                    $amount = $data['balance'];
                    if($amount < 0){ $amount = (-1)*$amount; }
                    $remark = $data['remark'];
                    $uid = $data['uid'];
                    $type = 'DEPOSIT';

                    $pUser = DB::table('tbl_user_info')
                        ->where('uid',$parentId)->first();
                    if( ($pUser->balance-$amount) >= 0 ){
                        DB::table('tbl_user_info')->where('uid',$uid)
                            ->update(['balance' => DB::raw('balance + ' .$amount) ]);
                        DB::table('tbl_user_info')->where('uid',$parentId)
                            ->update(['balance' => DB::raw('balance + ' .(-1)*$amount) ]);

                        CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);

                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Deposit successfully!'
                            ]
                        ];
                        $data['title'] = 'Deposit/Withdrawal';
                        $data['description'] = 'User ( '. $user->username .' ) to deposit '.$amount.' chips';
                        $this->addNotification($data);
                    }
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Withdrawal Balance
     * Action - Post
     * Created at DEC 2020
     */
    public function withdrawalBalance(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];

        try{
            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                return $response; exit;
            }
            $data = $request->input();
            if( isset($data['uid']) && isset($data['balance'])
            && $data['uid'] != null && $data['balance'] != null){

                $regExp = "#[?!%^&*=><()'\"+-]+#";
                if( isset($data['remark']) && preg_match($regExp,trim($data['remark'])) ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! ?!%^&*=><()\'" are not allowed ' ] ];
                    return $response; exit;
                }

                $cUser = Auth::user();
                $user = User::where('id',$data['uid'])->first();
                if( $user != null ){
                    $parentId = $user->parentId;
                    if( $parentId != $cUser->id && $cUser->roleName != 'ADMIN' ){
                        return $response; exit;
                    }

                    $amount = $data['balance'];
                    if($amount < 0){ $amount = (-1)*$amount; }
                    $remark = $data['remark'];
                    $uid = $data['uid'];
                    $type = 'WITHDRAWAL';

                    // child user
                    $userInfo = DB::table('tbl_user_info')
                        ->where('uid',$uid)->first();
                    if( $user->role == 4 ){
                        if( $userInfo->pl_balance < 0 ){
                            $available = $userInfo->balance+$userInfo->pl_balance-$userInfo->expose-$amount;
                        }else{
                            $available = $userInfo->balance-$userInfo->expose-$amount;
                        }
                    }else{
                        $available = $userInfo->balance-$amount;
                    }

                    if( $available >= 0 ){
                        DB::table('tbl_user_info')->where('uid',$uid)
                            ->update(['balance' => DB::raw('balance + ' .(-1)*$amount) ]);
                        DB::table('tbl_user_info')->where('uid',$parentId)
                            ->update(['balance' => DB::raw('balance + ' .$amount) ]);

                        CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);
                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Withdrawal successfully!'
                            ]
                        ];
                        $data['title'] = 'Deposit/Withdrawal';
                        $data['description'] = 'User ( '. $user->username .' ) from withdrawal '.$amount.' chips';
                        $this->addNotification($data);
                    }
                }

            }


            return response()->json($response);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
