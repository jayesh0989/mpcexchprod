<?php

namespace App\Http\Controllers\AppUser;
use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;


class ClientController extends Controller
{

    /**
     * Client Manage List
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function manage()
    {
        try{
            $data = Client::getList();
            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Reference List
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function reference($id)
    {
        try{
            $data = Client::getList($id, null);

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Master Create
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function create(Request $request)
    {
        try{

            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
                return $response; exit;
            }

            $response = Client::create($request->input());
            if( $response['status'] == 1 ){
                $data['title'] = 'Create User';
                $data['description'] = 'New client ( '. $request->username .' ) created';
                $this->addNotification($data);
            }
            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
