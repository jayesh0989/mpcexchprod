<?php

namespace App\Http\Controllers\AppUser;


use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\UserLimits;
use App\Models\UserSportMarketLimits;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{

    /**
     * User Limit Data
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function limit($uid)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $user = User::where('id',$uid)->first();
            if( $user != null ){
                $userName = $user->username;
                $data = UserLimits::where([['uid',$uid]])->first();
                if( $data != null ){
                    $response = [ 'status' => 1, 'data' => $data, 'userName' => $userName ];
                }
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * User Limit Setting Data
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function limitSetting($uid)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $user = User::where('id',$uid)->first();
            if( $user != null ){
                $userName = $user->username;
                $data = UserSportMarketLimits::where([['uid',$uid]])->first();
                if( $data != null ){

                    $resData = [
                        'uid' => $uid,
                        'cricket_matchodd' => json_decode( $data->cricket_matchodd ),
                        'cricket_fancy' => json_decode( $data->cricket_fancy ),
                        'cricket_bookmaker' => json_decode( $data->cricket_bookmaker ),
                        'football_matchodd' => json_decode( $data->football_matchodd ),
                        'football_fancy' => json_decode( $data->football_fancy ),
                        'football_bookmaker' => json_decode( $data->football_bookmaker ),
                        'tennis_matchodd' => json_decode( $data->tennis_matchodd ),
                        'tennis_fancy' => json_decode( $data->tennis_fancy ),
                        'tennis_bookmaker' => json_decode( $data->tennis_bookmaker ),
                    ];

                    $response = [ 'status' => 1, 'data' => $resData, 'userName' => $userName ];
                }else{

                    $dataSet = [
                        'min_stack' => 0,
                        'max_stack' => 0,
                        'max_profit_limit' => 0,
                        'max_expose_limit' => 0,
                        'bet_delay' => 0
                    ];

                    $model = new UserSportMarketLimits();
                    $model->uid = $uid;
                    $model->systemId = $user->systemId;
                    $model->cricket_matchodd = json_encode($dataSet);
                    $model->cricket_fancy = json_encode($dataSet);
                    $model->cricket_bookmaker = json_encode($dataSet);
                    $model->football_matchodd = json_encode($dataSet);
                    $model->football_fancy = json_encode($dataSet);
                    $model->football_bookmaker = json_encode($dataSet);
                    $model->tennis_matchodd = json_encode($dataSet);
                    $model->tennis_fancy = json_encode($dataSet);
                    $model->tennis_bookmaker = json_encode($dataSet);
                    $model->updated_on = date('Y-m-d H:i:s');

                    if( $model->save() ){

                        $resData = [
                            'uid' => $uid,
                            'cricket_matchodd' => json_decode( $model->cricket_matchodd ),
                            'cricket_fancy' => json_decode( $model->cricket_fancy ),
                            'cricket_bookmaker' => json_decode( $model->cricket_bookmaker ),
                            'football_matchodd' => json_decode( $model->football_matchodd ),
                            'football_fancy' => json_decode( $model->football_fancy ),
                            'football_bookmaker' => json_decode( $model->football_bookmaker ),
                            'tennis_matchodd' => json_decode( $model->tennis_matchodd ),
                            'tennis_fancy' => json_decode( $model->tennis_fancy ),
                            'tennis_bookmaker' => json_decode( $model->tennis_bookmaker ),
                        ];

                        $response = [ 'status' => 1, 'data' => $resData, 'userName' => $userName ];
                    }

                }
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * User Limit Update
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function limitUpdate(Request $request)
    {
        try{

            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
                return $response; exit;
            }

            $this->validate($request, [
                'uid' => 'required',
                'min_stack' => 'required|min:1',
                'max_stack' => 'required|min:1',
                'max_profit_limit' => 'required|min:1',
                'max_expose_limit' => 'required|min:1',
                'bet_delay' => 'required|min:1',
            ]);

            $response = UserLimits::doLimitUpdate($request->input());

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * User Setting Limit Update
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function limitSettingUpdate(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
                return $response; exit;
            }
            $data = $request;

            if( isset( $data->frm_type ) && isset( $data->frm_type )){

                $uid = $data->uid;
                $updateData = UserSportMarketLimits::where([['uid',$uid]])->first();

                if( $updateData != null ){

                    $dataNew = [
                        'min_stack' => $data->min_stack,
                        'max_stack' => $data->max_stack,
                        'max_profit_limit' => $data->max_profit_limit,
                        'max_expose_limit' => $data->max_expose_limit,
                        'bet_delay' => $data->bet_delay,
                    ];
                    $logTitle = '';
                    if( $data->frm_type == 'cricketFrm1' ){
                        $logTitle = 'Cricket Matchodd';
                        $updateData->cricket_matchodd = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'cricketFrm2' ){
                        $logTitle = 'Cricket Fancy';
                        $updateData->cricket_fancy = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'cricketFrm3' ){
                        $logTitle = 'Cricket Bookmaker';
                        $updateData->cricket_bookmaker = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'footballFrm1' ){
                        $logTitle = 'Football Matchodd';
                        $updateData->football_matchodd = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'footballFrm2' ){
                        $logTitle = 'Football Fancy';
                        $updateData->football_fancy = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'footballFrm3' ){
                        $logTitle = 'Football Bookmaker';
                        $updateData->football_bookmaker = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'tennisFrm1' ){
                        $logTitle = 'Tennis Matchodd';
                        $updateData->tennis_matchodd = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'tennisFrm2' ){
                        $logTitle = 'Tennis Fancy';
                        $updateData->tennis_fancy = json_encode( $dataNew );
                    }elseif ( $data->frm_type == 'tennisFrm3' ){
                        $logTitle = 'Tennis Bookmaker';
                        $updateData->tennis_bookmaker = json_encode( $dataNew );
                    }

                    $updateData->updated_on = date('Y-m-d H:i:s');

                    if( $updateData->save() ){
                        $data = [];
                        $user = DB::table('tbl_user')->where([['id',$uid]])->first();
                        $data['title'] = 'Setting Updated';
                        $data['description'] = 'User ( '. $user->username .' ) setting ' .$logTitle. ' Data : ( '. json_encode( $dataNew ) .' ) updated';
                        $this->addNotification($data);
                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Updated successfully!'
                            ]
                        ];
                    }

                }

            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * User block Unblock
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function userBlockUnblock($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $pUser = Auth::user(); $parentId = $pUser->id;

            if( $pUser->role != 1){
                $where = [['id',$id],['parentId',$parentId],['status',1]];
            }else{
                $where = [['id',$id]];
            }

            $user = DB::table('tbl_user')->where($where)->first();
            if( $user != null ){
                if( $user->role == 5 ){
                    $status = $user->status == 1 ? 0 : 1;
                    $message = $user->status == 1 ? 'Unblock successfully!' : 'Block successfully!' ;
                    DB::table('tbl_user')->where($where)->update(['status'=>$status]);
                }else{
                    $userArr = CommonModel::userChildData($id);
                    $checkStatus = DB::table('tbl_user_block_status')->select(['uid','byuserId'])
                        ->where([['uid',$id],['type',1]])->first();

                    if( $checkStatus != null ){
                        // do unblock
                        if( $checkStatus->byuserId != $parentId ){
                            $message = 'This user blocked by other parent user!';
                        }else{
                            foreach ( $userArr as $usrId ){
                                DB::table('tbl_user_block_status')
                                    ->where([['uid',$usrId],['byuserId',$parentId],['type',1]])
                                    ->delete();
                            }

                            $message = 'Unblock successfully!';
                        }

                    }else{
                        // do block
                        foreach ( $userArr as $usrId ){
                            $userData = ['uid' => $usrId,'byuserId' => $parentId,'type' => 1 ];
                            DB::table('tbl_user_block_status')->insert($userData);
                            DB::table('oauth_access_tokens')->where([['user_id',$usrId]])->delete();
                        }

                        $message = 'Block successfully!';

                    }
                }

                $data['title'] = 'Block/Unblock';
                $data['description'] = 'User ( '. $user->username .' ) '.$message;
                $this->addNotification($data);

                $response = [
                    'status' => 1 , "success" => [ "message" => $message ]
                ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    /**
     * User Lock Unlock
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function userLockUnlock($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $parentId = Auth::id(); $pUser = Auth::user();

            if( $pUser->role != 1 ){
                $where = [['id',$id],['parentId',$parentId],['status',1]];
            }else{
                $where = [['id',$id],['status',1]];
            }

            $user = DB::table('tbl_user')->where($where)->first();

            if( $user != null ){

                $userArr = CommonModel::userChildData($id);

                $checkStatus = DB::table('tbl_user_block_status')->select(['uid','byuserId'])
                    ->where([['uid',$id],['type',2]])->first();

                if( $checkStatus != null ){
                    // do unblock
                    if( $checkStatus->byuserId != $parentId ){
                        $message = 'This user locked by other parent user!';
                    }else{
                        foreach ( $userArr as $usrId ){
                            DB::table('tbl_user_block_status')
                                ->where([['uid',$usrId],['byuserId',$parentId],['type',2]])
                                ->delete();
                        }

                        $message = 'Bet Unlock successfully!';
                    }

                }else{
                    // do block
                    foreach ( $userArr as $usrId ){
                        $userData = ['uid' => $usrId,'byuserId' => $parentId,'type' => 2 ];
                        DB::table('tbl_user_block_status')->insert($userData);
                    }

                    $message = 'Bet Lock successfully!';

                }

                $data['title'] = 'Bet Lock/Unlock';
                $data['description'] = 'User ( '. $user->username .' ) '.$message;
                $this->addNotification($data);

                $response = [
                    'status' => 1 , "success" => [ "message" => $message ]
                ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
