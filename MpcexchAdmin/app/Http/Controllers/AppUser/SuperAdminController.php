<?php
namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use App\Models\SuperAdmin;
use Illuminate\Http\Request;

class SuperAdminController extends Controller
{

    /**
     * Super Admin Manage List
     * Action - Get
     * Created at APR 2020 by dream
     */
    public function manage()
    {
        try{
            $data = SuperAdmin::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Super Admin Create
     * Action - Post
     * Created at APR 2020 by dream
     */
    public function create(Request $request)
    {
        try{
            $response = SuperAdmin::create($request->input());
            if( $response['status'] == 1 ) {
                $data['title'] = 'Create User';
                $data['description'] = 'New super admin ( ' . $request->username . ' ) created';
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
