<?php

namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use App\Models\SuperMaster;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class SuperMasterController extends Controller
{

    /**
     * Super Master Manage List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function manage()
    {
        try{
            $data = SuperMaster::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Super Master Reference List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function reference($id)
    {
        try{
            $data = SuperMaster::getList($id);

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Super Master Create
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function create(Request $request)
    {
        try{
            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
                return $response; exit;
            }
            $response = SuperMaster::create($request->input());
            if( $response['status'] == 1 ) {
                $data['title'] = 'Create User';
                $data['description'] = 'New super master ( ' . $request->username . ' ) created';
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
