<?php

namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use App\Models\Supervisor;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class SupervisorController extends Controller
{

    /**
     * Supervisor Manage List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function manage()
    {
        try{
            $data = Supervisor::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Supervisor Create
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function create(Request $request)
    {
        try{
            $response = Supervisor::create($request->input());
            if( $response['status'] == 1 ) {
                $data['title'] = 'Create User';
                $data['description'] = 'New supervisor ( ' . $request->username . ' ) created';
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
