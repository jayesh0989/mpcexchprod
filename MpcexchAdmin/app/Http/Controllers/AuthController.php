<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        try{
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
//            if( !$this->checkOrigin() ){
//                return response()->json($response);
//            }

            $credentials = [
                'username' => $request->username,
                'password' => $request->password,
            ];

            $setting = DB::table('tbl_common_setting')->select('value')
                ->where([['key_name','SITE_MAINTENANCE_MODE'],['status',1]])->first();

            if ( $setting != null ) {
                $data = json_decode($setting->value, 16);
                if( isset($data['status']) && trim($data['status']) == '0' ){
                    $response = [
                        'status' => 0,
                        'error' => [ 'message' => trim($data['message']) ]
                    ];
                    return response()->json($response);
                }
            }

            if (auth()->attempt($credentials)) {

                //new captcha
//                if ( strlen($request->recaptcha) != 5 ) {
//                    $response = [
//                        'status' => 0,
//                        'error' => [ 'message' => 'Captcha verification failed ! Please try again.' ]
//                    ];
//                    return response()->json($response); exit;
//                }

                /*                $ipAddress = $this->getClientIp();
                                $captcha = DB::table('tbl_captcha')->select(['code','created_on'])
                                    ->where([['code', $request->recaptcha],['ip_address',$ipAddress ]])->first();
                                if ( $captcha != null) {
                //                    $cTime = strtotime( date('Y-m-d H:i:s') );
                //                    $createdOn = strtotime( trim($captcha->created_on) );
                //                    if( ( $cTime-$createdOn ) > 300 ){
                //                        $response = [
                //                            'status' => 0,
                //                            'error' => [ 'message' => 'Something wrong ! Please page refresh and try again.' ]
                //                        ];
                //                        return response()->json($response);
                //                    }else{
                                        DB::table('tbl_captcha')
                                            ->where([['code', $request->recaptcha],['ip_address',$ipAddress ]])->delete();
                //                    }
                                }else{
                                    $response = [
                                        'status' => 0,
                                        'error' => [ 'message' => 'Something wrong ! Please page refresh and try again.' ]
                                    ];
                                    return response()->json($response);
                                }
                */
                /*google captcha
                 * $secret = '6LeKDPcUAAAAAJ_p3One25jIFOsKBMKzCvXr4axD';
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$request->recaptcha);
                $responseData = json_decode($verifyResponse);
                if(!$responseData->success)
                {
                    $response = [
                        'status' => 0,
                        'error' => [ 'message' => 'Robot verification failed, please try again.' ]
                    ];
                    $this->logout();
                    return response()->json($response);
                }*/

                $token = auth()->user()->createToken('TutsForWeb')->accessToken;
                $userData = auth()->user();

                $checkBlock = DB::table('tbl_user_block_status')->where([['uid',$userData->id],['type',1]])->first();

                if( $userData->status != 1 ){

                    $message = 'This user is not active !!';

                    if( $userData->status == 2 ){
                        $message = 'This user is already deleted !!';
                    }

                    $this->logout();
                    $response = [
                        'status' => 0,
                        'error' => [ 'message' => $message ]
                    ];

                    return response()->json($response);

                }

                if( $checkBlock != null || !in_array($userData->role,[1,2,3,5,6]) || $userData->systemId != $request->systemId ){

                    $this->logout();
                    $message = 'Invalid User For This System !!';
                    if( $checkBlock != null ){
                        $message = 'This user is blocked by parent!!';
                    }

                    $response = [
                        'status' => 0,
                        'error' => [ 'message' => $message ]
                    ];

                    return response()->json($response);

                }

                $details = [
                    'token' => $token,
                    'systemId' => $userData->systemId,
                    'auth_id' => $userData->id,
                    'name' => $userData->name,
                    'username' => $userData->username,
                    'role' => $userData->roleName,
                    'session_expired_time' => 300000,
                    'last_logged_on' => $userData->last_logged_on,
                    'is_password_updated' => $userData->is_password_updated
                ];

                DB::table('tbl_user')->where([['id',$userData->id]])->update(['is_login' => 1]);

                $response = [
                    'status' => 1,
                    'data' => $details,
                    'success' => [
                        'message' => 'Logged In successfully!'
                    ]
                ];

                $data['title'] = 'Login';
                $data['description'] = 'Login Activity';
                $this->addNotification($data);

                return response()->json($response, 200);


            } else {

                $response = [
                    'status' => 0,
                    'error' => [ 'message' => 'Incorrect username or password !' ]
                ];

                return response()->json($response);
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }
    /**
     * Handles siteMode Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function siteMode()
    {
        try{
            if (isset($_GET['captcha'])) {
                $ipAddress = $this->getClientIp();
                DB::table('tbl_captcha')
                    ->where([['code', $_GET['captcha']],['ip_address',$ipAddress]])->delete();
            }
            $apkUrl = 'https://adminapi.mpcexch.com/download/'; $apkName = '';
            if (isset($_GET['optId'])) {
                $systemArr = [
                    '1' => 'AdminMpcExch',
                    '10001' => 'AdminBetexch'
                ];
                $apkUrl = $apkUrl.$systemArr[$_GET['optId']].'.apk';
                $apkName = $systemArr[$_GET['optId']];
            }

            $setting = DB::table('tbl_common_setting')->select('value')
                ->where([['key_name','SITE_MAINTENANCE_MODE'],['status',1]])->first();

            if ( $setting != null ) {
                $data = json_decode($setting->value,16);

                $dataNew = [
                    'siteMode' => trim($data['status']),
                    'message' => trim($data['message']),
                ];

                if( trim($data['status']) == 1 ){
                    $randomKey = rand(12345,98765);
                    $ipAddress = $this->getClientIp();
                    DB::table('tbl_captcha')
                        ->insert(['code' => $randomKey,'created_on' => date('Y-m-d H:i:s'),'status' => 1,'ip_address' => $ipAddress]);
                    $dataNew['captcha'] = $randomKey;
                }

                $dataNew['apkUrl'] = $apkUrl;
                $dataNew['apkName'] = $apkName;

                $response = [ 'status' => 1, 'data' => $dataNew ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'status' => 0,
                    'error' => [ 'message' => 'Something want wrong !' ]
                ];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Handles change pass Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changePassword(Request $request)
    {
//        if( !$this->checkOrigin() ){
//            $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad Request !!' ] ];
//            return response()->json($response);
//        }
        if( strlen($request->password2) >= 6
            && $request->password1 == $request->password2
            && $request->password != $request->password2 ){

            // $uppercase = preg_match('@[A-Z]@', $request->password2);
            // $number    = preg_match('@[0-9]@', $request->password2);

            // if( !$uppercase || !$number ) {
            //     $response = [
            //         'status' => 0,
            //         'error' => [ 'message' => 'Password must like this format. example : Xyz$@123' ]
            //     ];
            //     return response()->json($response);
            // }

            $credentials = [
                'username' => $request->username,
                'password' => $request->password,
            ];

            if (auth()->attempt($credentials)) {
                $userData = auth()->user();
                $user = User::find($userData->id);
                if( $user != null ){
                    $user->password = bcrypt( $request->password2 );
                    $user->is_password_updated = 1;
                    if( $user->save() ){
                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Change Password successfully!'
                            ]
                        ];
                    }
                }
                return response()->json($response, 200);

            } else {
                $response = [
                    'status' => 0,
                    'error' => [ 'message' => 'Something wrong! Please try again !' ]
                ];
                return response()->json($response);
            }

        }else{
            $message = 'Both password did not match !!';
            if( strlen($request->password2) < 6){
                $message = 'password length must be minimum 6 char !!';
            }
            if( $request->password == $request->password2 ){
                $message = 'old password and new password must be different !!';
            }
            $response = [
                'status' => 0,
                'error' => [ 'message' => $message ]
            ];

            return response()->json($response);

        }

    }

    /**
     * Handles Logout Request
     *
     * @return JsonResponse
     */
    public function logout()
    {
        $id = auth()->id();
        DB::table('tbl_user')->where('id',$id)->update(['is_login' => 0]);
        $data['title'] = 'Logout';
        $data['description'] = 'Logout Activity';
        $this->addNotification($data);
        return response()->json(['status' => 1, 'success' => ["message" => "Successfully logged out!"]]);
    }

    /**
     * Returns Authenticated User Details
     *
     * @return JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }
}
