<?php

namespace App\Http\Controllers\Casino;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ParentTransactionController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }
    /**
     * action Bet
     */
    public function actionTransaction()
    {
        try {
            $mongodb = DB::connection('mongodb');
            $conn = DB::connection('mysql');
            $conn3 = DB::connection('mysql3');

            $casinoTransData = $mongodb->table('tbl_casino_transaction')
                ->distinct()->select('round')->where([['round_closed',1]])->get();

            if ($casinoTransData->isNotEmpty()) {
                $marketIdsArr = [];
                foreach ($casinoTransData as $round) {
                    if( !in_array($round . '-B',$marketIdsArr) ){
                        $marketIdsArr[] = $round . '-B';
                    }
                    if( !in_array($round . '-W',$marketIdsArr) ){
                        $marketIdsArr[] = $round . '-W';
                    }

                    $mongodb->table('tbl_casino_transaction')
                        ->where([['round',$round]])->update(['round_closed'=>3]);
                }

                if( $marketIdsArr != null ){
                    foreach ( $marketIdsArr as $marketId ){
                        $newUserArr = [];
                        $transTbl = $this->currentTable('tbl_transaction_client');
                        $transClientData = $conn3->table($transTbl)
                            ->where([['status', 1],['mid',$marketId]])->get();
                        if ($transClientData->isNotEmpty()) {
                            $newUserArr = [];
                            foreach ($transClientData as $transData) {
                                $userId = $transData->userId;
                                $type = $transData->type;
                                $amount = $transData->amount;
                                $sid = $transData->sid;
                                $eid = $transData->eid;
                                $mid = $transData->mid;
                                $result = $transData->result;
                                $eType = $transData->eType;
                                $mType = $transData->mType;
                                $description = $transData->description;

                                if ($type == 'CREDIT') { $pType = 'DEBIT'; } else { $pType = 'CREDIT'; }

                                $parentUserData = $conn->table('tbl_user_profit_loss as pl')
                                    ->select(['pl.userId as userId', 'pl.parentId as parentId', 'pl.actual_profit_loss as apl', 'pl.profit_loss as gpl', 'ui.balance as balance', 'ui.pl_balance as pl_balance'])
                                    ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
                                    ->where([['pl.clientId', $userId], ['pl.actual_profit_loss', '!=', 0]])->orderBy('pl.userId', 'desc')->get();

                                $childId = $userId;

                                foreach ($parentUserData as $user) {
                                    $cUser = $conn->table('tbl_user')
                                        ->select(['systemId','parentId','role'])
                                        ->where([['id',$user->userId]])->first();
                                    $systemId = $cUser->systemId;
                                    $parentId = $cUser->parentId;
                                    $role = $cUser->role;
                                    $tAmount = ($amount * $user->apl) / 100;
                                    $pTAmount = ($amount * (100 - $user->gpl)) / 100;
                                    $cTAmount = ($amount * ($user->gpl - $user->apl)) / 100;

                                    if (isset($newUserArr[$user->userId])) {

                                        if ($pType != 'CREDIT') {
                                            $tAmount = (-1) * $tAmount;
                                            $pTAmount = (-1) * $pTAmount;
                                            $cTAmount = (-1) * $cTAmount;
                                        }

                                        $tAmount = $newUserArr[$user->userId]['tAmount'] + $tAmount;
                                        $pTAmount = $newUserArr[$user->userId]['pTAmount'] + $pTAmount;
                                        $cTAmount = $newUserArr[$user->userId]['cTAmount'] + $cTAmount;

                                        $newUserArr[$user->userId]['tAmount'] = $tAmount;
                                        $newUserArr[$user->userId]['pTAmount'] = $pTAmount;
                                        $newUserArr[$user->userId]['cTAmount'] = $cTAmount;

                                    } else {

                                        if ($pType != 'CREDIT') {
                                            $tAmount = (-1) * $tAmount;
                                            $pTAmount = (-1) * $pTAmount;
                                            $cTAmount = (-1) * $cTAmount;
                                        }

                                        $newUserArr[$user->userId] = [
                                            'role' => $role,
                                            'systemId' => $systemId,
                                            'clientId' => $userId,
                                            'userId' => $user->userId,
                                            'childId' => $childId,
                                            'parentId' => $parentId,
                                            'tAmount' => $tAmount,
                                            'pTAmount' => $pTAmount,
                                            'cTAmount' => $cTAmount,
                                            'sid' => $sid,
                                            'eid' => $eid,
                                            'mid' => $mid,
                                            'result' => $result,
                                            'eType' => $eType,
                                            'mType' => $mType,
                                            'description' => $description,
                                        ];
                                    }
                                    $childId = $user->userId;
                                }

                            }
                        }
                        if ($newUserArr != null) {
                            foreach ($newUserArr as $userData) {
                                if ($userData['tAmount'] < 0) {
                                    $pType = 'DEBIT';
                                    $amount = (-1) * round($userData['tAmount'], 2);
                                    $pAmount = (-1) * round($userData['pTAmount'], 2);
                                    $cAmount = (-1) * round($userData['cTAmount'], 2);
                                } else {
                                    $pType = 'CREDIT';
                                    $amount = round($userData['tAmount'], 2);
                                    $pAmount = round($userData['pTAmount'], 2);
                                    $cAmount = round($userData['cTAmount'], 2);
                                }

                                $this->summaryUpdate($userData['userId'], $amount, $pAmount, $pType);
                                $balance = $this->currentBalance($userData['userId'], $amount, $pType, 'PARENT');
                                $resultArr = [
                                    'systemId' => $userData['systemId'],
                                    'clientId' => $userData['clientId'],
                                    'userId' => $userData['userId'],
                                    'childId' => $userData['childId'],
                                    'parentId' => $userData['parentId'],
                                    'sid' => $userData['sid'],
                                    'eid' => $userData['eid'],
                                    'mid' => $userData['mid'],
                                    'result' => $userData['result'],
                                    'eType' => $userData['eType'],
                                    'mType' => $userData['mType'],
                                    'type' => $pType,
                                    'amount' => $amount,
                                    'p_amount' => $pAmount,
                                    'c_amount' => $cAmount,
                                    'balance' => $balance == null ? 0 : $balance,
                                    'description' => $userData['description'],
                                    'remark' => 'Transactional Entry',
                                    'status' => 1,
                                    'created_on' => date('Y-m-d H:i:s'),
                                    'updated_on' => date('Y-m-d H:i:s')
                                ];

                                if ($userData['role'] == 1) {
                                    $tbl = 'tbl_transaction_admin';
                                } else {
                                    $tbl = 'tbl_transaction_parent';
                                }
                                $transTbl = $this->currentTable($tbl);
                                if ($conn3->table($transTbl)->insert($resultArr)) {
                                    // do something
                                }

                            }
                        }
                    }
                }
            }

            // Update Pending Transaction closed
            $start = Carbon::now()->subHours(2)->format('Y-m-d H:i:s');
            $end = Carbon::now()->subHours(1)->format('Y-m-d H:i:s');

            $mongodb->table('tbl_casino_transaction')
                ->where([['round_closed',0]])->whereBetween('updated_on',[$start,$end])
                ->update(['round_closed' => 1]);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    //Summary Update
    public function summaryUpdate($userId,$ownPl,$parentPl,$type){

        try {
            $conn = DB::connection('mysql');
            $userSummary = $conn->table('tbl_settlement_summary')
                ->where([['status',1],['uid',$userId]])->first();
            if( $userSummary != null ){

                $updatedOn = date('Y-m-d H:i:s');
                if( isset( $type ) && $type != 'CREDIT' ){
                    $ownPl = (-1)*round($ownPl,2);
                    $parentPl = (-1)*round($parentPl,2);
                }

                $updateArr = [
                    'ownPl' => $conn->raw('ownPl + ' . $ownPl),
                    'parentPl' => $conn->raw('parentPl + ' . $parentPl),
                    'updated_on' => $updatedOn
                ];

                if( $conn->table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$userId]])->update($updateArr) ){
                }else{
                    $userSummary = $conn->table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$userId]])->first();
                    $updatedOn = date('Y-m-d H:i:s');
                    $betTimeDiff = (strtotime($updatedOn)-strtotime($userSummary->updated_on) );
                    if($betTimeDiff == 0) {
                        usleep(1000);
                        $userSummary = $conn->table('tbl_settlement_summary')
                            ->where([['status',1],['uid',$userId]])->first();
                    }
                    $updateArr = [
                        'ownPl' => $userSummary->ownPl+$ownPl,
                        'parentPl' => $userSummary->parentPl+$parentPl,
                        'updated_on' => date('Y-m-d H:i:s')
                    ];
                    if( $conn->table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$userId]])->update($updateArr) ){
                        //do something
                    }
                }

            }else{
                if( isset( $type ) && $type != 'CREDIT' ){
                    $ownPl = (-1)*round($ownPl,2);
                    $parentPl = (-1)*round($parentPl,2);
                }

                $insertArr = [  'uid' => $userId, 'updated_on' => date('Y-m-d H:i:s'),
                    'ownPl' => $ownPl, 'ownComm' => 0,
                    'parentPl' => $parentPl,'parentComm' => 0 ];

                if( $conn->table('tbl_settlement_summary')->insert($insertArr) ){
                    //do something
                }

            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    // get Current Balance
    public function currentBalance($userId,$amount,$type,$role)
    {
        try {
            $conn = DB::connection('mysql');
            if( $type != 'CREDIT' ){ $amount = (-1)*$amount; }
            $updated_on = date('Y-m-d H:i:s');
            $updateArr = [
                'pl_balance' => $conn->raw('pl_balance + ' . $amount),
                'updated_on' => $updated_on
            ];

            if( $conn->table('tbl_user_info')->where([['uid',$userId]])->update($updateArr) ){
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                $balance = $cUserInfo->balance;
                $plBalance = $cUserInfo->pl_balance;
                if( $role != 'CLIENT' ){
                    return round( ( $plBalance ),2);
                }else{
                    return round( ( $balance + $plBalance ),2);
                }
            }else{
                $updated_on = date('Y-m-d H:i:s');
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                $betTimeDiff = (strtotime($updated_on)-strtotime($cUserInfo->updated_on) );
                if($betTimeDiff == 0) {
                    usleep(1000);
                    $cUserInfo = $conn->table('tbl_user_info')
                        ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                }

                $balance = $cUserInfo->balance;
                $newPlBalance = $cUserInfo->pl_balance;

                $updateArr = [
                    'pl_balance' => $newPlBalance,
                    'updated_on' => $updated_on
                ];

                if( $conn->table('tbl_user_info')->where([['uid', $userId]])->update($updateArr) ){
                    if( $role != 'CLIENT' ){
                        return round( ( $newPlBalance ),2);
                    }else{
                        return round( ( $balance + $newPlBalance ),2);
                    }
                }
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
