<?php

namespace App\Http\Controllers\Casino;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class TransactionsController extends Controller
{

    /**
     * action Insert
     */
    public function actionInsert(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            if( isset($request->user) && isset($request->amount) && isset($request->balance)
            && isset($request->request_uuid) && isset($request->round) && isset($request->transaction_uuid)
            && isset($request->rolled_back)){
                $user = DB::connection('mongodb')->table('tbl_casino_user')
                    ->where([['username',trim($request->user)]])->first();
                if( $user != null ){
                    $insertArr = [
                        'uid' => $user['uid'],
                        'username' => $user['username'],
                        'request_uuid' => $request->request_uuid,
                        'transaction_uuid' => $request->transaction_uuid,
                        'transaction_type' => $request->transaction_type,
                        'round' => $request->round,
                        'reference_transaction_uuid' => $request->reference_transaction_uuid,
                        'amount' => (int)$request->amount/100000,
                        'balance' => (int)$request->balance/100000,
                        'rolled_back' => $request->rolled_back,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'status' => 1
                    ];
                    if( DB::connection('mongodb')->table('tbl_casino_transaction')->insert($insertArr) ){
                        $response = [ 'status' => 'ok' ];
                    }else{
                        $response = [ 'status' => 'None' ];
                    }
                }else{
                    $response = [ 'status' => 'None' ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Transaction
     */
    public function actionTransaction(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            if( isset($request->transaction_uuid) ){
                $transaction = DB::connection('mongodb')->table('tbl_casino_transaction')
                    ->where([['transaction_uuid',trim($request->transaction_uuid)]])->first();
                if( $transaction != null ){
                    $response = [
                        'transaction_uuid' => trim($transaction['transaction_uuid']),
                        'reference_transaction_uuid' => trim($transaction['reference_transaction_uuid']),
                        'round' => trim($transaction['round']),
                        'amount' => (int)$transaction['amount']*100000,
                        'balance' => (int)$transaction['balance']*100000
                    ];
                }else{
                    $response = [
                        'transaction_uuid' => 'None',
                        'round' => 'None',
                        'amount' => 'None',
                        'balance' => 'None'
                    ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Transaction Uid
     */
    public function actionTransactionUid(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            if( isset($request->transaction_uuid) ){
                $transaction = DB::connection('mongodb')->table('tbl_casino_transaction')
                    ->where([['transaction_uuid',trim($request->transaction_uuid)]])->first();
                if( $transaction != null ){
                    $response = [ 'transaction_uuid' => true ];
                }else{
                    $response = [ 'transaction_uuid' => 'None' ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Balance
     */
    public function actionBalance(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            if( isset($request->request_uuid) ){
                $transaction = DB::connection('mongodb')->table('tbl_casino_transaction')
                    ->where([['request_uuid',trim($request->request_uuid)]])->first();
                if( $transaction != null ){
                    $response = [ 'balance' => (int)$transaction['balance']*100000 ];
                }else{
                    $response = [ 'balance' => 'None' ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Amount
     */
    public function actionAmount(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            if( isset($request->transaction_uuid) ){
                $transaction = DB::connection('mongodb')->table('tbl_casino_transaction')
                    ->where([['transaction_uuid',trim($request->transaction_uuid)]])->first();
                if( $transaction != null ){
                    $response = [ 'amount' => (int)$transaction['amount']*100000, 'transaction_type' => $transaction['transaction_type'], 'rolled_back' => $transaction['rolled_back'] ];
                }else{
                    $response = [ 'amount' => 'None', 'transaction_type' => 'None', 'rolled_back' => 'None' ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

    /**
     * action Update Rollback
     */
    public function actionUpdateRollback(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
        try{
            if( isset($request->transaction_uuid) ){
                $transaction = DB::connection('mongodb')->table('tbl_casino_transaction')
                    ->where([['transaction_uuid',trim($request->transaction_uuid)]])->first();
                if( $transaction != null ){
                    if( DB::connection('mongodb')->table('tbl_casino_transaction')
                        ->where([['transaction_uuid',trim($request->transaction_uuid)]])
                        ->update(['rolled_back' => 1]) ){
                        $response = [ 'status' => 'ok' ];
                    }else{
                        $response = [ 'status' => 'None' ];
                    }
                }else{
                    $response = [ 'status' => 'None' ];
                }
            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

}
