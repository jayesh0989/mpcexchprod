<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $whiteListIps = [];

    /**
     * checkOrigin
     */
    public function checkUserChild($uid)
    {
        $cUser = Auth::user(); $cUserId = $cUser->id; $isChild = false;
        if( $cUser->roleName = 'ADMIN' ){
            $isChild = true;
            return $isChild;
        }
        $cUserChild = DB::table('tbl_user_child_data')->select(['super_masters', 'masters', 'clients'])
            ->where('uid', $cUserId)->first();

        if( $cUserChild != null ){
            $smArr = json_decode($cUserChild->super_masters, 16);
            $mArr = json_decode($cUserChild->masters, 16);
            $cArr = json_decode($cUserChild->clients, 16);
            if ( ( $smArr != null && in_array($uid, $smArr) ) ) {
                $isChild = true;
            }
            if ( ( $mArr != null && in_array($uid, $mArr) ) ) {
                $isChild = true;
            }
            if ( ( $cArr != null && in_array($uid, $cArr) ) ) {
                $isChild = true;
            }
        }
        return $isChild;
    }
    /**
     * checkOrigin
     */
    public function checkOrigin()
    {
        $removeChar = ["https://", "http://", "/"];
        $allowedOrigins = ['34.254.28.80','localhost:10001'];
        if( isset( $_SERVER['HTTP_ORIGIN'] ) ){
            $origin = $_SERVER['HTTP_ORIGIN'];
            $origin = str_replace($removeChar, '', $origin);
            if (in_array($origin, $allowedOrigins)) { return true; }
        }elseif( isset( $_SERVER['SERVER_NAME'] ) ){
            $origin = $_SERVER['SERVER_NAME'];
            if (in_array($origin, $allowedOrigins)) { return true; }
        }else{
            return false;
        }
        return false;
    }

    /**
     * Decrypt data from a CryptoJS json encoding string
     */
    public function cryptoJsAesDecrypt($jsonString){
        $passphrase = 'ca0761c0b720b9ce1c93183d8f03d854';
        $jsondata = json_decode($jsonString, true);
        // $jsondata = $jsondata["data"];

        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, 16);
    }

    /**
     * Encrypt value to a cryptojs compatiable json encoding string
     */
    public function cryptoJsAesEncrypt($value){
        $passphrase = 'ca0761c0b720b9ce1c93183d8f03d854';
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx.$passphrase.$salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv  = substr($salted, 32,16);
        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return json_encode(['data' => $data]);
    }

    /**
     * errorLog
     */
    public function errorLog($e)
    {
        $uid = Auth::id();
        $message = 'Error: '.$e->getMessage().' | File: '.$e->getFile().' | Line: '.$e->getLine().' | User ID: '.$uid;
        Log::channel('daily-log')->info($message);
        return [ 'status' => 0, 'error' => [ 'message' => $message ] ];
    }

    /**
     * activityLog
     */
    public function activityLog($message)
    {
        Log::channel('activity-log')->info($message);
    }

    /**
     * Game Recall Backup Logs
     */
    public function gameRecallBackupLogs($data)
    {
        if( isset($data['title']) && isset($data['data']) ){
            $updateArr = [
                'title' => $data['title'],
                'data' => $data['data'],
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $this->getClientIp()
            ];
            $message = json_encode($updateArr);
            Log::channel('activity-log')->info($message);

        }

    }

    /**
     * Notification
     */
    public function addNotification($data)
    {
        if( isset($data['title']) && isset($data['description']) ){
            $user = Auth::user();
            $description = $data['description'].' by '.$user->username;
            $updateArr = [
                'uid' => $user->id,
                'systemId' => $user->systemId,
                'title' => $data['title'],
                'description' => $description,
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $this->getClientIp()
            ];
            $message = json_encode($updateArr);
            Log::channel('activity-log')->info($message);

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['status', 1], ['key_name', 'ACTIVITY_LOG_USER_ID']])->first();
            if ($setting != null) {
                $userArr = json_decode($setting->value);
                if( $userArr == null ){ $userArr = [1]; }
            } else { $userArr = [1]; }

            if( in_array($user->id,$userArr) ){
                DB::table('tbl_user_activity_log')->insert($updateArr);
                //return;
            }
            //return;
        }

    }

    /**
     * getClientIp
     */
    public function getClientIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
