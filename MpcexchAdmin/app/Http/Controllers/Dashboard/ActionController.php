<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\Sport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class ActionController extends Controller
{

    /**
     * actionBetDeleteByBetId
     * Action - Get
     * Created at DEC 2020
     */
    public function actionBetDeleteByBetId()
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        return response()->json($response, 200);
        try{
            $conn = DB::connection('mongodb');
            if( isset( $_GET['betId'] ) ){

                $betId = $_GET['betId'];
                $tbl = 'tbl_bet_pending_5';
                if( $betId >= 10000000 && $betId < 20000000 ){
                    $tbl = 'tbl_bet_pending_1';
                }elseif ( $betId >= 20000000 && $betId < 30000000 ){
                    $tbl = 'tbl_bet_pending_2';
                }elseif ( $betId >= 30000000 && $betId < 40000000 ){
                    $tbl = 'tbl_bet_pending_3';
                }elseif ( $betId >= 40000000 && $betId < 50000000 ){
                    $tbl = 'tbl_bet_pending_4';
                }elseif ( $betId >= 50000000 && $betId < 60000000 ){
                    $tbl = 'tbl_bet_pending_5';
                }

                $betData = $conn->table($tbl)->where([ ['id',$betId],['status',1],['result','PENDING'] ])->first();

                if( $betData != null ){

                    if( $conn->table($tbl)->where([ ['id',$betId],['status',1],['result','PENDING'] ])->update(['status' => 2]) ){

                        $uid = $betData->uid;
                        $marketId = $betData->mid;

                        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

                        $getNewExpose = $this->checkExposeBalance($betData);

                        if(!empty($getNewExpose)) {

                            $userExpose = $conn->table('tbl_user_market_expose')
                                ->where([['uid',$uid],['status',1],['mid','!=',$marketId]])->sum("expose");

                            if(!empty($userExpose)){
                                if( $userExpose < 0 ){
                                    $oldMarketExpose = (-1)*($userExpose);
                                }else{
                                    $oldMarketExpose = $userExpose;
                                }
                            }

                            $newMarketExpose = $getNewExpose['expose'];
                            $newMarketProfit = $getNewExpose['profit'];

                            if( $newMarketExpose < 0 ){
                                $newMarketExpose = (-1)*$newMarketExpose;
                                $exposeBalance = $oldMarketExpose+$newMarketExpose;
                            }else{
                                $exposeBalance = $oldMarketExpose+$newMarketExpose;
                            }

                            $updateArr = ['expose' => $exposeBalance,'updated_on' => date('Y-m-d H:i:s')];
                            if( DB::table('tbl_user_info')->where('uid',$uid)->update($updateArr) ){

                                $conn->table('tbl_user_market_expose')->where([['uid',$uid],['status',1],['mid',$marketId]])
                                    ->update(['expose' => $newMarketExpose , 'profit' => $newMarketProfit]);

//                                $data['title'] = 'BetDelete';
//                                $data['description'] = 'BetId: '.$betId.' bet deleted';
//                                $this->addNotification($data);
                            }

                        }

                    }

                }

                $response = [ 'status' => 1, 'success' => [ 'message' => 'Bet Deleted Successfully!' ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Search User Data from username
     * Action - Post
     * Created at DEC 2020
     */
    public function searchUserData(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( !isset( $request->username ) ){
                return response()->json($response, 200);
            }

            $cUser = Auth::user();
            if( in_array($cUser->role,[2,3,5])){
                $where = [['systemId',$cUser->systemId],['parentId',$cUser->id],['username', trim($request->username)],['status','!=',2]];
            }else{
                if($cUser->roleName == 'ADMIN' || $cUser->roleName == 'SA'){
                    $where = [['username', trim($request->username)],['status','!=',2]];
                }else{
                    $where = [['systemId',$cUser->systemId],['username', trim($request->username)],['status','!=',2]];
                }

            }
            $user = DB::table('tbl_user')
                //->select(['id','name','username','roleName'])
                ->where($where)->whereIn('role',[2,3,4])->first();

            if( $user != null ){

                $userData = DB::table('tbl_user_info')
                    ->select(['balance','pl','pl_balance','pName','expose'])
                    ->where([['uid',$user->id]])->first();

                if( $userData != null ){

                    $isLock = $isBlock = 1;
                    $status1 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $user->id], ['type', 1]])->first();
                    if ($status1 != null) {
                        $isBlock = 0;
                    }

                    $status2 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $user->id], ['type', 2]])->first();
                    if ($status2 != null) {
                        $isLock = 0;
                    }
                    $isBlockArr = $isLockArr = [];
                    $isBlockArr = [
                        'text' => $isBlock == 1 ? 'Unblock' : 'Block',
                        'class1' => $isBlock == 1 ? 'success' : 'danger',
                        'class2' => $isBlock == 1 ? 'mdi-check' : 'mdi-close',
                    ];
                    $isLockArr = [
                        'text' => $isLock == 1 ? 'Bet Unlock' : 'Bet Lock',
                        'class1' => $isLock == 1 ? 'success' : 'danger',
                        'class2' => $isLock == 1 ? 'mdi-toggle-switch' : 'mdi-toggle-switch-off',
                    ];
                    $reference = [];
                    $reference['cCount'] = CommonModel::getChildCount($user->id,4);
                    $reference['mCount'] = CommonModel::getChildCount($user->id,3);
                    if( $user->roleName == 'SM1' ){
                        $reference['smCount'] = CommonModel::getChildCount($user->id,2);
                    }

                    $data = [
                        'uid' => $user->id,
                        'role' => $user->roleName,
                        'name' => $user->name . ' [ '.$user->username.' ]',
                        'pName' => $userData->pName,
                        'pl' => $userData->pl,
                        'balance' => round($userData->balance),
                        'pl_balance' => round($userData->pl_balance),
                        'expose' => round($userData->expose),
                        'available' => round($userData->balance)+round($userData->pl_balance)-round($userData->expose),
                        'isBlock' => $isBlockArr,
                        'isLock' => $isLockArr,
                        'reference' => $reference
                    ];

                }else{
                    $data = [
                        'name' => $user->name . ' [ '.$user->username.' ]',
                        'last_logged_on' => $user->last_logged_on,
                        'role' => $user->roleName,
                        'uid' => $user->id
                    ];
                }

                $response = [ 'status' => 1 , "data" => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * get User Data
     * Action - Get
     * Created at DEC 2020
     */
    public function getUserData()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
//            if( !$this->checkOrigin() ){
//                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad Request !!' ] ];
//                return response()->json($response);
//            }
            if( auth()->user() ){

                $user = auth()->user();
                if( $user->role == 6 ){
                    $user->id = 1;
                }

//                $userData = DB::table('tbl_user_info')
//                    ->select(['balance','pl_balance'])
//                    ->where([['uid',$user->id]])->first();
//
//                if( $userData != null ){
//                    $adminSettlementAmount = 0;
//                    if( $user->roleName == 'ADMIN' || $user->roleName == 'SA' ){
//                        $adminSettlement = DB::table('tbl_transaction_admin_settlement')
//                            ->where([['status', 1]])->sum('amount');
//                        if ( $adminSettlement != null) {
//                            $adminSettlementAmount = round($adminSettlement);
//                        }
//                    }

                // Detail API call Manage
                $apiCallTiming = ['detail' => 10000,'betList' => 8000,'odds' => 1000];
                if( $user->roleName == 'ADMIN' ){
                    $apiCallTiming = ['detail' => 5000,'betList' => 3000,'odds' => 1000];
                }elseif ( $user->roleName == 'ADMIN2' ){
                    $apiCallTiming = ['detail' => 5000,'betList' => 3000,'odds' => 1000];
                }elseif ( $user->roleName == 'SM' || $user->roleName == 'SM2' || $user->roleName == 'M' ){
                    $apiCallTiming = ['detail' => 5000,'betList' => 3000,'odds' => 1000];
                }elseif ( $user->roleName == 'SA' || $user->roleName == 'SV'){
                    $apiCallTiming = ['detail' => 7000,'betList' => 5000,'odds' => 1000];
                }

                $data = [
                    'name' => $user->username,
                    'last_logged_on' => $user->last_logged_on,
                    // 'balance' => round($userData->balance),
                    // 'pl_balance' => round($userData->pl_balance)-$adminSettlementAmount,
                    'role' => $user->roleName,
                    'uid' => $user->id,
                    'api_call_timing' => $apiCallTiming
                ];

                if( $user->roleName == 'ADMIN' || $user->roleName == 'SA' ){
//                        $cUserSummaryData = DB::table('tbl_settlement_summary')
//                            ->select(['parentPl','parentComm','parentCash'])
//                            ->where([['pid',$user->id],['status',1],['systemId','!=',1]])->get();
//                        $ownPl = 0;
//                        if( $cUserSummaryData->isNotEmpty() ) {
//                            foreach ($cUserSummaryData as $cUserSummary) {
//                                $ownPl = $ownPl + round($cUserSummary->parentPl + $cUserSummary->parentComm, 2);
//                            }
//                        }
//                        $data['w_pl_balance'] = round($ownPl);

                    $activityLogCount = DB::table('tbl_user_activity_log')
                        ->where([['is_seen',0]])->count();
                    if( $activityLogCount >= 100 ){
                        $activityLogCount = '99+';
                    }
                    $data['activityLogCount'] = $activityLogCount;

                }

//                }else{
//                    // Detail API call Manage
//                    $apiCallTiming = ['detail' => 10000,'betList' => 8000,'odds' => 1000];
//                    if ( $user->roleName == 'SA' || $user->roleName == 'SV'){
//                        $apiCallTiming = ['detail' => 7000,'betList' => 5000,'odds' => 1000];
//                    }
//                    $data = [
//                        'name' => $user->name,
//                        'last_logged_on' => $user->last_logged_on,
//                        'role' => $user->roleName,
//                        'uid' => $user->id,
//                        'api_call_timing' => $apiCallTiming
//                    ];
//                }

                $commentary = 'This is global commentary!';
                $commentaryData = DB::table('tbl_common_setting')
                    ->select(['value'])->where([['key_name','GLOBAL_COMMENTARY'],['status',1]])->first();

                if( $commentaryData != null ){
                    $commentary = $commentaryData->value;
                }

                $lastcommentary = DB::table('tbl_global_commentary')->select('commentary')
                    ->orderBy('id','desc')->limit(3)->get();
                $lastcomm = 'No last data!';
                if( $lastcommentary->isNotEmpty() ){
                    $lastcomm = '<ul class="p-2 mb-0">';
                    foreach ( $lastcommentary as $cdata ){
                        $lastcomm .= '<li>'.$cdata->commentary.'</li>';
                    }
                    $lastcomm .= '</ul>';
                }

                $data['commentary'] = $commentary;
                $data['lastcomm'] = $lastcomm;

                $response = [ 'status' => 1 , "data" => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * get User Balance
     * Action - Get
     * Created at DEC 2020
     */
    public function getUserBalance()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
//            if( !$this->checkOrigin() ){
//                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad Request !!' ] ];
//                return response()->json($response);
//            }
            $data = [ 'balance' => 0, 'pl_balance' => 0 ];
            if( auth()->user() ){

                $user = auth()->user();
                if( $user->role == 6 ){
                    $user->id = 1;
                }

                $userData = DB::table('tbl_user_info')
                    ->select(['balance','pl_balance'])
                    ->where([['uid',$user->id]])->first();

                if( $userData != null ){
                    $adminSettlementAmount = 0;
                    if( $user->roleName == 'ADMIN' || $user->roleName == 'SA' ){
                        $adminSettlement = DB::table('tbl_transaction_admin_settlement')
                            ->where([['status', 1]])->sum('amount');
                        if ( $adminSettlement != null) {
                            $adminSettlementAmount = round($adminSettlement);
                        }
                    }

                    $data = [
                        'balance' => round($userData->balance),
                        'pl_balance' => round($userData->pl_balance)-$adminSettlementAmount,
                    ];
                }

                $response = [ 'status' => 1 , 'data' => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * get Data List
     * Action - Get
     * Created at DEC 2020
     */
    public function getDataList($type)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $user = Auth::user(); $client = [];

            if( $user->roleName != 'ADMIN' && $user->roleName != 'ADMIN2' && $user->roleName != 'SA'){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                if( $clientJson != null && isset($clientJson->clients) ){
                    $client = json_decode($clientJson->clients);
                }
            }

            $sportList = DB::table('tbl_sport')
                ->select(['sportId','name','sportId','slug','icon','img','admin_img','is_block']);

            if( $user->roleName != 'ADMIN' && $user->roleName != 'SA'){
                $blockEvents = $this->getBlockEventIds($user->id);
                $blockSports = $this->getBlockSportIds($user->id);
                if( $type != 'all' && $type != 'market' ){
                    $where = [['status',1],['is_block',0],['slug',$type]];
                }else{
                    $where = [['status',1],['is_block',0]];
                }
                $sportList->where($where)->whereNotIn('sportId', $blockSports);

                if( $user->roleName == 'SV'){
                    $sportsAccess = $this->sportsAccess($user->id);
                    if( $sportsAccess != null ){
                        $sportList = $sportList->whereIn('sportId', $sportsAccess);
                    }
                }

            }else{
                $blockEvents = [];
                if( $type != 'all' && $type != 'market' ){
                    $where = [['status',1],['slug',$type]];
                }else{
                    $where = [['status',1]];
                }
                $sportList->where($where);
            }
            $sportList = $sportList->orderBy('id','asc')->get();
            if( $sportList != null ){
                $sportData = [];
                foreach ( $sportList as $sport ){

                    if( $user->roleName != 'ADMIN' && $user->roleName != 'SA'){
                        $isBlock = $this->getBlockStatus($user,$sport->sportId,'sport');
                    }else{
                        $isBlock = $sport->is_block;
                    }

                    $sportData[] = [
                        'name' => $sport->name,
                        'sportId' => $sport->sportId,
                        'slug' => $sport->slug,
                        'icon' => $sport->icon,
                        'img' => $sport->img,
                        'image' => $sport->admin_img,
                        'count' => 0,
                        'isBlock' => $isBlock,
                        'isNew' => 0
                    ];
                }
                $response = [ 'status' => 1, 'data' => $sportData ];
            }

            if( $type != 'all' && $type != 'market' ){

                $sport = Sport::where([['slug',$type]])->select(['sportId'])->first();
                $response['sport'] = [];
                if( $sport != null ){
                    if( !in_array($sport->sportId,[99,999,9999])){

                        $redis = Redis::connection();
                        $eventArrJson = $redis->get("Event_List_".$sport->sportId);
                        $eventArr = json_decode($eventArrJson);

                        if( $eventArr != null && isset($eventArr->eventData) && !empty($eventArr->eventData) ){
                            $event = $eventArr->eventData;
                            $eventData = $oddsData = $matchOddMarket = $matchOddMarketJson = [];
                            foreach ( $event as $data ){
                                $oddsData = [];
                                if( !in_array( $data->eventId,$blockEvents )
                                    && $data->game_over != 1 ){

                                    $matchOddMarketJson = $redis->get('Match_Odd_'.$data->eventId);
                                    $matchOddMarket = json_decode($matchOddMarketJson);

                                    if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
                                        $oddsDataJson = $redis->get($matchOddMarket->marketId);
                                        $oddsData = json_decode($oddsDataJson);
                                    }
                                    $isBlockAdmin = $this->getBlockStatusAdmin($data->eventId,'event');
                                    if( $user->roleName != 'ADMIN' && $user->roleName != 'SA'){
                                        if( $isBlockAdmin != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->eventId,
                                                'name' => $data->name,
                                                'league' => $data->league,
                                                'time' => $data->time,
                                                'isBlock' => $this->getBlockStatus($user,$data->eventId,'event'),
                                                'type' => $data->type,
                                                'betCount' => $this->getBetCount($sport->sportId,$data->eventId,$client,$user),
                                                'oddsData' => $oddsData,
                                            ];
                                        }
                                    }else{
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'league' => $data->league,
                                            'time' => $data->time,
                                            'isBlock' => $isBlockAdmin,
                                            'type' => $data->type,
                                            'betCount' => $this->getBetCount($sport->sportId,$data->eventId,$client,$user),
                                            'oddsData' => $oddsData,
                                        ];
                                    }
                                }
                            }

                            $response['event'] = array_reverse($eventData);
                        }
                    }else if($sport->sportId == 9999){

                        /*$query = DB::connection('mongodb')
                            ->table('tbl_casino_games');
                        $where = [['status',1]];
                        if( !in_array($user->roleName,['ADMIN','SA']) ){
                            //$where = [['is_block',0]];
                            $query->whereIn('status',[0,1]);
                            if( $blockEvents != null ){
                                $query->whereNotIn('game_id',$blockEvents);
                            }
                        }
                        $event = $query->where($where)->get();
                        if( $event != null ){
                            $eventData = [];
                            foreach ( $event as $data ){
                                $data = (object)$data;
                                if( !in_array( $data->game_id,$blockEvents ) ) {
                                    $isBlockAdmin = $this->getBlockStatusAdmin($data->game_id,'event');
                                    if( !in_array($user->roleName,['ADMIN','SA']) ){
                                        if( $isBlockAdmin != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->game_id,
                                                'name' => $data->name,
                                                'isBlock' => $this->getBlockStatus($user,$data->game_id,'event'),
                                                'betCount' => $this->getBetCountCasino($data->game_id,$client,$user),
                                            ];
                                        }
                                    }else{
                                        $eventData[] = [
                                            'eventId' => $data->game_id,
                                            'name' => $data->name,
                                            'isBlock' => $isBlockAdmin,
                                            'betCount' => $this->getBetCountCasino($data->game_id,$client,$user),
                                        ];
                                    }
                                }
                            }
                        }
                        */

                        $eventData[] = [
                            'eventId' => '99991',
                            'name' => 'Casino Bet List',
                            'isBlock' => 0,
                            'betCount' => $this->getBetCountCasino($client,$user),
                        ];

                        $response['event'] = array_reverse($eventData);
                    }else{
                        $query = DB::table('tbl_live_game')->select(['eventId','name','is_block']);
                        $where = [['sportId',$sport->sportId],['status',1]];
                        if( !in_array($user->roleName,['ADMIN','SA']) ){
                            $where = [['sportId',$sport->sportId],['is_block',0]];
                            $query->whereIn('status',[0,1]);
                            if( $blockEvents != null ){
                                $query->whereNotIn('eventId',$blockEvents);
                            }
                        }
                        $event = $query->where($where)->get();
                        if( $event != null ){
                            $eventData = [];
                            foreach ( $event as $data ){
                                if( !in_array( $data->eventId,$blockEvents ) ) {
                                    $isBlockAdmin = $this->getBlockStatusAdmin($data->eventId,'event');
                                    if( !in_array($user->roleName,['ADMIN','SA']) ){
                                        if( $isBlockAdmin != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->eventId,
                                                'name' => $data->name,
                                                'isBlock' => $this->getBlockStatus($user,$data->eventId,'event'),
                                                'betCount' => $this->getBetCountTeenPatti($data->eventId,$client,$user),
                                            ];
                                        }
                                    }else{
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'isBlock' => $isBlockAdmin,
                                            'betCount' => $this->getBetCountTeenPatti($data->eventId,$client,$user),
                                        ];
                                    }
                                }
                            }

                            $response['event'] = array_reverse($eventData);
                        }
                    }

                }

            }else if( $type == 'market' ){

                $sport = Sport::select(['sportId','name','slug']);
                if( $user->roleName == 'SV'){
                    $sportsAccess = $this->sportsAccess($user->id);
                    $sportsAccess = array_intersect($sportsAccess,[1,2,4]);
                    if( $sportsAccess != null ){
                        $sport = $sport->whereIn('sportId',$sportsAccess)->get();
                    }
                }else{
                    $sport = $sport->whereIn('sportId',[1,2,4])->get();
                }

                $sportData = [];
                foreach ( $sport as $spt ){
                    $redis = Redis::connection();
                    $eventArrJson = $redis->get("Event_List_".$spt->sportId);
                    $eventArr = json_decode($eventArrJson);
                    $eventData = [];
                    if( isset($eventArr->eventData) && $eventArr != null &&  !empty($eventArr->eventData) ){
                        $event = $eventArr->eventData;
                        foreach ( $event as $data ){
                            if( !in_array( $data->eventId,$blockEvents ) && $data->game_over == 0 ) {
                                $betCount = $this->getBetCount(null,$data->eventId,$client,$user);
                                $runners = [];
                                if($user->role != 5){
                                    $runners = $this->runnerDataWithBook($user, $data->eventId);
                                }

                                if ($betCount > 0) {
                                    $isBlockAdmin = $this->getBlockStatusAdmin($data->eventId,'event');
                                    if( $user->roleName != 'ADMIN' && $user->roleName != 'SA'){
                                        if( $isBlockAdmin != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->eventId,
                                                'name' => $data->name,
                                                'league' => $data->league,
                                                'time' => $data->time,
                                                'isBlock' => $this->getBlockStatus($user,$data->eventId,'event'),
                                                'type' => $data->type,
                                                'betCount' => $betCount,
                                                'runners' => $runners
                                            ];
                                        }
                                    }else{
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'league' => $data->league,
                                            'time' => $data->time,
                                            'isBlock' => $isBlockAdmin,
                                            'type' => $data->type,
                                            'betCount' => $betCount,
                                            'runners' => $runners
                                        ];
                                    }


                                }
                            }
                        }
                    }

                    if( $eventData != null ){
                        $sportData[] = [
                            'sportId' => $spt->sportId,
                            'name' => $spt->name,
                            'slug' => $spt->slug,
                            'event' => $eventData
                        ];
                    }

                }

                $response['sport'] = $sportData;

            }else{
                $response['event'] = [];
                $response['sport'] = [];
            }

            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
                $response['userData'] = $userData;
            }

            $slider[] = array('image' => 'https://adminapi.mpcexch.com/slider/b01.jpg');
            $slider[] = array('image' => 'https://adminapi.mpcexch.com/slider/b02.jpg');
            $slider[] = array('image' => 'https://adminapi.mpcexch.com/slider/dash-slider_01.jpg');

            $response['slider'] = $slider;

//            $setting = DB::table('tbl_common_setting')->select(['value'])
//                ->where([['status',1],['key_name','BANNER_URL']])->first();
//            if( $setting != null ){
//                $bannerUrl = trim($setting->value);
//            }else{
//                $bannerUrl = 'http://34.254.28.80/DevBetexAdmin/upload/banner1.jpg';
//            }
//
//            $response['bannerUrl'] = $bannerUrl;

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * getDataListByUser
     * Action - Get
     * Created at DEC 2020
     */
    public function getDataListByUser($uid)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $cUser = Auth::user();
            if( $uid == null ){
                return $response;
            }else{
                $user = DB::table('tbl_user')->where([['id',$uid]])->first();
            }

            $sportList = DB::table('tbl_sport')
                ->select(['sportId','name','slug','is_block']);

            // $blockSports = $this->getBlockSportIds($user->id);
            $where = [['status',1],['is_block',0]];
            $sportList->where($where); //->whereNotIn('sportId', $blockSports);
            $sportList = $sportList->orderBy('id','asc')->get();
            if( $sportList != null ){
                $sportData = [];
                foreach ( $sportList as $sport ){
                    $isBlock = $this->getUserSportBlockStatus($user,$sport->sportId);
                    $sportData[] = [
                        'name' => $sport->name,
                        'sportId' => $sport->sportId,
                        'slug' => $sport->slug,
                        'isBlock' => $isBlock,
                    ];
                }
                $response = [ 'status' => 1, 'data' => $sportData ];
            }

            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
                $response['userData'] = $userData;
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * get Data List By System
     * Action - Post
     * Created at DEC 2020
     */
    public function getDataListBySystem($type)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $request = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $sportList = $system = $client = [];

            $user = Auth::user(); $uid = $user->id;
            if( $user->role == 6 ){ $uid = 1; }
            $systemData = DB::table('tbl_system')->where([['status',1]])->get();

            if( $systemData->isNotEmpty() ){

                $system = [
                    'systemName' => 'White Label',
                    'uid' => $uid,
                ];

            }else{
                return response()->json($response, 200);
            }

            if( $user->roleName != 'ADMIN' && $user->roleName != 'ADMIN2' ){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                if( $clientJson != null && isset($clientJson->clients) ){
                    $client = json_decode($clientJson->clients);
                }
            }

            $blockEvents = [];
            $sportList = DB::table('tbl_sport')->where([['status',1],['is_block',0]])
                ->select(['sportId','name','sportId','slug','icon','img','admin_img','is_block']);

            if( $user->roleName == 'SV'){
                $sportsAccess = $this->sportsAccess($user->id);
                if( $sportsAccess != null ){
                    $sportList = $sportList->whereIn('sportId', $sportsAccess);
                }
            }
            $sportList = $sportList->orderBy('id','ASC')->get();
            if( $sportList != null ){
                $sportData = [];

                foreach ( $sportList as $sport ){

                    $newArr = [998917,7522,3503,22,13,14,16,7]; $isNew = 0;
                    if( in_array($sport->sportId,$newArr) ){
                        $isNew = 1;
                    }

                    $sportData[] = [
                        'name' => $sport->name,
                        'sportId' => $sport->sportId,
                        'slug' => $sport->slug,
                        'icon' => $sport->icon,
                        'img' => $sport->img,
                        'image' => $sport->admin_img,
                        'count' => 0,
                        'isBlock' => $sport->is_block,
                        'isNew' => $isNew
                    ];
                }

                $response = ['status' => 1,'data' => $sportData , 'system' => $system ];

            }

            if( $type != 'all' && $type != 'market' ){

                $sport = Sport::where([['slug',$type]])->select(['sportId'])->first();
                $response['sport'] = [];
                if( $sport != null ){
                    if( !in_array($sport->sportId,[99,999,9999]) ){

                        $redis = Redis::connection();
                        $eventArrJson = $redis->get("Event_List_".$sport->sportId);
                        $eventArr = json_decode($eventArrJson);

                        if( $eventArr != null && isset($eventArr->eventData) && !empty($eventArr->eventData) ){
                            $event = $eventArr->eventData;
                            $eventData = [];
                            foreach ( $event as $data ){
                                $oddsData = [];
                                if( $data->game_over != 1 && $data->is_block != 1){

                                    $matchOddMarketJson = $redis->get('Match_Odd_'.$data->eventId);
                                    $matchOddMarket = json_decode($matchOddMarketJson);

                                    if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
                                        $oddsDataJson = $redis->get($matchOddMarket->marketId);
                                        $oddsData = json_decode($oddsDataJson);
                                    }

                                    if( $data->is_block != 1 ) {
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'league' => $data->league,
                                            'time' => $data->time,
                                            'isBlock' => $data->is_block,
                                            'type' => $data->type,
                                            'betCount' => $this->getBetCountForSystem($sport->sportId,$data->eventId,$client),
                                            'oddsData' => $oddsData,
                                        ];
                                    }

                                }
                            }

                            //$response['event'] = $eventData;
                            $response['event'] = array_reverse($eventData);
                        }
                    }else if( $sport->sportId == 9999 ){
                        $eventData[] = [
                            'eventId' => '99991',
                            'name' => 'Casino Bet List',
                            'isBlock' => 0,
                            'betCount' => $this->getBetCountCasinoForSystem($client,$user),
                        ];

                        $response['event'] = array_reverse($eventData);
                    }else{
                        $query = DB::table('tbl_live_game')->select(['eventId','name','is_block']);
                        $where = [['sportId',$sport->sportId]];

                        $event = $query->where($where)->get();
                        if( $event != null ){
                            $eventData = [];
                            foreach ( $event as $data ){
                                if( $data->is_block != 1 ) {
                                    $eventData[] = [
                                        'eventId' => $data->eventId,
                                        'name' => $data->name,
                                        'isBlock' => $data->is_block,
                                        'betCount' => $this->getBetCountTeenPattiForSystem($data->eventId,$client),
                                    ];
                                }
                            }

                            $response['event'] = $eventData;
                        }
                    }

                }

            }else if( $type == 'market' ){

                $sport = Sport::select(['sportId','name','slug']);
                if( $user->roleName == 'SV'){
                    $sportsAccess = $this->sportsAccess($user->id);
                    $sportsAccess = array_intersect($sportsAccess,[1,2,4]);
                    if( $sportsAccess != null ){
                        $sport = $sport->whereIn('sportId',$sportsAccess)->get();
                    }
                }else{
                    $sport = $sport->whereIn('sportId',[1,2,4])->get();
                }

                $sportData = [];
                foreach ( $sport as $spt ){
                    $redis = Redis::connection();
                    $eventArrJson = $redis->get("Event_List_".$spt->sportId);
                    $eventArr = json_decode($eventArrJson);
                    $eventData = [];
                    if( isset($eventArr->eventData) && $eventArr != null &&  !empty($eventArr->eventData) ){
                        $event = $eventArr->eventData;
                        foreach ( $event as $data ){
                            $betCount = $this->getBetCountForSystem(null,$data->eventId,$client);
                            $runners = $this->runnerDataWithBookForSystem($uid,$data->eventId);
                            if ($betCount > 0) {
                                if( $data->is_block != 1 ) {
                                    $eventData[] = [
                                        'eventId' => $data->eventId,
                                        'name' => $data->name,
                                        'league' => $data->league,
                                        'time' => $data->time,
                                        'isBlock' => $data->is_block,
                                        'type' => $data->type,
                                        'betCount' => $betCount,
                                        'runners' => $runners
                                    ];
                                }
                            }
                        }
                    }
                    if( $eventData != null ) {
                        $sportData[] = [
                            'sportId' => $spt->sportId,
                            'name' => $spt->name,
                            'slug' => $spt->slug,
                            'event' => $eventData
                        ];
                    }
                }

                $response['sport'] = $sportData;

            }else{
                $response['event'] = [];
                $response['sport'] = [];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Bet Allowed
     * Action - Get
     * Created at DEC 2020
     */
    public function actionBetAllowed($eventId)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $redis = Redis::connection();
            $cUser = Auth::user();
            if( $cUser != null && !in_array($cUser->role,[1,5]) ){ return $response; }

            if( isset( $eventId ) ){
                $tbl = 'tbl_bet_allowed_event';
                $conn = DB::connection('mysql');
                $event = $conn->table($tbl)->where([ ['eid',$eventId] ])->first();
                if( $event != null ){
                    $msg = 'Bet Not Allowed Successfully!';
                    $conn->table($tbl)->where([ ['eid',$eventId] ])->delete();
                }else{
                    $msg = 'Bet Allowed Successfully!';
                    $conn->table($tbl)->insert(['eid' => $eventId]);
                }

                $eventIds = [];
                $eventData = $conn->table($tbl)->get();
                if( $eventData->isNotEmpty() ){
                    foreach ($eventData as $event){
                        $eventIds[] = $event->eid;
                    }
                }
                $redis->set('Bet_Allowed_Events',json_encode($eventIds));

                $data['title'] = 'BetAllowed';
                $data['description'] = $msg.' for event ('.$eventId.')';
                $this->addNotification($data);

                $response = [ 'status' => 1, 'success' => [ 'message' => $msg ] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Bet Allowed Market
     * Action - POST
     * Created at DEC 2020
     */
    public function actionBetAllowedMarket(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $redis = Redis::connection();
            $cUser = Auth::user();
            if( $cUser != null && !in_array($cUser->role,[1,5]) ){ return $response; }

            if( isset( $request->mid ) && isset( $request->type )){
                $tbl = 'tbl_bet_allowed_market';
                $conn = DB::connection('mysql');
                $marketId = $request->mid;
                $market = $conn->table($tbl)->where([ ['mid',$marketId] ])->first();
                if( $market != null ){
                    $msg = 'Bet Not Allowed Successfully!';
                    $conn->table($tbl)->where([ ['mid',$marketId] ])->delete();
                }else{
                    $msg = 'Bet Allowed Successfully!';
                    $conn->table($tbl)->insert(['mid' => $marketId]);
                }

                $marketIds = [];
                $marketData = $conn->table($tbl)->get();
                if( $marketData->isNotEmpty() ){
                    foreach ($marketData as $market){
                        $marketIds[] = $market->mid;
                    }
                }
                $redis->set('Bet_Allowed_Markets',json_encode($marketIds));

                $data['title'] = 'BetAllowed';
                $data['description'] = $msg.' for market ('.$marketId.')';
                $this->addNotification($data);

                $response = [ 'status' => 1, 'success' => [ 'message' => $msg ] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action All Logout
     * Action - Get
     * Created at DEC 2020
     */
    public function actionAllLogout()
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $cUser = Auth::user();
            if( $cUser != null && $cUser->roleName != 'ADMIN'){ return $response; }

            if( DB::table('oauth_access_tokens')
                ->where([['user_id','!=',$cUser->id]])->delete() ){
                DB::table('tbl_user')->where([['id','!=',$cUser->id]])->update(['is_login' => 0]);
                DB::table('tbl_captcha')->truncate();
                $response = [ 'status' => 1, 'success' => [ 'message' => 'All User Logout Successfully !' ] ];
            }else{
                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! User Not Logout!!" ] ];
            }

            if( $response['status'] == 1 ){
                $data['title'] = 'AllLogout';
                $data['description'] = 'All user logout';
                $this->addNotification($data);
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Bet Delete
     * Action - Get
     * Created at DEC 2020
     */
    public function actionBetDelete()
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                return $response; exit;
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $cUser = Auth::user();
            if( $cUser != null && $cUser->role != 1){ return $response; }

            if( isset( $requestData['password'] ) ){
                $password = md5($requestData['password']);
                $checkPass = DB::table('tbl_common_setting')->select(['value'])
                    ->where([['key_name','ADMIN_ACTIVITY_PASSWORD']])->first();

                if( $checkPass == null || ( $checkPass->value != $password ) ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'Wrong password ! Plz try again !!' ] ];
                    return response()->json($response, 200);
                }

            }

            if( isset( $requestData['betIds'] ) ){
                foreach ( $requestData['betIds'] as $betId ){
                    $betId = new \MongoDB\BSON\ObjectId($betId);
                    $tbl = null; $betData = [];
                    $where = [['_id',$betId],['status',1],['result','PENDING']];
                    for($i=1;$i<=6;$i++){
                        $tbl1 = 'tbl_bet_pending_'.$i;
                        $betData1 = $conn->table($tbl1)->where($where)->first();
                        if( $betData1 != null ){
                            $tbl = $tbl1;
                            $betData = $betData1;
                        }
                    }
                    if( $betData == null ){
                        $tbl1 = 'tbl_bet_pending_teenpatti';
                        $betData1 = $conn->table($tbl1)->where($where)->first();
                        if( $betData1 != null ){
                            $tbl = $tbl1;
                            $betData = $betData1;
                        }
                    }

                    if( $tbl != null && $betData != null ){
                        $betData = (object)$betData;
                        if( $conn->table($tbl)->where($where)->update(['status' => 2]) ){
                            $uid = $betData->uid;
                            $marketId = $betData->mid;
                            $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;
                            $getNewExpose = $this->checkExposeBalance($betData);

                            if(!empty($getNewExpose)) {

                                $userExpose = $conn->table('tbl_user_market_expose')
                                    ->where([['uid',$uid],['status',1],['mid','!=',$marketId]])->sum("expose");

                                if(!empty($userExpose)){
                                    if( $userExpose < 0 ){
                                        $oldMarketExpose = (-1)*($userExpose);
                                    }else{
                                        $oldMarketExpose = $userExpose;
                                    }
                                }

                                $newMarketExpose = $getNewExpose['expose'];
                                $newMarketProfit = $getNewExpose['profit'];

                                if( $newMarketExpose < 0 ){
                                    $newMarketExpose = (-1)*$newMarketExpose;
                                    $exposeBalance = $oldMarketExpose+$newMarketExpose;
                                }else{
                                    $exposeBalance = $oldMarketExpose+$newMarketExpose;
                                }

                                $updateArr = ['expose' => $exposeBalance,'updated_on' => date('Y-m-d H:i:s')];
                                if( DB::table('tbl_user_info')->where('uid',$uid)->update($updateArr) ){

                                    $conn->table('tbl_user_market_expose')->where([['uid',$uid],['status',1],['mid',$marketId]])
                                        ->update(['expose' => $newMarketExpose , 'profit' => $newMarketProfit]);

                                    $data['title'] = 'BetDelete';
                                    $data['description'] = 'BetId: '.$betId.' bet deleted';
                                    $this->addNotification($data);
                                }

                            }

                        }

                    }

                }

                $response = [ 'status' => 1, 'success' => [ 'message' => 'Bet Deleted Successfully!' ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // sportsAccess
    public function sportsAccess($uid){
        $sportArr = [];
        $setting = DB::table('tbl_common_setting')->select(['value'])
            ->where([['status',1],['key_name','SUPERVISOR_SPORT_ACCESS']])->first();
        if( $setting != null ){
            $accessData = json_decode($setting->value);
            if( $accessData != null ){
                foreach ( $accessData as $acc ){
                    if( $uid == $acc->id ){
                        $sportArr = $acc->sport;
                    }
                }
            }
        }
        return $sportArr;
    }

    // get Block Status Admin
    public function getBlockStatusAdmin($eventIdOrSportId,$type)
    {
        if( $type == 'event' ) {
            $eventId = $eventIdOrSportId;
            $redis = Redis::connection();
            $blockEventIdsJson = $redis->get('Block_Event_Market');
            $blockEventIds = json_decode($blockEventIdsJson);
            if (!empty($blockEventIds) && in_array($eventId, $blockEventIds)) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    // get Block Status
    public function getBlockStatus($user,$eventIdOrSportId,$type)
    {
        $uid = $user->id;
        if( $type == 'event' ){
            $eventId = $eventIdOrSportId;
            $check = DB::table('tbl_user_event_status')->where([['eid',$eventId],['uid',$uid]])->first();
            if( $check != null ){
                return 1;
            }else{
                return 0;
            }
        }else{
            $sportId = $eventIdOrSportId;
            $check = DB::table('tbl_user_sport_status')->where([['sid',$sportId],['uid',$uid]])->first();
            if( $check != null ){
                return 1;
            }else{
                return 0;
            }
        }
    }

    //getBlockStatusUser
    public function getUserSportBlockStatus($user,$sportId)
    {
        $uid = $user->id;
        $check = DB::table('tbl_user_sport_status')
            ->where([['sid',$sportId],['uid',$uid],['byuserId',1]])->first();
        if( $check != null ){
            return 1;
        }else{
            return 0;
        }
    }

    // get Bet Count
    public function getBetCount($sportId,$eventId,$client,$user)
    {
        $conn = DB::connection('mongodb');
        if( in_array($user->role,[2,3,4]) && $client == null){
            return 0;
        }
        $countArr = []; $systemId = $user->systemId;
        $where = [['result','PENDING'],['systemId',(int)$systemId],['eid',(int)$eventId],['status',1]];
        if( $sportId == 7 ){
            $p5 = $conn->table('tbl_bet_pending_5_view')->where($where);
            if( $client != null ){ $p5 = $p5->whereIn('uid',$client); }
            if( $p5->first() != null ){ $p5 = $p5->count(); $countArr[] = $p5; }
        }elseif ( $sportId == 10 ){
//            $p2 = $conn->table('tbl_bet_pending_2_view')->where($where);
//            if( $client != null ){ $p2 = $p2->whereIn('uid',$client); }
//            if( $p2->first() != null ){ $p2 = $p2->count(); $countArr[] = $p2; }
//
//            $p3 = $conn->table('tbl_bet_pending_3_view')->where($where);
//            if( $client != null ){ $p3 = $p3->whereIn('uid',$client); }
//            if( $p3->first() != null ){ $p3 = $p3->count(); $countArr[] = $p3; }

            $p4 = $conn->table('tbl_bet_pending_4_view')->where($where);
            if( $client != null ){ $p4 = $p4->whereIn('uid',$client); }
            if( $p4->first() != null ){ $p4 = $p4->count(); $countArr[] = $p4; }
        }elseif ( $sportId == 11 ){
            $p4 = $conn->table('tbl_bet_pending_4_view')->where($where);
            if( $client != null ){ $p4 = $p4->whereIn('uid',$client); }
            if( $p4->first() != null ){ $p4 = $p4->count(); $countArr[] = $p4; }
        }else{
            $p1 = $conn->table('tbl_bet_pending_1_view')->where($where);
            if( $client != null ){ $p1 = $p1->whereIn('uid',$client); }
            if( $p1->first() != null ){ $p1 = $p1->count(); $countArr[] = $p1; }

//            $p2 = $conn->table('tbl_bet_pending_2_view')->where($where);
//            if( $client != null ){ $p2 = $p2->whereIn('uid',$client); }
//            if( $p2->first() != null ){ $p2 = $p2->count(); $countArr[] = $p2; }
//
//            $p3 = $conn->table('tbl_bet_pending_3_view')->where($where);
//            if( $client != null ){ $p3 = $p3->whereIn('uid',$client); }
//            if( $p3->first() != null ){ $p3 = $p3->count(); $countArr[] = $p3; }
//
//            $p6 = $conn->table('tbl_bet_pending_6_view')->where($where);
//            if( $client != null ){ $p6 = $p6->whereIn('uid',$client); }
//            if( $p6->first() != null ){ $p6 = $p6->count(); $countArr[] = $p6; }
        }

        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count TeenPatti
    public function getBetCountTeenPatti($eventId,$client,$user)
    {
        $conn = DB::connection('mongodb');
        if( in_array($user->role,[2,3,4]) && $client == null){
            return 0;
        }
        $countArr = []; $systemId = $user->systemId;
        $p1 = $conn->table('tbl_bet_pending_teenpatti_view')
            ->where([['result','PENDING'],['systemId',$systemId],['eid',$eventId],['status',1]]);
        if( $client != null ){ $p1 = $p1->whereIn('uid',$client); }
        if( $p1->first() != null ){ $p1 = $p1->count(); $countArr[] = $p1; }
        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count Casino
    public function getBetCountCasino($client,$user)
    {
        $conn = DB::connection('mongodb');
        if( in_array($user->role,[2,3,4]) && $client == null){
            return 0;
        }
        $countArr = []; $systemId = $user->systemId;
        $p1 = $conn->table('tbl_casino_transaction_view')
            ->where([['type','DEBIT'],['round_closed',0],['systemId',$systemId],['status',1]]);
        if( $client != null ){ $p1 = $p1->whereIn('uid',$client); }
        if( $p1->first() ){ $p1 = $p1->count(); $countArr[] = $p1; }
        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count Casino For System
    public function getBetCountCasinoForSystem($client,$user)
    {
        $conn = DB::connection('mongodb');
        if( in_array($user->role,[2,3,4]) && $client == null){
            return 0;
        }
        $countArr = [];
        $p1 = $conn->table('tbl_casino_transaction_view')
            ->where([['type','DEBIT'],['round_closed',0],['systemId','!=',1],['status',1]]);
        if( $client != null ){ $p1 = $p1->whereIn('uid',$client); }
        if( $p1->first() ){ $p1 = $p1->count(); $countArr[] = $p1; }
        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count For System
    public function getBetCountForSystem($sportId,$eventId,$client)
    {
        $conn = DB::connection('mongodb');
        $countArr = [];
        if( $sportId == 7 ){
            $p5 = $conn->table('tbl_bet_pending_5_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p5 = $p5->whereIn('uid',$client); } $p5 = $p5->count();

            if( $p5 != null ){ $countArr[] = $p5; }
        }elseif ( $sportId == 10 ){
            $p2 = $conn->table('tbl_bet_pending_2_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p2 = $p2->whereIn('uid',$client); } $p2 = $p2->count();
            $p3 = $conn->table('tbl_bet_pending_3_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p3 = $p3->whereIn('uid',$client); } $p3 = $p3->count();
            $p4 = $conn->table('tbl_bet_pending_4_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p4 = $p4->whereIn('uid',$client); } $p4 = $p4->count();

            if( $p2 != null ){ $countArr[] = $p2; }
            if( $p3 != null ){ $countArr[] = $p3; }
            if( $p4 != null ){ $countArr[] = $p4; }
        }elseif ( $sportId == 11 ){
            $p4 = $conn->table('tbl_bet_pending_4_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p4 = $p4->whereIn('uid',$client); } $p4 = $p4->count();

            if( $p4 != null ){ $countArr[] = $p4; }
        }else{
            $p1 = $conn->table('tbl_bet_pending_1_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p1 = $p1->whereIn('uid',$client); } $p1 = $p1->count();
            $p2 = $conn->table('tbl_bet_pending_2_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p2 = $p2->whereIn('uid',$client); } $p2 = $p2->count();
            $p3 = $conn->table('tbl_bet_pending_3_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p3 = $p3->whereIn('uid',$client); } $p3 = $p3->count();
            $p6 = $conn->table('tbl_bet_pending_6_view')->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p6 = $p6->whereIn('uid',$client); } $p6 = $p6->count();

            if( $p1 != null ){ $countArr[] = $p1; }
            if( $p2 != null ){ $countArr[] = $p2; }
            if( $p3 != null ){ $countArr[] = $p3; }
            if( $p6 != null ){ $countArr[] = $p6; }
        }

        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count TeenPatti
    public function getBetCountTeenPattiForSystem($eventId,$client)
    {
        $conn = DB::connection('mongodb');
        $countArr = [];
        $p1 = $conn->table('tbl_bet_pending_teenpatti_view')
            ->where([['result','PENDING'],['systemId','!=',1],['eid',$eventId],['status',1]]);
        if( $client != null ){ $p1 = $p1->whereIn('uid',$client); } $p1 = $p1->count();

        if( $p1 != null ){ $countArr[] = $p1; }

        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // runnerDataWithBook
    public function runnerDataWithBookForSystem($uid,$eventId)
    {
        $conn = DB::connection('mongodb');
        $runnersArr = $finalTotalArr = $clientArr = [];
        $redis = Redis::connection();
        $matchOddMarketJson = $redis->get('Match_Odd_'.$eventId);
        $matchOddMarket = json_decode($matchOddMarketJson);

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
            $marketId = $matchOddMarket->marketId;
            $runnerData = json_decode($matchOddMarket->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);

            $betCount = $query->count();
            if( $betCount > 0 ){
                // Redis check and return
                $redisDataKey = 'WL-'.$uid.'-'.$marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if( $getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr) ){
                    if( $getRedisData->count == $betCount ){
                        $redisRunnersArr = $getRedisData->runnersArr;
                        foreach ( $redisRunnersArr as $runner ){
                            $runnersArr[] = [
                                'secId' => $runner->secId,
                                'runnerName' => $runner->runnerName,
                                'profitLoss' => $runner->profit_loss,
                            ];
                        }
                        return $runnersArr;
                    }
                }
            }

            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];

                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }

                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$uid]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }
                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }
                    $runnersArr[] = [
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profitLoss' => $profitLoss,
                    ];
                }
            }

        }

        return $runnersArr;

    }

    // runnerDataWithBook
    public function runnerDataWithBook($user,$eventId)
    {
        $conn = DB::connection('mongodb');
        $runnersArr = $finalTotalArr = $clientArr = [];
        $redis = Redis::connection();
        $matchOddMarketJson = $redis->get('Match_Odd_'.$eventId);
        $matchOddMarket = json_decode($matchOddMarketJson);

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
            $marketId = $matchOddMarket->marketId;
            $runnerData = json_decode($matchOddMarket->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId',$user->systemId]]);

            if( $user->role != 1 && $user->role != 6 ){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                if( $clientJson != null ){
                    $client = json_decode($clientJson->clients);
                    $query->whereIn('uid',$client);
                }
            }

            $betCount = $query->count();
            if( $betCount > 0 ){
                // Redis check and return
                $redisDataKey = $user->id.'-'.$marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if( $getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr) ){
                    if( $getRedisData->count == $betCount ){
                        $redisRunnersArr = $getRedisData->runnersArr;
                        foreach ( $redisRunnersArr as $runner ){
                            $runnersArr[] = [
                                'secId' => $runner->secId,
                                'runnerName' => $runner->runnerName,
                                'profitLoss' => $runner->profit_loss,
                            ];
                        }
                        return $runnersArr;
                    }
                }
            }

            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];

                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }

                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profitLoss' => $profitLoss,
                    ];
                }
            }

        }

        return $runnersArr;

    }

    // get Block EventIds
    public function getBlockEventIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_event_status')->select('eid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->eid;
            }
        }

        return $returnArr;

    }

    // get Block SportIds
    public function getBlockSportIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_sport_status')->select('sid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->sid;
            }
        }

        return $returnArr;

    }

    // checkExposeBalance
    public function checkExposeBalance($betData)
    {
        $uid = $betData->uid;
        $mType = $betData->mType;
        $marketId = $betData->mid;
        $eventId = $betData->eid;
        $resultArray = $dataExpose = $maxBal['expose'] = $maxBal['plus'] = $balPlus = $balPlus1 = $balExpose = $balExpose2 = $profitLossNew = [];

        $minExpose = $maxProfit = 0;

        // match odd
        if( in_array($mType,['set_market','goals','winner','match_odd','completed_match','tied_match'])) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossMatchOddOnZeroFinal($betData);

                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // bookmaker
        if( in_array($mType,['bookmaker','virtual_cricket']) ) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossBookmakerOnZeroFinal($betData);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // fancy
        if( in_array($mType,['fancy2','fancy'])) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossFancyOnZeroFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // meter
        if( $mType == 'meter' ) {
            if (!empty($marketId)) {
                $minExpose = $this->getProfitLossMeterFinal($uid,$marketId);
                $maxProfit = 0;
            }
        }

        // binary
        if( in_array($mType,['USDINR','GOLD','SILVER','EURINR','GBPINR','ALUMINIUM','COPPER','CRUDEOIL','ZINC','BANKNIFTY','NIFTY']) ) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossBinary($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // fancy 3
        if( $mType == 'fancy3' ){
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossSingleFancyOnZeroFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if( $profitLossData != null ){
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // oddeven
        if( $mType == 'oddeven' ){
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossOddEvenOnZeroFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if( $profitLossData != null ){
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // ballbyball
        if( $mType == 'ballbyball' ) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossBallByBallFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // khado
        if( $mType == 'khado' ) {
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossKhadoFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if( $profitLossData != null ){
                    $maxProfit = max($profitLossData);
                }
            }
        }

        // cricket casino
        if( $mType == 'cricket_casino' ){
            if( !empty( $marketId) ) {
                $balExpose = $balPlus = [];
                for($n=0;$n<10;$n++){
                    $profitLoss = $this->getCasinoProfitLossFinal($uid,$eventId,$marketId,$n);
                    if( $profitLoss < 0 ){
                        $balExpose[] = $profitLoss;
                    }else{
                        $balPlus[] = $profitLoss;
                    }
                }

                if( $balExpose != null ){
                    $minExpose = min($balExpose);
                }
                if( $balPlus != null ){
                    $maxProfit = max($balPlus);
                }
            }
        }

        // jackpot
        if( $mType == 'jackpot' ){
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossJackpotOnZeroFinal($uid,$marketId);

                if( isset($profitLossData['balExpose']) && $profitLossData['balExpose'] != null ){
                    $minExpose = min($balExpose);
                }

                if( isset($profitLossData['balPlus']) && $profitLossData['balPlus'] != null ){
                    $maxProfit = max($balPlus);
                }

            }

        }

        $resultArray = [ 'expose' => $minExpose,'profit' => $maxProfit ];

        return $resultArray;

    }

    // get ProfitLoss MatchOdd OnZeroFinal
    public function getProfitLossMatchOddOnZeroFinal($betData)
    {

        $returnArr = [];
        $redis = Redis::connection();
        $matchOddJson = $redis->get("Market_" . $betData->mid);
        $matchOdd = json_decode($matchOddJson);

        if( $matchOdd != null && isset( $matchOdd->runners )){

            $eventId = $matchOdd->eid;
            $marketId = $matchOdd->mid;

            $runners = json_decode($matchOdd->runners);

            if( $runners != null ){
                $i = 0; $bookDataArr = [];
                foreach ( $runners as $runner ){

                    $bookDataArr[$i] = [
                        'uid' => $betData->uid,
                        'eid' => $eventId,
                        'mid' => $marketId,
                    ];

                    if( $betData->bType == 'lay' ){

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate-1 )*$betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }

                    }else{

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate-1 )*$betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }

                    }

                    $i++;
                }

                if( $bookDataArr != null ){

                    $book = DB::table('tbl_book_matchodd_view')->select(['book'])
                        ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->first();

                    if( $book != null ){

                        foreach ( $bookDataArr as $bookData ){
                            $where = [['uid',$betData->uid],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
                            $bookCheck = DB::table('tbl_book_matchodd_view')->select(['book'])->where($where)->first();
                            if( $bookCheck != null ){
                                $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
                                DB::table('tbl_book_matchodd')->where($where)->update(['book'=>$newBook]);
                            }else{
                                DB::table('tbl_book_matchodd')->insert($bookData);
                            }
                        }

                    }else{
                        DB::table('tbl_book_matchodd')->insert($bookDataArr);
                    }


                }


                $bookDataNew = DB::table('tbl_book_matchodd_view')->select(['book'])
                    ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->get();
                if( $bookDataNew->isNotEmpty() ){
                    foreach ( $bookDataNew as $bookNew ){
                        $returnArr[] = $bookNew->book;
                    }
                }

            }
        }

        return $returnArr;

    }

    // get ProfitLoss Bookmaker OnZeroFinal
    public function getProfitLossBookmakerOnZeroFinal($betData)
    {

        $returnArr = [];
        $redis = Redis::connection();
        $bookMakerJson = $redis->get("Market_" . $betData->mid);
        $bookMaker = json_decode($bookMakerJson);

        if( $bookMaker != null && isset( $bookMaker->runners )){

            $eventId = $bookMaker->eid;
            $marketId = $bookMaker->mid;

            $runners = json_decode($bookMaker->runners);

            if( $runners != null ){
                $i = 0; $bookDataArr = [];
                foreach ( $runners as $runner ){

                    $bookDataArr[$i] = [
                        'uid' => $betData->uid,
                        'eid' => $eventId,
                        'mid' => $marketId,
                    ];

                    if( $betData->bType == 'lay' ){

                        if( $runner->secId == $betData->secId ){
                            if( $betData->mType == 'virtual_cricket' ){
                                $book = ( $betData->rate-1 )*$betData->size;
                            }else{
                                $book = ( $betData->rate*$betData->size )/100;
                            }
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }

                    }else{

                        if( $runner->secId == $betData->secId ){
                            if( $betData->mType == 'virtual_cricket' ){
                                $book = ( $betData->rate-1 )*$betData->size;
                            }else{
                                $book = ( $betData->rate*$betData->size )/100;
                            }
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }

                    }

                    $i++;
                }

                if( $bookDataArr != null ){

                    $book = DB::table('tbl_book_bookmaker_view')->select(['book'])
                        ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->first();

                    if( $book != null ){

                        foreach ( $bookDataArr as $bookData ){
                            $where = [['uid',$betData->uid],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
                            $bookCheck = DB::table('tbl_book_bookmaker_view')->select(['book'])->where($where)->first();
                            if( $bookCheck != null ){
                                $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
                                DB::table('tbl_book_bookmaker')->where($where)->update(['book'=>$newBook]);
                            }else{
                                DB::table('tbl_book_bookmaker')->insert($bookData);
                            }
                        }

                    }else{
                        DB::table('tbl_book_bookmaker')->insert($bookDataArr);
                    }

                }

                $bookDataNew = DB::table('tbl_book_bookmaker_view')->select(['book'])
                    ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->get();
                if( $bookDataNew->isNotEmpty() ){
                    foreach ( $bookDataNew as $bookNew ){
                        $returnArr[] = $bookNew->book;
                    }
                }

            }
        }

        return $returnArr;

    }

    // get Profit Loss Jackpot OnZeroFinal
    public function getProfitLossJackpotOnZeroFinal($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $marketList = $conn->table('tbl_bet_pending_4_view')->select('secId','mid','eid','win','loss')
            ->where([['uid',$userId],['mid',$marketId],['result','PENDING']])
            ->whereIn('status',[1,5])->orderBy('created_on' ,'DESC')
            ->get();

        $balExpose = $balPlus = $betmarkrtID = [];
        if( $marketList != null ){
            $betarray = [];

            foreach ( $marketList as $market ){
                $market = (object)$market;
                $betarray[] = $market;
                $betsecID[] = $market->secId;
            }

            if(!empty( $betsecID ) ){

                // $gameid = 1;
                $cache = Redis::connection();
                $listData = $cache->get("Jackpot_type");
                $listData = json_decode($listData);

                foreach ($listData as $value) {
                    $gameid = $value->id;

                    $jackpotMarket = $cache->get("Jackpot_" . $gameid . "_" . $marketId);
                    $jackpotMarket = json_decode($jackpotMarket);

                    $total = $expose = $count = $totalLoss1 = 0;
                    $final_bet_array = $expose_array = $jackpot_array = [];

                    if(!empty($jackpotMarket)){
                        foreach ($jackpotMarket as $key => $jackpotarray ) {

                            $rate = $jackpotarray->rate;
                            $secId = $jackpotarray->secId;

                            $total = $totalWin = $totalLoss = 0;

                            foreach ($betarray as $key => $betarrays) {
                                if($secId!= $betarrays->secId){
                                    $totalLoss= $totalLoss+$betarrays->loss;
                                    $totalLoss1=$totalLoss;
                                }
                            }

                            $total1 = 0;
                            foreach ($betarray as $key => $betarrays) {
                                if($secId== $betarrays->secId){
                                    $totalWin= $totalWin+$betarrays->win;
                                    $total1 = $totalWin-$totalLoss;
                                }
                            }

                            $total = $totalWin-$totalLoss;

                            if( $total < 0 ){
                                $balExpose[] = $total;
                            }else{
                                $balPlus[] = $total;
                            }

                        }
                    }
                }
            }
        }

        return [ 'balExpose' => $balExpose, 'balPlus' => $balPlus ];

    }

    // getProfitLossMeterFinal
    public function getProfitLossMeterFinal($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $betList = $conn->table('tbl_bet_pending_3_view')->select(['size'])
            ->where([['uid',$userId],['result','PENDING'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();
        $newExpose = 0;

        if( $betList != null ){
            foreach ($betList as $bet) {
                $bet = (object)$bet;
                $newExpose = $newExpose+( ($bet->size)*100 );
            }
            $newExpose = (-1)*$newExpose;
        }
        return $newExpose;
    }

    // get Profit Loss FancyOnZeroFinal
    public function getProfitLossFancyOnZeroFinal($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $betList = $conn->table('tbl_bet_pending_2_view')->select(['bType','price','win','loss'])
            ->where([['uid',$userId],['result','PENDING'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();
        $result = $newbetresult = [];

        if( $betList != null ){
            $result = $betresult = [];
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                $betresult[] = [ 'price' => $bet->price, 'bType' => $bet->bType, 'loss' => $bet->loss, 'win' => $bet->win ];
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1;
            $max = $max+1;
            $win = $loss=0;
            $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bType == 'no'){
                        $top = $top+$value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                        $newresult[] = ['count'=>$count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss']];
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bType == 'no'){
                        $newresult[] = ['count' => $count,'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss']];
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }

                    $count++;
                }
                $result[] = ['count' => $value['price'], 'bType' => $value['bType'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult];
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = [ 'bet_price' => $result[$x]['count'], 'bType' => $result[$x]['bType'], 'expose' => $expose ];
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = [ 'exposearray' => $newbetarray1, 'finalexpose' => $finalexpose ];
                }
            }

            return $newbetresult;
        }
    }

    // get Profit Loss Binary
    public function getProfitLossBinary($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $betList = $conn->table('tbl_bet_pending_5_view')->select(['bType','price','win','loss'])
            ->where([['uid',$userId],['result','PENDING'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();
        $result = $newbetresult = [];

        if( $betList != null ){
            $result = $betresult = [];
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                $betresult[] = [ 'price' => $bet->price, 'bType' => $bet->bType, 'loss' => $bet->loss, 'win' => $bet->win ];
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1;
            $max = $max+1;
            $win = $loss=0;
            $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bType == 'no'){
                        $top = $top+$value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                        $newresult[] = ['count'=>$count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss']];
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bType == 'no'){
                        $newresult[] = ['count' => $count,'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss']];
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }

                    $count++;
                }
                $result[] = ['count' => $value['price'], 'bType' => $value['bType'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult];
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = [ 'bet_price' => $result[$x]['count'], 'bType' => $result[$x]['bType'], 'expose' => $expose ];
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = [ 'exposearray' => $newbetarray1, 'finalexpose' => $finalexpose ];
                }
            }

            return $newbetresult;
        }
    }

    // get Profit Loss BallByBallFinal
    public function getProfitLossBallByBallFinal($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $betList = $conn->table('tbl_bet_pending_3_view')->select(['bType','price','win','loss'])
            ->where([['uid',$userId],['result','PENDING'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();

        $result = $newbetresult = [];
        if( $betList != null ){
            $result =  $betresult = [];
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                $betresult[] = array('price'=>$bet->price,'bType'=>$bet->bType,'loss'=>$bet->loss,'win'=>$bet->win);
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1; $win = $loss = 0; $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bType == 'no'){
                        $top = $top+$value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                        $newresult[] = ['count'=>$count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss']];
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bType == 'no'){
                        $newresult[] = ['count' => $count,'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss']];
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }

                    $count++;
                }
                $result[] = ['count' => $value['price'], 'bType' => $value['bType'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult];
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = ['bet_price' => $result[$x]['count'], 'bType' => $result[$x]['bType'], 'expose' => $expose];
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = ['exposearray' => $newbetarray1, 'finalexpose' => $finalexpose];
                }
            }

        }

        return $newbetresult;
    }

    // get Profit Loss KhadoFinal
    public function getProfitLossKhadoFinal($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $betList = $conn->table('tbl_bet_pending_3_view')->select(['bType','price','win','loss'])
            ->where([['uid',$userId],['result','PENDING'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();

        $result = $newbetresult = [];
        if( $betList != null ){
            $result = $betresult = []; $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                $betresult[] = [ 'price' => $bet->price, 'bType' => $bet->bType, 'loss' => $bet->loss, 'win' => $bet->win ];
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1; $win = $loss = 0; $count = $min;

            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    $newresult[] = [ 'count'=>$count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss'] ];
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    $newresult[] = [ 'count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] ];
                    $count++;
                }
                $result[] = ['count' => $value['price'], 'bType' => $value['bType'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult];
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = ['bet_price' => $result[$x]['count'], 'bType' => $result[$x]['bType'], 'expose' => $expose];
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = ['exposearray' => $newbetarray1, 'finalexpose' => $finalexpose];
                }
            }

        }

        return $newbetresult;

    }

    // get Profit Loss Single FancyOnZeroFinal
    public function getProfitLossSingleFancyOnZeroFinal($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $yesWin = $yesLoss = $noWin = $noLoss = 0;

        $where = [['result', 'PENDING'], ['bType', 'back'], ['uid', $userId], ['mid', $marketId]];

        $betList = $conn->table('tbl_bet_pending_2_view')->where($where)->whereIn('status',[1,5]);
        $yesWinSum = $betList->sum("win");
        if( !empty($yesWinSum) ){ $yesWin = $yesWinSum; }

        $yesLossSum = $betList->sum("loss");
        if( !empty($yesLossSum) ){ $yesLoss = $yesLossSum; }

        $where = [['result', 'PENDING'], ['bType', 'lay'], ['uid', $userId], ['mid', $marketId]];

        $betList = $conn->table('tbl_bet_pending_2_view')->where($where)->whereIn('status',[1,5]);
        $noWinSum = $betList->sum("win");
        if( !empty($noWinSum) ){ $noWin = $noWinSum; }

        $noLossSum = $betList->sum("loss");
        if( !empty($noLossSum) ){ $noLoss = $noLossSum; }

        $yesProfitLoss = round($yesWin - $noLoss);
        $noProfitLoss = round($noWin - $yesLoss) ;

        $newResult[] = $yesProfitLoss;
        $newResult[] = $noProfitLoss;

        return $newResult;

    }

    // get Profit Loss Odd Even OnZeroFinal
    public function getProfitLossOddEvenOnZeroFinal($userId,$marketId)
    {
        $conn = DB::connection('mongodb');
        $yesWin = $yesLoss = $noWin = $noLoss = 0;

        $where = [['result', 'PENDING'], ['bType', 'back'], ['uid', $userId], ['mid', $marketId]];

        $betList = $conn->table('tbl_bet_pending_2_view')->where($where)->whereIn('status',[1,5]);
        $yesWinSum = $betList->sum("win");
        if( !empty($yesWinSum) ){ $yesWin = $yesWinSum; }

        $yesLossSum = $betList->sum("loss");
        if( !empty($yesLossSum) ){ $yesLoss = $yesLossSum; }

        $where = [['result', 'PENDING'], ['bType', 'lay'], ['uid', $userId], ['mid', $marketId]];

        $betList = $conn->table('tbl_bet_pending_2_view')->where($where)->whereIn('status',[1,5]);
        $noWinSum = $betList->sum("win");
        if( !empty($noWinSum) ){ $noWin = $noWinSum; }

        $noLossSum = $betList->sum("loss");
        if( !empty($noLossSum) ){ $noLoss = $noLossSum; }

        $yesProfitLoss = round($yesWin - $noLoss);
        $noProfitLoss = round($noWin - $yesLoss) ;

        $newResult[] = $yesProfitLoss;
        $newResult[] = $noProfitLoss;

        return $newResult;

    }

    // get Casino Profit Loss On Bet
    public function getCasinoProfitLossFinal($userId,$eventId,$marketId,$selectionId)
    {
        $conn = DB::connection('mongodb');
        $total = $totalWin = $totalLoss = 0;
        $query = $conn->table('tbl_bet_pending_4_view')->whereIn('status',[1,5]);

        // IF RUNNER WIN
        $where = [['uid', $userId],['mid', $marketId],['eid',$eventId],['secId',$selectionId]];
        $betWinSum = $query->where($where)->sum("win");
        if( !empty( $betWinList ) ){ $totalWin = $betWinSum; }

        // IF RUNNER LOSS
        $where = [['uid', $userId],['mid', $marketId],['eid',$eventId],['secId','!=',$selectionId]];
        $betLossSum = $query->where($where)->sum("loss");
        if( !empty( $betLossList ) ){ $totalWin = $betLossSum; }

        $total = $totalWin+$totalLoss;

        return $total;

    }

}
