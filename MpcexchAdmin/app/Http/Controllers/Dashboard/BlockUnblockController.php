<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\Sport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class BlockUnblockController extends Controller
{

    /**
     * Event BlockUnblock
     * Action - Get
     * Created at DEC 2020
     */
    public function event($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $redis = Redis::connection();
            $user = Auth::user(); $uid = Auth::id(); $eventId = $id;
            $message = 'Block/Unblock';
            if( $user->roleName != 'ADMIN' ){

                if( $user->roleName == 'ADMIN2' ){
                    $userArrData = DB::table('tbl_user')->select(['id'])
                        ->where([['systemId', $user->systemId],['status','!=',2]])->get();
                    if ($userArrData->isNotEmpty()) {
                        foreach ($userArrData as $uData) {
                            $userArr[] = $uData->id;
                        }
                    }
                    $userArr[] = $uid;
                }else{
                    $userArr = CommonModel::userChildData($uid);
                }

                $checkStatus = DB::table('tbl_user_event_status')->select(['uid'])
                    ->where([['uid',$uid],['eid',$eventId]])->first();

                if( $checkStatus != null ){
                    // do unblock
                    foreach ( $userArr as $usrId ){
                        DB::table('tbl_user_event_status')
                            ->where([['uid',$usrId],['eid',$eventId],['byuserId',$uid]])
                            ->delete();
                    }

                    $message = 'Unblock successfully!';

                }else{
                    // do block
                    foreach ( $userArr as $usrId ){
                        $userData = ['uid' => $usrId,'eid' => $eventId,'byuserId' => $uid ];
                        DB::table('tbl_user_event_status')->insert($userData);
                    }

                    $message = 'Block successfully!';

                }

                $response = [
                    'status' => 1 , "success" => [ "message" => $message ]
                ];

            }else{

                $tbl = 'tbl_block_event_market';
                $event = DB::table($tbl)->where([['eid',$eventId]])->first();
                $message = 'Something Wrong! Status Not Change!';
                if( $event != null ){
                    if( DB::table($tbl)->where([['eid',$eventId]])->delete() ){
                        $message = 'Unblock successfully!';
                    }
                }else{
                    $insertArr = ['eid' => $eventId,'mid' => 0];
                    if( DB::table($tbl)->insert($insertArr) ){
                        $message = 'Block successfully!';
                    }
                }

                $eventIds = [];
                $eventBlockArr = DB::table($tbl)->get();
                if( $eventBlockArr->isNotEmpty() ){
                    foreach ($eventBlockArr as $blockArr){
                        $eventIds[] = $blockArr->eid;
                    }
                }
                $redis->set('Block_Event_Market',json_encode($eventIds));

                $response = [
                    'status' => 1 , "success" => [ "message" => $message ]
                ];

            }
            
            if( $response['status'] == 1 ) {
                $data['title'] = 'Block/Unblock';
                $data['description'] = 'Event (' . $eventId . ') ' . $message;
                $this->addNotification($data);
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    /**
     * Sport BlockUnblock
     * Action - Get
     * Created at DEC 2020
     */
    public function sport($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
        try{

            $user = Auth::user(); $uid = Auth::id(); $sportId = $id;
            $message = 'Block/Unblock';
            if( $user->roleName != 'ADMIN' ){

                if( $user->roleName == 'ADMIN2' ){
                    $userArrData = DB::table('tbl_user')->select(['id'])
                        ->where([['systemId', $user->systemId],['status','!=',2]])->get();
                    if ($userArrData->isNotEmpty()) {
                        foreach ($userArrData as $uData) {
                            $userArr[] = $uData->id;
                        }
                    }
                    $userArr[] = $uid;
                }else{
                    $userArr = CommonModel::userChildData($uid);
                }

                $checkStatus = DB::table('tbl_user_sport_status')->select(['uid'])
                    ->where([['uid',$uid],['sid',$sportId]])->first();

                if( $checkStatus != null ){
                    // do unblock
                    foreach ( $userArr as $usrId ){
                        DB::table('tbl_user_sport_status')
                            ->where([['uid',$usrId],['sid',$sportId],['byuserId',$uid]])
                            ->delete();
                    }

                    $message = 'Unblock successfully!';

                }else{
                    // do block
                    foreach ( $userArr as $usrId ){
                        $userData = ['uid' => $usrId,'sid' => $sportId,'byuserId' => $uid ];
                        DB::table('tbl_user_sport_status')->insert($userData);
                    }

                    $message = 'Block successfully!';

                }

                $response = [
                    'status' => 1 , "success" => [ "message" => $message ]
                ];

            }else{
                $conn = DB::connection('mysql');
                $where = [['sportId',$sportId]];
                $sport = $conn->table('tbl_sport')->where($where)->first();

                if( $sport != null ){
                    $message = 'Block successfully!';
                    $updateArr = ['is_block' => 1];
                    if( $sport->is_block == 1 ){
                        $message = 'Unblock successfully!';
                        $updateArr = ['is_block' => 0];
                    }

                    if( $conn->table('tbl_sport')->where($where)->update($updateArr) ){
                        $response = [
                            'status' => 1 , "success" => [ "message" => $message ]
                        ];
                    }

                }

            }
            $this->redisSportUpdate();
            if( $response['status'] == 1 ) {
                $data['title'] = 'Block/Unblock';
                $data['description'] = 'Sport (' . $sportId . ') ' . $message;
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Sport BlockUnblock for user
     * Action - Get
     * Created at DEC 2020
     */
    public function sportUser()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
        try{

            if( isset($_GET['uid']) && isset($_GET['sid']) ){
                $user = Auth::user(); $uid = $_GET['uid']; $sportId = $_GET['sid'];
                $message = 'Block/Unblock';
                if( $user->roleName == 'ADMIN' || $user->roleName == 'ADMIN2' ){
                    $userArr = CommonModel::userChildData($uid);
                    $checkStatus = DB::table('tbl_user_sport_status')->select(['uid'])
                        ->where([['uid',$uid],['sid',$sportId]])->first();

                    if( $checkStatus != null ){
                        // do unblock
                        foreach ( $userArr as $usrId ){
                            DB::table('tbl_user_sport_status')
                                ->where([['uid',$usrId],['sid',$sportId],['byuserId',$user->id]])
                                ->delete();
                        }

                        $message = 'Unblock successfully!';

                    }else{
                        // do block
                        foreach ( $userArr as $usrId ){
                            $userData = ['uid' => $usrId,'sid' => $sportId,'byuserId' => $user->id ];
                            DB::table('tbl_user_sport_status')->insert($userData);
                        }

                        $message = 'Block successfully!';

                    }

                    $response = [
                        'status' => 1 , "success" => [ "message" => $message ]
                    ];
                    if( $response['status'] == 1 ) {
                        $data['title'] = 'Block/Unblock';
                        $data['description'] = 'Sport (' . $sportId . ') ' . $message;
                        $this->addNotification($data);
                    }
                    return response()->json($response, 200);
                }else{
                    return response()->json($response, 200);
                }
            }else{
                return response()->json($response, 200);
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // redisSportUpdate
    public function redisSportUpdate()
    {
        $redis = Redis::connection();
        $sportList = DB::connection('mysql')->table('tbl_sport')->where('status',1)->get();
        if($sportList->isNotEmpty()) {
            $sportData = json_encode($sportList);
            $redis->set('SportList', $sportData);
            foreach ($sportList as $sport_List) {
                $sportId = $sport_List->sportId;
                $sportDT = json_encode($sport_List);
                $redis->set('Sport_' . $sportId, $sportDT);
            }
        }
    }
}
