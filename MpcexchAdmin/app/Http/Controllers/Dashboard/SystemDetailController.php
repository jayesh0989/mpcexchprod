<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class SystemDetailController extends Controller
{

    private $marketIdsArr = [];

    // getBlockEventIds
    public function getBlockEventIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_event_status')->select('eid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->eid;
            }
        }

        return $returnArr;

    }

    // getBlockSportIds
    public function getBlockSportIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_sport_status')->select('sid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->sid;
            }
        }

        return $returnArr;

    }

    /**
     * action Event Detail
     */
    public function actionEventDetail($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $sportArr = ['1' => 'football', '2' => 'tennis', '4' => 'cricket', '6' => 'election', '7' => 'binary', '10' => 'jackpot',
                '11' => 'cricket-casino', '13' => 'badminton', '14' => 'kabaddi', '16' => 'boxing', '22' => 'table-tennis',
                '99' => 'live-games', '999' => 'live-games2', '3503' => 'dart', '7522' => 'basketball', '998917' => 'volleyball'];

            $user = Auth::user();
            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData == null){
                $eventData = DB::table('tbl_live_game')->where([['eventId',$eventId],['status',1]])->first();
            }

            if( $eventData != null ){
                if( isset($eventData->game_over) && $eventData->game_over == 1 ){
                    return $response;
                }
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;
                $sportSlug = $sportArr[$sportId];

                $eventArr = [
                    'title' => $title,
                    'sport' => ucfirst( str_replace('-',' ', $sportSlug) ),
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 0,
                ];

                if( !in_array($sportId,['99','999','9999']) ){

                    if( in_array($sportId,['1','2','4']) ){
                        $eventArr['tvAllowed'] = 1;
                    }

                    if( !in_array($sportId,['6','7','10','11']) ){
                        $eventArr['otherMarket'] = $this->getOtherMarket($user,$eventData,$eventId,$sportSlug);
                    }

                    if( !in_array($sportId,['7','11']) ){
                        $eventArr['bookMaker'] = $this->getBookMaker($user,$sportId,$eventId);
                        $eventArr['fancy2Market'] = $this->getFancy2Market($sportId,$eventId,$sportSlug);
                        $eventArr['fancy3Market'] = $this->getFancy3Market($sportId,$eventId,$sportSlug);
                    }

                    if( $sportSlug == 'cricket' ){
                        $eventArr['virtualCricket'] = $this->getVirtualCricket($user,$sportId,$eventId);
                        $eventArr['fancyMarket'] = $this->getFancyMarket($sportId,$eventId,$sportSlug);
                        $eventArr['meterFancy'] = $this->getMeterFancy($sportId,$eventId,$sportSlug);
                        $eventArr['oddEvenMarket'] = $this->getOddEvenMarket($sportId,$eventId,$sportSlug);
                        $eventArr['ballSession'] = $this->getBallSession($sportId,$eventId,$sportSlug);
                    }elseif ( $sportSlug == 'football' ){
                        $eventArr['goalsMaker'] = $this->getGoalsMaker($user,$sportId,$eventId);
                    }elseif ( $sportSlug == 'tennis' ){
                        $eventArr['setMarket'] = $this->getSetMarket($user,$sportId,$eventId);
                    }elseif ( $sportSlug == 'binary' ){
                        $eventArr['binaryMarket'] = $this->getBinaryMarket($sportId,$eventId,$sportSlug);
                    }elseif ( $sportSlug == 'jackpot' ){
                        $eventArr['jackpotData'] = $this->getJackpotData($sportId,$eventId);
                    }elseif ( $sportSlug == 'cricket-casino' ){
                        $eventArr['cricketCasino'] = $this->getCricketCasino($sportId,$eventId);
                    }
                    if ( $sportSlug == 'cricket' || $sportSlug == 'jackpot' ){
                        $eventArr['khadoSession'] = $this->getKhadoSession($sportId,$eventId,$sportSlug);
                    }
                }

                $response = [ 'status' => 1, 'data' => [ 'items' => $eventArr, 'userData' => $userData, 'marketIdsArr' => $this->marketIdsArr ] ];

            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data Jackpot
     */
    public function actionJackpotDetails(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $conn = DB::connection('mongodb');
            $marketArr = null; $betCount = 0;
            if( isset( $request->type ) && isset( $request->mid ) && isset( $request->eid )){
                $marketName = $gameType = 'Jackpot';
                $gType = $request->type; $marketId = $request->mid; $eventId = $request->eid;
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $systemId = $user->systemId;
                $redis = Redis::connection();
                $eventDataJson = $redis->get("Event_".$eventId);
                $jackpotAct = $redis->get("jackpot_activity_id_" . $gType . "_" . $marketId);
                $eventData = json_decode($eventDataJson);
                if ($eventData != null) {
                    $eventName = $eventData->name;
                    $sportId = $eventData->sportId;

                    $jackpotMarketJson = $redis->get("Jackpot_market_".$gType."_". $eventId."_".$marketId);
                    $jackpotMarket = json_decode($jackpotMarketJson);

                    if( $jackpotMarket != null ){
                        $marketName = $jackpotMarket->jackpotmarketList->title;
                        $gameTypeDataJson = $redis->get("Jackpot_type");
                        $gameTypeData = json_decode($gameTypeDataJson);
                        foreach ($gameTypeData as $type) {
                            if ( $gType == $type->id ) {
                                $gameType = $type->title;
                            }
                        }
                    }

                    $query = $conn->table('tbl_bet_pending_4_view')
                        ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                    if( in_array( $user->role,[2,3] )){
                        $clientJson = DB::table('tbl_user_child_data')->select('clients')
                            ->where([['uid',$user->id]])->first();
                        if( $clientJson != null && isset( $clientJson->clients ) ){
                            $client = json_decode($clientJson->clients);
                            $query->whereIn('uid',$client);
                        }
                    }

                    $betCount = $query->count();
                    $betList = $betListArr = [];
                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ) {
                        $client = [];
                        foreach ($clientArr as $cuid) {
                            $client[] = $cuid;
                        }

                        $client = array_unique($client);
                        $result = $betArray = [];
                        $min = $max = $totalLoss = $actualProfitLoss = 0;

                        if ( $betCount > 0 ){
                            $query1 = $conn->table('tbl_bet_pending_4_view')
                                ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                            if( in_array( $user->role,[2,3] )){
                                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                                    ->where([['uid',$user->id]])->first();
                                if( $clientJson != null && isset( $clientJson->clients ) ){
                                    $client = json_decode($clientJson->clients);
                                    $query1->whereIn('uid',$client);
                                }
                            }
                            $betListArr = $query1->select(['_id','runner','client','master','price','size','rate','created_on','ip_address','bType'])
                                ->whereIn('uid',$client)
                                ->orderBy('created_on','asc')->limit(10)->get();
                            if( $betListArr->isNotEmpty() ){
                                $betList = [ 'list' => $betListArr, 'count' => $betCount ];
                            }

                            // Redis check and return
                            $redisDataKey = $user->id.'-'.$marketId;
                            $getRedisDataJson = $redis->get( $redisDataKey);
                            $getRedisData = json_decode($getRedisDataJson);
                            if( $getRedisData != null && isset($getRedisData->jackpotAct) && isset($getRedisData->count) && isset($getRedisData->marketArr) ){
                                $actDiff = false;
                                if( $jackpotAct == null || empty($jackpotAct) ){
                                    $jackpotAct = time();
                                    if( $getRedisData->jackpotAct-$jackpotAct > 60 ){
                                        $actDiff = true;
                                    }
                                }else{
                                    if( $getRedisData->jackpotAct != $jackpotAct ){
                                        $actDiff = true;
                                    }
                                }

                                if( $getRedisData->count == $betCount && $actDiff == false ){
                                    $marketArr = $getRedisData->marketArr;
                                    $response = [ 'status' => 1, 'data' => [ 'items' => $marketArr, 'betList' => $betList ] ];
                                    if( $user != null ){
                                        $userData = [
                                            'name' => $user->username,
                                            'role' => $user->roleName,
                                        ];
                                        $response['userData'] = $userData;
                                    }
                                    return response()->json($response, 200);
                                }
                            }

                            $userProfitLoss = DB::table('tbl_user_profit_loss')
                                ->select(['actual_profit_loss as apl','clientId'])
                                ->where([['userId',$uid]])->whereIn('clientId',$client)->get();
                            if( $userProfitLoss->isNotEmpty() ){

                                $betList11 = $conn->table('tbl_bet_pending_4_view')
                                    ->select(['secId','win','loss','price','uid'])
                                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId','!=',1]])
                                    ->whereIn('uid',$client)->get();

                                if( $betList11->isNotEmpty() ){
                                    $dataReturn['betList'] = [];
                                    foreach ( $betList11 as $betData ){
                                        $betData = (object)$betData;
                                        $totalLoss = 0;
                                        foreach ( $userProfitLoss as $upl ){
                                            if( $upl->clientId == $betData->uid ){
                                                $totalLoss = $totalLoss+$betData->loss;
                                                $actualProfitLoss = $upl->apl;
                                                $betArray[] = [
                                                    'sec_id' => $betData->secId,
                                                    'user_id' => $betData->uid,
                                                    'loss' => $betData->loss,
                                                    'win' => $betData->win,
                                                    'actual_profit_loss' => $upl->apl
                                                ];
                                            }
                                        }
                                    }
                                }
                            }

                        }else{
                            $betList = [ 'list' => [], 'count' => 0 ];
                        }

                        $apl = 0;
                        $jackpotDataJson =  $redis->get('Jackpot_' . $gType . '_' . $marketId);
                        $jackpotData =  json_decode($jackpotDataJson);
                        $runners = [];
                        if(!empty($jackpotData)) {
                            foreach ( $jackpotData as $jackpot ) {
                                $totalWin = $userTotalLoss = 0;
                                foreach ($betArray as $bet) {
                                    if( $bet['sec_id'] == $jackpot->secId ){
                                        $totalWin = $totalWin+$bet['win'];
                                        $userTotalLoss = $userTotalLoss+$bet['loss'];
                                        $apl = $bet['actual_profit_loss'];
                                    }
                                }
                                // $profitLoss = 0;
                                if( $totalWin > 0 ) {
                                    $userLoss = $totalLoss-$userTotalLoss;
                                    $finalExpose = $userLoss-$totalWin;
                                    $finalExpose = ($finalExpose*$apl) / 100;
                                    $profitLoss =  $finalExpose;
                                }else{
                                    $finalExpose = ($totalLoss*$actualProfitLoss) / 100;
                                    $profitLoss = $finalExpose;
                                }

                                $runners[] = [
                                    'sportId' => $sportId,
                                    'event_id' => $eventId,
                                    'market_id' => $marketId,
                                    'sec_id' => $jackpot->secId,
                                    'col1' => $jackpot->col_1,
                                    'col2' => $jackpot->col_2,
                                    'col3' => $jackpot->col_3,
                                    'col4' => $jackpot->col_4,
                                    'slug' => 'jackpot',
                                    'rate' => $jackpot->rate,
                                    'suspended' => $jackpot->suspended,
                                    'profitLoss' => $profitLoss,
                                ];
                            }
                        }

                        $marketArr = [
                            'sport' => 'Jackpot',
                            'eventName' => $eventName,
                            'marketName' => $marketName,
                            'gameType' => $gameType,
                            'runners' => $runners,
                        ];

                    }else{
                        // default
                        $jackpotDataJson =  $redis->get('Jackpot_' . $gType . '_' . $marketId);
                        $jackpotData =  json_decode($jackpotDataJson);
                        $runners = [];
                        if(!empty($jackpotData)) {
                            foreach ( $jackpotData as $jackpot ) {
                                $profitLoss = 0;
                                $runners[] = [
                                    'sportId' => $sportId,
                                    'event_id' => $eventId,
                                    'market_id' => $marketId,
                                    'sec_id' => $jackpot->secId,
                                    'col1' => $jackpot->col_1,
                                    'col2' => $jackpot->col_2,
                                    'col3' => $jackpot->col_3,
                                    'col4' => $jackpot->col_4,
                                    'slug' => 'jackpot',
                                    'rate' => $jackpot->rate,
                                    'suspended' => $jackpot->suspended,
                                    'profitLoss' => $profitLoss,
                                ];
                            }
                        }

                        $marketArr = [
                            'sport' => 'Jackpot',
                            'eventName' => $eventName,
                            'marketName' => $marketName,
                            'gameType' => $gameType,
                            'runners' => $runners,
                        ];
                    }

                    $response = [ 'status' => 1, 'data' => [ 'items' => $marketArr, 'betList' => $betList ] ];

                    if( $user != null ){
                        $userData = [
                            'name' => $user->username,
                            'role' => $user->roleName,
                        ];
                        $response['userData'] = $userData;
                    }

                    if ( $betCount > 0 ){
                        if( $jackpotAct == null || empty($jackpotAct) ){ $jackpotAct = time(); }
                        $redisDataKey = $user->id.'-'.$marketId;
                        $redisData = [ 'count' => $betCount, 'marketArr' => $marketArr, 'jackpotAct' => $jackpotAct ];
                        $redisDataJson = json_encode($redisData);
                        $redis->set( $redisDataKey, $redisDataJson );
                    }

                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data Jackpot
     */
    public function actionJackpotDetailsOLD(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $conn = DB::connection('mongodb');
//            if( !$this->checkOrigin() ){
//                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad Request !!' ] ];
//                return response()->json($response);
//            }
            $marketArr=null;
            if( isset( $request->type ) && isset( $request->mid ) && isset( $request->eid )){
                $marketName = $gameType = 'Jackpot';
                $gType = $request->type; $marketId = $request->mid; $eventId = $request->eid;
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                // $systemId = $user->systemId;


                $redis = Redis::connection();
                $eventDataJson = $redis->get("Event_".$eventId);
                $eventData = json_decode($eventDataJson);
                $jackpotAct = $redis->get("jackpot_activity_id_" . $gType . "_" . $marketId);
                if ($eventData != null) {
                    $eventName = $eventData->name;
                    $sportId = $eventData->sportId;

                    $jackpotMarketJson = $redis->get("Jackpot_market_".$gType."_". $eventId."_".$marketId);
                    $jackpotMarket = json_decode($jackpotMarketJson);

                    if( $jackpotMarket != null ){
                        $marketName = $jackpotMarket->title;

                        $gameTypeDataJson = $redis->get("Jackpot_type");
                        $gameTypeData = json_decode($gameTypeDataJson);
                        foreach ($gameTypeData as $type) {
                            if ( $gType == $type->id ) {
                                $gameType = $type->title;
                            }
                        }

                    }


                    $query = $conn->table('tbl_bet_pending_4_view')
                        ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                    $clientArr = $query->distinct()->select('uid')->get();
                    if( $clientArr->isNotEmpty() ) {
                        $client = [];
                        foreach ($clientArr as $cuid) {
                            $client[] = $cuid;
                        }

                        $client = array_unique($client);
                        $result = $newbetresult = [];

                        $result = [];
                        $betresult = [];
                        $min = 0;
                        $max = 0;
                        $totalLoss=0;  $actualprofitloss=0; $betArray=[];
                        /*$betList = DB::table('tbl_bet_pending_4_view as pb')->select('pb.win','pb.loss','pb.uid','pb.secId','upl.actual_profit_loss')
                            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                            ->where([['pb.mid',$marketId],['upl.userId',$uid],['pb.status',1],['result','PENDING'],['pb.systemId','!=',1]])//,['pb.systemId',$systemId]
                            ->whereIn('pb.uid',$client)->get();
                        foreach ($betList as $index => $bet) {
                            $totalLoss= $totalLoss+$bet->loss;
                            $actualprofitloss=       $bet->actual_profit_loss;
                            $betArray[]=array('sec_id'=>$bet->secId,'user_id'=>$bet->uid,'loss'=>$bet->loss,'win'=>$bet->win,'actual_profit_loss'=>$bet->actual_profit_loss);

                        }*/

                        $userProfitLoss = DB::table('tbl_user_profit_loss')
                            ->select(['actual_profit_loss as apl','clientId'])
                            ->where([['userId',$uid]])->whereIn('clientId',$client)->get();
                        if( $userProfitLoss->isNotEmpty() ){

                            $betList11 = $conn->table('tbl_bet_pending_4_view')
                                ->select(['secId','win','loss','price','uid'])
                                ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId','!=',1]])
                                ->whereIn('uid',$client)->get();

                            if( $betList11->isNotEmpty() ){
                                $dataReturn['betList'] = [];
                                foreach ( $betList11 as $betData ){
                                    $betData = (object)$betData;
                                    $totalLoss = 0;
                                    foreach ( $userProfitLoss as $upl ){
                                        if( $upl->clientId == $betData->uid ){
                                            $totalLoss = $totalLoss+$betData->loss;
                                            $actual_profit_loss = $upl->apl;
                                            $betArray[] = [
                                                'sec_id' => $betData->secId,
                                                'user_id' => $betData->uid,
                                                'loss' => $betData->loss,
                                                'win' => $betData->win,
                                                'actual_profit_loss' => $upl->apl
                                            ];
                                        }
                                    }
                                }
                            }
                        }


                        //echo "test"; echo "<pre>";  print_r($betArray); exit;
                        $actual_profit_loss=0;
                        $jackpotDataJson =  $redis->get('Jackpot_' . $gType . '_' . $marketId);
                        $jackpotData =  json_decode($jackpotDataJson);
                        $runners = [];
                        if(!empty($jackpotData)) {
                            foreach ( $jackpotData as $jackpot ) {
                                $profitLoss = 0;
                                $totalWin=0;   $totalLoss2=0;
                                $usertotalLoss=0;
                                foreach ($betArray as $bet) {
                                    if($bet['sec_id']==$jackpot->secId){
                                        $totalWin=  $totalWin+  $bet['win'];
                                        $usertotalLoss=  $usertotalLoss+  $bet['loss'];
                                        $actual_profit_loss=  $bet['actual_profit_loss'];
                                    }
                                }
                                $profitLoss= 0;
                                if($totalWin>0) {
                                    $userLoss=$totalLoss-$usertotalLoss;
                                    $finalexpose= $userLoss-$totalWin;
                                    $finalexpose=$finalexpose * $actual_profit_loss / 100;
                                    $profitLoss=  $finalexpose;
                                }else{
                                    $finalexpose=$totalLoss *  $actualprofitloss / 100;
                                    $profitLoss= $finalexpose;
                                }

                                $runners[] = [
                                    'sportId' => $sportId,
                                    'event_id' => $eventId,
                                    'market_id' => $marketId,
                                    'sec_id' => $jackpot->secId,
                                    'col1' => $jackpot->col_1,
                                    'col2' => $jackpot->col_2,
                                    'col3' => $jackpot->col_3,
                                    'col4' => $jackpot->col_4,
                                    'slug' => 'jackpot',
                                    'rate' => $jackpot->rate,
                                    'suspended' => $jackpot->suspended,
                                    'profitLoss' => $profitLoss,
                                ];

                            }

                        }

                        $marketArr = [
                            'sport' => 'Jackpot',
                            'eventName' => $eventName,
                            'marketName' => $marketName,
                            'gameType' => $gameType,
                            'runners' => $runners,
                        ];

                    }else{
                        $jackpotDataJson =  $redis->get('Jackpot_' . $gType . '_' . $marketId);
                        $jackpotData =  json_decode($jackpotDataJson);
                        $runners = [];
                        if(!empty($jackpotData)) {
                            foreach ( $jackpotData as $jackpot ) {
                                $profitLoss = 0;
                                $runners[] = [
                                    'sportId' => $sportId,
                                    'event_id' => $eventId,
                                    'market_id' => $marketId,
                                    'sec_id' => $jackpot->secId,
                                    'col1' => $jackpot->col_1,
                                    'col2' => $jackpot->col_2,
                                    'col3' => $jackpot->col_3,
                                    'col4' => $jackpot->col_4,
                                    'slug' => 'jackpot',
                                    'rate' => $jackpot->rate,
                                    'suspended' => $jackpot->suspended,
                                    'profitLoss' => $profitLoss,
                                ];
                            }
                        }

                        $marketArr = [
                            'sport' => 'Jackpot',
                            'eventName' => $eventName,
                            'marketName' => $marketName,
                            'gameType' => $gameType,
                            'runners' => $runners,
                        ];
                    }


                    $betList = []; $betCount = 0;
                    $query = $conn->table('tbl_bet_pending_4_view')
                        ->select(['id','runner','client','master','price','size','rate','created_on','ip_address','bType'])
                        ->where([['mid',$marketId],['systemId','!=',1],['status',1]]);
                    $betCount = $query->count();
                    if( $betCount > 0 ){
                        $betList = $query->orderBy('id','desc')->limit(10)->get();
                    }

                    $betList = [ 'list' => $betList, 'count' => $betCount ];

                    $response = [ "status" => 1, "data" => [ "items" => $marketArr, 'betList' => $betList ] ];

                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data Casino
     */
    public function actionDataCricketCasino($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
//            if( !$this->checkOrigin() ){
//                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad Request !!' ] ];
//                return response()->json($response);
//            }
            $user = Auth::user();

            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData != null && $eventData->game_over != 1){
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                $cricketCasino = $this->getCricketCasino($sportId,$eventId);

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Cricket Casino',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 0,
                    'cricketCasino' => $cricketCasino,
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr , 'userData' => $userData ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // getBetAllowed
    public function getBetAllowed($id,$type)
    {
        $redis = Redis::connection();
        if( $type == 'market' ){
            $marketId = $id;
            $marketJsonData = $redis->get('Bet_Allowed_Markets');
            $marketData = json_decode($marketJsonData);
            if( !empty($marketData) && in_array($marketId,$marketData) ){
                return 0;
            }else{
                return 1;
            }
        }
        if( $type == 'event' ){
            $eventId = $id;
            $eventJsonData = $redis->get('Bet_Allowed_Events');
            $eventData = json_decode($eventJsonData);
            if( !empty($eventData) && in_array($eventId,$eventData) ){
                return 0;
            }else{
                return 1;
            }
        }

    }

    /**
     * action Bet List
     */
    public function actionBetList($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $conn = DB::connection('mongodb');
            $user = Auth::user(); $uid = $user->id;
            $checkGame = DB::table('tbl_live_game')->where([['eventId',$eventId]])->first();

            if( $checkGame != null ){
                $betList5 = $marketIds = []; $betCount5 = 0;
                $where = [['eid',(int)$eventId], ['status', 1], ['systemId', '!=', 1]];
                $query5 = $conn->table('tbl_bet_pending_teenpatti')
                    ->select(['_id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType'])
                    ->where($where);
                $betCount5 = $query5->count();
                if( $betCount5 > 0 ){
                    $betList5 = $query5->orderBy('created_on','desc')->limit(10)->get();
                }

                $data = [
                    'betList5' => [ 'list' => $betList5, 'count' => $betCount5 ],
                ];

            }else {

                $betList1 = $betList2 = $betList3 = $betList4 = $betList5 = $betList6 = [];
                $betCount1 = $betCount2 = $betCount3 = $betCount4 = $betCount5 = $betCount6 = 0;

                $where = [['eid',(int)$eventId], ['status', 1], ['systemId', '!=', 1]];

                $query1 = $conn->table('tbl_bet_pending_1_view')
                    ->select(['_id', 'runner', 'client', 'master', 'price', 'size', 'rate', 'win', 'loss', 'created_on', 'ip_address', 'bType'])
                    ->where($where);

                $betCount1 = $query1->count();
                if ($betCount1 > 0) {
                    $betList1 = $query1->orderBy('created_on', 'desc')->limit(10)->get();
                }

                $query2 = $conn->table('tbl_bet_pending_2_view')
                    ->select(['_id', 'runner', 'client', 'master', 'price', 'size', 'rate', 'win', 'loss', 'created_on', 'ip_address', 'bType'])
                    ->where($where);
                $betCount2 = $query2->count();
                if ($betCount2 > 0) {
                    $betList2 = $query2->orderBy('created_on', 'desc')->limit(10)->get();
                }

                $query3 = $conn->table('tbl_bet_pending_3_view')
                    ->select(['_id', 'runner', 'client', 'master', 'price', 'size', 'rate', 'win', 'loss', 'created_on', 'ip_address', 'bType'])
                    ->where($where);
                $betCount3 = $query3->count();
                if ($betCount3 > 0) {
                    $betList3 = $query3->orderBy('created_on', 'desc')->limit(10)->get();
                }

                $query4 = $conn->table('tbl_bet_pending_4_view')
                    ->select(['_id', 'runner', 'client', 'master', 'price', 'size', 'rate', 'win', 'loss', 'created_on', 'ip_address', 'bType'])
                    ->where($where);
                $betCount4 = $query4->count();
                if ($betCount4 > 0) {
                    $betList4 = $query4->orderBy('created_on', 'desc')->limit(10)->get();
                }

                $query5 = $conn->table('tbl_bet_pending_5_view')
                    ->select(['_id', 'runner', 'client', 'master', 'price', 'size', 'rate', 'win', 'loss', 'created_on', 'ip_address', 'bType'])
                    ->where($where);
                $betCount5 = $query5->count();
                if ($betCount5 > 0) {
                    $betList5 = $query5->orderBy('created_on', 'desc')->limit(10)->get();
                }

                $query6 = $conn->table('tbl_bet_pending_6_view')
                    ->select(['_id', 'runner', 'client', 'master', 'price', 'size', 'rate', 'win', 'loss', 'created_on', 'ip_address', 'bType'])
                    ->where($where);
                $betCount6 = $query6->count();
                if ($betCount6 > 0) {
                    $betList6 = $query6->orderBy('created_on', 'desc')->limit(10)->get();
                }

                $data = [
                    'betList1' => ['list' => $betList1, 'count' => $betCount1],
                    'betList2' => ['list' => $betList2, 'count' => $betCount2],
                    'betList3' => ['list' => $betList3, 'count' => $betCount3],
                    'betList4' => ['list' => $betList4, 'count' => $betCount4],
                    'betList5' => ['list' => $betList5, 'count' => $betCount5],
                    'betList6' => ['list' => $betList6, 'count' => $betCount6],
                ];

                $client = $marketIds = [];
                $clientJson = DB::table('tbl_user')->select('id')
                    ->where([['systemId','!=',1],['role',4],['status',1]])->get();
                if( $clientJson->isNotEmpty() ){
                    foreach ( $clientJson as $cdata ){
                        $client[] = $cdata->id;
                    }
                }

                $marketsOnBet = $conn->table('tbl_user_market_expose');
                if( $client != null && in_array($user->role , [2,3]) ){
                    $marketsOnBet = $marketsOnBet->where([['status',1],['sid','!=',99],['sid','!=',999],['systemId','!=',1]])
                        ->whereIn('uid',$client);
                }elseif ($user->role == 1 || $user->role == 6){
                    $marketsOnBet = $marketsOnBet->where([['status',1],['sid','!=',99],['sid','!=',999],['systemId','!=',1]]);
                }else{
                    $marketsOnBet = $marketsOnBet->where([['uid',$user->id],['status',1],['sid','!=',99],['sid','!=',999],['systemId','!=',1]]);
                }
                $marketsOnBet = $marketsOnBet->distinct()->select('mid')->get();
                if( $marketsOnBet != null ){
                    $marketIds = [];
                    foreach ( $marketsOnBet as $key => $mid ){
                        $marketIds[] = $mid;
                    }
                }
            }
            $response = [ "status" => 1, "data" => $data, 'marketIds' => $marketIds ];

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Bet List
     */
    public function actionMarketBetList(Request $request)
    {

        try{
            $conn = DB::connection('mongodb');
            $user = Auth::user();
            $uid = $user->id;

            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $client = [];
            $offset = 0; $limit = $loadCount = 20;
            if( isset($request->page) ){
                $offset = ($request->page - 1) * $limit;
                $loadCount = $request->page*$loadCount;
            }

            if( !isset($request->page) ){
                $offset = 0; $limit = $loadCount = 5000;
            }
            if( isset($request->mtype) && isset($request->eid)){
                $betList = []; $betCount = 0;
                $mType = $request->mtype;
                $systemId = $user->systemId;

                if( $mType == 'live-games' ){
                    $tbl = 'tbl_bet_pending_teenpatti_view';
                }elseif ( $mType == 'casino'){
                    $tbl = 'tbl_casino_transaction_view';
                }elseif ( $mType == 'binary'){
                    $tbl = 'tbl_bet_pending_5_view';
                }elseif ( $mType == 'other' || $mType == 'bookmaker' || $mType == 'virtual-cricket'){
                    $tbl = 'tbl_bet_pending_1_view';
                }elseif ( $mType == 'fancy' || $mType == 'fancy2' || $mType == 'fancy3' || $mType == 'oddeven'  ){
                    $tbl = 'tbl_bet_pending_2_view';
                }elseif ( $mType == 'meter' || $mType == 'ball' || $mType == 'khado' ){
                    $tbl = 'tbl_bet_pending_3_view';
                }elseif ( $mType == 'goals' || $mType == 'set_market' || $mType == 'winner' ){
                    $tbl = 'tbl_bet_pending_6_view';
                }else{ $tbl = 'tbl_bet_pending_4_view'; }

                if( isset($request->mid) && $request->mid != null){
                    $marketId = $request->mid;
                    $eventId = $request->eid;
                    if( $mType == 'casino' ){
                        if( strpos($request->mid,'B') > 0 ){
                            $marketId = str_replace('-B','',$request->mid);
                        }else{
                            $marketId = str_replace('-W','',$request->mid);
                        }
                        $where = [['round', $marketId],['round_closed',0],['status', 1], ['systemId','!=', 1]];
                    }else {
                        $where = [['mid',$marketId],['eid',(int)$eventId],['status',1],['systemId','!=',1]];
                    }

                }else{
                    $eventId = $request->eid;
                    if( $mType == 'casino' ){
                        $where = [['status',1],['systemId','!=',1]];
                    }else {
                        $where = [['status',1],['eid',(int)$eventId],['systemId','!=',1]];
                    }

                }

                if( $mType == 'casino' ){
                    if( isset($request->client) && $request->client != null ){
                        array_push($where, ['username', 'like', '%' . $request->client . '%']);
                    }
                    if( isset($request->runner) && $request->runner != null ){
                        array_push($where, ['round', 'like', '%' . $request->runner . '%']);
                    }
                }else{
                    if( isset($request->client) && $request->client != null ){
                        array_push($where, ['client', 'like', '%' . $request->client . '%']);
                    }
                    if( isset($request->runner) && $request->runner != null ){
                        array_push($where, ['runner', 'like', '%' . $request->runner . '%']);
                    }
                }

                $query = $conn->table($tbl)->where($where);
                if( $client != null ){ $query->whereIn('uid',$client); }

                if( in_array( $mType,['fancy','fancy2','fancy3','oddeven'] ) ){
                    $mTypeArr = [];$mTypeArr[] = $mType;
                    $query->whereIn('mType',$mTypeArr);
                }

                if( in_array( $mType,['meter','ball','khado'])  ){
                    $mTypeArr = [];
                    if( $mType == 'ball' ){
                        $mType = 'ballbyball';
                    }
                    $mTypeArr[] = $mType;
                    $query->whereIn('mType',$mTypeArr);
                }

                $betCount = $query->count();
                if( $betCount > 0 ){
                    if( ( isset($request->client) && $request->client != null )
                        || ( isset($request->runner) && $request->runner != null ) ){
                        $betList = $query->orderBy('created_on','desc')->get();
                    }else{
                        $betList = $query->orderBy('created_on','desc')
                            ->offset($offset)->limit($limit)->get();
                    }

                    if( $mType == 'casino' && $betList->isNotEmpty() ){

                        $casinoGameList = $conn->table('tbl_casino_games')
                            ->where([['status',1]])->get();
                        if( $casinoGameList->isNotEmpty() ){
                            $casinoGameArr = [];
                            foreach ($casinoGameList as $casinoGame){
                                $casinoGame = (object)$casinoGame;
                                $casinoGameArr[$casinoGame->game_id] = $casinoGame->name;
                            }
                        }

                        $betListArr = $betList;
                        $betList1 = [];
                        foreach ($betListArr as $bets){
                            $bets = (object)$bets;

                            if( isset($casinoGameArr[$bets->game_id]) ){
                                $gameName = $casinoGameArr[$bets->game_id];
                                $description = 'Casino > '.$casinoGameArr[$bets->game_id].' > Round #'.$bets->round;
                            }else{
                                $gameName = 'GameId: '.$bets->game_id;
                                $description = 'Casino > GameId: '.$bets->game_id.' > Round #'.$bets->round;
                            }

                            $betList1[] = [
                                '_id' => $bets->_id,
                                'price' => '-',
                                'size' => $bets->amount,
                                'bType' => 'back',
                                'rate' => '-',
                                'mType' => 'casino',
                                'runner' => $gameName,
                                'ip_address' => isset($bets->ip_address) ? $bets->ip_address : '-' ,
                                'client' => $bets->username,
                                'master' => '-',
                                'description' => $description,
                                'win' => 0,
                                'loss' => $bets->amount,
                                'result' => '-',
                                'created_on' => $bets->created_on
                            ];
                        }
                        $betList = $betList1;
                    }
                }

                $response = [ "status" => 1, 'userData' => $userData,"data" => [ 'list' => $betList, 'count' => $betCount , 'loadCount' => $loadCount ] ];

            }else{

                $betList = []; $betCount = 0; $systemId = $user->systemId;
                $casinoList = false;
                if( strpos($request->mid,'B') > 0 || strpos($request->mid,'W') > 0 ){
                    $casinoList = true;
                }
                if( strpos($request->mid,'BM') > 0 || strpos($request->mid,'BB') > 0 ){
                    $casinoList = false;
                }

                if( isset($request->mid) && $request->mid != null && $casinoList ){
                    $tbl = 'tbl_casino_transaction_view';
                    if( strpos($request->mid,'B') > 0 ){
                        $marketId = str_replace('-B','',$request->mid);
                    }else{
                        $marketId = str_replace('-W','',$request->mid);
                    }

                    $where = [['type','DEBIT'],['round',$marketId],['status',1],['systemId','!=',1]];
                    $query = $conn->table($tbl)->where($where);

                    if( $client != null ){ $query->whereIn('uid',$client); }

                    $betCount = $query->count();
                    $betListArr = $query->orderBy('created_on','desc')
                        ->offset($offset)->limit($limit)->get();

                    if( $betListArr->isNotEmpty() ){

                        $casinoGameList = $conn->table('tbl_casino_games')
                            ->where([['status',1]])->get();
                        if( $casinoGameList->isNotEmpty() ){
                            $casinoGameArr = [];
                            foreach ($casinoGameList as $casinoGame){
                                $casinoGame = (object)$casinoGame;
                                $casinoGameArr[$casinoGame->game_id] = $casinoGame->name;
                            }
                        }

                        $betList = [];
                        foreach ($betListArr as $bets){
                            $bets = (object)$bets;

                            if( isset($casinoGameArr[$bets->game_id]) ){
                                $gameName = $casinoGameArr[$bets->game_id];
                                $description = 'Casino > '.$casinoGameArr[$bets->game_id].' > Round #'.$bets->round;
                            }else{
                                $gameName = 'GameId: '.$bets->game_id;
                                $description = 'Casino > GameId: '.$bets->game_id.' > Round #'.$bets->round;
                            }

                            $betList[] = [
                                '_id' => $bets->_id,
                                'price' => '-',
                                'size' => $bets->amount,
                                'bType' => 'back',
                                'rate' => '-',
                                'mType' => 'casino',
                                'runner' => $gameName,
                                'ip_address' => isset($bets->ip_address) ? $bets->ip_address : '-' ,
                                'client' => $bets->username,
                                'master' => '-',
                                'description' => $description,
                                'win' => 0,
                                'loss' => $bets->amount,
                                'result' => '-',
                                'created_on' => $bets->created_on
                            ];
                        }

                    }


                }else{
                    $tbl = 'tbl_bet_history';

                    if( isset($request->mid) && $request->mid != null){
                        $marketId = $request->mid;
                        $where = [['mid',$marketId],['status',1],['systemId','!=',1]];
                    }else{
                        $where = [['status',1],['systemId','!=',1]];
                    }

                    if( isset($request->client) && $request->client != null ){
                        array_push($where, ['client', 'like', '%' . $request->client . '%']);
                    }
                    if( isset($request->runner) && $request->runner != null ){
                        array_push($where, ['runner', 'like', '%' . $request->runner . '%']);
                    }

                    $query = $conn->table($tbl)
                        ->select(['_id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType','mType','result','description'])
                        ->where($where);

                    if( $client != null ){ $query->whereIn('uid',$client); }

                    $betCount = $query->count();
                    if( $betCount > 0 ){
                        $betList = $query->orderBy('created_on','desc')->get();
                    }else{
                        $tbl = 'tbl_bet_pending_teenpatti_view';
                        $query = $conn->table($tbl)
                            ->select(['_id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType','mType','result','description'])
                            ->where($where);

                        if( $client != null ){ $query->whereIn('uid',$client); }

                        $betCount = $query->count();
                        if( $betCount > 0 ){
                            $betList = $query->orderBy('created_on','desc')->get();
                        }

                    }
                }
                $response = [ "status" => 1,'userData' => $userData, "data" => [ 'list' => $betList, 'count' => $betCount ] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // get Other Market
    public function getOtherMarket($user,$eventData,$eventId,$slug)
    {
        $otherMarketArr = $runnersArr = [];
        $redis = Redis::connection();

        // Winner
        $winnerMarketJson = $redis->get('Winner_'.$eventId);
        $winnerMarket = json_decode($winnerMarketJson);

        if (!empty( $winnerMarket ) && isset( $winnerMarket->marketId )
            && isset( $winnerMarket->game_over ) && $winnerMarket->game_over == 0 && $winnerMarket->status == 1 ) {

            $marketId = $winnerMarket->marketId;
            $suspended = $winnerMarket->suspended;
            $ballRunning = $winnerMarket->ball_running;
            $betAllowed = $winnerMarket->bet_allowed;
            $info = $winnerMarket->info;
            $time = $eventData->time;
            $sportId = $eventData->sportId;

            $runnersArr = $this->winnerRunnerDataWithBook($user,$slug,$sportId,$eventId,$winnerMarket);

            $otherMarketArr['Winner'] = [
                'sportId' => $sportId,
                'slug' => $slug,
                'mType' => 'winner',
                'marketId' => $marketId,
                'eventId' => $eventId,
                'suspended' => $suspended,
                'ballRunning' => $ballRunning,
                'bet_allowed' => $betAllowed,
                'time' => $time,
                'marketName' => 'Winner',
                'matched' => '',
                'info' => $info,
                'runners' => $runnersArr,
            ];

        }

        // Match_Odd
        $matchOddMarketJson = $redis->get('Match_Odd_'.$eventId);
        $matchOddMarket = json_decode($matchOddMarketJson);

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )
            && isset( $matchOddMarket->game_over ) && $matchOddMarket->game_over == 0 && $matchOddMarket->status == 1 ) {

            $marketId = $matchOddMarket->marketId;
            $suspended = $matchOddMarket->suspended;
            $ballRunning = $matchOddMarket->ball_running;
            $betAllowed = $matchOddMarket->bet_allowed;
            $info = $matchOddMarket->info;
            $time = $eventData->time;
            $sportId = $eventData->sportId;

            $runnersArr = $this->matchOddsRunnerDataWithBook($user,$slug,$sportId,$eventId,$matchOddMarket);

            $otherMarketArr['MatchOdd'] = [
                'sportId' => $sportId,
                'slug' => $slug,
                'mType' => 'match_odd',
                'marketId' => $marketId,
                'eventId' => $eventId,
                'suspended' => $suspended,
                'ballRunning' => $ballRunning,
                'bet_allowed' => $betAllowed,
                'time' => $time,
                'marketName' => 'Match Odds',
                'matched' => '',
                'info' => $info,
                'runners' => $runnersArr,
            ];

        }

        // Completed_Match
        $completedMatchMarketJson = $redis->get('Completed_Match_'.$eventId);
        $completedMatchMarket = json_decode($completedMatchMarketJson);

        if (!empty( $completedMatchMarket ) && isset( $completedMatchMarket->marketId )
            && isset( $completedMatchMarket->game_over ) && $completedMatchMarket->game_over == 0 && $completedMatchMarket->status == 1 ) {

            $marketId = $completedMatchMarket->marketId;
            $suspended = $completedMatchMarket->suspended;
            $ballRunning = $completedMatchMarket->ball_running;
            $betAllowed = $completedMatchMarket->bet_allowed;
            $info = $completedMatchMarket->info;
            $time = $eventData->time;
            $sportId = $eventData->sportId;

            $runnersArr = $this->completedMatchRunnerDataWithBook($user,$slug,$sportId,$eventId,$completedMatchMarket);

            $otherMarketArr['CompletedMatch'] = [
                'sportId' => $sportId,
                'slug' => $slug,
                'mType' => 'completed_match',
                'marketId' => $marketId,
                'eventId' => $eventId,
                'suspended' => $suspended,
                'ballRunning' => $ballRunning,
                'bet_allowed' => $betAllowed,
                'time' => $time,
                'marketName' => 'Completed Match',
                'matched' => '',
                'info' => $info,
                'runners' => $runnersArr,
            ];

        }

        // Tied_Match
        $tiedMatchMarketJson = $redis->get('Tied_Match_'.$eventId);
        $tiedMatchMarket = json_decode($tiedMatchMarketJson);

        if (!empty( $tiedMatchMarket ) && isset( $tiedMatchMarket->marketId )
            && isset( $tiedMatchMarket->game_over ) && $tiedMatchMarket->game_over == 0 && $tiedMatchMarket->status == 1 ) {

            $marketId = $tiedMatchMarket->marketId;
            $suspended = $tiedMatchMarket->suspended;
            $ballRunning = $tiedMatchMarket->ball_running;
            $betAllowed = $tiedMatchMarket->bet_allowed;
            $info = $tiedMatchMarket->info;
            $time = $eventData->time;
            $sportId = $eventData->sportId;

            $runnersArr = $this->tiedMatchRunnerDataWithBook($user,$slug,$sportId,$eventId,$tiedMatchMarket);

            $otherMarketArr['TiedMatch'] = [
                'sportId' => $sportId,
                'slug' => $slug,
                'mType' => 'tied_match',
                'marketId' => $marketId,
                'eventId' => $eventId,
                'suspended' => $suspended,
                'ballRunning' => $ballRunning,
                'bet_allowed' => $betAllowed,
                'time' => $time,
                'marketName' => 'Tied Match',
                'matched' => '',
                'info' => $info,
                'runners' => $runnersArr,
            ];

        }

        return $otherMarketArr;

    }

    // Winner Runner Data With Book New
    public function winnerRunnerDataWithBook($user,$slug,$sportId,$eventId,$winnerMarket)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;

        if (!empty( $winnerMarket ) && isset( $winnerMarket->marketId )) {

            $marketId = $winnerMarket->marketId;
            $suspended = $winnerMarket->suspended;
            $ballRunning = $winnerMarket->ball_running;
            $runnerData = json_decode($winnerMarket->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);

            $betCount = $query->count();
            if( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }

            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];
                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'slug' => $slug,
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profit_loss' => $profitLoss,
                        'mType' => 'winner',
                        'backPrice' => '-',
                        'backSize' => '',
                        'layPrice' => '-',
                        'laySize' => ''
                    ];
                }
            }

            $this->marketIdsArr[] = $marketId;
            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }
        }

        return $runnersArr;

    }

    // Match Odds Runner Data With Book New
    public function matchOddsRunnerDataWithBook($user,$slug,$sportId,$eventId,$matchOddMarket)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {

            $marketId = $matchOddMarket->marketId;
            $suspended = $matchOddMarket->suspended;
            $ballRunning = $matchOddMarket->ball_running;
            $runnerData = json_decode($matchOddMarket->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);

            $betCount = $query->count();
            if( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }

            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];
                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'slug' => $slug,
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profit_loss' => $profitLoss,
                        'mType' => 'match_odd',
                        'backPrice' => '-',
                        'backSize' => '',
                        'layPrice' => '-',
                        'laySize' => ''
                    ];
                }
            }

            $this->marketIdsArr[] = $marketId;
            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }
        }

        return $runnersArr;

    }

    // Completed Match Runner Data With Book New
    public function completedMatchRunnerDataWithBook($user,$slug,$sportId,$eventId,$completedMatchMarket)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;
        if (!empty( $completedMatchMarket ) && isset( $completedMatchMarket->marketId )) {

            $marketId = $completedMatchMarket->marketId;
            $suspended = $completedMatchMarket->suspended;
            $ballRunning = $completedMatchMarket->ball_running;
            $runnerData = json_decode($completedMatchMarket->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);
            $betCount = $query->count();
            if( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }
            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];
                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'slug' => $slug,
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profit_loss' => $profitLoss,
                        'mType' => 'completed_match',
                        'backPrice' => '-',
                        'backSize' => '',
                        'layPrice' => '-',
                        'laySize' => ''
                    ];
                }
            }

            $this->marketIdsArr[] = $marketId;
            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }
        }

        return $runnersArr;

    }

    // Tied Match Runner Data With Book New
    public function tiedMatchRunnerDataWithBook($user,$slug,$sportId,$eventId,$tiedMatchMarket)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;
        if (!empty( $tiedMatchMarket ) && isset( $tiedMatchMarket->marketId )) {

            $marketId = $tiedMatchMarket->marketId;
            $suspended = $tiedMatchMarket->suspended;
            $ballRunning = $tiedMatchMarket->ball_running;
            $runnerData = json_decode($tiedMatchMarket->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);
            $betCount = $query->count();
            if( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }
            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];

                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'slug' => $slug,
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profit_loss' => $profitLoss,
                        'mType' => 'tied_match',
                        'backPrice' => '-',
                        'backSize' => '',
                        'layPrice' => '-',
                        'laySize' => ''
                    ];
                }
            }

            $this->marketIdsArr[] = $marketId;
            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }

        }

        return $runnersArr;

    }

    // get Goals Maker
    public function getGoalsMaker($user,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $goalsMakerJson = $redis->get("Goals_" . $eventId);
        $goalsMaker = json_decode($goalsMakerJson);
        if ($goalsMaker != null) {
            foreach ( $goalsMaker as $data ) {
                if( !isset( $data->game_over ) ){ $data = json_decode($data); }
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1){
                    $runners = $this->goalsMakerRunnerDataWithBook($user,$sportId,$eventId,$data);

                    $items[] = [
                        'marketName' => $data->title,
                        'sportId' => $sportId,
                        'market_id' => $data->marketId,
                        'event_id' => $eventId,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'info' => $data->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $data->marketId;
                }

            }

        }

        return $items;
    }

    // get Set Market
    public function getSetMarket($user,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $setMarketJson = $redis->get("Set_Market_" . $eventId);
        $setMarket = json_decode($setMarketJson);

        if ($setMarket != null) {
            foreach ( $setMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1){
                    $runners = $this->setMarketRunnerDataWithBook($user,$sportId,$eventId,$data);

                    $items[] = [
                        'marketName' => $data->title,
                        'sportId' => $sportId,
                        'market_id' => $data->marketId,
                        'event_id' => $eventId,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'info' => $data->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $data->marketId;
                }

            }

        }

        return $items;
    }

    // get Book Maker
    public function getBookMaker($user,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $bookMakerJson = $redis->get("Bookmaker_" . $eventId);
        $bookMaker = json_decode($bookMakerJson);

        if ($bookMaker != null) {
            foreach ( $bookMaker as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ){
                    $runners = $this->bookmakerRunnerDataWithBook($user,$sportId,$eventId,$data);

                    $items[] = [
                        'marketName' => $data->title,
                        'sportId' => $sportId,
                        'market_id' => $data->marketId,
                        'event_id' => $eventId,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'info' => $data->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $data->marketId;
                }

            }

        }

        return $items;
    }

    // GoalsMaker Runner Data With Book New
    public function goalsMakerRunnerDataWithBook($user,$sportId,$eventId,$goalsMakerData)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;
        if (!empty( $goalsMakerData ) && isset( $goalsMakerData->marketId )) {
            $marketId = $goalsMakerData->marketId;
            $suspended = $goalsMakerData->suspended;
            $ballRunning = $goalsMakerData->ball_running;
            $runnerData = json_decode($goalsMakerData->runners);

            $query = $conn->table('tbl_bet_pending_6_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);
            $betCount = $query->count();
            if ( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }
            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];
                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'secId' => $runner->secId,
                        'runner' => $runner->runner,
                        'mType' => 'goals',
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'profitLoss' => $profitLoss,
                        'backPrice' => 0,
                        'backSize' => '',
                        'layPrice' => 0,
                        'laySize' => ''
                    ];

                }
            }

            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }
        }

        return $runnersArr;

    }

    // Set Market Runner Data With Book New
    public function setMarketRunnerDataWithBook($user,$sportId,$eventId,$setMarketData)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;
        if (!empty( $setMarketData ) && isset( $setMarketData->marketId )) {
            $marketId = $setMarketData->marketId;
            $suspended = $setMarketData->suspended;
            $ballRunning = $setMarketData->ball_running;
            $runnerData = json_decode($setMarketData->runners);

            $query = $conn->table('tbl_bet_pending_6_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);
            $betCount = $query->count();
            if ( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }
            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];
                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'secId' => $runner->secId,
                        'runner' => $runner->runner,
                        'mType' => 'set_market',
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'profitLoss' => $profitLoss,
                        'backPrice' => 0,
                        'backSize' => '',
                        'layPrice' => 0,
                        'laySize' => ''
                    ];

                }
            }

            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }
        }

        return $runnersArr;

    }

    // Bookmaker Runner Data With Book New
    public function bookmakerRunnerDataWithBook($user,$sportId,$eventId,$bookmakerData)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;
        if (!empty( $bookmakerData ) && isset( $bookmakerData->marketId )) {
            $marketId = $bookmakerData->marketId;
            $suspended = $bookmakerData->suspended;
            $ballRunning = $bookmakerData->ball_running;
            $runnerData = json_decode($bookmakerData->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);
            $betCount = $query->count();
            if ( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }
            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];
                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_bookmaker_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'secId' => $runner->secId,
                        'runner' => $runner->runner,
                        'mType' => 'bookmaker',
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'profitLoss' => $profitLoss,
                        'backPrice' => 0,
                        'backSize' => '',
                        'layPrice' => 0,
                        'laySize' => ''
                    ];

                }
            }

            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }
        }

        return $runnersArr;

    }

    // get virtual Cricket
    public function getVirtualCricket($user,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $virtualCricketJson = $redis->get("Virtual_" . $eventId);
        $virtualCricket = json_decode($virtualCricketJson);

        if ($virtualCricket != null) {
            foreach ( $virtualCricket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ){
                    $runners = $this->virtualCricketRunnerDataWithBook($user,$sportId,$eventId,$data);

                    $items[] = [
                        'marketName' => $data->title,
                        'sportId' => $sportId,
                        'market_id' => $data->marketId,
                        'event_id' => $eventId,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'info' => $data->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $data->marketId;
                }

            }

        }

        return $items;
    }

    // virtual Cricket Runner Data With Book New
    public function virtualCricketRunnerDataWithBook($user,$sportId,$eventId,$virtualCricketData)
    {
        $conn = DB::connection('mongodb');
        $redis = Redis::connection();
        $runnersArr = $finalTotalArr = $clientArr = []; $betCount = 0;
        if (!empty( $virtualCricketData ) && isset( $virtualCricketData->marketId )) {
            $marketId = $virtualCricketData->marketId;
            $suspended = $virtualCricketData->suspended;
            $ballRunning = $virtualCricketData->ball_running;
            $runnerData = json_decode($virtualCricketData->runners);

            $query = $conn->table('tbl_bet_pending_1_view')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);
            $betCount = $query->count();
            if( $betCount > 0 ) {
                // Redis check and return
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $getRedisDataJson = $redis->get($redisDataKey);
                $getRedisData = json_decode($getRedisDataJson);
                if ($getRedisData != null && isset($getRedisData->count) && isset($getRedisData->runnersArr)) {
                    if ($getRedisData->count == $betCount) {
                        $runnersArr = $getRedisData->runnersArr;
                        $this->marketIdsArr[] = $marketId;
                        return $runnersArr;
                    }
                }
            }
            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = $clientListArr = [];

                if( $user->role == 6 ){ $user->id = 1; }
                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }
                $bookDataProfitLoss = DB::table('tbl_book_bookmaker_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['upl.userId',$user->id]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[$book->secId][] = $total;
                        }else{
                            $totalArr[$book->secId][] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'secId' => $runner->secId,
                        'runner' => $runner->runner,
                        'mType' => 'virtual_cricket',
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'profitLoss' => $profitLoss,
                        'backPrice' => 0,
                        'backSize' => '',
                        'layPrice' => 0,
                        'laySize' => ''
                    ];

                }
            }
            if( $betCount > 0 ) {
                $redisDataKey = 'WL-' . $user->id . '-' . $marketId;
                $redisData = ['count' => $betCount, 'runnersArr' => $runnersArr];
                $redisDataJson = json_encode($redisData);
                $redis->set($redisDataKey, $redisDataJson);
            }
        }

        return $runnersArr;

    }

    // get Fancy Market
    public function getFancyMarket($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $fancyMarketJson = $redis->get("Fancy_" . $eventId);
        $fancyMarket = json_decode($fancyMarketJson);

        if ( $fancyMarket != null ) {
            foreach ( $fancyMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1) {
                    $items[] = [
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'fancy',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Fancy2 Market
    public function getFancy2Market($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $fancyMarketJson = $redis->get("Fancy_2_" . $eventId);
        $fancyMarket = json_decode($fancyMarketJson);

        if ( $fancyMarket != null ) {

            foreach ( $fancyMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'fancy2',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Meter Fancy
    public function getMeterFancy($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $meterFancyJson = $redis->get("Meter_" . $eventId);
        $meterFancy = json_decode($meterFancyJson);

        if ( $meterFancy != null ) {

            foreach ( $meterFancy as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'meter',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Fancy3 Market
    public function getFancy3Market($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $fancyMarketJson = $redis->get("Fancy_3_" . $eventId);
        $fancyMarket = json_decode($fancyMarketJson);

        if ( $fancyMarket != null ) {
            foreach ( $fancyMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'fancy3',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Odd Even Market
    public function getOddEvenMarket($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $oddEvenMarketJson = $redis->get("Odd_Even_" . $eventId);
        $oddEvenMarket = json_decode($oddEvenMarketJson);
        if ( $oddEvenMarket != null ) {
            foreach ( $oddEvenMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'oddeven',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Khado Session
    public function getKhadoSession($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $khadoSessionJson = $redis->get("Khado_" . $eventId);
        $khadoSession = json_decode($khadoSessionJson);

        if ( $khadoSession != null ) {

            foreach ( $khadoSession as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'khado',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                        'diff' => $data->diff,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }
        return $items;
    }

    // get Ball Session
    public function getBallSession($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $ballSessionJson = $redis->get("Ballbyball_" . $eventId);
        $ballSession = json_decode($ballSessionJson);

        if ( $ballSession != null ) {

            foreach ( $ballSession as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'ballbyball',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                        'ball' => $data->ball,
                        'onEdit' => $data->editOn
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }
        return $items;
    }

    // get Jackpot Data
    public function getJackpotData($sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();

        $jackpotGameTypeJson = $redis->get("Jackpot_type");
        $jackpotGameType = json_decode($jackpotGameTypeJson);

        foreach ( $jackpotGameType as $type ) {

            $jackpotDataJson = $redis->get("Jackpot_data_".$type->id."_". $eventId);
            $jackpotData = json_decode($jackpotDataJson);
            if( $jackpotData != null ){
                $listData = [];
                if( isset($jackpotData->game_over) ){
                    $data = $jackpotData;
                    if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                        $isBetAllowed = $this->getBetAllowed($data->marketId,'market');
                        $listData[] = [
                            'marketId' => $data->marketId,
                            'title' => $data->title,
                            'suspended' => $data->suspended,
                            'ballRunning' => $data->ball_running,
                            'bet_allowed' => $isBetAllowed,
                        ];
                    }
                }else{
                    foreach ( $jackpotData as $data ) {
                        if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                            $isBetAllowed = $this->getBetAllowed($data->marketId,'market');
                            $listData[] = [
                                'marketId' => $data->marketId,
                                'title' => $data->title,
                                'suspended' => $data->suspended,
                                'ballRunning' => $data->ball_running,
                                'bet_allowed' => $isBetAllowed,
                            ];
                        }
                    }
                }


                if( isset( $type->status ) && $type->status == 1 ) {
                    $items[] = [
                        'eventId' => $eventId,
                        'title' => $type->title,
                        'sportId' => $sportId,
                        'typeId' => $type->id,
                        'listData' => $listData
                    ];
                }
            }

        }

        return $items;

    }

    // get Jackpot Data
    public function getJackpotDataOLD($sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();

        $jackpotGameTypeJson = $redis->get("Jackpot_type");
        $jackpotGameType = json_decode($jackpotGameTypeJson);

        foreach ( $jackpotGameType as $type ) {

            $jackpotDataJson = $redis->get("Jackpot_data_".$type->id."_". $eventId);
            $jackpotData = json_decode($jackpotDataJson);

            if( $jackpotData != null ){
                $listData = [];
                foreach ( $jackpotData as $data ) {
                    if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                        $listData[] = [
                            'marketId' => $data->marketId,
                            'title' => $data->title,
                            'suspended' => $data->suspended,
                            'ballRunning' => $data->ball_running,
                            'bet_allowed' => $data->bet_allowed,
                        ];
                    }
                }

                if( isset( $type->status ) && $type->status == 1 ) {
                    $items[] = [
                        'eventId' => $eventId,
                        'title' => $type->title,
                        'sportId' => $sportId,
                        'typeId' => $type->id,
                        'listData' => $listData
                    ];
                }
            }

        }

        return $items;

    }

    // get Cricket Casino
    public function getCricketCasino($sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $casinoDataJson = $redis->get("Casino_". $eventId);
        $casinoData = json_decode($casinoDataJson);

        if( $casinoData != null ){
            foreach ( $casinoData as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1) {
                    $items[] = [
                        'id' => $data->id,
                        'sportId' => $sportId,
                        'mType' => 'cricket_casino',
                        'eventId' => $eventId,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'is_book' => 0,
                        'rate' => $data->rate,
                        'bet_allowed' => $data->bet_allowed,
                    ];
                }
            }
        }

        return $items;

    }

    // setBookData
    public function setBookData($uid,$betData)
    {
        $redis = Redis::connection();
        $bookMakerJson = $redis->get("Market_" . $betData->mid);
        $bookMaker = json_decode($bookMakerJson);

        if( $bookMaker != null && isset( $bookMaker->runners )){

            $eventId = $bookMaker->eid;
            $marketId = $bookMaker->mid;

            $runners = json_decode($bookMaker->runners);

            if( $runners != null ){
                $i = 0; $bookDataArr = [];
                foreach ( $runners as $runner ){

                    $bookDataArr[$i] = [
                        'uid' => $betData->uid,
                        'eid' => $eventId,
                        'mid' => $marketId,
                    ];

                    if( $betData->bType == 'lay' ){

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate*$betData->size )/100;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book, 2);
                        }

                    }else{

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate*$betData->size )/100;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book, 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                        }

                    }


                    $i++;
                }

                if( $bookDataArr != null ){

                    $book = DB::table('tbl_book_bookmaker_view')->select(['book'])
                        ->where([['uid',$uid],['eid',$eventId],['mid',$marketId]])->first();

                    if( $book != null ){

                        foreach ( $bookDataArr as $bookData ){
                            $where = [['uid',$uid],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
                            $bookCheck = DB::table('tbl_book_bookmaker_view')->select(['book'])->where($where)->first();
                            if( $bookCheck != null ){
                                $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
                                DB::table('tbl_book_bookmaker')->where($bookDataArr)->update(['book'=>$newBook]);
                            }else{
                                DB::table('tbl_book_bookmaker')->insert($bookData);
                            }
                        }

                    }else{
                        DB::table('tbl_book_bookmaker')->insert($bookDataArr);
                    }


                }


            }
        }

    }

    // get Binary Market
    public function getBinaryMarket($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $binaryMarketJson = $redis->get("Binary_" . $eventId);
        $binaryMarket = json_decode($binaryMarketJson);

        if ( $binaryMarket != null ) {
            //dd($binaryMarket);
            foreach ( $binaryMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => $data->mType,
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }
}
