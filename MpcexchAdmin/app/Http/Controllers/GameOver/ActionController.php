<?php

namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class ActionController extends Controller
{
    public function __construct(){
        $this->redis = Redis::connection();
    }

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }

    // Cron Game Over -->other market not in 'match_odd','bookmaker','fancy2','goals','set_market'
    public function index(){
        try {
            $conn = DB::connection('mongodb');
            $response = ["status" => 0, "error" => ["code" => 400, "message" => "Bad request!"]];

           // $marketResult = $conn->table('tbl_market_result')->where([['game_over', 0],['status', 1]])->first();
            $gameoveractiveStatus = $this->redis->get("gameoverStatus");
            if($gameoveractiveStatus != 1 || $gameoveractiveStatus != 2) {
                $this->redis->set("gameoverStatus", 2);
            }
            $endTime = time() + 60;
            while ($endTime > time()) {
                //sleep(1); 300000 micro s == 300 ms
                usleep(20000000);
                $gameoverStatus = $this->redis->get("gameoverStatus");
                $marketResult = $conn->table('tbl_market_result')
                    ->where([['game_over', 0], ['status', 1]])->whereNotIn('mType', ['match_odd', 'bookmaker', 'fancy2'])->limit(5)->get();
                if ($marketResult != null && $gameoverStatus == 2) {
                    $this->redis->set("gameoverStatus", 1);
                    foreach ($marketResult as $market) {
                        $market = (object)$market;
                        $eventId = (int)$market->eventId;
                        $marketId = $market->marketId;
                        $mType = $market->mType;

                        $eventData = $this->redis->get("Event_" . $eventId);
                        $eventData = json_decode($eventData);
                        $event_name = '';
                        if (isset($eventData->name)) {
                            $event_name = $eventData->name;
                        }

                        $mData = $this->redis->get("Market_" . $marketId);
                        $mData = json_decode($mData);
                        $market_name = '';
                        if (isset($mData->title)) {
                            $market_name = $mData->title;
                        }

                        $mtbl = 'tbl_market_data';
                        $where = [['eventId', $eventId], ['marketId', $marketId]];
                        $result = $conn->table($mtbl)->where($where)->first();
                        if (empty($result)) {
                            $insertArr = [
                                'eventId' => (int)$eventId,
                                'marketId' => $marketId,
                                'event_name' => $event_name,
                                'market_name' => $market_name,
                                'mData' => json_encode($mData)
                            ];
                            $conn->table($mtbl)->insert($insertArr);

                        }


                        $mTypeArr1 = ['match_odd', 'completed_match', 'tied_match', 'bookmaker', 'virtual_cricket'];
                        $mTypeArr2 = ['fancy', 'fancy2', 'fancy3', 'oddeven'];
                        $mTypeArr3 = ['meter', 'khado', 'ballbyball'];
                        $mTypeArr4 = ['cricket_casino', 'jackpot'];
                        $mTypeArr5 = ['USDINR', 'GOLD', 'SILVER', 'EURINR', 'GBPINR', 'ALUMINIUM', 'COPPER', 'CRUDEOIL', 'ZINC', 'NIFTY', 'BANKNIFTY'];
                        $mTypeArr6 = ['set_market', 'goals', 'winner'];
                        if (in_array($mType, $mTypeArr1)) {
                            $tbl = 'tbl_bet_pending_1';
                        } else if (in_array($mType, $mTypeArr2)) {
                            $tbl = 'tbl_bet_pending_2';
                        } else if (in_array($mType, $mTypeArr3)) {
                            $tbl = 'tbl_bet_pending_3';
                        } else if (in_array($mType, $mTypeArr4)) {
                            $tbl = 'tbl_bet_pending_4';
                        } else if (in_array($mType, $mTypeArr5)) {
                            $tbl = 'tbl_bet_pending_5';
                        } else if (in_array($mType, $mTypeArr6)) {
                            $tbl = 'tbl_bet_pending_6';
                        } else {
                            $tbl = 'tbl_bet_pending_7';
                        }

                        $market = $conn->table('tbl_market_result')
                            ->where([['marketId', $marketId], ['game_over', 0]])->first();
                        if ($market != null) {
                            // check bet exist;
                            $checkBets = $conn->table($tbl)
                                ->where([['mid', $marketId], ['result', 'PENDING']])->first();
                            if ($checkBets != null) {
                                if (isset($market['recall']) && $market['recall'] == 2) {
                                    $res1 = $this->abundantResult($marketId, $tbl);
                                } else {
                                    $res1 = $this->gameOverResult($market, $tbl);
                                    $res2 = $this->transactionResult($market, $tbl);
                                }
                            }
                            //Update User Expose
                            $res3 = $this->userExposeUpdate($marketId, $tbl);

                            if ($conn->table('tbl_market_result')->where([['marketId', $marketId], ['status', 1]])
                                ->update(['game_over' => 1, 'updated_on' => date('Y-m-d H:i:s')])) {
                                $where = [['status', 1], ['mid', $marketId], ['result', '!=', 'PENDING']];
                                $betListData = $conn->table($tbl)->where($where)->get();
                                if ($betListData->isNotEmpty()) {
                                    foreach ($betListData as $betList) {
                                        $conn->table('tbl_bet_history')->insert($betList);
                                    }
                                    $data['title'] = 'Game Over Market:' . $marketId;
                                    $data['bets'] = $betListData;
                                    $this->gameRecallBackupLogs($data);
                                    $conn->table($tbl)->where($where)->delete();
                                } else {
                                    $where2 = [['status', 2], ['mid', $marketId]];
                                    $betListData = $conn->table($tbl)->where($where2)->get();
                                    if ($betListData->isNotEmpty()) {
                                        foreach ($betListData as $betList) {
                                            $conn->table('tbl_bet_history')->insert($betList);
                                        }
                                        $data['title'] = 'Game Over Market:' . $marketId;
                                        $data['bets'] = $betListData;
                                        $this->gameRecallBackupLogs($data);
                                        $conn->table($tbl)->where($where2)->delete();
                                    }
                                }
                                $response = ["status" => 1, "message" => "Game Over Successfully Done !"];
                            } else {
                                $response["error"] = [
                                    "message" => "Something wrong! Yet game over not done!"
                                ];
                            }
                        } else {
                            $response["error"] = [
                                "message" => "Something wrong! Yet game over not done!"
                            ];
                        }
                    }

                    $this->redis->set("gameoverStatus", 2);
                } else {
                    $response = ["status" => 1, "message" => "No Any Game Over Pending !"];
                }
            }
            $teenpattiPetListData = $conn->table('tbl_bet_pending_teenpatti')
                ->where([['result','!=','PENDING']])->whereIn('status',[1,2])->get();
            if( $teenpattiPetListData->isNotEmpty() ){
                foreach ( $teenpattiPetListData as $betList ){
                    $conn->table('tbl_bet_history_teenpatti')->insert($betList);
                }
                $data['title'] = 'Game Over Market: Live Game';
                $data['bets'] = $teenpattiPetListData;
                $this->gameRecallBackupLogs($data);
                $conn->table('tbl_bet_pending_teenpatti')
                    ->where([['result','!=','PENDING']])->delete();
            }

            return $response;
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


    // Cron Game Over -> betfare market/ bookmaker / session
    public function mainGameover()
    {
        try {


            $conn = DB::connection('mongodb');
            $response = ["status" => 0, "error" => ["code" => 400, "message" => "Bad request!"]];
            $gameoveractiveStatus = $this->redis->get("maingameoverStatus");
            if($gameoveractiveStatus != 1 || $gameoveractiveStatus != 2) {
                $this->redis->set("maingameoverStatus", 2);
            }
            $endTime = time() + 60;
            while ($endTime > time()) {
                //sleep(1); 300000 micro s == 300 ms
                usleep(30000000);
                $gameoverStatus = $this->redis->get("maingameoverStatus");

                $marketResult = $conn->table('tbl_market_result')->where([['game_over', 0], ['status', 1]])
                    ->whereIn('mType', ['match_odd', 'bookmaker', 'fancy2'])->first();
                // $marketResult = $conn->table('tbl_market_result')->where([['game_over', 0], ['status', 1]])->whereNotIn('mType',['match_odd','bookmaker','fancy2','goals','set_market'])->first();
                /*$marketResult = $conn->table('tbl_market_result')
                    ->where([['game_over', 1], ['status', 1]])->whereNotIn('mType',['match_odd','bookmaker','fancy2','goals','set_market'])->get();
               echo "<pre>"; print_r($marketResult);
                */
                if ($marketResult != null && $gameoverStatus==2) {
                    $this->redis->set("maingameoverStatus", 1);
                    //foreach ( $marketResult as $market ){
                    $market = (object)$marketResult;
                    $eventId = (int)$market->eventId;
                    $marketId = $market->marketId;
                    $mType = $market->mType;

                    $eventData = $this->redis->get("Event_" . $eventId);
                    $eventData = json_decode($eventData);
                    $event_name = '';
                    if (isset($eventData->name)) {
                        $event_name = $eventData->name;
                    }

                    $mData = $this->redis->get("Market_" . $marketId);
                    $mData = json_decode($mData);
                    $market_name = '';
                    if (isset($mData->title)) {
                        $market_name = $mData->title;
                    }

                    $mtbl = 'tbl_market_data';
                    $where = [['eventId', $eventId], ['marketId', $marketId]];
                    $result = $conn->table($mtbl)->where($where)->first();
                    if (empty($result)) {
                        $insertArr = [
                            'eventId' => (int)$eventId,
                            'marketId' => $marketId,
                            'event_name' => $event_name,
                            'market_name' => $market_name,
                            'mData' => json_encode($mData)
                        ];
                        $conn->table($mtbl)->insert($insertArr);

                    }


                    $mTypeArr1 = ['match_odd', 'completed_match', 'tied_match', 'bookmaker', 'virtual_cricket'];
                    $mTypeArr2 = ['fancy', 'fancy2', 'fancy3', 'oddeven'];
                    $mTypeArr3 = ['meter', 'khado', 'ballbyball'];
                    $mTypeArr4 = ['cricket_casino', 'jackpot'];
                    $mTypeArr5 = ['USDINR', 'GOLD', 'SILVER', 'EURINR', 'GBPINR', 'ALUMINIUM', 'COPPER', 'CRUDEOIL', 'ZINC', 'NIFTY', 'BANKNIFTY'];
                    $mTypeArr6 = ['set_market', 'goals', 'winner'];
                    if (in_array($mType, $mTypeArr1)) {
                        $tbl = 'tbl_bet_pending_1';
                    } else if (in_array($mType, $mTypeArr2)) {
                        $tbl = 'tbl_bet_pending_2';
                    } else if (in_array($mType, $mTypeArr3)) {
                        $tbl = 'tbl_bet_pending_3';
                    } else if (in_array($mType, $mTypeArr4)) {
                        $tbl = 'tbl_bet_pending_4';
                    } else if (in_array($mType, $mTypeArr5)) {
                        $tbl = 'tbl_bet_pending_5';
                    } else if (in_array($mType, $mTypeArr6)) {
                        $tbl = 'tbl_bet_pending_6';
                    } else {
                        $tbl = 'tbl_bet_pending_7';
                    }

                    $market = $conn->table('tbl_market_result')
                        ->where([['marketId', $marketId], ['game_over', 0]])->first();
                    if ($market != null) {
                        // check bet exist;
                        $checkBets = $conn->table($tbl)
                            ->where([['mid', $marketId], ['result', 'PENDING']])->first();
                        if ($checkBets != null) {
                            if (isset($market['recall']) && $market['recall'] == 2) {
                                $res1 = $this->abundantResult($marketId, $tbl);
                            } else {
                                $res1 = $this->gameOverResult($market, $tbl);
                                $res2 = $this->transactionResult($market, $tbl);
                            }
                        }
                        //Update User Expose
                        $res3 = $this->userExposeUpdate($marketId, $tbl);

                        if ($conn->table('tbl_market_result')->where([['marketId', $marketId], ['status', 1]])
                            ->update(['game_over' => 1, 'updated_on' => date('Y-m-d H:i:s')])) {
                            $where = [['status', 1], ['mid', $marketId], ['result', '!=', 'PENDING']];
                            $betListData = $conn->table($tbl)->where($where)->get();
                            if ($betListData->isNotEmpty()) {
                                foreach ($betListData as $betList) {
                                    $conn->table('tbl_bet_history')->insert($betList);
                                }
                                $data['title'] = 'Game Over Market:' . $marketId;
                                $data['bets'] = $betListData;
                                $this->gameRecallBackupLogs($data);
                                $conn->table($tbl)->where($where)->delete();
                            } else {
                                $where2 = [['status', 2], ['mid', $marketId]];
                                $betListData = $conn->table($tbl)->where($where2)->get();
                                if ($betListData->isNotEmpty()) {
                                    foreach ($betListData as $betList) {
                                        $conn->table('tbl_bet_history')->insert($betList);
                                    }
                                    $data['title'] = 'Game Over Market:' . $marketId;
                                    $data['bets'] = $betListData;
                                    $this->gameRecallBackupLogs($data);
                                    $conn->table($tbl)->where($where2)->delete();
                                }
                            }
                            $response = ["status" => 1, "message" => "Game Over Successfully Done !"];
                        } else {
                            $response["error"] = [
                                "message" => "Something wrong! Yet game over not done!"
                            ];
                        }
                    } else {
                        $response["error"] = [
                            "message" => "Something wrong! Yet game over not done!"
                        ];
                    }

                    $this->redis->set("maingameoverStatus", 2);
                    //}
                } else {
                    $response = ["status" => 1, "message" => "No Any Game Over Pending !"];
                }
            }
            return $response;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    //Game Over Result
    public function gameOverResult($marketResult,$tbl){
        //DB::connection('mongodb')->beginTransaction();
        try {
            $marketResult = (object)$marketResult;
            $connMongoDb = DB::connection('mongodb');
            $marketId = $marketResult->marketId;
            $secId = (int)$marketResult->secId;
            $mType = $marketResult->mType;
            $winner = $marketResult->winner;

            $updateWin = ['result' => 'WIN','updated_on' => date('Y-m-d H:i:s')];
            $updateLoss = ['result' => 'LOSS','updated_on' => date('Y-m-d H:i:s')];
            $updateCancel = ['result' => 'CANCELED','updated_on' => date('Y-m-d H:i:s')];

            if( $tbl == 'tbl_bet_pending_1' ){
                // Win Update for Back
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['secId',$secId],['mType',$mType],
                        ['result','PENDING'],['bType','back'],['status',1],['is_match',1] ])
                    ->update($updateWin);
                // Win Update for Lay
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['secId','!=',$secId],['mType',$mType],
                        ['result','PENDING'],['bType','lay'],['status',1],['is_match',1] ])
                    ->update($updateWin);

                // Loss Update
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['mType',$mType],
                        ['result','PENDING'],['status',1],['is_match',1] ])
                    ->update($updateLoss);

                // Unmatched Bet canceled
                if( $mType == 'match_odd' ){
                    $connMongoDb->table($tbl)
                        ->where([['mid',$marketId],['mType',$mType],
                            ['result','PENDING'],['status',1],['is_match',0] ])
                        ->update($updateCancel);
                }
                //DB::connection('mongodb')->commit();
            }
            if( $tbl == 'tbl_bet_pending_2' ) {
                // Win Update for NO
                if( $mType == 'fancy3' || $mType == 'oddeven' ){
                    $connMongoDb->table($tbl)
                        ->where([['bType',$winner],['mid',$marketId],['mType',$mType],['result','PENDING'],['status',1],['is_match',1]])
                        ->update($updateWin);

                    // Loss Update
                    $connMongoDb->table($tbl)
                        ->where([['mid',$marketId],['mType',$mType],['result','PENDING'],['status',1],['is_match',1]])
                        ->update($updateLoss);
                }else{
                    $connMongoDb->table($tbl)
                        ->where([['mid', $marketId], ['mType', $mType], ['price', '>', ($winner+0)],
                            ['result', 'PENDING'], ['bType', 'no'], ['status', 1], ['is_match', 1]])
                        ->update($updateWin);

                    // Win Update for YES
                    $connMongoDb->table($tbl)
                        ->where([['mid', $marketId], ['mType', $mType], ['price', '<=', ($winner+0)],
                            ['result', 'PENDING'], ['bType', 'yes'], ['status', 1], ['is_match', 1]])
                        ->update($updateWin);

                    // Loss Update
                    $connMongoDb->table($tbl)
                        ->where([['mid', $marketId], ['mType', $mType],
                            ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                        ->update($updateLoss);
                }
                //DB::connection('mongodb')->commit();
            }
            if ( $tbl == 'tbl_bet_pending_3' ){
                if( $mType == 'khado' ){
                    // Win Update
                    $connMongoDb->table($tbl)
                        ->where([['mid',$marketId],['mType',$mType],['diff','>',($winner+0)],
                            ['price','<=',($winner+0)],['result','PENDING'],['status',1],['is_match',1] ])
                        ->update($updateWin);

                    // Loss Update
                    $connMongoDb->table($tbl)
                        ->where([['mid',$marketId],['mType',$mType],
                            ['result','PENDING'],['status',1],['is_match',1] ])
                        ->update($updateLoss);

                } else if ( $mType == 'meter' ) {
                    $connMongoDb->table($tbl)
                        ->where([['mid', $marketId],['mType', $mType], ['price', '>', ($winner+0)],
                            ['result', 'PENDING'], ['bType', 'no'], ['status', 1], ['is_match', 1]])
                        ->update($updateWin);

                    // Win Update for YES
                    $connMongoDb->table($tbl)
                        ->where([['mid', $marketId],['mType', $mType], ['price', '<=', ($winner+0)],
                            ['result', 'PENDING'], ['bType', 'yes'], ['status', 1], ['is_match', 1]])
                        ->update($updateWin);

                    // Loss Update
                    $connMongoDb->table($tbl)
                        ->where([['mid', $marketId], ['mType', $mType],
                            ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                        ->update($updateLoss);

                    //show result
                    $betList = $connMongoDb->table($tbl)
                        ->select(['_id','uid', 'systemId','price','size','rate','result','bType'])
                        ->where([['mid', $marketId], ['status', 1], ['is_match', 1]])
                        ->whereIn('result', ['WIN', 'LOSS'])->get();

                    if ($betList != null) {
                        foreach ($betList as $betdata) {
                            $betdata = (object)$betdata;
                            $price = $betdata->price;
                            $size = $betdata->size;
                            $rate = $betdata->rate;
                            $win = $loss = $result_diff= 0 ;
                            if($betdata->result == 'WIN'){
                                if($betdata->bType == 'no') {
                                    $result_diff = $price - $winner;
                                    if($result_diff != 0){
                                        $win_cal=$result_diff * $size;
                                        $win = round(($win_cal * $rate) / 100);
                                    }else{
                                        $win = $size;
                                    }

                                }

                                if($betdata->bType == 'yes') {
                                    $result_diff = $winner - $price;
                                    if($result_diff != 0){
                                        $win_cal=$result_diff * $size;
                                        $win = round(($win_cal * $rate) / 100);
                                    }else{
                                        $win = round(($size * $rate) / 100);
                                    }
                                }
                            }else{
                                if($betdata->bType == 'no') {
                                    $result_diff = $winner - $price;
                                    if($result_diff != 0){
                                        $loss_cal = $size * $result_diff;
                                        $loss = round(($loss_cal * $rate) / 100);
                                    }else{
                                        $loss = round(($size * $rate) / 100);
                                    }

                                }

                                if($betdata->bType == 'yes') {
                                    $result_diff = $price - $winner;
                                    if($result_diff != 0){
                                        $loss_cal = $size * $result_diff;
                                        $loss = round(($loss_cal * $rate) / 100);
                                    }else{
                                        $loss = $size;
                                    }
                                }
                            }

                            // Loss Update
                            $connMongoDb->table($tbl)->where([['_id',$betdata->_id]])
                                ->update(['win' => $win,'loss' => $loss]);

                        }
                    }
                    //DB::connection('mongodb')->commit();

                }else{
                    // Win Update for NO
                    $connMongoDb->table($tbl)
                        ->where([['mid',$marketId],['mType',$mType],['price','>',($winner+0)],
                            ['result','PENDING'],['bType','no'],['status',1],['is_match',1] ])
                        ->update($updateWin);

                    // Win Update for YES
                    $connMongoDb->table($tbl)
                        ->where([['mid',$marketId],['mType',$mType],['price','<=',($winner+0)],
                            ['result','PENDING'],['bType','yes'],['status',1],['is_match',1] ])
                        ->update($updateWin);

                    // Loss Update
                    $connMongoDb->table($tbl)
                        ->where([['mid',$marketId],['mType',$mType],
                            ['result','PENDING'],['status',1],['is_match',1] ])
                        ->update($updateLoss);
                    //DB::connection('mongodb')->commit();
                }

            }
            if ( $tbl == 'tbl_bet_pending_4' ){
                // Win Update
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['mType',$mType],['secId',$secId],['bType','back'],
                        ['result','PENDING'],['status',1],['is_match',1] ])
                    ->update($updateWin);

                // Loss Update
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['mType',$mType],['secId','!=',$secId],['bType','back'],
                        ['result','PENDING'],['status',1],['is_match',1] ])
                    ->update($updateLoss);
                //DB::connection('mongodb')->commit();
            }
            if ( $tbl == 'tbl_bet_pending_5' ){
                $connMongoDb->table($tbl)
                    ->where([['mid', $marketId], ['mType', $mType], ['price', '>', ($winner+0)],
                        ['result', 'PENDING'], ['bType', 'no'], ['status', 1], ['is_match', 1]])
                    ->update($updateWin);

                // Win Update for YES
                $connMongoDb->table($tbl)
                    ->where([['mid', $marketId], ['mType', $mType], ['price', '<=', ($winner+0)],
                        ['result', 'PENDING'], ['bType', 'yes'], ['status', 1], ['is_match', 1]])
                    ->update($updateWin);

                // Loss Update
                $connMongoDb->table($tbl)
                    ->where([['mid', $marketId],['mType', $mType],
                        ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                    ->update($updateLoss);

                //DB::connection('mongodb')->commit();
            }
            if ( $tbl == 'tbl_bet_pending_6' ){

                // Win Update for Back
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['secId',$secId],['mType',$mType],
                        ['result','PENDING'],['bType','back'],['status',1],['is_match',1] ])
                    ->update($updateWin);

                // Win Update for Lay
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['secId','!=',$secId],['mType',$mType],
                        ['result','PENDING'],['bType','lay'],['status',1],['is_match',1] ])
                    ->update($updateWin);

                // Loss Update
                $connMongoDb->table($tbl)
                    ->where([['mid',$marketId],['mType',$mType],
                        ['result','PENDING'],['status',1],['is_match',1] ])
                    ->update($updateLoss);
                //DB::connection('mongodb')->commit();
            }

            //return ['status' => 1];

        } catch (\Exception $e) {
            //DB::connection('mongodb')->rollBack();
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    //abundant Result
    public function abundantResult($marketId,$tbl){
        try {
            $conn = DB::connection('mongodb');
            $update = ['result' => 'CANCELED','updated_on' => date('Y-m-d H:i:s')];
            $where = [['mid',$marketId],['result','PENDING'],['status',1]];
            $conn->table($tbl)->where($where)->update($update);

            if( $conn->table('tbl_user_market_expose')
                ->where([['mid', $marketId]])->update(['status' => 2]) ) {

                $userList = $conn->table($tbl)->distinct()->select('uid')
                    ->where([['status', 1],['mid',$marketId]])->get();
                if ( $userList->isNotEmpty() ) {
                    foreach ($userList as $userId) {
                        //$userId = $user->uid;
                        $userExpose = $conn->table('tbl_user_market_expose')
                            ->select('expose')
                            ->where([['uid',$userId],['status',1]])->get();
                        if ($userExpose != null) {
                            $expose = 0;
                            foreach ( $userExpose as $uex ){
                                $uex = (object)$uex;
                                $expose = $expose+(int)$uex->expose;
                            }
                            $expose = round($expose, 2);
                            $updateData = [ 'expose' => $expose, 'updated_on' => date('Y-m-d H:i:s') ];

                            if( DB::connection('mysql')->table('tbl_user_info')->where([['uid',$userId]])->update($updateData) ){
                                //DB::connection('mongodb')->commit();
                            }
                        }
                    }
                    //return ['status' => 1];
                }
            }


        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    //transaction Result MatchOdds
    public function transactionResult($marketResult,$tbl){
        $response = ["status" => 0, "error" => ["code" => 400, "message" => "Bad request!1"]];
        try {
            $marketResult = (object)$marketResult;
            $connMongoDb = DB::connection('mongodb');
            $marketId = $marketResult->marketId;

            $marketBetList = $connMongoDb->table($tbl)->select(['uid','systemId','result','win','loss'])
                ->where([ ['mid',$marketId],['status',1],['is_match',1] ])
                ->whereIn('result',['WIN','LOSS'])->get();

            if( $marketBetList != null ){
                //dd($marketBetList);
                $userResultArr = []; $amount = 0;
                foreach ( $marketBetList as $betList ){
                    $betList = (object)$betList;
                    $uid = $betList->uid; $amount = 0;
                    $systemId = $betList->systemId;
                    if( isset( $betList->result ) ){
                        if( $betList->result == 'WIN' ){
                            $amount = (int)$betList->win;
                        }
                        if( $betList->result == 'LOSS' ){
                            $amount = (int)$betList->loss;
                            if( $amount >= 0 ){ $amount = (-1)*$amount; }
                        }
                    }
                    if( !isset($userResultArr[$uid]) ){
                        $userResultArr[$uid] = [
                            'uid' => $uid,
                            'systemId' => $systemId,
                            'amount' => $amount
                        ];
                    }else{
                        $userResultArr[$uid]['amount'] = round($userResultArr[$uid]['amount']+$amount,2);
                    }

                }

                if( $userResultArr != null ){
                    //dd($userResultArr);
                    foreach ( $userResultArr as $userResult ){
                        if( round($userResult['amount']) != 0 ){
                            $res = $this->updateTransactionHistory($userResult,$marketResult,$tbl);
                        }
                    }
                }

                $res2 = $this->updateTransactionParentHistory($marketId);

            }
            //return $response;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    //user Expose Update On Game Over
    public function userExposeUpdate($marketId,$tbl){
        try {
            $conn = DB::connection('mongodb');
            if( $conn->table('tbl_user_market_expose')
                ->where([['mid', $marketId]])->update(['status' => 2]) ) {

                $userList = $conn->table($tbl)->distinct()->select('uid')
                    ->where([['status', 1],['mid',$marketId]])->get();
                if ( $userList->isNotEmpty() ) {
                    foreach ($userList as $userId) {
                        $userExpose = $conn->table('tbl_user_market_expose')
                            ->select('expose')
                            ->where([['uid',$userId],['status',1]])->get();
                        if ($userExpose != null) {
                            $expose = 0;
                            foreach ( $userExpose as $uex ){
                                $uex = (object)$uex;
                                $expose = $expose+(int)$uex->expose;
                            }
                            $expose = round($expose, 2);
                            $updateData = [ 'expose' => $expose, 'updated_on' => date('Y-m-d H:i:s') ];

                            if( DB::connection('mysql')->table('tbl_user_info')->where([['uid',$userId]])->update($updateData) ){
                                // do something
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Update Transaction History
    public function updateTransactionHistory($userResult,$marketResult,$tbl)
    {
        try {
            $marketResult = (object)$marketResult;
            $conn = DB::connection('mysql');
            $conn3 = DB::connection('mysql3');
            $updatedOn = date('Y-m-d H:i:s');
            $userId = $userResult['uid'];
            $amount = $userResult['amount'];
            $systemId = $userResult['systemId'];

            $sportId = $marketResult->sportId;
            $eventId = (int)$marketResult->eventId;
            $marketId = $marketResult->marketId;
            $winner = $marketResult->winner;
            $mType = $marketResult->mType;

            if( $amount > 0 ){ $type = 'CREDIT'; }else{ $amount = (-1)*$amount; $type = 'DEBIT'; }

            $cUser = $conn->table('tbl_user')->select(['parentId'])->where([['id',$userId]])->first();
            $parentId = $cUser->parentId;
            $description = $this->setDescription($marketResult,$tbl);

            $this->summaryUpdate($userId,$amount,0,$type,0,0);
            $balance = $this->currentBalanceClient($userId,$amount,$type,'CLIENT');

            $transDefaultArr = [
                'systemId' => $systemId,
                'clientId' => $userId,
                'childId' => 0,
                'sid' => $sportId,
                'eid' => $eventId,
                'mid' => $marketId,
                'result' => $winner,
                'mType' => $mType,
                'p_amount' => 0,
                'c_amount' => 0,
                'description' => $description,
                'remark' => 'Transactional Entry',
                'status' => 1,
                'created_on' => $updatedOn,
                'updated_on' => $updatedOn
            ];

            $insertArr = $transDefaultArr;

            $insertArr['userId'] = $userId;
            $insertArr['parentId'] = $parentId;
            $insertArr['amount'] = $amount;
            $insertArr['balance'] = $balance;
            $insertArr['type'] = $type;
            $insertArr['eType'] = 0; // 0 for transaction

            $transTbl = $this->currentTable('tbl_transaction_client');
            if( $conn3->table($transTbl)->insert($insertArr) ){
                if( $type == 'CREDIT' && ( in_array($mType,['match_odd','completed_match','tied_match','goals','set_market']) )){

                    $operatorCommission = 1;
                    if($mType == 'completed_match'){
                        $where = [['key_name','COMPLETED_MATCH_COMM']];
                    }elseif($mType == 'tied_match'){
                        $where = [['key_name','TIED_MATCH_COMM']];
                    }else{
                        $where = [['key_name','MATCH_ODD_COMM']];
                    }
                    $commonSetting = $conn->table('tbl_common_setting')
                        ->select(['value'])->where($where)->first();
                    if( $commonSetting != null ){
                        $operatorCommission = $commonSetting->value;
                    }
                    $commission=0;
                    if($operatorCommission > 0) {
                        $commission = round(($amount*$operatorCommission)/100,2);
                    }
                    if($commission > 0) {
                        $this->summaryUpdate($userId, 0, 0, 'DEBIT', $commission, 0);
                        $balance = $this->currentBalanceClient($userId, $commission, 'DEBIT','CLIENT');

                        $insertArr = $transDefaultArr;

                        $insertArr['userId'] = $userId;
                        $insertArr['parentId'] = $parentId;
                        $insertArr['amount'] = $commission;
                        $insertArr['balance'] = $balance;
                        $insertArr['description'] = $description.' Comm';
                        $insertArr['type'] = 'DEBIT';
                        $insertArr['eType'] = 3; // 3 for commission
                        $transTbl = $this->currentTable('tbl_transaction_client');
                        if( $conn3->table($transTbl)->insert($insertArr) ){
                            // do something
                        }
                    }

                }
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Update Transaction Parent History
    public function updateTransactionParentHistory($marketId)
    {
        try {
            $conn = DB::connection('mysql');
            $conn3 = DB::connection('mysql3');
            $transTbl = $this->currentTable('tbl_transaction_client');
            $clientTransData = $conn3->table($transTbl)
                ->where([['status', 1],['mid',$marketId]])->get();

            if ( $clientTransData->isNotEmpty() ) {
                $newUserArr = [];
                foreach ($clientTransData as $transData){
                    $userId = $transData->userId;
                    $type = $transData->type;
                    $amount = $transData->amount;
                    $sid = $transData->sid;
                    $eid = $transData->eid;
                    $mid = $transData->mid;
                    $result = $transData->result;
                    $eType = $transData->eType;
                    $mType = $transData->mType;
                    $description = $transData->description;

                    if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }

                    $parentUserData = $conn->table('tbl_user_profit_loss as pl')
                        ->select(['pl.userId as userId','pl.parentId as parentId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
                        ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
                        ->where([['pl.clientId',$userId],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

                    $childId = $userId;

                    foreach ( $parentUserData as $user ){
                        $cUser = $conn->table('tbl_user')->select(['systemId','parentId','role'])
                            ->where([['id',$user->userId]])->first();
                        $systemId = $cUser->systemId;
                        $parentId = $cUser->parentId;
                        $role = $cUser->role;
                        $tAmount = ( $amount*$user->apl )/100;
                        $pTAmount = ( $amount*(100-$user->gpl) )/100;
                        $cTAmount = ( $amount*($user->gpl-$user->apl) )/100;

                        if( $eType == 3 ){
                            $user->userId = $user->userId.'-COM';
                        }

                        if( isset($newUserArr[$user->userId]) ){

                            if( $pType != 'CREDIT' ){
                                $tAmount = (-1)*$tAmount;
                                $pTAmount = (-1)*$pTAmount;
                                $cTAmount = (-1)*$cTAmount;
                            }

                            $tAmount = $newUserArr[$user->userId]['tAmount']+$tAmount;
                            $pTAmount = $newUserArr[$user->userId]['pTAmount']+$pTAmount;
                            $cTAmount = $newUserArr[$user->userId]['cTAmount']+$cTAmount;

                            $newUserArr[$user->userId]['tAmount'] = $tAmount;
                            $newUserArr[$user->userId]['pTAmount'] = $pTAmount;
                            $newUserArr[$user->userId]['cTAmount'] = $cTAmount;

                        }else{

                            if( $pType != 'CREDIT' ){
                                $tAmount = (-1)*$tAmount;
                                $pTAmount = (-1)*$pTAmount;
                                $cTAmount = (-1)*$cTAmount;
                            }

                            $newUserArr[$user->userId] = [
                                'role' => $role,
                                'systemId' => $systemId,
                                'clientId' => $userId,
                                'userId' => $user->userId,
                                'childId' => $childId,
                                'parentId' => $parentId,
                                'tAmount' => $tAmount,
                                'pTAmount' => $pTAmount,
                                'cTAmount' => $cTAmount,
                                'sid' => $sid,
                                'eid' => $eid,
                                'mid' => $mid,
                                'result' => $result,
                                'eType' => $eType,
                                'mType' => $mType,
                                'description' => $description,
                            ];
                        }
                        $childId = $user->userId;
                    }

                }

                if( $newUserArr != null ){
                    foreach ( $newUserArr as $userData ){

                        if( $userData['tAmount'] < 0 ){
                            $pType = 'DEBIT';
                            $amount = (-1)*round($userData['tAmount'],2);
                            $pAmount = (-1)*round($userData['pTAmount'],2);
                            $cAmount = (-1)*round($userData['cTAmount'],2);
                        }else{
                            $pType = 'CREDIT';
                            $amount = round($userData['tAmount'],2);
                            $pAmount = round($userData['pTAmount'],2);
                            $cAmount = round($userData['cTAmount'],2);
                        }

                        if( $userData['eType'] == 3 ){
                            if( strpos($userData['userId'],'-COM') > 0 ){
                                $userData['userId'] = str_replace('-COM','',$userData['userId']);
                            }

                            if( strpos($userData['childId'],'-COM') > 0 ){
                                $userData['childId'] = str_replace('-COM','',$userData['childId']);
                            }

                            $this->summaryUpdate($userData['userId'],0,0,$pType,$amount,$pAmount);
                        }else{
                            $this->summaryUpdate($userData['userId'],$amount,$pAmount,$pType,0,0);
                        }

                        $balance = $this->currentBalanceClient($userData['userId'], $amount, $pType,'PARENT');

                        $resultArr = [
                            'systemId' => $userData['systemId'],
                            'clientId' => $userData['clientId'],
                            'userId' => $userData['userId'],
                            'childId' => $userData['childId'],
                            'parentId' => $userData['parentId'],
                            'sid' => $userData['sid'],
                            'eid' => $userData['eid'],
                            'mid' => $userData['mid'],
                            'result' => $userData['result'],
                            'eType' => $userData['eType'],
                            'mType' => $userData['mType'],
                            'type' => $pType,
                            'amount' => $amount,
                            'p_amount' => $pAmount,
                            'c_amount' => $cAmount,
                            'balance' => $balance,
                            'description' => $userData['description'],
                            'remark' => 'Transactional Entry',
                            'status' => 1,
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s')
                        ];

                        if( $userData['role'] == 1 ){ $tbl = 'tbl_transaction_admin';
                        }else{ $tbl = 'tbl_transaction_parent'; }
                        $transTbl = $this->currentTable($tbl);
                        if( $conn3->table($transTbl)->insert($resultArr) ){
                            // do something
                        }

                    }


                }
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Function to get the Description
    public function setDescription($marketResult,$tbl)
    {
        $conn = DB::connection('mongodb');
        $description = '';
        $marketId = $marketResult->marketId;
        $winner = $marketResult->winner;

        $betData = $conn->table($tbl)->select(['sport', 'event', 'market', 'runner', 'mType', 'description'])
            ->where([['status', 1], ['mid', $marketId]])->orderBy('id', 'desc')->first();

        if( $betData != null ){
            $betData = (object)$betData;
            if( in_array($betData->mType,['match_odd','bookmaker','virtual_cricket','completed_match','tied_match','goals','set_market'])){
                $description = $betData->sport. ' > ' .$betData->event. ' > '.$betData->mType. ' > '.$betData->market. ' > '.$winner;
            }else if( in_array( $betData->mType, ['fancy2','fancy3','oddeven'] ) ){
                $marketName = ['fancy2' => 'Session', 'fancy3' => 'Other Market','oddeven' => 'Odd Even Market'];
                $description = $betData->sport. ' > ' .$betData->event. ' > '.$marketName[$betData->mType].'  > '.$betData->market;
            }else if( in_array( $betData->mType, ['winner','jackpot','cricket_casino'] ) ){
                $description = $betData->description;
                if( $betData->mType == 'winner' ){
                    $description = $betData->sport. ' > ' .$betData->event. ' > '.$betData->mType. ' > '.$winner;
                }
                if( $betData->mType == 'jackpot' ){
//                    $jData = $conn->table('tbl_jackpot_data')->select(['col_1', 'col_2', 'col_3', 'col_4'])
//                        ->where([['secId', $winner], ['mid', $marketId]])->first();
//                    $jResult = '('.$jData->col_1."-".$jData->col_2." / ".$jData->col_3."-".$jData->col_4.')';
                    $description = $betData->sport. ' > ' .$betData->event. ' > '.$betData->mType. ' > '.$betData->market. ' > '.$winner;
                }
            }else if( in_array( $betData->mType, ['USDINR','GOLD','SILVER','EURINR','GBPINR','ALUMINIUM','COPPER','CRUDEOIL','ZINC','BANKNIFTY','NIFTY'] ) ){
                $description = $betData->sport. ' > ' .$betData->event.' > '. $betData->market;
            }else{
                $description = $betData->sport. ' > ' .$betData->event. ' > '.$betData->mType. ' > '.$betData->market;
            }

        }

        return $description;
    }

    //Summary Update
    public function summaryUpdate($userId,$ownPl,$parentPl,$type,$ownComm,$parentComm){

        //DB::connection('mysql')->beginTransaction();
        try {
            $conn = DB::connection('mysql');
            $userSummary = $conn->table('tbl_settlement_summary')
                ->where([['status',1],['uid',$userId]])->first();
            if( $userSummary != null ){

                $updatedOn = date('Y-m-d H:i:s');
                if( isset( $type ) && $type != 'CREDIT' ){
                    $ownPl = (-1)*round($ownPl,2);
                    $parentPl = (-1)*round($parentPl,2);
                    $ownComm = (-1)*round($ownComm,2);
                    $parentComm = (-1)*round($parentComm,2);
                }

                $updateArr = [
                    'ownPl' => $conn->raw('ownPl + ' . $ownPl),
                    'ownComm' => $conn->raw('ownComm + ' . $ownComm),
                    'parentPl' => $conn->raw('parentPl + ' . $parentPl),
                    'parentComm' => $conn->raw('parentComm + ' . $parentComm),
                    'updated_on' => $updatedOn
                ];

                if( $conn->table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$userId]])->update($updateArr) ){
                    //DB::connection('mysql')->commit();
                }else{
                    $userSummary = $conn->table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$userId]])->first();
                    $updatedOn = date('Y-m-d H:i:s');
                    $betTimeDiff = (strtotime($updatedOn)-strtotime($userSummary->updated_on) );
                    if($betTimeDiff == 0) {
                        usleep(1000);
                        $userSummary = $conn->table('tbl_settlement_summary')
                            ->where([['status',1],['uid',$userId]])->first();
                    }
                    $updateArr = [
                        'ownPl' => $userSummary->ownPl+$ownPl,
                        'ownComm' => $userSummary->ownComm+$ownComm,
                        'parentPl' => $userSummary->parentPl+$parentPl,
                        'parentComm' => $userSummary->parentComm+$parentComm,
                        'updated_on' => date('Y-m-d H:i:s')
                    ];
                    if( $conn->table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$userId]])->update($updateArr) ){
                        //DB::connection('mysql')->commit();
                    }
                }

            }else{
                if( isset( $type ) && $type != 'CREDIT' ){
                    $ownPl = (-1)*round($ownPl,2);
                    $parentPl = (-1)*round($parentPl,2);
                    $ownComm = (-1)*round($ownComm,2);
                    $parentComm = (-1)*round($parentComm,2);
                }

                $insertArr = [  'uid' => $userId, 'updated_on' => date('Y-m-d H:i:s'),
                    'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl,'parentComm' => $parentComm ];

                if( $conn->table('tbl_settlement_summary')->insert($insertArr) ){
                    //DB::connection('mysql')->commit();
                }

            }
        } catch (\Exception $e) {
            //DB::connection('mysql')->rollBack();
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    // get Current Balance
    public function currentBalanceClient($userId,$amount,$type,$role)
    {
        //DB::connection('mysql')->beginTransaction();
        try {
            $conn = DB::connection('mysql');
            if( $type != 'CREDIT' ){ $amount = (-1)*$amount; }
            $updated_on = date('Y-m-d H:i:s');
            $updateArr = [
                'pl_balance' => $conn->raw('pl_balance + ' . $amount),
                'updated_on' => $updated_on
            ];

            if( $conn->table('tbl_user_info')->where([['uid',$userId]])->update($updateArr) ){
                //DB::connection('mysql')->commit();
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                $balance = $cUserInfo->balance;
                $plBalance = $cUserInfo->pl_balance;
                if( $role != 'CLIENT' ){
                    return round( ( $plBalance ),2);
                }else{
                    return round( ( $balance + $plBalance ),2);
                }
            }else{
                $updated_on = date('Y-m-d H:i:s');
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                $betTimeDiff = (strtotime($updated_on)-strtotime($cUserInfo->updated_on) );
                if($betTimeDiff == 0) {
                    usleep(1000);
                    $cUserInfo = $conn->table('tbl_user_info')
                        ->select(['balance','pl_balance','updated_on'])->where([['uid',$userId]])->first();
                }

                $balance = $cUserInfo->balance;
                $newPlBalance = $cUserInfo->pl_balance;

                $updateArr = [
                    'pl_balance' => $newPlBalance,
                    'updated_on' => $updated_on
                ];

                if( $conn->table('tbl_user_info')->where([['uid', $userId]])->update($updateArr) ){
                    //DB::connection('mysql')->commit();
                    if( $role != 'CLIENT' ){
                        return round( ( $newPlBalance ),2);
                    }else{
                        return round( ( $balance + $newPlBalance ),2);
                    }
                }
            }

        } catch (\Exception $e) {
            //DB::connection('mysql')->rollBack();
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
