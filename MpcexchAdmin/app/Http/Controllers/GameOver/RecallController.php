<?php

namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class RecallController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }
    // Cron Game Over
    public function index(){
        try {
            $conn = DB::connection('mongodb');
            $response = ["status" => 0, "error" => ["code" => 400, "message" => "Bad request!"]];

            $marketResult = $conn->table('tbl_market_result')
                ->where([['game_over', 1],['recall', 1],['status', 1]])->first();

            if( $marketResult != null ){
                    $market = (object)$marketResult;
                    $marketId = $market->marketId;
                    $mType = $market->mType;

                    $mTypeArr1 = ['set_market','goals','winner','match_odd','completed_match','tied_match','bookmaker','virtual_cricket'];
                    $mTypeArr2 = ['fancy','fancy2','fancy3','oddeven'];
                    $mTypeArr3 = ['meter','khado','ballbyball'];
                    $mTypeArr4 = ['cricket_casino','jackpot'];
                    $mTypeArr5 = ['USDINR','GOLD','SILVER','EURINR','GBPINR','ALUMINIUM','COPPER','CRUDEOIL','ZINC','BANKNIFTY','NIFTY'];
                    if( in_array($mType,$mTypeArr1) ){
                        $tbl = 'tbl_bet_pending_1';
                    }else if( in_array($mType,$mTypeArr2) ){
                        $tbl = 'tbl_bet_pending_2';
                    }else if( in_array($mType,$mTypeArr3) ){
                        $tbl = 'tbl_bet_pending_3';
                    }else if( in_array($mType,$mTypeArr4) ){
                        $tbl = 'tbl_bet_pending_4';
                    }else if( in_array($mType,$mTypeArr5) ){
                        $tbl = 'tbl_bet_pending_5';
                    }else{
                        $tbl = 'tbl_bet_pending_6';
                    }

                    $market = $conn->table('tbl_market_result')
                        ->where([['marketId',$marketId],['game_over',1],['recall',1]])->first();
                    if ($market != null) {
                        // check bet exist;
                        $checkBets = $conn->table('tbl_bet_history')
                            ->where([['mid',$marketId],['result','!=','PENDING']])->first();
                        if ($checkBets != null) {
                            $res1 = $this->recallResult($marketId,$tbl);
                            $res2 = $this->transactionRecall($marketId);
                        }
                        //Update User Expose
                        $res3 = $this->userExposeUpdate($marketId);

                        if( $conn->table('tbl_market_result')->where([['marketId', $marketId],['status', 1]])
                            ->update(['game_over' => 1,'recall' => 0,'updated_on' => date('Y-m-d H:i:s')]) ){

                            $response = ["status" => 1, "message" => "Recall Successfully Done !"];
                        }else{
                            $response["error"] = [
                                "message" => "Something wrong! Yet game over not done!"
                            ];
                        }
                    } else {
                        $response["error"] = [
                            "message" => "Something wrong! Yet game over not done!"
                        ];
                    }
                //}
            }else{
                $response = ["status" => 1, "message" => "No Any Game Over Pending !"];
            }

            return $response;
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    //Recall Result
    public function recallResult($marketId,$tbl){
        try {
            $connMongoDb = DB::connection('mongodb');
            $update = ['result' => 'PENDING','updated_on' => date('Y-m-d H:i:s')];
            $where = [['mid',$marketId],['result','!=','PENDING'],['status',1]];
            if( $connMongoDb->table('tbl_bet_history')->where($where)->update($update) ){
                $betListData = $connMongoDb->table('tbl_bet_history')
                    ->where([['mid', $marketId], ['result','PENDING']])->get();
                if( $betListData->isNotEmpty() ){
                    $data['title'] = 'Game Recall Market:'.$marketId;
                    $data['bets'] = $betListData;
                    $this->gameRecallBackupLogs($data);
                    foreach ( $betListData as $betList ){
                        $connMongoDb->table($tbl)->insert($betList);
                    }
                    $connMongoDb->table('tbl_bet_history')
                        ->where([['mid',$marketId],['result','PENDING']])->delete();
                }
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    //transaction Result MatchOdds
    public function transactionRecall($marketId){
        // $response = ["status" => 0, "error" => ["code" => 400, "message" => "Bad request!1"]];

        try {
            $conn3 = DB::connection('mysql3');
            $transArr = [];
            $select = ['userId', 'type', 'amount', 'p_amount', 'eType'];
            $where = [['status', 1],['mid',$marketId]];

            $transTbl = $this->currentTable('tbl_transaction_client');
            $transClient = $conn3->table($transTbl)
                ->select($select)->where($where)->get();
            if ($transClient != null) {
                foreach ( $transClient as $cData ){
                    $transArr[] = $cData;
                }
            }

            $transTbl = $this->currentTable('tbl_transaction_parent');
            $transParent = $conn3->table($transTbl)
                ->select($select)->where($where)->get();
            if ($transParent != null) {
                foreach ( $transParent as $pData ){
                    $transArr[] = $pData;
                }
            }
            $transTbl = $this->currentTable('tbl_transaction_admin');
            $transAdmin = $conn3->table($transTbl)
                ->select($select)->where($where)->get();

            if ($transAdmin != null) {
                foreach ( $transAdmin as $aData ){
                    $transArr[] = $aData;
                }
            }

            if ($transArr != null) {

                foreach ($transArr as $trans) {

                    $user = DB::table('tbl_user_info')->select(['pl_balance'])
                        ->where([['uid', $trans->userId]])->first();
                    if ($user != null) {
                        if ($trans->type == 'CREDIT') {
                            $amount = (-1)*$trans->amount;
                        } else {
                            $amount = $trans->amount;
                        }

                        $updateArr = [
                            'pl_balance' => DB::raw('pl_balance + ' . $amount),
                            'updated_on' => date('Y-m-d H:i:s')
                        ];

                        DB::table('tbl_user_info')->where([['uid', $trans->userId]])
                            ->update($updateArr);

                        $summaryData = [];

                        $summaryData['uid'] = $trans->userId;
                        $summaryData['type'] = $trans->type;

                        if ($trans->eType == 3) {
                            $summaryData['ownComm'] = $trans->amount;
                            $summaryData['parentComm'] = $trans->p_amount;
                        } else {
                            $summaryData['ownPl'] = $trans->amount;
                            $summaryData['parentPl'] = $trans->p_amount;
                        }

                        $this->userSummaryUpdate($summaryData);

                    }
                }

                // update transaction status
                $update = ['status' => 2];

                $transTbl = $this->currentTable('tbl_transaction_client');
                $conn3->table($transTbl)->where($where)->update($update);
                $transTbl = $this->currentTable('tbl_transaction_parent');
                $conn3->table($transTbl)->where($where)->update($update);
                $transTbl = $this->currentTable('tbl_transaction_admin');
                $conn3->table($transTbl)->where($where)->update($update);

            }
            //return $response;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    //user Summary Update
    public function userSummaryUpdate($summaryData)
    {
        try{
            if (isset($summaryData['uid'])) {
                $uid = $summaryData['uid'];
                $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
                $parentPl = isset($summaryData['parentPl']) ? $summaryData['parentPl'] : 0;

                $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
                $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

                $userSummary = DB::table('tbl_settlement_summary')
                    ->where([['status', 1], ['uid', $uid]])->first();

                if ($userSummary != null) {

                    if( isset($summaryData['type']) && $summaryData['type'] == 'CREDIT' ){
                        $ownPl = (-1)*round($ownPl,2);
                        $ownComm = (-1)*round($ownComm,2);
                        $parentPl = (-1)*round($parentPl,2);
                        $parentComm = (-1)*round($parentComm,2);
                    }

                    $updateArr = [
                        'ownPl' => DB::raw('ownPl + ' . $ownPl),
                        'ownComm' => DB::raw('ownComm + ' . $ownComm),
                        'parentPl' => DB::raw('parentPl + ' . $parentPl),
                        'parentComm' => DB::raw('parentComm + ' . $parentComm),
                        'updated_on' => date('Y-m-d H:i:s')
                    ];

                    DB::table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$uid]])->update($updateArr);

                } else {

                    $insertArr = ['uid' => $uid, 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                        'parentPl' => $parentPl, 'parentComm' => $parentComm];

                    DB::table('tbl_settlement_summary')->insert($insertArr);

                }

            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    //user Expose Update On Recall
    public function userExposeUpdate($marketId){
        //$response = ["status" => 0, "error" => ["code" => 400, "message" => "Bad request!4"]];
        try {
            $conn = DB::connection('mongodb');
            if( $conn->table('tbl_user_market_expose')
                ->where([['mid', $marketId]])->update(['status' => 1]) ) {

                $userList = $conn->table('tbl_user_market_expose')
                    ->select(['uid'])->where([['status', 1],['mid',$marketId]])->get();
                if ( $userList->isNotEmpty() ) {
                    foreach ($userList as $user) {
                        $user = (object)$user;
                        $userId = $user->uid;
                        $userExpose = $conn->table('tbl_user_market_expose')
                            ->select('expose')
                            ->where([['uid',$userId],['status',1]])->get();
                        if ($userExpose != null) {
                            $expose = 0;
                            foreach ( $userExpose as $uex ){
                                $uex = (object)$uex;
                                $expose = $expose+(int)$uex->expose;
                            }
                            $expose = round($expose, 2);
                            $updateData = [ 'expose' => $expose, 'updated_on' => date('Y-m-d H:i:s') ];
                            DB::connection('mysql')->table('tbl_user_info')
                                ->where([['uid',$userId]])->update($updateData);
                        }
                    }
                }
            }
            //return $response;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
