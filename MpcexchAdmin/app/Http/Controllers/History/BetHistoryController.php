<?php

namespace App\Http\Controllers\History;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class BetHistoryController extends Controller
{

    /**
     * Bet History List
     * @param null $uid
     * @return JsonResponse
     */
    public function list($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $conn = DB::connection('mongodb');
            if( $uid != null ){
                if( $uid == 1 ){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user(); $uid = $user->id;
            }
            $uid = (int)$uid;
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $type = 1; $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'type' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                $type = $requestData[ 'type' ];
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);
                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'type' ] ) ) {
                $type = $requestData[ 'type' ]; $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $start = $end = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( $user != null ){
                $userName = $user->name;
                $clientArr = [];

                if( in_array($user->role,[2,3]) ){
                    $client = DB::table('tbl_user_child_data')->select(['clients'])
                        ->where([['uid',$uid]])->first();
                    if( $client != null ){
                        $clientArr = json_decode($client->clients);
                    }

                    if( $clientArr == null ){
                        $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                        return response()->json($response, 200);
                    }
                }

                if( $type == 2 ){
                    $select = ['_id','price','size','rate','win','loss','client','master','bType','mType','result','description','ip_address','updated_on','status'];
                    $query = $conn->table('tbl_bet_history')->select($select);
                    if( $user->role == 1 || $user->role == 6 ){
                        $query->where([['status',1],['result','!=','PENDING'],['systemId',$user->systemId]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['status',1],['uid',$uid],['result','!=','PENDING'],['systemId',$user->systemId]]);
                    }else{
                        if( $clientArr != null && $user->role != 4 ){
                            $query->where([['status',1],['result','!=','PENDING'],['systemId',$user->systemId]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $start1 = $start->format('Y-m-d')." 00:00:01";
                        $end1 = $end->format('Y-m-d')." 23:59:59";
                        $query->whereBetween('updated_on',[$start1,$end1]);
                    }

                    $list = $query->orderBy('updated_on', 'asc')->get();
                    if( $list->isNotEmpty() ){
                        $response = [ 'status' => 1, 'data' => $list, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif ( $type == 3 ){
                    $select = ['id','price','size','rate','win','loss','client','master','bType','mType','result','description','ip_address','updated_on','status'];
                    $query = $conn->table('tbl_bet_pending_teenpatti')->select($select);
                    if( $user->role == 1 || $user->role == 6){
                        $query->where([['status',1],['result','PENDING'],['systemId',$user->systemId]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['status',1],['uid',$uid],['result','PENDING'],['systemId',$user->systemId]]);
                    }else{
                        if( $clientArr != null && $user->role != 4){
                            $query->where([['status',1],['result','PENDING'],['systemId',$user->systemId]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $start1 = $start->format('Y-m-d')." 00:00:01";
                        $end1 = $end->format('Y-m-d')." 23:59:59";
                        $query->whereBetween('updated_on',[$start1,$end1]);
                    }

                    $list = $query->orderBy('updated_on', 'asc')->get();

                    if( $list->isNotEmpty() ){
                        $response = [ 'status' => 1, 'data' => $list, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif ( $type == 4 ){
                    $select = ['id','price','size','rate','win','loss','client','master','bType','mType','result','description','ip_address','updated_on','status'];
                    $query = $conn->table('tbl_bet_history_teenpatti')->select($select);
                    if( $user->role == 1 || $user->role == 6){
                        $query->where([['status',1],['result','!=','PENDING'],['systemId',$user->systemId]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['status',1],['uid',$uid],['result','!=','PENDING'],['systemId',$user->systemId]]);
                    }else{
                        if( $clientArr != null && $user->role != 4){
                            $query->where([['status',1],['result','!=','PENDING'],['systemId',$user->systemId]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $start1 = $start->format('Y-m-d')." 00:00:01";
                        $end1 = $end->format('Y-m-d')." 23:59:59";
                        $query->whereBetween('updated_on',[$start1,$end1]);
                    }

                    $list = $query->orderBy('updated_on', 'asc')->get();

                    if( $list->isNotEmpty() ){
                        $response = [ 'status' => 1, 'data' => $list, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif ( $type == 5 ){

                    $casinoGameList = $conn->table('tbl_casino_games')
                        ->where([['status',1]])->get();
                    if( $casinoGameList->isNotEmpty() ){
                        $casinoGameArr = [];
                        foreach ($casinoGameList as $casinoGame){
                            $casinoGame = (object)$casinoGame;
                            $casinoGameArr[$casinoGame->game_id] = $casinoGame->name;
                        }
                    }

                    $query = $conn->table('tbl_casino_transaction_view');
                    if( $user->role == 1 || $user->role == 6){
                        $query->where([['status',1],['type','DEBIT'],['round_closed',0],['systemId',$user->systemId]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['status',1],['type','DEBIT'],['uid',$uid],['round_closed',0],['systemId',$user->systemId]]);
                    }else{
                        if( $clientArr != null && $user->role != 4){
                            $query->where([['status',1],['type','DEBIT'],['round_closed',0],['systemId',$user->systemId]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $start1 = $start->format('Y-m-d')." 00:00:01";
                        $end1 = $end->format('Y-m-d')." 23:59:59";
                        $query->whereBetween('updated_on',[$start1,$end1]);
                    }

                    $list = $query->orderBy('updated_on', 'desc')->get();

                    if( $list->isNotEmpty() ){

                        $betListArr = $list;
                        $betList = [];
                        foreach ($betListArr as $bets){
                            $bets = (object)$bets;
                            if( isset($casinoGameArr[$bets->game_id]) ){
                                $gameName = $casinoGameArr[$bets->game_id];
                                $description = 'Casino > '.$casinoGameArr[$bets->game_id].' > Round #'.$bets->round;
                            }else{
                                $gameName = 'GameId: '.$bets->game_id;
                                $description = 'Casino > GameId: '.$bets->game_id.' > Round #'.$bets->round;
                            }
                            $betList[] = [
                                '_id' => $bets->_id,
                                'price' => '-',
                                'size' => $bets->amount,
                                'bType' => 'back',
                                'rate' => '-',
                                'mType' => 'casino',
                                'runner' => $gameName,
                                'ip_address' => isset($bets->ip_address) ? $bets->ip_address : '-' ,
                                'client' => $bets->username,
                                'master' => '-',
                                'description' => $description,
                                'win' => 0,
                                'loss' => $bets->amount,
                                'result' => '-',
                                'updated_on' => $bets->updated_on
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $betList, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif ( $type == 6 ){

                    $casinoGameList = $conn->table('tbl_casino_games')
                        ->where([['status',1]])->get();
                    if( $casinoGameList->isNotEmpty() ){
                        $casinoGameArr = [];
                        foreach ($casinoGameList as $casinoGame){
                            $casinoGame = (object)$casinoGame;
                            $casinoGameArr[$casinoGame->game_id] = $casinoGame->name;
                        }
                    }

                    $query = $conn->table('tbl_casino_transaction_view');
                    if( $user->role == 1 || $user->role == 6){
                        $query->where([['status',1],['type','DEBIT'],['round_closed','!=',0],['systemId',$user->systemId]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['status',1],['uid',$uid],['type','DEBIT'],['round_closed','!=',0],['systemId',$user->systemId]]);
                    }else{
                        if( $clientArr != null && $user->role != 4){
                            $query->where([['status',1],['type','DEBIT'],['round_closed','!=',0],['systemId',$user->systemId]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $start1 = $start->format('Y-m-d')." 00:00:01";
                        $end1 = $end->format('Y-m-d')." 23:59:59";
                        $query->whereBetween('updated_on',[$start1,$end1]);
                    }

                    $list = $query->orderBy('updated_on', 'desc')->get();

                    if( $list->isNotEmpty() ){

                        $betListArr = $list;
                        $betList = [];
                        foreach ($betListArr as $bets){
                            $bets = (object)$bets;
                            if( isset($casinoGameArr[$bets->game_id]) ){
                                $gameName = $casinoGameArr[$bets->game_id];
                                $description = 'Casino > '.$casinoGameArr[$bets->game_id].' > Round #'.$bets->round;
                            }else{
                                $gameName = 'GameId: '.$bets->game_id;
                                $description = 'Casino > GameId: '.$bets->game_id.' > Round #'.$bets->round;
                            }
                            $betList[] = [
                                '_id' => $bets->_id,
                                'price' => '-',
                                'size' => $bets->amount,
                                'bType' => 'back',
                                'rate' => '-',
                                'mType' => 'casino',
                                'runner' => $gameName,
                                'ip_address' => isset($bets->ip_address) ? $bets->ip_address : '-' ,
                                'client' => $bets->username,
                                'master' => '-',
                                'description' => $description,
                                'win' => 0,
                                'loss' => $bets->amount,
                                'result' => '-',
                                'updated_on' => $bets->updated_on
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $betList, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif ( $type == 7 ){
                    $data = [];
                    $tblArr = ['tbl_bet_pending_1_view','tbl_bet_pending_2_view','tbl_bet_pending_3_view','tbl_bet_pending_4_view','tbl_bet_pending_5_view','tbl_bet_pending_6_view','tbl_bet_pending_teenpatti_view','tbl_bet_history','tbl_bet_history_teenpatti'];
                    foreach( $tblArr as $tbl ){
                        if( $tbl == 'tbl_bet_history'){
                            $select = ['_id','price','size','rate','win','loss','client','master','bType','mType','result','description','ip_address','updated_on','status'];
                        }else{
                            $select = ['_id','price','size','rate','win','loss','client','master','bType','mType','result','description','ip_address','updated_on','status'];
                        }

                        $query = $conn->table($tbl)->select($select);
                        if( $user->role == 1 || $user->role == 6){
                            $query->where([['status',2],['systemId',$user->systemId]]);
                        }elseif ( $user->role == 4 ){
                            $query->where([['status',2],['uid',$uid],['systemId',$user->systemId]]);
                        }else{
                            if( $clientArr != null ){
                                $query->where([['status',2],['result','PENDING'],['systemId',$user->systemId]]);
                                $query->whereIn('uid',$clientArr);
                            }
                        }

                        if( $searchDate == true && $start != null && $end != null ){
                            $start1 = $start->format('Y-m-d')." 00:00:01";
                            $end1 = $end->format('Y-m-d')." 23:59:59";
                            $query->whereBetween('updated_on',[$start1,$end1]);
                        }

                        $list = $query->orderBy('updated_on', 'asc')->limit(100)->get();

                        if( $list->isNotEmpty() ){
                            foreach ( $list as $item ){
                                $data[] = $item;
                            }
                        }

                    }

                    if( $data != null ){
                        $response = [ 'status' => 1, 'data' => $data , 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }else{
                    $data = [];
                    $select = ['_id','price','size','rate','win','loss','client','master','bType','mType','result','description','ip_address','updated_on','status'];
                    for( $i=1;$i<=6;$i++ ){
                        $tbl = 'tbl_bet_pending_'.$i.'_view';
                        $query = $conn->table($tbl)->select($select);
                        if( ( $user->role == 1 || $user->role == 6 ) && $user->roleName != 'ADMIN' ){
                            $query->where([['status',1],['result','PENDING'],['systemId',$user->systemId]]);
                        }elseif ( $user->roleName == 'ADMIN' ){
                            $query->where([['status',1],['result','PENDING']]);
                        }elseif ( $user->role == 4 ){
                            $query->where([['status',1],['result','PENDING'],['uid',$uid],['systemId',$user->systemId]]);
                        }else{
                            if( $clientArr != null ){
                                $query->where([['status',1],['result','PENDING'],['systemId',$user->systemId]]);
                                $query->whereIn('uid',$clientArr);
                            }
                        }

                        if( $searchDate == true && $start != null && $end != null ){
                            $start1 = $start->format('Y-m-d')." 00:00:01";
                            $end1 = $end->format('Y-m-d')." 23:59:59";
                            $query->whereBetween('updated_on',[$start1,$end1]);
                        }

                        $list = $query->orderBy('updated_on', 'asc')->get();

                        if( $list->isNotEmpty() ){
                            foreach ( $list as $item ){
                                $data[] = $item;
                            }
                        }

                    }

                    if( $data != null ){
                        $response = [ 'status' => 1, 'data' => $data , 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
