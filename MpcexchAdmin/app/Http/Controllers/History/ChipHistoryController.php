<?php

namespace App\Http\Controllers\History;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ChipHistoryController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }
    /**
     * mergeTable
     */
    public function mergeTable($requestData, $role){

        if( $role == 1 || $role == 6 ){
            $tbl = 'tbl_transaction_admin';
        }elseif ( $role == 4 ){
            $tbl = 'tbl_transaction_client';
        }else{
            $tbl = 'tbl_transaction_parent';
        }

        if( isset($requestData[ 'start' ]) && isset($requestData[ 'end' ]) ){
            $sd = explode('-',trim($requestData[ 'start' ]));
            $ed = explode('-',trim($requestData[ 'end' ]));
            $td = explode('-',trim(Carbon::now()->format('Y-m-d')));
            if( count($td) == 3 && count($sd) == 3 && count($ed) == 3){
                $sm = $sd[1]; $em = $ed[1]; $tm = $td[1];
                $sy = $sd[0]; $ey = $sd[0]; $ty = $td[0];
                $smy = $sm.$sy; $emy = $em.$ey; $tmy = $tm.$ty;

                if( $smy == $emy && $smy == $tmy ){
                    $tbl1 = $tbl.'_'.$tmy;
                    $tbl2 = null;
                }elseif($smy != $emy && $emy == $tmy){
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$tmy;
                }else{
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$emy;
                }
            }else{
                $tbl = $this->currentTable($tbl);
                $tbl1 = $tbl;
                $tbl2 = null;
            }
        }else{
            $tbl = $this->currentTable($tbl);
            $tbl1 = $tbl;
            $tbl2 = null;
        }

        return ['tbl1' => $tbl1,'tbl2' => $tbl2];
    }

    /**
     * Chip History List
     */
    public function list($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1 ){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);
                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) && $requestData[ 'ftype' ] != 'wl' ) {
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(1)->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( $user != null ){
                $userName = $user->name;
                $tbls = $this->mergeTable($requestData,$user->role);
                $tbl1 = $tbls['tbl1'];
                if( strpos($tbl1,'_012021') > 0 ){
                    $tbl1 = str_replace('_012021','_022021',$tbl1);
                }
                $tbl2 = $tbls['tbl2'];

                if( isset( $requestData[ 'ftype' ] ) && $requestData[ 'ftype' ] == 'wl' ) {
                    $query = DB::connection('mysql3')->table($tbl1)
                        ->select(['description','balance','remark','updated_on','amount','type'])
                        ->where([['userId',$uid],['status',1],['eType',1],['systemId','!=',1]]);
                }else{
                    $query = DB::connection('mysql3')->table($tbl1)
                        ->select(['description','balance','remark','updated_on','amount','type'])
                        ->where([['userId',$uid],['status',1],['eType',1],['systemId',$user->systemId]]);

                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }else{
                        $now = Carbon::now();
                        $end = $start = $now->format('Y-m-d');
                        $start = Carbon::parse($start); $end = Carbon::parse($end);
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }
                }

                $list1 = $query->orderBy('updated_on', 'DESC')->get();

                $listArr = [];
                if( $tbl2 != null ){
                    if( isset( $requestData[ 'ftype' ] ) && $requestData[ 'ftype' ] == 'wl' ) {
                        $query2 = DB::connection('mysql3')->table($tbl1)
                            ->select(['description','balance','remark','updated_on','amount','type'])
                            ->where([['userId',$uid],['status',1],['eType',1],['systemId','!=',1]]);
                    }else{
                        $query2 = DB::connection('mysql3')->table($tbl1)
                            ->select(['description','balance','remark','updated_on','amount','type'])
                            ->where([['userId',$uid],['status',1],['eType',1],['systemId',$user->systemId]]);

                        if( $searchDate == true && $start != null && $end != null ){
                            $query2->whereDate('updated_on','<=',$end->format('Y-m-d'))
                                ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                        }else{
                            $now = Carbon::now();
                            $end = $start = $now->format('Y-m-d');
                            $start = Carbon::parse($start); $end = Carbon::parse($end);
                            $query2->whereDate('updated_on','<=',$end->format('Y-m-d'))
                                ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                        }
                    }

                    $list2 = $query2->orderBy('updated_on', 'DESC')->get();
                    $listArr = [];
                    if($list2->isNotEmpty() && $list1->isNotEmpty()){
                        if($list2) {
                            foreach ( $list2 as $data ){
                                $listArr[] = $data;
                            }
                        }
                        if($list1) {
                            foreach ( $list1 as $data ){
                                $listArr[] = $data;
                            }
                        }
                    }elseif($list2->isNotEmpty() && $list1->isEmpty()){
                        $listArr = $list2;
                    }else{
                        $listArr = $list1;
                    }
                }else{
                    $listArr = $list1;
                }

                if( !empty($listArr) ){
                    $response = [ 'status' => 1, 'data' => $listArr, 'userName' => $userName ];
                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
