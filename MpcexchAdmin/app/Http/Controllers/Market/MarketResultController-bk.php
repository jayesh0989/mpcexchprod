<?php
namespace App\Http\Controllers\Market;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
class MarketResultController extends Controller
{
  public function marketResult(Request $request)
  {
    try{
        $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
        $arr      = [];

        // if( json_last_error() == JSON_ERROR_NONE ){
        //   if( isset( $request->start_date ) && isset( $request->end_date )
        //     && ( $request->start_date != '' ) && ( $request->end_date != '' )
        //     && ( $request->start_date != null ) && ( $request->end_date != null ) ){

        //     $startDate = date('Y-m-d', strtotime($request->start_date));
        //     $startDate = $startDate." 00:00:01";     
        //     $endDate   = date('Y-m-d', strtotime($request->end_date));
        //     $endDate   = $endDate." 23:59:59";
        //   }else{
        //     $start     = new \DateTime('now +1 day');
        //     $endDate   =  $start->format('yy-m-d h:i:s');
        //     $end       = new \DateTime('now -7 day');
        //     $startDate = $end->format('yy-m-d h:i:s');  
        //   }
        // }
        
        $query = DB::connection('mysql3')->table('tbl_transaction_client')
                                          ->select('eid','mid','result','description','updated_on','mType','type')
                                          ->where([['status',1]])
                                          ->where('result','!=','null')
                                          ->orderBy('created_on','DESC');
        $now        = Carbon::now();
        $endDate    = $now->format('Y-m-d');
        if(isset($request->ftype) && $request->ftype == 'week')
        {
          $startDate  = $now->subDays(7)->format('Y-m-d');
          $marketList = $query->whereBetween('created_on',[$startDate, $endDate])->get();
        }
        else
        {
          $marketList = $query->whereDate('created_on',$endDate)->get();
        }

        if(!$marketList->isEmpty())
        {
          foreach ($marketList as $marketList) 
          {
            if (isset($marketList->mType) && $marketList->mType == 'casino') {
              $desc        = explode(">", $marketList->description);
              $description = $desc[0].'>'.$desc[1].'>'.$desc[2];
              $result      = $marketList->type;
            }
            $arr [] = [
                        'eventId'     => $marketList->eid,
                        'marketId'    => $marketList->mid,
                        'description' => isset($description) ? $description : $marketList->description,
                        'result'      => isset($result) ? $result : $marketList->result,
                        'date'        => $marketList->updated_on
                      ];
          }
          $response = [ "status" => 1 ,'code'=> 200, "data" => $arr ,'message'=> 'Data Found !!' ];
        }
        else
        {
         $response = [ "status" => 1 ,'code'=> 200, "data" => [] ,'message'=> 'Data Not Found !!' ];
        }
        return $response;
    }catch (\Exception $e) {
      $response = $this->errorLog($e);
      return response()->json($response, 501);
    }
  }
}
