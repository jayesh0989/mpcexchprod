<?php

namespace App\Http\Controllers\Summary;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class SystemSettlementController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }
    /**
     * action getPlusUsers
     */
    public function getPlusUsers($uid = null){

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $plusAcc = $totalData = []; $total = 0;

            if( $uid == null ){
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }else{
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }

            if( $user != null ){

                $userArrParent = DB::table('tbl_settlement_summary')
                    ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                    ->where([['pid',$uid],['status',1],['systemId','!=',1]])
                    ->whereIn('role',['ADMIN2','SM1','SM2','M1'])
                    ->get();

                if( $userArrParent->isNotEmpty() ){

                    foreach ( $userArrParent as $uData ){

                        $pl = round($uData->parentPl+$uData->parentCash+$uData->parentComm,0);

                        if( $pl <= 0 ){
                            $plusAcc[] = [
                                'uid' => $uData->uid,
                                'pid' => $uData->pid,
                                'name' => $uData->name,
                                'role' => $uData->role,
                                'pl' => (-1)*$pl,
                            ];

                            $total = $total+(-1)*$pl;
                        }

                    }

                }

                $userArrClient = DB::table('tbl_settlement_summary')
                    ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                    ->where([['role','C'],['pid',$uid],['status',1],['systemId','!=',1]])
                    ->get();

                if( $userArrClient->isNotEmpty() ){

                    foreach ( $userArrClient as $uData ){

                        $pl = round($uData->ownPl-$uData->parentCash+$uData->ownComm,0);
                        if( $pl >= 0){
                            $plusAcc[] = [
                                'uid' => $uData->uid,
                                'pid' => $uData->pid,
                                'name' => $uData->name,
                                'role' => $uData->role,
                                'pl' => $pl,
                            ];

                            $total = $total+$pl;
                        }

                    }

                }

                if( $user->roleName !== 'ADMIN' && $user->roleName !== 'SA'){
                    $cUserSummary = DB::table('tbl_settlement_summary')
                        ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                        ->where([['uid',$uid],['status',1],['systemId','!=',1]])->first();

                    if( $cUserSummary != null ){

                        if( round($cUserSummary->ownPl+$cUserSummary->ownComm,0) >= 0){
                            $totalData['ownPl'] = round($cUserSummary->ownPl+$cUserSummary->ownComm,0);
                            $total = $total+$totalData['ownPl'];
                        }

                        if( $user->roleName !== 'ADMIN' && $user->roleName !== 'SA') {
                            if (round($cUserSummary->ownCash - $cUserSummary->parentCash, 0) >= 0) {
                                $totalData['cash'] = round($cUserSummary->ownCash - $cUserSummary->parentCash, 0);
                                $total = $total + $totalData['cash'];
                            }

                            if( round($cUserSummary->parentPl+$cUserSummary->parentCash+$cUserSummary->parentComm, 0) >= 0 ){
                                $totalData['parentPl'] = round($cUserSummary->parentPl+$cUserSummary->parentCash+$cUserSummary->parentComm,0);
                                $total = $total+$totalData['parentPl'];
                            }else{
                                $totalData['parentPl'] = 0;
                            }

                            $totalData['parentComm'] = round($cUserSummary->parentComm,0);

                        }else{
                            $cUserSummaryData = DB::table('tbl_settlement_summary')
                                ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                                ->where([['pid',$uid],['status',1],['systemId','!=',1]])->get();
                            $cash = 0;
                            if( $cUserSummaryData->isNotEmpty() ){
                                foreach ( $cUserSummaryData as $cUserSummary2 ){
                                    $cash = $cash+round( $cUserSummary2->parentCash,0);
                                }
                            }

                            if (round($cash, 2) >= 0) {
                                $totalData['cash'] = round($cash, 0);
                                $total = $total + $totalData['cash'];
                            }
                        }

                        $totalData['ownComm'] = round($cUserSummary->ownComm,0);
                        $totalData['total'] = round($total,0);
                    }
                }else{

                    $cUserSummaryData = DB::table('tbl_settlement_summary')
                        ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                        ->where([['pid',$uid],['status',1],['systemId','!=',1]])->get();

                    if( $cUserSummaryData->isNotEmpty() ){
                        $ownPl = $cash = $ownComm = 0;
                        foreach ( $cUserSummaryData as $cUserSummary ){
                            $ownPl = $ownPl+round($cUserSummary->parentPl+$cUserSummary->parentComm,2);
                            $cash = $cash+round( $cUserSummary->parentCash,0);
                            $ownComm = $ownComm+round($cUserSummary->parentComm,0);
                        }

                        if( round($ownPl,2) >= 0){
                            $totalData['ownPl'] = round($ownPl,0);
                            $total = $total+$totalData['ownPl'];
                        }
                        if( round($cash,0) >= 0){
                            $totalData['cash'] = round($cash,0);
                            $total = $total+$totalData['cash'];
                        }

                        $totalData['ownComm'] = $ownComm;
                        $totalData['total'] = round($total,0);

                    }

                }



                $response = [ 'status' => 1, 'data' => $plusAcc , 'totalData' => $totalData ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action getMinusUsers
     */
    public function getMinusUsers($uid = null){

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $minusAcc = $totalData = []; $total = 0;

            if( $uid == null ){
                $isBack = false;
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }else{
                $isBack = true;
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }

            if( $uid == Auth::id() ){
                $isBack = false;
            }
            $isCanSettle = true;
            if( Auth::user()->role == 6 ){
                $isCanSettle = false;
            }

            if( $user != null ){
                $cUserData = [
                    'cUserName' =>  $user->name. ' [' .$user->username. ']',
                    'pid' => $user->parentId,
                    'role' => $user->roleName,
                    'isBack' => $isBack,
                    'isCanSettle' => $isCanSettle
                ];

                $settledAmount = 0;
                $settlementManage = DB::table('tbl_settlement_manage')
                    ->select('amount','type')
                    ->where([['userId', $uid]])->first();

                if( $settlementManage != null ){
                    $settledAmount = $settlementManage->amount;
                    if( $settlementManage->type != 'CREDIT' ){
                        $settledAmount = (-1)*$settlementManage->amount;
                    }
                }

                $userArrParent = DB::table('tbl_settlement_summary')
                    ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                    ->where([['pid',$uid],['status',1],['systemId','!=',1]])
                    ->whereIn('role',['ADMIN2','SM1','SM2','M1'])
                    ->get();

                if( $userArrParent->isNotEmpty() ){

                    foreach ( $userArrParent as $uData ){

                        $pl = round($uData->parentPl+$uData->parentCash+$uData->parentComm,0);

                        if( $pl > 0 ){
                            $minusAcc[] = [
                                'uid' => $uData->uid,
                                'pid' => $uData->pid,
                                'name' => $uData->name,
                                'role' => $uData->role,
                                'pl' => $pl,
                            ];

                            $total = $total+$pl;
                        }

                    }

                }

                $userArrClient = DB::table('tbl_settlement_summary')
                    ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                    ->where([['role','C'],['pid',$uid],['status',1],['systemId','!=',1]])
                    ->get();

                if( $userArrClient->isNotEmpty() ){

                    foreach ( $userArrClient as $uData ){

                        $pl = round($uData->ownPl-$uData->parentCash+$uData->ownComm,0);

                        if( $pl < 0 ){
                            $minusAcc[] = [
                                'uid' => $uData->uid,
                                'pid' => $uData->pid,
                                'name' => $uData->name,
                                'role' => $uData->role,
                                'pl' => (-1)*$pl,
                            ];

                            $total = $total+(-1)*$pl;
                        }

                    }

                }

                if( $user->roleName !== 'ADMIN' && $user->roleName !== 'SA' ){
                    $cUserSummary = DB::table('tbl_settlement_summary')
                        ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                        ->where([['uid',$uid],['status',1],['systemId','!=',1]])->first();

                    if( $cUserSummary != null ){

                        if( round($cUserSummary->ownPl+$cUserSummary->ownComm,0) < 0){
                            $totalData['ownPl'] = (-1)*round( $cUserSummary->ownPl+$cUserSummary->ownComm,0);
                            $total = $total+$totalData['ownPl'];
                        }

                        if( $user->roleName !== 'ADMIN' && $user->roleName !== 'SA'){

                            if( round( $cUserSummary->ownCash-$cUserSummary->parentCash,0 ) < 0){
                                $totalData['cash'] = (-1)*round( $cUserSummary->ownCash-$cUserSummary->parentCash,0 );
                                $total = $total+$totalData['cash'];
                            }

                            if( ( round($cUserSummary->parentPl+$cUserSummary->parentCash+$cUserSummary->parentComm,0)) < 0 ){
                                $totalData['parentPl'] = (-1)*round($cUserSummary->parentPl+$cUserSummary->parentCash+$cUserSummary->parentComm,0);
                                $total = $total+$totalData['parentPl'];
                            }

                        }else{

                            $cUserSummaryData = DB::table('tbl_settlement_summary')
                                ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                                ->where([['pid',$uid],['status',1],['systemId','!=',1]])->get();
                            $cash = 0;
                            if( $cUserSummaryData->isNotEmpty() ){
                                foreach ( $cUserSummaryData as $cUserSummary ){
                                    $cash = $cash+round( $cUserSummary->parentCash,0 );
                                }
                            }
                            if (round($cash, 0) < 0) {
                                $totalData['cash'] = (-1)*round($cash, 0);
                                $total = $total + $totalData['cash'];
                            }
                        }

                        $totalData['total'] = round($total,0);
                        if( $settledAmount != 0 ){
                            $total = $total+$settledAmount;
                            $totalData['total'] = round($total,0).' ( settled amount : '.$settledAmount.' )';
                        }
                    }
                }else{

                    $cUserSummaryData = DB::table('tbl_settlement_summary')
                        ->select(['uid','pid','name','role','ownPl','ownComm','ownCash','parentPl','parentComm','parentCash'])
                        ->where([['pid',$uid],['status',1],['systemId','!=',1]])->get();

                    if( $cUserSummaryData->isNotEmpty() ){
                        $ownPl = $cash = 0;
                        foreach ( $cUserSummaryData as $cUserSummary ){
                            $ownPl = $ownPl+round( $cUserSummary->parentPl+$cUserSummary->parentComm,0);
                            $cash = $cash+round( $cUserSummary->parentCash,0 );
                        }

                        if( round($ownPl,2) < 0){
                            $totalData['ownPl'] = (-1)*round($ownPl,0);
                            $total = $total+$totalData['ownPl'];
                        }
                        if( round($cash,0) < 0){
                            $totalData['cash'] = (-1)*round($cash,0);
                            $total = $total+$totalData['cash'];
                        }

                        $totalData['total'] = round($total,0);
                        if( $settledAmount != 0 ){
                            $total = $total+$settledAmount;
                            $totalData['total'] = round($total,0).' ( settled amount : '.$settledAmount.' )';
                        }

                    }

                }

                $response = [ 'status' => 1, 'data' => $minusAcc , 'totalData' => $totalData , 'cUserData' => $cUserData ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    /**
     * action clear Settlement
     */
    public function clearSettlement()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];

        try{

            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                return $response; exit;
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

            if ($requestData) {
                $cUser = Auth::user();
                if( $cUser->role == 6 ){
                    return $response; exit;
                }
                // ( $requestData['type'] == 'plus' || $requestData['type'] == 'minus' )
                if ( $requestData['amount'] > $requestData['tempamount'] ) {
                    $response["error"]["message"] = "Invalid amount entered !!";
                    return $response;
                }

                $regExp = "#[?!%^&*=><()'\"+-]+#";
                if( isset($requestData['remark']) && preg_match($regExp,trim($requestData['remark'])) ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! ?!%^&*=><()\'" are not allowed ' ] ];
                    return $response;
                }

                if (isset($requestData['uid'])) {

                    $user = DB::table('tbl_user as u')
                        ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                        ->select(['uid','parentId','username','balance','pl','pl_balance','expose','role','u.systemId'])
                        ->where([['u.id',$requestData['uid']],['u.systemId','!=',1]])->first();

                    if ($user->role == 1) {
                        $pWhere = [['u.id',$user->parentId]];
                    }else{
                        $pWhere = [['u.id',$user->parentId],['u.systemId','!=',1]];
                    }

                    $pUser = DB::table('tbl_user as u')
                        ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                        ->select(['uid','parentId','username','balance','pl','pl_balance','expose','role','u.systemId'])
                        ->where($pWhere)->first();

                    if ($user != null && $pUser != null ) {

                        if( $user->parentId != $cUser->id && $cUser->roleName != 'ADMIN' ){
                            return $response; exit;
                        }

                        $settlementSummary = DB::table('tbl_settlement_summary')
                            ->select(['role','parentPl','parentComm','parentCash','ownPl','parentCash','ownComm'])
                            ->where([['pid', $user->parentId],['uid', $user->uid],['status', 1],['systemId','!=',1]])
                            ->first();
                        $settlementType = 'plus';
                        if( $settlementSummary != null ){
                            if( $settlementSummary->role == 'C' ){
                                $settlementAmount = round($settlementSummary->ownPl - $settlementSummary->parentCash + $settlementSummary->ownComm, 0);
                                if( $settlementAmount < 0 ){ $settlementType = 'minus'; }
                            }else{
                                $settlementAmount = round($settlementSummary->parentPl + $settlementSummary->parentCash + $settlementSummary->parentComm, 0);
                                if( $settlementAmount > 0 ){ $settlementType = 'minus'; }
                            }
                            if( $settlementAmount < 0 ){ $settlementAmount = (-1)*$settlementAmount; }
                            if( $requestData['amount'] > $settlementAmount ){
                                return $response; exit;
                            }
                        }

                        $amount = $requestData['amount'];
                        if ($requestData['amount'] < 0) {
                            $amount = (-1) * $requestData['amount'];
                        }

                        if ($user->role == 4) {
                            $userAvailableBalance = (float)$user->balance + (float)$user->pl_balance - (float)$user->expose;
                            if ( $settlementType == 'plus' && $requestData['amount'] > $userAvailableBalance ) {
                                $response["error"]["message"] = "Balance not available for this settlement !!";
                                return $response;
                            }
                        }

                        if ( $settlementType == 'minus') {

                            if( $user->role == 4 ){
                                $userPl = ($user->pl_balance + $amount);
                            }else{
                                $userPl = $user->pl_balance; // ( $user->pl_balance - $amount );
                            }

                            if( $requestData['checkbox'] != true && $user->role == 4){
                                $balance = ( $user->balance - $amount );

                                if ( $balance < 0 ) {
                                    $response["error"]["message"] = "Chips Balance not available !!";
                                    return $response;
                                }

                                $updateArr = ['balance' => $balance,'pl_balance' => $userPl];
                            }else if( $requestData['checkbox'] == true && $user->role == 4){
                                $balance = $user->balance;
                                if ( ( $pUser->balance - $amount ) < 0 ) {
                                    $response["error"]["message"] = "Chips Balance not available for parent!!";
                                    return $response;
                                }

                                $updateArr = ['balance' => $balance,'pl_balance' => $userPl];
                            }else{
                                $updateArr = ['balance' => $user->balance,'pl_balance' => $userPl];
                            }

                        } else {

                            if( $user->role == 4 ){
                                $userPl = ( $user->pl_balance - $amount );
                            }else{
                                $userPl = $user->pl_balance; //( $user->pl_balance + $amount );
                            }

                            $updateArr = ['balance' => $user->balance,'pl_balance' => $userPl];
                        }

                        if ( $updateArr != null ) {

                            if ($this->clearChipsTransaction($user,$pUser,$amount,$settlementType,$requestData['remark']) == true) {

                                if( $requestData['checkbox'] != true && $settlementType == 'minus' && $user->role == 4 ){
                                    if( $this->updateChipsTransaction($user,$pUser,$requestData,$settlementType) == true ){

                                        DB::table('tbl_user_info')
                                            ->where([['uid',$requestData['uid']]])->update( $updateArr );

                                        $response = [
                                            "status" => 1,
                                            "success" => [
                                                "message" => "Cash Deposit Successfully!"
                                            ]
                                        ];
                                    }else{
                                        $response["error"] = [
                                            "message" => "Something wrong! user not updated!"
                                        ];
                                    }
                                }else if( $settlementType == 'minus' && $requestData['checkbox'] == true && $user->role == 4  ){
                                    if( $this->updateChipsBackTransaction($user,$pUser,$requestData,$settlementType) == true ){

                                        DB::table('tbl_user_info')
                                            ->where([['uid',$requestData['uid']]])->update( $updateArr );

                                        $response = [
                                            "status" => 1,
                                            "success" => [
                                                "message" => "Cash Deposit Successfully!"
                                            ]
                                        ];
                                    }else{
                                        $response["error"] = [
                                            "message" => "Something wrong! user not updated!"
                                        ];
                                    }
                                }else{

                                     DB::table('tbl_user_info')
                                        ->where([['uid',$requestData['uid']]])->update( $updateArr );

                                    $response = [
                                        "status" => 1,
                                        "success" => [
                                            "message" => "Cash Deposit Successfully!"
                                        ]
                                    ];
                                }

                            } else {
                                $response["error"] = [
                                    "message" => "Something wrong! user not updated!"
                                ];
                            }

                        } else {
                            $response["error"] = [
                                "message" => "Something wrong! user not updated!"
                            ];
                        }

                    }
                }
            }

            if ( $response['status'] == 1 && isset($user->username) && isset($amount)){
                $data['title'] = 'Settlement';
                $data['description'] = 'User ( '. $user->username .' ) Settlement of '.$amount;
                $this->addNotification($data);
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Clear Chips Transaction
    public function clearChipsTransaction($user, $pUser, $amount, $typ, $remark)
    {
        $parentUserName = $pUser->username;
        $userName = $user->username;

        if ($typ == 'minus') {
            if ($user->role == 4) {
                $balance1 = $user->balance + $user->pl_balance + $amount;
            }else{
                $balance1 = ' - '; //$user->pl_balance - $amount;
            }
            $type1 = 'CREDIT';//'DEBIT';
            $description1 = 'Cash Deposit By ' . $userName . ' To ' . $parentUserName;

            $balance2 = ' - ';//$pUser->pl_balance;
            $type2 = 'DEBIT';//'CREDIT';
            $description2 = 'Cash Received By ' . $parentUserName . ' From ' . $userName;

        } else {
            if ($user->role == 4) {
                $balance1 = $user->balance + $user->pl_balance - $amount;
            }else{
                $balance1 = ' - '; //$user->pl_balance + $amount;
            }
            $type1 = 'DEBIT';//'CREDIT';
            $description1 = 'Cash Received By ' . $userName . ' From ' . $parentUserName;

            $balance2 = ' - ';//$pUser->pl_balance;
            $type2 = 'CREDIT';//'DEBIT';
            $description2 = 'Cash Deposit By ' . $parentUserName . ' To ' . $userName;
        }

        $resultArr1 = [
            'systemId' => $user->systemId,
            'clientId' => $user->uid,
            'userId' => $user->uid,
            'childId' => 0,
            'parentId' => $pUser->uid,
            'sid' => 0, 'eid' => 0, 'mid' => 0,
            'eType' => 2, // 2 for cash
            'mType' => 'Cash Entry',
            'type' => $type1,
            'amount' => $amount,
            'p_amount' => 0, 'c_amount' => 0,
            'balance' => $balance1,
            'description' => $description1,
            'remark' => $remark,
            'status' => 1,
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s')
        ];

        $resultArr2 = [
            'systemId' => $user->systemId,
            'clientId' => $user->uid,
            'userId' => $pUser->uid,
            'childId' => 0,
            'parentId' => $pUser->parentId,
            'sid' => 0, 'eid' => 0, 'mid' => 0,
            'eType' => 2, // 2 for cash
            'mType' => 'Cash Entry',
            'type' => $type2,
            'amount' => $amount,
            'p_amount' => 0, 'c_amount' => 0,
            'balance' => $balance2,
            'description' => $description2,
            'remark' => $remark,
            'status' => 1,
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s')
        ];

        if( $user->role == 4 ){
            $tbl1 = 'tbl_transaction_client';
        }elseif ( $user->role == 1 ){
            $tbl1 = 'tbl_transaction_admin';
        }else{
            $tbl1 = 'tbl_transaction_parent';
        }
        $tbl1 = $this->currentTable($tbl1);
        if ( $pUser->role == 1 ){
            $tbl2 = 'tbl_transaction_admin';
        }else{
            $tbl2 = 'tbl_transaction_parent';
        }
        $tbl2 = $this->currentTable($tbl2);

        if ( DB::connection('mysql3')->table($tbl1)->insert($resultArr1) && DB::connection('mysql3')->table($tbl2)->insert($resultArr2)) {
            $this->updateSettlementSummary($user->uid,$typ,$amount,$tbl1,'child');
            $this->updateSettlementSummary($pUser->uid,$typ,$amount,$tbl2,'parent');
            return true;
        } else {
            return false;
        }

    }

    // Update Chips Transaction
    public function updateChipsTransaction($user,$pUser,$request,$settlementType)
    {
        if ( isset( $settlementType ) && ( $settlementType == 'minus' ) ) {

            if ( $request['amount'] != null || $request['amount'] != "") {
                $amount = (float)$request['amount'];
            } else {
                $amount = 0;
            }

            $resultArr = [
                'systemId' => $user->systemId,
                'clientId' => $user->uid,
                'userId' => $user->uid,
                'childId' => 0,
                'parentId' => $pUser->uid,
                'sid' => 0, 'eid' => 0, 'mid' => 0,
                'eType' => 1, // 1 for chip
                'mType' => 'Chip Entry',
                'type' => 'DEBIT',
                'amount' => $amount,
                'p_amount' => 0, 'c_amount' => 0,
                'balance' => $user->balance,
                'description' => 'Chips clear by settlement !',
                'remark' => $request['remark'],
                'status' => 1,
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s')
            ];

            if( $user->role == 4 ){
                $tbl = 'tbl_transaction_client';
            }elseif ( $user->role == 1 ){
                $tbl = 'tbl_transaction_admin';
            }else{
                $tbl = 'tbl_transaction_parent';
            }
            $tbl = $this->currentTable($tbl);
            if ( DB::connection('mysql3')->table($tbl)->insert($resultArr) ) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }

    }

    // Update Chips Transaction
    public function updateChipsBackTransaction($user,$pUser,$request,$settlementType)
    {

        if ( isset( $settlementType ) && ( $settlementType == 'minus' ) ) {

            if ( $request['amount'] != null || $request['amount'] != "") {
                $amount = (float)$request['amount'];
            } else {
                $amount = 0;
            }

            $resultArr = [
                'systemId' => $user->systemId,
                'clientId' => $user->uid,
                'userId' => $user->uid,
                'childId' => 0,
                'parentId' => $pUser->uid,
                'sid' => 0, 'eid' => 0, 'mid' => 0,
                'eType' => 1, // 1 for chip
                'mType' => 'Chip Entry',
                'type' => 'DEBIT',
                'amount' => $amount,
                'p_amount' => 0, 'c_amount' => 0,
                'balance' => $user->balance,
                'description' => 'Chips clear by settlement !',
                'remark' => $request['remark'],
                'status' => 1,
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s')
            ];

            if( $user->role == 4 ){
                $tbl = 'tbl_transaction_client';
            }elseif ( $user->role == 1 ){
                $tbl = 'tbl_transaction_admin';
            }else{
                $tbl = 'tbl_transaction_parent';
            }
            $tbl = $this->currentTable($tbl);
            if ( DB::connection('mysql3')->table($tbl)->insert($resultArr) ) {

                $resultArr1 = [
                    'systemId' => $user->systemId,
                    'clientId' => $user->uid,
                    'userId' => $user->uid,
                    'childId' => 0,
                    'parentId' => $pUser->uid,
                    'sid' => 0, 'eid' => 0, 'mid' => 0,
                    'eType' => 1, // 1 for chip
                    'mType' => 'Chip Entry',
                    'type' => 'CREDIT',
                    'amount' => $amount,
                    'p_amount' => 0, 'c_amount' => 0,
                    'balance' => ( $user->balance + $amount ),
                    'description' => 'Chip Receive From ' . $pUser->username,
                    'remark' => $request['remark'],
                    'status' => 1,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s')
                ];

                if( $user->role == 4 ){
                    $tbl1 = 'tbl_transaction_client';
                }elseif ( $user->role == 1 ){
                    $tbl1 = 'tbl_transaction_admin';
                }else{
                    $tbl1 = 'tbl_transaction_parent';
                }
                $tbl1 = $this->currentTable($tbl1);
                $resultArr2 = [
                    'systemId' => $user->systemId,
                    'clientId' => $user->uid,
                    'userId' => $pUser->uid,
                    'childId' => 0,
                    'parentId' => $pUser->parentId,
                    'sid' => 0, 'eid' => 0, 'mid' => 0,
                    'eType' => 1, // 1 for chip
                    'mType' => 'Chip Entry',
                    'type' => 'DEBIT',
                    'amount' => $amount,
                    'p_amount' => 0, 'c_amount' => 0,
                    'balance' => ( $pUser->balance - $amount ),
                    'description' => 'Chip Deposit To ' . $user->username,
                    'remark' => $request['remark'],
                    'status' => 1,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s')
                ];

                if ( $pUser->role == 1 ){
                    $tbl2 = 'tbl_transaction_admin';
                }else{
                    $tbl2 = 'tbl_transaction_parent';
                }
                $tbl2 = $this->currentTable($tbl2);
                if( DB::connection('mysql3')->table($tbl1)->insert($resultArr1) && DB::connection('mysql3')->table($tbl2)->insert($resultArr2) ){

                    if ( $user->role == 1 ){
                        DB::table('tbl_user_info')
                            ->where([['uid',$pUser->uid]])->update(['balance' => ($pUser->balance - $amount) ]);
                    }else{
                        DB::table('tbl_user_info')
                            ->where([['uid',$pUser->uid],['systemId','!=',1]])->update(['balance' => ($pUser->balance - $amount) ]);
                    }

                    DB::table('tbl_user_info')
                        ->where([['uid',$user->uid],['systemId','!=',1]])->update(['balance' => ($user->balance + $amount) ]);

                    return true;
                }else{
                    return false;
                }

            }else{
                return false;
            }

        } else {
            return false;
        }

    }

    // update Settlement Summary
    public function updateSettlementSummary($uid,$type,$amount,$tbl,$cashType)
    {
        $userSummary = DB::table('tbl_settlement_summary')
            ->where([['status',1],['uid',$uid]])->first();
        if( $userSummary != null ){
            if( $type == 'plus' ){
                if( $cashType == 'child' ){
                    $newCash = $userSummary->parentCash+round($amount);
                }else{
                    $newCash = $userSummary->ownCash+round($amount);
                }

            }else{

                if( $cashType == 'child' ){
                    $newCash = $userSummary->parentCash-round($amount);
                }else{
                    $newCash = $userSummary->ownCash-round($amount);
                }
            }

            $lastEntry = DB::connection('mysql3')->table($tbl)->select('id')
                ->where([['eType',2],['userId',$uid]])->orderBy('id','desc')->first();

            if( $lastEntry != null ){
                $lastTransId = $lastEntry->id;
                if( $cashType == 'child' ){
                    $updateArr = ['parentCash' => $newCash, 'lastTransId' => $lastTransId ];
                }else{
                    $updateArr = ['ownCash' => $newCash, 'lastTransId' => $lastTransId ];
                }

            }else{

                if( $cashType == 'child' ){
                    $updateArr = ['parentCash' => $newCash ];
                }else{
                    $updateArr = ['ownCash' => $newCash ];
                }
            }

            DB::table('tbl_settlement_summary')->where([['status',1],['uid',$uid]])
                ->update($updateArr);
            return;
        }
        return;

    }

}
