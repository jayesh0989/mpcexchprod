<?php

namespace App\Http\Controllers\Transaction;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class ProfitLossController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }

    /**
     * mergeTable
     */
    public function mergeTable($requestData, $role){

        if( $role == 1 || $role == 6 ){
            $tbl = 'tbl_transaction_admin';
        }elseif ( $role == 4 ){
            $tbl = 'tbl_transaction_client';
        }else{
            $tbl = 'tbl_transaction_parent';
        }

        if( isset($requestData[ 'start' ]) && isset($requestData[ 'end' ]) ){
            $sd = explode('-',trim($requestData[ 'start' ]));
            $ed = explode('-',trim($requestData[ 'end' ]));
            $td = explode('-',trim(Carbon::now()->format('Y-m-d')));
            if( count($td) == 3 && count($sd) == 3 && count($ed) == 3){
                $sm = $sd[1]; $em = $ed[1]; $tm = $td[1];
                $sy = $sd[0]; $ey = $sd[0]; $ty = $td[0];
                $smy = $sm.$sy; $emy = $em.$ey; $tmy = $tm.$ty;

                if( $smy == $emy && $smy == $tmy ){
                    $tbl1 = $tbl.'_'.$tmy;
                    $tbl2 = null;
                }elseif($smy != $emy && $emy == $tmy){
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$tmy;
                }else{
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$emy;
                }
            }else{
                $tbl = $this->currentTable($tbl);
                $tbl1 = $tbl;
                $tbl2 = null;
            }
        }else{
            $tbl = $this->currentTable($tbl);
            $tbl1 = $tbl;
            $tbl2 = null;
        }

        return ['tbl1' => $tbl1,'tbl2' => $tbl2];
    }

    /**
     * switchTable
     */
    public function switchTable($requestData, $tbl){
        if( isset($requestData[ 'start' ]) && isset($requestData[ 'end' ]) ){
            $sd = explode('-',trim($requestData[ 'start' ]));
            $ed = explode('-',trim($requestData[ 'end' ]));
            $td = explode('-',trim(Carbon::now()->format('Y-m-d')));
            $switchTable = false; $smy = '';
            if( count($td) == 3 && count($sd) == 3 && count($ed) == 3){
                $sm = $sd[1]; $em = $ed[1]; $tm = $td[1];
                $sy = $sd[0]; $ey = $sd[0]; $ty = $td[0];
                $smy = $sm.$sy; $emy = $em.$ey;

                if( $smy == $emy && $sy == $ty && ( ( $tm-$sm ) >=2 )){
                    $switchTable = true;
                }elseif ( $smy == $emy && $sy != $ty && ( ( $tm-$sm ) <= 10 ) ){
                    $switchTable = true;
                }
            }
            if( $switchTable == true ){
                $tbl1 = $tbl.'_'.$smy;
                if( Schema::connection('mysql3')->hasTable($tbl1) ){
                    $tbl = $tbl1;
                }
            }
        }
        return $tbl;
    }

    /**
     * Profit Loss List
     */
    public function list($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
            }

            if( $user != null ){
                $userName = $user->username;
                $sportList = DB::table('tbl_sport')
                    ->select(['name','sportId'])->where([['status',1]])
                    ->orderBy('position', 'ASC')->get();
                if( $sportList != null ){
                    $data = []; $totalPl = 0;
                    foreach ( $sportList as $sport ){

                        if( $sport->sportId != 99 && $sport->sportId != 999 && $sport->sportId != 9999 ){
                            $profitLoss = $this->getProfitLossBySport($user, $sport->sportId, $requestData);
                            $data[] = [
                                'name' => $sport->name,
                                'sportId' => $sport->sportId,
                                'profitLoss' => $profitLoss,
                            ];
                            $totalPl = $totalPl+$profitLoss;
                        }

                    }

                    $response = [ 'status' => 1, 'data' => $data, 'userName' => $userName,'total' => [ 'pl' => round($totalPl,2) ] ];
                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * listEventDetail
     */
    public function listEventDetail($uid = null)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            if( !isset( $requestData[ 'sid' ] ) ) {
                return $response;
            }

            $sportId = $requestData[ 'sid' ]; $searchDate = false; $start = $end = null;

            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }

            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);

                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) ) {
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $now->format('Y-m-d');
                 // echo "test".  $start = $now->format('Y-m-d');
                   $start = $now->subDays(1)->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            $systemId = $user->systemId;
            if( $user != null ){

                $userName = $user->username;

                if( $user->role == 1 || $user->role == 6 ){
                    $tbl = 'tbl_transaction_admin_view';
                }elseif ( $user->role == 4 ){
                    $tbl = 'tbl_transaction_client_view';
                }else{
                    $tbl = 'tbl_transaction_parent_view';
                }
                $tbl = $this->switchTable($requestData,$tbl);

                if( $user->roleName == 'ADMIN' || $user->role == 6 ){
                    $where = [['userId',$uid],['sid',$sportId],['status',1]];
                }else{
                    $where = [['userId',$uid],['sid',$sportId],['status',1],['systemId',$systemId]];
                }

                $query = DB::connection('mysql3')->table($tbl)
                    ->select(['eid','description','balance','updated_on','amount','type','eType','mType','result'])
                    ->where($where)
                    ->whereIn('eType',[0,3]);

                if( $searchDate == true && $start != null && $end != null ){
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }else{
                    $now = Carbon::now();
                    $end = $start = $now->format('Y-m-d');
                    $start = Carbon::parse($start); $end = Carbon::parse($end);
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }

                $listArr = $query->orderBy('updated_on', 'DESC')->get();

                if( $listArr != null ){
                    $eidArr = $transArr = []; $sport = 'Event';
                    foreach ( $listArr as $list ){
                        $amount = $amount1 = 0;
                        if( $list->eType == 0 ){
                            $amount = $list->amount;
                            if( $list->type != 'CREDIT' ){
                                $amount = (-1)*$list->amount;
                            }
                        }else{
                            $amount1 = $list->amount;
                            if( $list->type != 'CREDIT' ){
                                $amount1 = (-1)*$list->amount;
                            }
                        }

                        if( !in_array( $list->eid , $eidArr ) ){
                            $eidArr[] = $list->eid;
                            $description = 'Not Found!';
                            if( $list->description != null ){
                                $descData = explode( '>',$list->description );
                                if( $descData != null ){
                                    $sport = trim($descData[0]);
                                    $description = implode(' > ',[trim($descData[0]),trim($descData[1])]);
                                }
                            }

                            $transArr[$list->eid] = [
                                'description' => $description,
                                'time' => $list->updated_on,
                                'pl' => $amount,
                                'comm' => $amount1,
                                'eid' => $list->eid,
                            ];
                            if( $list->mType == 'match_odd' ){
                                $transArr[$list->eid]['result'] = $list->result;
                            }
                        }else{
                            if( $list->mType == 'match_odd' ){
                                $transArr[$list->eid]['result'] = $list->result;
                            }
                            $transArr[$list->eid]['comm'] = round( ( $transArr[$list->eid]['comm']+$amount1 ) , 2 );
                            $transArr[$list->eid]['pl'] = round( ( $transArr[$list->eid]['pl']+$amount ) , 2 );
                            $transArr[$list->eid]['time'] = $list->updated_on;
                        }

                    }
                    $dataArr = []; $totalPl = $totalComm = 0;
                    if( $transArr != null ){
                        foreach ( $transArr as $eid => $data ){
                            $dataArr[] = $data;
                            $totalPl = $totalPl+$data['pl'];
                            $totalComm = $totalComm+$data['comm'];
                        }
                    }

                    $response = [ 'status' => 1, 'data' => $dataArr, 'userName' => $userName ,'sport' => $sport,'total' => [ 'pl' => round($totalPl,2) , 'comm' => round($totalComm,2) ] ];

                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * listEventDetail
     */
    public function listMarketDetail($uid = null)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

            if( !isset( $requestData[ 'eid' ] ) ) {
                return $response;
            }

            $eventId = $requestData[ 'eid' ]; $searchDate = false; $start = $end = null;

            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }

            $eType = 0; $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);

                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) ) {
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $start = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            $systemId = $user->systemId;
            if( $user != null ){
                $userName = $user->username;
                if( $user->role == 1 || $user->role == 6 ){
                    $tbl = 'tbl_transaction_admin_view';
                }elseif ( $user->role == 4 ){
                    $tbl = 'tbl_transaction_client_view';
                }else{
                    $tbl = 'tbl_transaction_parent_view';
                }
                $tbl = $this->switchTable($requestData,$tbl);

                if( $user->roleName == 'ADMIN' || $user->role == 6 ){
                    $where = [['userId',$uid],['status',1],['eid',$eventId]];
                }else{
                    $where = [['userId',$uid],['status',1],['eid',$eventId],['systemId',$systemId]];
                }

                $query = DB::connection('mysql3')->table($tbl)
                    ->select(['mid','sid','description','balance','remark','updated_on','amount','type','eType','mType','result'])
                    ->where($where)
                    ->whereIn('eType',[0,3]);

                if( $searchDate == true && $start != null && $end != null ){
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }else {
                    if ( isset($requestData['sid']) && ( $requestData['sid'] == '99' || $requestData['sid'] == '999' )) {
                        $now = Carbon::now();
                        $end = $start = $now->format('Y-m-d');
                        $start = Carbon::parse($start);
                        $end = Carbon::parse($end);
                        $query->whereDate('updated_on', '<=', $end->format('Y-m-d'))
                            ->whereDate('updated_on', '>=', $start->format('Y-m-d'));

                    }
                }

                $listArr = $query->orderBy('updated_on', 'DESC')->get();

                if( $listArr != null ){
                    if( $user->role == 4 ){
                        $transArr = []; $totalPl = $totalComm = 0; $event = 'Market';
                        foreach ( $listArr as $list ) {
                            $amount = $amount1 = 0;
                            if( $list->eType == 0 ){
                                $amount = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount = (-1)*$list->amount;
                                }
                                $totalPl = $totalPl+$amount;
                            }else{
                                $amount1 = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount1 = (-1)*$list->amount;
                                }
                                $totalComm = $totalComm+$amount1;
                            }

                            $description = 'Not Found!';
                            if( $list->description != null ){
                                $descData = explode( '>',$list->description );
                                if( $descData != null ){
                                    if( !in_array($list->sid,[7,11,99,999]) ){
                                        $event = trim($descData[1]);
                                        if( $list->mType == 'winner' || $list->mType == 'match_odd' || $list->mType == 'completed_match' || $list->mType == 'tied_match'){
                                            $description = implode(' > ',[trim($descData[0]),trim($descData[1]),trim($descData[3])]);
                                        }else{
                                            $description = implode(' > ',[trim($descData[0]),trim($descData[1]),trim($descData[2]),trim($descData[3])]);
                                        }
                                    }else{
                                        $description = $list->description;
                                    }
                                }
                            }

                            $transArr[] = [
                                'description' => $description,
                                'time' => $list->updated_on,
                                'result' => $list->result,
                                'pl' => $amount,
                                'comm' => $amount1,
                                'mid' => $list->mid,
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $transArr , 'userName' => $userName , 'event' => $event,'total' => [ 'pl' => round($totalPl,2) , 'comm' => round($totalComm,2) ]];

                    }else{
                        $midArr = $transArr = []; $event = 'Market';
                        foreach ( $listArr as $list ){

                            $amount = $amount1 = 0;
                            if( $list->eType == 0 ){
                                $amount = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount = (-1)*$list->amount;
                                }
                            }else{
                                $amount1 = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount1 = (-1)*$list->amount;
                                }
                            }

                            $description = 'Not Found!';
                            if( $list->description != null ){
                                $descData = explode( '>',$list->description );
                                if( $descData != null ){
                                    if( !in_array($list->sid,[7,11,99,999,9999]) ){
                                        $event = trim($descData[1]);
                                        if( $list->mType == 'winner' || $list->mType == 'match_odd' || $list->mType == 'completed_match' || $list->mType == 'tied_match'){
                                            $description = implode(' > ',[trim($descData[0]),trim($descData[1]),trim($descData[3])]);
                                        }else{
                                            $description = implode(' > ',[trim($descData[0]),trim($descData[1]),trim($descData[2]),trim($descData[3])]);
                                        }
                                    }else{
                                        $description = $list->description;
                                    }
                                }
                            }

                            if( !in_array( $list->mid , $midArr ) ){

                                $midArr[] = $list->mid;
                                $transArr[$list->mid] = [
                                    'description' => $description,
                                    'time' => $list->updated_on,
                                    'result' => $list->result,
                                    'pl' => $amount,
                                    'comm' => $amount1,
                                    'mid' => $list->mid,
                                ];

                            }else{
                                $transArr[$list->mid]['comm'] = round( ( $transArr[$list->mid]['comm']+$amount1 ) , 2 );
                                $transArr[$list->mid]['pl'] = round( ( $transArr[$list->mid]['pl']+$amount ) , 2 );
                                $transArr[$list->mid]['balance'] = $list->balance;
                                $transArr[$list->mid]['time'] = $list->updated_on;
                            }

                        }
                        $dataArr = []; $totalPl = $totalComm = 0;
                        if( $transArr != null ){
                            foreach ( $transArr as $mid => $data ){
                                $dataArr[] = $data;
                                $totalPl = $totalPl+$data['pl'];
                                $totalComm = $totalComm+$data['comm'];
                            }
                        }

                        $response = [ 'status' => 1, 'data' => $dataArr, 'userName' => $userName,'event' => $event, 'total' => [ 'pl' => round($totalPl,2) , 'comm' => round($totalComm,2) ] ];
                    }

                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // getProfitLossBySport
    public function getProfitLossBySport($user, $sportId, $requestData)
    {
        $total = 0; $searchDate = false; $start = $end = null;
        $uid = $user->id; if( $user->role == 6 ){ $uid = 1; }

        if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
            $start = Carbon::parse($requestData[ 'start' ]);
            $end = Carbon::parse($requestData[ 'end' ]);
            $searchDate = true;
        }

        if( isset( $requestData[ 'ftype' ] ) ) {
            $now = Carbon::now();
            if( $requestData[ 'ftype' ] == 'week' ){
                $end = $now->format('Y-m-d');
                $start = $now->subDays(7)->format('Y-m-d');
            }elseif ( $requestData[ 'ftype' ] == 'month' ){
                $end = $now->format('Y-m-d');
                $start = $now->subDays(30)->format('Y-m-d');
            }else{
                $end = $start = $now->format('Y-m-d');
            }

            $start = Carbon::parse($start); $end = Carbon::parse($end);
            $searchDate = true;
        }

        $systemId = $user->systemId;
        $tbls = $this->mergeTable($requestData,$user->role);
        $tbl1 = $tbls['tbl1'];
        if( strpos($tbl1,'_012021') > 0 ){
            $tbl1 = str_replace('_012021','_022021',$tbl1);
        }
        $tbl2 = $tbls['tbl2'];
        if( $user->roleName == 'ADMIN' || $user->role == 6 ){
            $where = [['userId',$uid],['status',1],['eType',0],['sid',$sportId]];
        }else{
            $where = [['userId',$uid],['status',1],['eType',0],['sid',$sportId],['systemId',$systemId]];
        }

        $query = DB::connection('mysql3')->table($tbl1)
            ->select(['amount','type'])->where($where);

        if( $searchDate == true && $start != null && $end != null ){
            $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                ->whereDate('updated_on','>=',$start->format('Y-m-d'));
        }

        $listArr1 = $query->orderBy('updated_on', 'DESC')->get();

        $listArr = [];
        if( $tbl2 != null ){
            $query2 = DB::connection('mysql3')->table($tbl2)
                ->select(['amount','type'])->where($where);

            if( $searchDate == true && $start != null && $end != null ){
                $query2->whereDate('updated_on','<=',$end->format('Y-m-d'))
                    ->whereDate('updated_on','>=',$start->format('Y-m-d'));
            }

            $listArr2 = $query2->orderBy('updated_on', 'DESC')->get();

            if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                if($listArr2) {
                    foreach ( $listArr2 as $data ){
                        $listArr[] = $data;
                    }
                }
                if($listArr1) {
                    foreach ( $listArr1 as $data ){
                        $listArr[] = $data;
                    }
                }
            }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                $listArr = $listArr2;
            }else{
                $listArr = $listArr1;
            }
        }else{
            $listArr = $listArr1;
        }

        if( !empty($listArr) ){
            foreach ( $listArr as $list ){

                $amount = $list->amount;
                if ($list->type != 'CREDIT') {
                    $amount = (-1) * $list->amount;
                }
                $total = $total+$amount;
            }
        }

        if( $total != 0 ){ return round( $total , 2 ); }else{ return $total;}

    }

    /**
     * Teen patti Profit Loss List
     */
    public function listTeenPatti($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            if( $requestData != null && isset($requestData['sid']) ){
                $sid = $requestData['sid'];
            }else{
                return response()->json($response, 200);
            }

            $uid = isset($requestData['uid']) ? $requestData['uid'] : null;

            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
            }

            if( $user != null ){
                $userName = $user->username;
                $liveGameList = DB::table('tbl_live_game')
                    ->select(['name','sportId','eventId'])->where([['sportId',$sid],['status',1]])
                    ->orderBy('id', 'ASC')->get();
                if( $liveGameList != null ){
                    $data = []; $totalPl = 0;
                    foreach ( $liveGameList as $list ){

                        $profitLoss = $this->getProfitLossByEvent($user, $list->eventId, $requestData);

                        $data[] = [
                            'name' => $list->name,
                            'sportId' => $list->sportId,
                            'eventId' => $list->eventId,
                            'profitLoss' => $profitLoss,
                        ];
                        $totalPl = $totalPl+$profitLoss;
                    }
                    $response = [ 'status' => 1, 'data' => $data, 'userName' => $userName ,'total' => [ 'pl' => round($totalPl,2) ] ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    /**
     * Casino Profit Loss List
     */
    public function listCasino($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $uid = isset($requestData['uid']) ? $requestData['uid'] : null;

            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
            }

            if( $user != null ){
                $userName = $user->username;
                $liveGameList = DB::connection('mongodb')->table('tbl_casino_games')
                    ->select(['name','game_id'])->where([['status',1]])->get();
                if( $liveGameList != null ){
                    $data = []; $totalPl = 0;
                    foreach ( $liveGameList as $list ){
                        $list = (object)$list;
                        $eventId = $list->game_id;
                        $profitLoss = $this->getProfitLossByEvent($user,$eventId,$requestData);

                        $data[] = [
                            'name' => $list->name,
                            'sportId' => 9999,
                            'eventId' => $eventId,
                            'profitLoss' => $profitLoss,
                        ];
                        $totalPl = $totalPl+$profitLoss;
                    }
                    $response = [ 'status' => 1, 'data' => $data, 'userName' => $userName ,'total' => [ 'pl' => round($totalPl,2) ] ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    // getProfitLossByEvent
    public function getProfitLossByEvent($user, $eventId, $requestData)
    {
        $total = 0; $searchDate = false; $start = $end = null;
        $uid = $user->id; if( $user->role == 6 ){ $uid = 1; }

        if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
            $start = Carbon::parse($requestData[ 'start' ]);
            $end = Carbon::parse($requestData[ 'end' ]);
            $searchDate = true;
        }

        if( isset( $requestData[ 'ftype' ] ) ) {
            $now = Carbon::now();
            if( $requestData[ 'ftype' ] == 'week' ){
                $end = $now->format('Y-m-d');
                $start = $now->subDays(7)->format('Y-m-d');
            }elseif ( $requestData[ 'ftype' ] == 'month' ){
                $end = $now->format('Y-m-d');
                $start = $now->subDays(30)->format('Y-m-d');
            }else{
                $end = $start = $now->format('Y-m-d');
            }

            $start = Carbon::parse($start); $end = Carbon::parse($end);
            $searchDate = true;
        }

        $systemId = $user->systemId;

        $tbls = $this->mergeTable($requestData,$user->role);
        $tbl1 = $tbls['tbl1'];
        if( strpos($tbl1,'_012021') > 0 ){
            $tbl1 = str_replace('_012021','_022021',$tbl1);
        }
        $tbl2 = $tbls['tbl2'];
        if( $user->roleName == 'ADMIN' || $user->role == 6 ){
            $where = [['userId',$uid],['status',1],['eType',0],['eid',$eventId]];
        }else{
            $where = [['userId',$uid],['status',1],['eType',0],['eid',$eventId],['systemId',$systemId]];
        }

        $query = DB::connection('mysql3')->table($tbl1)
            ->select(['amount','type'])->where($where);
        if( $searchDate == true && $start != null && $end != null ){
            $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                ->whereDate('updated_on','>=',$start->format('Y-m-d'));
        }
        $listArr1 = $query->orderBy('updated_on', 'DESC')->get();
        $listArr = [];
        if( $tbl2 != null ){
            $query2 = DB::connection('mysql3')->table($tbl2)
                ->select(['amount','type'])->where($where);
            if( $searchDate == true && $start != null && $end != null ){
                $query2->whereDate('updated_on','<=',$end->format('Y-m-d'))
                    ->whereDate('updated_on','>=',$start->format('Y-m-d'));
            }
            $listArr2 = $query2->orderBy('updated_on', 'DESC')->get();
            if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                if($listArr2) {
                    foreach ( $listArr2 as $data ){
                        $listArr[] = $data;
                    }
                }
                if($listArr1) {
                    foreach ( $listArr1 as $data ){
                        $listArr[] = $data;
                    }
                }
            }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                $listArr = $listArr2;
            }else{
                $listArr = $listArr1;
            }
        }else{
            $listArr = $listArr1;
        }

        if( !empty($listArr) ){
            foreach ( $listArr as $list ){
                $amount = $list->amount;
                if ($list->type != 'CREDIT') {
                    $amount = (-1) * $list->amount;
                }
                $total = $total+$amount;
            }
        }

        if( $total != 0 ){ return round( $total , 2 ); }else{ return $total;}

    }


}
