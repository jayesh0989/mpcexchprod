<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    // getList
    public static function getList($rid = false)
    {
        if( $rid != false ){
            $user = DB::table('tbl_user')->where([['id',$rid],['status',1]])->first();
        }else{
            $user = Auth::user();
        }
        $list = [];
        if( $user != null ) {

            $systemId = $user->systemId;
            $query = DB::table('tbl_user as u')
                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                ->select(['u.id as id', 'u.name', 'u.username', 'u.is_login', 'pl', 'balance', 'pl_balance', 'expose', 'mc', 'pName', 'remark'])
                ->where([['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]]);

            if ($rid != false) {
                $query->where([['u.parentId', $rid], ['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]]);
            } else {
                if( $user->role == 1 || $user->roleName == 'ADMIN' || $user->role == 6){
                    $query->where([['u.status', 1], ['u.role', 4], ['u.systemId', $systemId],['pl_balance','!=',0]]);
                } else {
                    $uid = $user->id; // current user id
                    $query->where([['u.parentId', $uid], ['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]]);
                }
            }

            $userData = $query->orderBy('u.id', 'desc')->get();
            $list = [];
            if ($userData->isNotEmpty()) {
                foreach ($userData as $data) {
                    $isLock = $isBlock = 1;
                    $status1 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $data->id], ['type', 1]])->first();
                    if ($status1 != null) {
                        $isBlock = 0;
                    }

                    $status2 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $data->id], ['type', 2]])->first();
                    if ($status2 != null) {
                        $isLock = 0;
                    }

                    $list[] = [
                        'id' => $data->id,
                        'name' => $data->name,
                        'username' => $data->username,
                        'is_login' => $data->is_login,
                        'pl_balance' => round($data->pl_balance,2),
                        'balance' => round($data->balance),
                        'mc' => $data->mc,
                        'available' => round(($data->balance - $data->expose + $data->pl_balance),2),
                        'expose' => $data->expose,
                        'pName' => $data->pName,
                        'remark' => $data->remark,
                        'isLock' => $isLock,
                        'isBlock' => $isBlock,
                        'isActive' => self::userAct($data->id, $data->is_login)
                    ];
                }

            }
        }

        return $list;
    }

    // getList New Load More
    public static function getListNewLoadMore($rid = false, $request)
    {

        $data = $list = [];
        $offset = 0; $loadCount = $limit = 10; $userCount = 0;
        if( isset($request) && $request != null && isset($request->page) ){
            $offset = ($request->page - 1) * $limit;
            $loadCount = $request->page*$limit;
        }

        if( $rid != false ){
            $user = DB::table('tbl_user')->where([['id',$rid],['status',1]])->first();
        }else{
            $user = Auth::user();
        }
        if( $user != null ) {
            $systemId = $user->systemId;
            $query = DB::table('tbl_user as u')
                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                ->select(['u.id as id', 'u.name', 'u.username', 'u.is_login', 'pl', 'balance', 'pl_balance', 'expose', 'mc', 'pName', 'remark']);
            $where = [['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]];
            if ($rid != false) {
                $where = [['u.parentId', $rid], ['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]];
            } else {
                if( $user->role == 1 || $user->roleName == 'ADMIN' || $user->role == 6){
                    $where = [['u.status', 1], ['u.role', 4], ['u.systemId', $systemId],['pl_balance','!=',0]];
                } else {
                    $uid = $user->id; // current user id
                    $where = [['u.parentId', $uid], ['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]];
                }
            }
            $userCount = $query->where($where)->count();
            if( isset($request->client) && $request->client != null ){
                array_push($where, ['u.username', 'like', '%' . $request->client . '%']);

                $userData = $query->where($where)->orderBy('u.id', 'desc')->get();
                $loadCount = $userCount;
            }else{
                $userData = $query->where($where)->orderBy('u.id', 'desc')
                    ->offset($offset)->limit($limit)->get();
            }

            if ($userData->isNotEmpty()) {
                foreach ($userData as $data) {
                    $isLock = $isBlock = 1;
                    $status1 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $data->id], ['type', 1]])->first();
                    if ($status1 != null) {
                        $isBlock = 0;
                    }

                    $status2 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $data->id], ['type', 2]])->first();
                    if ($status2 != null) {
                        $isLock = 0;
                    }

                    $list[] = [
                        'id' => $data->id,
                        'name' => $data->name,
                        'username' => $data->username,
                        'is_login' => $data->is_login,
                        'pl_balance' => round($data->pl_balance,2),
                        'balance' => round($data->balance),
                        'mc' => $data->mc,
                        'available' => round(($data->balance - $data->expose + $data->pl_balance),2),
                        'expose' => $data->expose,
                        'pName' => $data->pName,
                        'remark' => $data->remark,
                        'isLock' => $isLock,
                        'isBlock' => $isBlock,
                        'isActive' => self::userAct($data->id, $data->is_login)
                    ];
                }
            }
        }
        $data = [ 'list' => $list, 'loadCount' => $loadCount, 'count' => $userCount ];
        return $data;
    }

    public static function userAct($uid,$isLogin)
    {
        //logout 3,login 2,active 1
        $act = 3;

        $conn = DB::connection('mongodb');
        if( $isLogin == 1 ){
            $act = 2; $t = time() - 60*60;

            $bet1 = $conn->table('tbl_bet_pending_1')->select('created_on')
                ->where([ ['uid',$uid] ])->orderBy('_id','desc')->first();
            if( $bet1 != null && strtotime($bet1['created_on']) >= $t ){
                $act = 1;
            }else{
                $bet2 = $conn->table('tbl_bet_pending_2')->select('created_on')
                    ->where([ ['uid',$uid] ])->orderBy('_id','desc')->first();
                if( $bet2 != null && strtotime($bet2['created_on']) >= $t ){
                    $act = 1;
                }else{
                    $bet3 = $conn->table('tbl_bet_pending_3')->select('created_on')
                        ->where([ ['uid',$uid] ])->orderBy('_id','desc')->first();
                    if( $bet3 != null && strtotime($bet3['created_on']) >= $t ){
                        $act = 1;
                    }else{
                        $bet4 = $conn->table('tbl_bet_pending_4')->select('created_on')
                            ->where([ ['uid',$uid] ])->orderBy('_id','desc')->first();
                        if( $bet4 != null && strtotime($bet4['created_on']) >= $t ){
                            $act = 1;
                        }else{
                            $bet5 = $conn->table('tbl_bet_pending_5')->select('created_on')
                                ->where([ ['uid',$uid] ])->orderBy('_id','desc')->first();
                            if( $bet5 != null && strtotime($bet5['created_on']) >= $t ){
                                $act = 1;
                            }
                        }
                    }
                }
            }
        }

        return $act;
    }

    public static function create($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $pUser = Auth::user();

        if( !in_array($pUser->roleName,['SM1','SM2','M1']) ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This user can not create client !' ] ];
            return $response; exit;
        }

        if( strlen($data['name']) < 3 || strlen($data['username']) < 3 || strlen($data['password']) < 6 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! min char limit' ] ];
            return $response;
        }
        if( strlen($data['name']) > 25 || strlen($data['username']) > 25 || strlen($data['password']) > 25
            || strlen($data['remark']) > 50 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! max char limit' ] ];
            return $response;
        }
        $regExp = "#[?!%^&*=><()'\"+-]+#";
        if( preg_match($regExp,trim($data['name']))
            || preg_match($regExp,trim($data['username']))
            || preg_match($regExp,trim($data['remark'])) ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! ?!%^&*=><()\'" are not allowed ' ] ];
            return $response;
        }

        if( !is_numeric(trim($data['balance'])) ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is must be a number!' ] ];
            return $response;
        }

        if( trim($data['balance']) < 0 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is must be more them 0!' ] ];
            return $response;
        }

        $checkUser = DB::table('tbl_user')->where([['username',trim($data['username'])]])->first();

        if( $checkUser != null ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The username has already exists!' ] ];
            return $response;
        }

        $parentId = $pUser->id; // current user id

        $user = new User();

        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->systemId = $pUser->systemId;
        $user->parentId = $parentId;
        $user->role = 4;
        $user->roleName = 'C';
        $user->password = bcrypt( $data['password'] );

        if( $user->save() ){

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['status',1],['key_name','DEFAULT_STACK_OPTION']])->first();
            if( $setting != null ){
                $betOption = trim($setting->value);
            }else{
                $betOption = '500,1000,5000,20000,50000';
            }

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['status',1],['key_name','MATCH_ODD_COMM']])->first();
            if( $setting != null ){
                $mc = trim($setting->value);
            }else{
                $mc = 1;
            }

            $userInfo = [
                'systemId' => $user->systemId,
                'uid' => $user->id,
                'pName' => $pUser->username,
                'pl' => 0,
                'balance' => $data['balance'],
                'mc' => $mc,
                'pl_balance' => 0,
                'expose' => 0,
                'bet_option' => $betOption,
                'remark' => $data['remark']
            ];

            $userLimits = [
                'systemId' => $user->systemId,
                'uid' => $user->id
            ];

            $settlementSummary = [
                'uid' => $user->id,
                'pid' => $user->parentId,
                'systemId' => $user->systemId,
                'role' => $user->roleName,
                'name' => $user->name.' [ '.$user->username.' ]'
            ];

            $pUserInfo = UserInfo::where('uid',$parentId)->first();
            if( trim($data['balance']) > 0 ){
                $pUserInfo->balance = ($pUserInfo->balance-$data['balance']);
            }else{
                $pUserInfo->balance = $pUserInfo->balance;
            }

            if( $pUserInfo->balance < 0 ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is more them parent user\'s balance ! So this user can\'t create!' ] ];
                DB::table('tbl_user')->delete($user->id);
                return $response;
            }


            if( DB::table('tbl_user_info')->insert($userInfo)
                && DB::table('tbl_user_limits')->insert($userLimits)
                && DB::table('tbl_settlement_summary')->insert($settlementSummary) ){

                if( $pUserInfo->save() ){

                    $pUser = Auth::user();
                    if( $pUser->role != 1 ){
                        CommonModel::updateUserChildData($user->id,$parentId,4);
                    }

                    if( self::updateProfitLossLevel($user) ){
                        CommonModel::updateSportBlock($user->id,$parentId);
                        if( trim($data['balance']) > 0 ){
                            CommonModel::updateChipTransaction($user->id,$parentId,$data['balance'],$data['remark'],'DEPOSIT');
                        }

                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Created successfully!'
                            ]
                        ];
                    }else{
                        DB::table('tbl_user')->delete($user->id);
                        DB::table('tbl_user_info')->where('uid',$user->id)->delete();
                        DB::table('tbl_user_limits')->where('uid',$user->id)->delete();
                        DB::table('tbl_settlement_summary')->where('uid',$user->id)->delete();
                    }

                }else{
                    DB::table('tbl_user')->delete($user->id);
                    DB::table('tbl_user_info')->where('uid',$user->id)->delete();
                    DB::table('tbl_user_limits')->where('uid',$user->id)->delete();
                    DB::table('tbl_settlement_summary')->where('uid',$user->id)->delete();
                }

            }else{
                DB::table('tbl_user')->delete($user->id);
            }

        }

        return $response;
    }

    // reset password
    /*public static function resetPassword($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $user = User::find($data['uid']);
        $user->password = bcrypt( $data['password'] );

        if( $user->save() ){
            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Reset Password successfully!'
                ]
            ];
        }

        return $response;
    }*/

    // deposit balance
    /*public static function depositBalance($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $parentId = Auth::id(); // current user id
        $amount = $data['balance'];
        $remark = $data['remark'];
        $uid = $data['uid'];
        $type = 'DEPOSIT';

        // child user
        $user = UserInfo::where('uid',$data['uid'])->first();
        $user->balance = ($user->balance+$data['balance']);

        // parent user
        $pUser = UserInfo::where('uid',$parentId)->first();
        $pUser->balance = ($pUser->balance-$data['balance']);

        if( $user->save() && $pUser->save() ){

            CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);

            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Deposit successfully!'
                ]
            ];
        }

        return $response;
    }*/

    // withdrawal balance
    /*public static function withdrawalBalance($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $parentId = Auth::id(); // current user id
        $amount = $data['balance'];
        $remark = $data['remark'];
        $uid = $data['uid'];
        $type = 'WITHDRAWAL';

        // child user
        $user = UserInfo::where('uid',$uid)->first();
        $user->balance = ($user->balance-$amount);

        // parent user
        $pUser = UserInfo::where('uid',$parentId)->first();
        $pUser->balance = ($pUser->balance+$amount);

        if( $user->save() && $pUser->save() ){

            CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);

            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Withdrawal successfully!'
                ]
            ];
        }

        return $response;
    }*/

    // Add Profit Loss Level
    public static function updateProfitLossLevel($user){

        $userProfitLossArr = [];

        $systemId = $user->systemId;
        $uid = $user->id;

        $userProfitLoss = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'parentId' => $user->parentId,
            'level' => 0,
            'profit_loss' => 0,
            'actual_profit_loss' => 0,
        ];

        array_push($userProfitLossArr, $userProfitLoss);

        $parentUser1 = DB::table('tbl_user as u')
            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
            ->select(['u.id as id','parentId','pl','role'])
            ->where([['u.status',1],['u.id',$user->parentId],['u.systemId',$systemId]])->first();

        if( $parentUser1 != null ){
            $profit_loss1 = $parentUser1->pl;
            $actual_profit_loss1 = $parentUser1->pl;
            if( $parentUser1->role == 1 && $parentUser1->pl == 0 ){
                $profit_loss1 = 0;
                $actual_profit_loss1 = 100;
            }

            $userProfitLoss1 = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $parentUser1->id,
                'parentId' => $parentUser1->parentId,
                'level' => 1,
                'profit_loss' => $profit_loss1,
                'actual_profit_loss' => $actual_profit_loss1,
            ];

            array_push($userProfitLossArr, $userProfitLoss1);

            if( $parentUser1->role != 1 ){

                $parentUser2 = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['u.id as id','parentId','pl','role'])
                    ->where([['u.status',1],['u.id',$parentUser1->parentId],['u.systemId',$systemId]])->first();

                if( $parentUser2 != null ){

                    $profit_loss2 = $parentUser2->pl;
                    $actual_profit_loss2 = $parentUser2->pl-$profit_loss1;
                    if( $parentUser2->role == 1 && $parentUser2->pl == 0 ){
                        $profit_loss2 = 0;
                        $actual_profit_loss2 = 100-$profit_loss1;
                    }

                    $userProfitLoss2 = [
                        'systemId' => $systemId,
                        'clientId' => $uid,
                        'userId' => $parentUser2->id,
                        'parentId' => $parentUser2->parentId,
                        'level' => 2,
                        'profit_loss' => $profit_loss2,
                        'actual_profit_loss' => $actual_profit_loss2,
                    ];

                    array_push($userProfitLossArr, $userProfitLoss2);

                    if( $parentUser2->role != 1 ){

                        $parentUser3 = DB::table('tbl_user as u')
                            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                            ->select(['u.id as id','parentId','pl','role'])
                            ->where([['u.status',1],['u.id',$parentUser2->parentId],['u.systemId',$systemId]])->first();

                        if( $parentUser3 != null ){

                            $profit_loss3 = $parentUser3->pl;
                            $actual_profit_loss3 = $parentUser3->pl-$profit_loss2;
                            if( $parentUser3->role == 1 && $parentUser3->pl == 0 ){
                                $profit_loss3 = 0;
                                $actual_profit_loss3 = 100-$profit_loss2;
                            }

                            $userProfitLoss3 = [
                                'systemId' => $systemId,
                                'clientId' => $uid,
                                'userId' => $parentUser3->id,
                                'parentId' => $parentUser3->parentId,
                                'level' => 3,
                                'profit_loss' => $profit_loss3,
                                'actual_profit_loss' => $actual_profit_loss3,
                            ];

                            array_push($userProfitLossArr, $userProfitLoss3);

                            if( $parentUser3->role != 1 ){

                                $parentUser4 = DB::table('tbl_user as u')
                                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                                    ->select(['u.id as id','parentId','pl','role'])
                                    ->where([['u.status',1],['u.id',$parentUser3->parentId],['u.systemId',$systemId]])->first();

                                if( $parentUser4 != null ){

                                    $profit_loss4 = $parentUser4->pl;
                                    $actual_profit_loss4 = $parentUser4->pl-$profit_loss3;
                                    if( $parentUser4->role == 1 && $parentUser4->pl == 0 ){
                                        $profit_loss4 = 0;
                                        $actual_profit_loss4 = 100-$profit_loss3;
                                    }

                                    $userProfitLoss4 = [
                                        'systemId' => $systemId,
                                        'clientId' => $uid,
                                        'userId' => $parentUser4->id,
                                        'parentId' => $parentUser4->parentId,
                                        'level' => 4,
                                        'profit_loss' => $profit_loss4,
                                        'actual_profit_loss' => $actual_profit_loss4,
                                    ];

                                    array_push($userProfitLossArr, $userProfitLoss4);

                                    if( $parentUser4->role != 1 ){

                                        $parentUser5 = DB::table('tbl_user as u')
                                            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                                            ->select(['u.id as id','parentId','pl','role'])
                                            ->where([['u.status',1],['u.id',$parentUser4->parentId],['u.systemId',$systemId]])->first();

                                        if( $parentUser5 != null ){

                                            $profit_loss5 = $parentUser5->pl;
                                            $actual_profit_loss5 = $parentUser5->pl-$profit_loss4;
                                            if( $parentUser5->role == 1 && $parentUser5->pl == 0 ){
                                                $profit_loss5 = 0;
                                                $actual_profit_loss5 = 100-$profit_loss4;
                                            }

                                            $userProfitLoss5 = [
                                                'systemId' => $systemId,
                                                'clientId' => $uid,
                                                'userId' => $parentUser5->id,
                                                'parentId' => $parentUser5->parentId,
                                                'level' => 5,
                                                'profit_loss' => $profit_loss5,
                                                'actual_profit_loss' => $actual_profit_loss5,
                                            ];

                                            array_push($userProfitLossArr, $userProfitLoss5);

                                        }

                                    }

                                }

                            }

                        }

                    }


                }

            }

        }

        if( $systemId != 1 ){

            $parentUserAdmin = DB::table('tbl_user as u')
                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                ->select(['pl','parentId'])->where([['u.role',1],['u.status',1],['u.systemId',$systemId]])->first();

            if( $parentUserAdmin != null ){
                $apl = 100-$parentUserAdmin->pl;
                $userProfitLossAdmin = [
                    'systemId' => $systemId,
                    'clientId' => $uid,
                    'userId' => $parentUserAdmin->parentId,
                    'parentId' => 0,
                    'level' => 6,
                    'profit_loss' => 0,
                    'actual_profit_loss' => $apl,
                ];

                array_push($userProfitLossArr, $userProfitLossAdmin);

            }

        }

        if( DB::table('tbl_user_profit_loss')->insert($userProfitLossArr) ){
            return true;
        }else{
            return false;
        }

    }

}



