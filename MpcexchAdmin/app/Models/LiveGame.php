<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LiveGame extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_live_game';

    public static function getList()
    {
        $query = DB::table('tbl_live_game')
            ->select(['name','eventId','slug','mType','status'])
            ->whereIn('status',[0,1])
            ->orderBy('id', 'DESC');

        $list = $query->get();

        return $list;
    }

    public static function doCreate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->role == 1){

            $mType = strtoupper(str_replace(' ','_',trim($data['mType'])));
            $slug = strtolower(str_replace(' ','-',trim($data['slug'])));

            $checkUnique = LiveGame::where([['eventId',trim($data['eventId'])],['mType',$mType]])->first();

            if( $checkUnique != null ){
                $response = ['status' => 0, 'error' => ['message' => 'This key is already used!']];
                return $response;
            }

            $liveGame = new LiveGame();

            $liveGame->name = trim($data['name']);
            $liveGame->sportId = 99;
            $liveGame->eventId = trim($data['eventId']);
            $liveGame->slug = $slug;
            $liveGame->mType = $mType;

            if ($liveGame->save()) {
                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Created successfully!'
                    ]
                ];
            }

        }

        return $response;
    }

    public static function doUpdate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->role == 1){

            $liveGame = LiveGame::where([['id',$data['id']]])->first();

            if( $liveGame != null ){
                if( isset($data['status']) ){
                    $liveGame->status = trim($data['status']);
                    if( $data['status'] == 1 ){
                        $message = 'Active successfully!';
                    }else if( $data['status'] == 0 ){
                        $message = 'InActive successfully!';
                    }else{
                        $message = 'Delete successfully!';
                    }
                }else{
                    $liveGame->name = trim($data['name']);
                    $message = 'Updated successfully!';
                }

                if ($liveGame->save()) {
                    $response = [
                        'status' => 1,
                        'success' => [
                            'message' => $message
                        ]
                    ];
                }
            }

        }

        return $response;
    }

}
