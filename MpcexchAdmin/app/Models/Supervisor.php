<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Supervisor extends Model
{
    // list
    public static function getList()
    {
        $user = Auth::user();
        $list = [];
        if( $user != null ){
            $systemId = $user->systemId;
            $query = DB::table('tbl_user')->select(['id','name','username','status'])
                ->where([['role',5], ['systemId',$systemId]])
                ->whereIn('status',[0,1]);

            $userData = $query->orderBy('id', 'desc')->get();

            if( $userData->isNotEmpty() ){
                foreach ( $userData as $data ){
                    $list[] = [
                        'id' => $data->id,
                        'name' => $data->name,
                        'username' => $data->username,
                        'isBlock' => $data->status,
                    ];
                }
            }

        }

        return $list;
    }

    // create
    public static function create($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $pUser = Auth::user();
        if( $pUser->role == 6 ){ return $response; exit; }
        $parentId = $pUser->id; // current user id

        if( strlen($data['name']) < 3 || strlen($data['username']) < 3 || strlen($data['password']) < 6 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! min char limit' ] ];
            return $response;
        }

        if( strlen($data['name']) > 25 || strlen($data['username']) > 25 || strlen($data['password']) > 25
            || strlen($data['remark']) > 50 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! max char limit' ] ];
            return $response;
        }

        $regExp = "#[?!%^&*=><()'\"+-]+#";
        if( preg_match($regExp,trim($data['name']))
            || preg_match($regExp,trim($data['username']))
            || preg_match($regExp,trim($data['remark'])) ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid ! ?!%^&*=><()\'" are not allowed ' ] ];
            return $response;
        }

        $checkUser = DB::table('tbl_user')->where([['username',trim($data['username'])]])->first();

        if( $checkUser != null ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The username has already exists!' ] ];
            return $response;
        }

        $user = new User();
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->systemId = $pUser->systemId;
        $user->parentId = $parentId;
        $user->role = 5;
        $user->roleName = 'SV';
        $user->password = bcrypt( $data['password'] );

        if( $user->save() ){
            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Created successfully!'
                ]
            ];
        }

        return $response;
    }

}



