<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeenpattiResult extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_teenpatti_result';

}
