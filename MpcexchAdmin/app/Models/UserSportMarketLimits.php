<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSportMarketLimits extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_user_sport_market_limits';

}
