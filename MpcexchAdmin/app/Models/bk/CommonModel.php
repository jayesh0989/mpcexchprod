<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use mysql_xdevapi\Exception;

class CommonModel extends Model
{

    // update Sport Block
    public static function updateSportBlock($userId, $parentId)
    {
        try {
            $blockData = DB::table('tbl_user_sport_status')
                ->select(['byuserId','sid'])->where([['uid',$parentId]])->get();

            if( $blockData->isNotEmpty() ){
                foreach ( $blockData as $block ){
                    $sportId = $block->sid;
                    if( $parentId == $block->byuserId ){
                        $userData = ['uid' => $userId,'sid' => $sportId,'byuserId' => $parentId ];
                        DB::table('tbl_user_sport_status')->insert($userData);
                    }else{
                        self::updateSportBlock($userId,$block->byuserId);
                    }
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    // update balance
    public static function updateProfitLossData($systemId, $clientId, $userId, $parentId, $level, $pl = 0)
    {

        try {

            $tbl = 'tbl_user_profit_loss';

            if ($level == 0) {

                $clientProfitLoss = [
                    'systemId' => $systemId,
                    'clientId' => $clientId,
                    'userId' => $userId,
                    'parentId' => $parentId,
                    'level' => $level,
                    'profit_loss' => 0,
                    'actual_profit_loss' => 0,
                ];

                if (DB::table($tbl)->insert($clientProfitLoss)) {
                    $level = $level + 1;

                    $pUser = DB::table('tbl_user')
                        ->select(['parentId', 'role'])
                        ->where([['id', $parentId]])->first();
                    if ($pUser != null) {
                        $userId = $parentId;
                        $parentId = $pUser->parentId;
                        self::updateProfitLossData($systemId, $clientId, $userId, $parentId, $level, $pl);
                    }

                }

            } else {

                $user = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['parentId', 'pl', 'role'])
                    ->where([['u.id', $userId]])->first();

                if ($user != null) {
                    if ($user->role == 1 && $user->pl == 0) {
                        $profitLoss = 0;
                        $actualProfitLoss = 100;
                    } else {
                        $profitLoss = $user->pl;
                        $actualProfitLoss = $user->pl - $pl;
                    }

                    $parantProfitLoss = [
                        'systemId' => $systemId,
                        'clientId' => $clientId,
                        'userId' => $userId,
                        'parentId' => $parentId,
                        'level' => $level,
                        'profit_loss' => $profitLoss,
                        'actual_profit_loss' => $actualProfitLoss,
                    ];

                    if (DB::table($tbl)->insert($parantProfitLoss)) {
                        $level = $level + 1;
                        if ($user->role != 1) {

                            $pUser = DB::table('tbl_user')
                                ->select(['parentId', 'role'])
                                ->where([['id', $parentId]])->first();

                            $userId = $parentId;
                            $parentId = $pUser->parentId;
                            $pl = $profitLoss;
                            self::updateProfitLossData($systemId, $clientId, $userId, $parentId, $level, $pl);
                        }
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }

            }

        } catch (Exception $e) {
            return false;
        }
    }

    // update balance
    public static function updateChipTransaction($uid, $pid, $amount, $remark, $type)
    {

        try {

            $userData = DB::table('tbl_user as u')
                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                ->select(['name', 'balance','pl_balance','role', 'u.systemId'])
                ->where([['u.id', $uid]])->first();

            if ($userData != null) {
                $userName = $userData->name;
                $userBalance = $userData->balance;
                $systemId = $userData->systemId;

                if ($userData->role == 4) {
                    $tbl1 = 'tbl_transaction_client';
                    $userBalance = $userData->balance+$userData->pl_balance;
                } else {
                    $tbl1 = 'tbl_transaction_parent';
                    $userBalance = $userData->balance;
                }

                $parentData = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['name', 'parentId', 'balance', 'role'])
                    ->where([['u.id', $pid]])->first();

                if ($parentData != null) {
                    $parentName = $parentData->name;
                    $parentBalance = $parentData->balance;
                    $pid2 = $parentData->parentId;

                    if ($parentData->role == 1) {
                        $tbl2 = 'tbl_transaction_admin';
                    } else {
                        $tbl2 = 'tbl_transaction_parent';
                    }

                    if ($type == 'WITHDRAWAL') {
                        $uType = 'DEBIT';
                        $pType = 'CREDIT';
                        $mType = 'Withdrawal';
                        $uDesc = 'Chip Withdrawal By ' . $parentName;
                        $pDesc = 'Chip Withdrawal From ' . $userName;
                    } else {
                        $uType = 'CREDIT';
                        $pType = 'DEBIT';
                        $mType = 'Deposit';
                        $uDesc = 'Chip Deposit By ' . $parentName;
                        $pDesc = 'Chip Deposit To ' . $userName;
                    }

                    $uTranData = [
                        'systemId' => $systemId,
                        'clientId' => $uid,
                        'userId' => $uid,
                        'childId' => 0,
                        'parentId' => $pid,
                        'eType' => 1, // 1 for chip transaction
                        'mType' => 'Chip ' . $mType,
                        'type' => $uType,
                        'amount' => $amount,
                        'balance' => $userBalance,
                        'description' => $uDesc,
                        'remark' => $remark,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s')
                    ];

                    $pTranData = [
                        'systemId' => $systemId,
                        'clientId' => $uid,
                        'userId' => $pid,
                        'childId' => $uid,
                        'parentId' => $pid2,
                        'eType' => 1, // 1 for chip transaction
                        'mType' => 'Chip ' . $mType,
                        'type' => $pType,
                        'amount' => $amount,
                        'balance' => $parentBalance,
                        'description' => $pDesc,
                        'remark' => $remark,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s')
                    ];

                    if ( DB::connection('mysql3')->table($tbl1)->insert($uTranData) && DB::connection('mysql3')->table($tbl2)->insert($pTranData)) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }

            } else {
                return false;
            }

        } catch (Exception $e) {
            return false;
        }

    }

    // update balance
    public static function updateUserChildData($uid, $pid, $role)
    {
        try {

            $tbl = 'tbl_user_child_data';
            $user = DB::table($tbl)->select(['super_masters', 'masters', 'clients'])
                ->where('uid', $pid)->first();

            $smArr = $mArr = $cArr = [];
            if ($role == 2) {
                if ($user != null) {
                    $smArr = json_decode($user->super_masters, 16);
                    if ($smArr != null && !in_array($uid, $smArr)) {
                        array_push($smArr, $uid);
                    } else {
                        $smArr[] = $uid;
                    }
                    DB::table($tbl)->where('uid', $pid)
                        ->update(['super_masters' => json_encode($smArr)]);
                } else {
                    array_push($smArr, $uid);

                    $insertArr = [
                        'uid' => $pid,
                        'super_masters' => json_encode($smArr),
                    ];

                    DB::table($tbl)->insert($insertArr);
                }

                $pUser = DB::table('tbl_user')->select(['parentId', 'role'])->where('id', $pid)->first();

                if ($pUser != null && $pUser->role != 1) {
                    self::updateUserChildData($uid, $pUser->parentId, 2);
                }

                return true;

            } elseif ($role == 3) {
                if ($user != null) {
                    $mArr = json_decode($user->masters, 16);
                    if ($mArr != null && !in_array($uid, $mArr)) {
                        array_push($mArr, $uid);
                    } else {
                        $mArr[] = $uid;
                    }
                    DB::table($tbl)->where('uid', $pid)
                        ->update(['masters' => json_encode($mArr)]);
                } else {
                    array_push($mArr, $uid);
                    $insertArr = [
                        'uid' => $pid,
                        'masters' => json_encode($mArr),
                    ];

                    DB::table($tbl)->insert($insertArr);
                }

                $pUser = DB::table('tbl_user')->select(['parentId', 'role'])->where('id', $pid)->first();

                if ($pUser != null && $pUser->role != 1) {
                    self::updateUserChildData($uid, $pUser->parentId, 3);
                }

                return true;

            } else {
                if ($user != null) {
                    $cArr = json_decode($user->clients, 16);
                    if ($cArr != null && !in_array($uid, $cArr)) {
                        array_push($cArr, $uid);
                    } else {
                        $cArr[] = $uid;
                    }
                    DB::table($tbl)->where('uid', $pid)
                        ->update(['clients' => json_encode($cArr)]);
                } else {
                    array_push($cArr, $uid);
                    $insertArr = [
                        'uid' => $pid,
                        'clients' => json_encode($cArr),
                    ];

                    DB::table($tbl)->insert($insertArr);
                }

                $pUser = DB::table('tbl_user')->select(['parentId', 'role'])->where('id', $pid)->first();

                if ($pUser != null && $pUser->role != 1) {
                    self::updateUserChildData($uid, $pUser->parentId, 4);
                }

                return true;
            }

            return false;

        } catch (Exception $e) {
            return false;
        }

    }

    // userChildData
    public static function userChildData($uid)
    {
        $superMastersArr = $mastersArr = $clientsArr = $userArr = [];

        $userChildData = DB::table('tbl_user_child_data')->select(['super_masters', 'masters', 'clients'])
            ->where([['uid', $uid]])->first();

        if ($userChildData != null) {

            if (isset($userChildData->super_masters) && $userChildData->super_masters != null) {
                $superMastersArr = json_decode($userChildData->super_masters);
            }
            if (isset($userChildData->masters) && $userChildData->masters != null) {
                $mastersArr = json_decode($userChildData->masters);
            }
            if (isset($userChildData->clients) && $userChildData->clients != null) {
                $clientsArr = json_decode($userChildData->clients);
            }

            if ($superMastersArr != null) {
                foreach ($superMastersArr as $ids) {
                    if (!in_array($ids, $userArr)) {
                        $userArr[] = $ids;
                    }
                }
            }
            if ($mastersArr != null) {
                foreach ($mastersArr as $ids) {
                    if (!in_array($ids, $userArr)) {
                        $userArr[] = $ids;
                    }
                }
            }
            if ($clientsArr != null) {
                foreach ($clientsArr as $ids) {
                    if (!in_array($ids, $userArr)) {
                        $userArr[] = $ids;
                    }
                }
            }
        }

        array_push($userArr, (int)$uid);
        return $userArr;

    }

    // get child count
    public static function getChildCount($uid, $role)
    {
        $count = DB::table('tbl_user')
            ->where([['status', 1],['parentId', $uid], ['role', $role]])->count();
        return $count;
    }

    // Game Over Common Function

    // Function to get the Description
    public function setDescription($marketId, $tbl)
    {
        $description = '';
        $betData = DB::table($tbl)->select(['sport', 'event', 'market', 'runner', 'mType'])
            ->where([['status', 1], ['mid', $marketId]])->first();

        if ($betData != null) {
            $description = $betData->sport . ' > ' . $betData->event . ' > ' . $betData->mType . ' > ' . $betData->market . ' > ' . $betData->runner;
        }

        return $description;
    }

    // get Current Balance
    public function getCurrentBalance($uid, $amount, $type, $balance, $plBalance, $role)
    {
        if ($type == 'CREDIT') {
            $newplBalance = $plBalance + $amount;
        } else {
            $newplBalance = $plBalance - $amount;
            $amount = -$amount;
        }

//        if( DB::table('tbl_user_info')->where([['uid',$uid]])->update(['pl_balance',$newplBalance]) ){
        if (DB::table('tbl_user_info')->where([['uid', $uid]])->update(['pl_balance' => DB::raw('pl_balance + ' . $amount)])) {

            if ($role != 'CLIENT') {
                return round(($newplBalance), 2);
            } else {
                return round(($balance + $newplBalance), 2);
            }

        } else {

            if ($role != 'CLIENT') {
                return round(($plBalance), 2);
            } else {
                return round(($balance + $plBalance), 2);
            }
        }

    }

    //Game Over Result
    public function gameOverResult($eventId, $marketId, $secId, $mType, $tbl)
    {

        if ($tbl == 'tbl_bet_pending_2') {
            $winResult = $secId;
            // Win Update for NO
            DB::table('tbl_bet_pending_2')
                ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['price', '>', ($winResult + 0)],
                    ['result', 'PENDING'], ['bType', 'NO'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'WIN']);

            // Win Update for YES
            DB::table('tbl_bet_pending_2')
                ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['price', '<=', ($winResult + 0)],
                    ['result', 'PENDING'], ['bType', 'YES'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'WIN']);

            // Loss Update
            DB::table('tbl_bet_pending_2')
                ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType],
                    ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'LOSS']);

        } elseif ($tbl == 'tbl_bet_pending_3') {
            $winResult = $secId;
            if ($mType == 'khado') {
                // Win Update
                DB::table('tbl_bet_pending_3')
                    ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['difference', '>', ($winResult + 0)],
                        ['price', '<=', ($winResult + 0)], ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                    ->update(['result' => 'WIN']);

                // Loss Update
                DB::table('tbl_bet_pending_3')
                    ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType],
                        ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                    ->update(['result' => 'LOSS']);

            } else {
                // Win Update for NO
                DB::table('tbl_bet_pending_3')
                    ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['price', '>', ($winResult + 0)],
                        ['result', 'PENDING'], ['bType', 'NO'], ['status', 1], ['is_match', 1]])
                    ->update(['result' => 'WIN']);

                // Win Update for YES
                DB::table('tbl_bet_pending_3')
                    ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['price', '<=', ($winResult + 0)],
                        ['result', 'PENDING'], ['bType', 'YES'], ['status', 1], ['is_match', 1]])
                    ->update(['result' => 'WIN']);

                // Loss Update
                DB::table('tbl_bet_pending_3')
                    ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType],
                        ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                    ->update(['result' => 'LOSS']);

            }

        } elseif ($tbl == 'tbl_bet_pending_4') {

            // Win Update
            DB::table($tbl)
                ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['secId', $secId], ['bType', 'BACK'],
                    ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'WIN']);

            // Loss Update
            DB::table($tbl)
                ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['secId', '!=', $secId], ['bType', 'BACK'],
                    ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'LOSS']);

        } else {

            // Win Update for Back
            DB::table($tbl)
                ->where([['mid', $marketId], ['eid', $eventId], ['secId', $secId], ['mType', $mType],
                    ['result', 'PENDING'], ['bType', 'BACK'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'WIN']);

            // Win Update for Lay
            DB::table($tbl)
                ->where([['mid', $marketId], ['eid', $eventId], ['secId', '!=', $secId], ['mType', $mType],
                    ['result', 'PENDING'], ['bType', 'LAY'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'WIN']);

            // Loss Update
            DB::table($tbl)
                ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType],
                    ['result', 'PENDING'], ['status', 1], ['is_match', 1]])
                ->update(['result' => 'LOSS']);

            // Unmatched Bet canceled
            if ($mType == 'match_odd') {
                DB::table($tbl)
                    ->where([['mid', $marketId], ['eid', $eventId], ['mType', $mType],
                        ['result', 'PENDING'], ['status', 1], ['is_match', 0]])
                    ->update(['result' => 'CANCELED']);
            }

        }

    }

    //Game Recall
    public function gameRecall($marketId, $mType, $tbl)
    {

        $transArr = [];
        $transClient = DB::table('tbl_transaction_client')->select(['userId', 'type', 'amount', 'p_amount', 'eType'])
            ->where([['status', 1], ['mid', $marketId], ['mType', $mType]])->get();

        if ($transClient != null) {
            $transArr = array_merge($transArr, $transClient);
        }

        $transParent = DB::table('tbl_transaction_parent')->select(['userId', 'type', 'amount', 'p_amount', 'eType'])
            ->where([['status', 1], ['mid', $marketId], ['mType', $mType]])->get();

        if ($transParent != null) {
            $transArr = array_merge($transArr, $transParent);
        }

        $transAdmin = DB::table('tbl_transaction_admin')->select(['userId', 'type', 'amount', 'p_amount', 'eType'])
            ->where([['status', 1], ['mid', $marketId], ['mType', $mType]])->get();

        if ($transAdmin != null) {
            $transArr = array_merge($transArr, $transAdmin);
        }

        if ($transArr != null) {

            foreach ($transArr as $trans) {

                $user = DB::table('tbl_user_info')->select(['pl_balance'])
                    ->where([['status', 1], ['uid', $trans->userId]])->first();

                if ($user != null) {
                    if ($trans->type == 'CREDIT') {
                        $plBalance = ($user->pl_balance - $trans->amount);
                    } else {
                        $plBalance = ($user->pl_balance + $trans->amount);
                    }

                    DB::table('tbl_user_info')->where([['uid', $trans->userId]])
                        ->update(['pl_balance' => $plBalance]);

                    $summaryData = [];

                    $summaryData['uid'] = $trans->userId;
                    $summaryData['type'] = $trans->type;

                    if ($trans->eType == 3) {
                        $summaryData['ownComm'] = $trans->amount;
                        $summaryData['parentComm'] = $trans->p_amount;
                    } else {
                        $summaryData['ownPl'] = $trans->amount;
                        $summaryData['parentPl'] = $trans->p_amount;
                    }

                    $this->userSummaryUpdate($summaryData);

                }
            }

            // update transaction status
            DB::table('tbl_transaction_client')->where([['status', 1], ['mid', $marketId]])
                ->update(['status' => 2]);
            DB::table('tbl_transaction_parent')->where([['status', 1], ['mid', $marketId]])
                ->update(['status' => 2]);
            DB::table('tbl_transaction_admin')->where([['status', 1], ['mid', $marketId]])
                ->update(['status' => 2]);

            // update result
            DB::table($tbl)->where([['status', 1], ['mid', $marketId]])
                ->update(['result' => 'PENDING']);

            return true;

        }

        return false;
    }

    //transaction Result MatchOdds
    public function transactionResult($sportId, $eventId, $marketId, $mType, $tbl)
    {

        $userList = DB::table($tbl)->select(['uid'])
            ->where([['mid', $marketId], ['eid', $eventId], ['status', 1], ['mType', $mType], ['is_match', 1]])
            ->whereIn('result', ['WIN', 'LOSS'])
            ->groupBy('uid')->get();

        if ($userList != null) {

            foreach ($userList as $user) {

                $uid = $user->uid;
                $profit = $loss = 0;

                $winAmount = DB::table($tbl)->select(['uid'])
                    ->where([['uid', $uid], ['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['result', 'WIN'], ['status', 1], ['is_match', 1]])
                    ->sum('win');

                $lossAmount = DB::table($tbl)->select(['uid'])
                    ->where([['uid', $uid], ['mid', $marketId], ['eid', $eventId], ['mType', $mType], ['result', 'LOSS'], ['status', 1], ['is_match', 1]])
                    ->sum('loss');

                if ($winAmount != null) {
                    $profit = round($winAmount, 2);
                }
                if ($lossAmount != null) {
                    $loss = round($lossAmount, 2);
                }

                $amount = $profit - $loss;

                if ($amount != 0) {
                    $this->updateTransactionHistory($sportId, $uid, $eventId, $marketId, $amount, $mType, $tbl);
                }
            }

        }

    }

    // Update Transaction History
    public function updateTransactionHistory($sportId, $uid, $eventId, $marketId, $amount, $mType, $tbl)
    {
        if ($amount > 0) {
            $type = 'CREDIT';
        } else {
            $amount = (-1) * $amount;
            $type = 'DEBIT';
        }

        $summaryData = [];

        $cUser = DB::table('tbl_user')->select(['parentId', 'systemId'])->where([['id', $uid]])->first();
        $cUserInfo = DB::table('tbl_user_info')->select(['balance', 'pl_balance'])->where([['uid', $uid]])->first();

        $parentId = $cUser->parentId;
        $systemId = $cUser->systemId;
        $description = $this->setDescription($marketId, $tbl);
        $balance = $this->getCurrentBalance($uid, $amount, $type, $cUserInfo->balance, $cUserInfo->pl_balance, 'CLIENT');

        $resultArr = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => $sportId,
            'eid' => $eventId,
            'mid' => $marketId,
            'eType' => 0,
            'mType' => $mType,
            'type' => $type,
            'amount' => $amount,
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Transactional Entry',
            'status' => 1,
        ];

        DB::table('tbl_transaction_client')->insert($resultArr);

        $summaryData['uid'] = $uid;
        $summaryData['ownPl'] = $amount;
        $summaryData['type'] = $type;

        if ( $type == 'CREDIT' && $mType == 'match_odd') {
            $commission = ($amount * 1) / 100;
            $balance = $this->getCurrentBalance($uid, $commission, 'DEBIT', $cUserInfo->balance, $cUserInfo->pl_balance, 'CLIENT');
            $resultArr = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $uid,
                'childId' => 0,
                'parentId' => $parentId,
                'sid' => $sportId,
                'eid' => $eventId,
                'mid' => $marketId,
                'eType' => 3, // 3 for commission
                'mType' => $mType,
                'type' => 'DEBIT',
                'amount' => $commission,
                'p_amount' => 0,
                'c_amount' => 0,
                'balance' => $balance,
                'description' => $description,
                'remark' => 'Transactional Entry',
                'status' => 1,
            ];

            DB::table('tbl_transaction_client')->insert($resultArr);

            $summaryData['ownComm'] = $commission;
        }

        $this->userSummaryUpdate($summaryData);

        if ($type == 'CREDIT') {
            $pType = 'DEBIT';
        } else {
            $pType = 'CREDIT';
        }

        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId', 'pl.actual_profit_loss as apl', 'pl.profit_loss as gpl', 'ui.balance as balance', 'ui.pl_balance as pl_balance'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId', $uid], ['pl.actual_profit_loss', '!=', 0]])->orderBy('pl.userId', 'desc')->get();

        $childId = $uid;

        $summaryData = [];

        foreach ($parentUserData as $user) {

            $cUser = DB::table('tbl_user')->select(['parentId', 'role'])->where([['id', $user->userId]])->first();
            $parentId = $cUser->parentId;

            $transactionAmount = ($amount * $user->apl) / 100;
            $pTransactionAmount = ($amount * (100 - $user->gpl)) / 100;
            $cTransactionAmount = ($amount * ($user->gpl - $user->apl)) / 100;
            $balance = $this->getCurrentBalance($user->userId, $transactionAmount, $pType, $user->balance, $user->pl_balance, 'PARENT');

            $resultArr = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $user->userId,
                'childId' => $childId,
                'parentId' => $parentId,
                'sid' => $sportId,
                'eid' => $eventId,
                'mid' => $marketId,
                'eType' => 0,
                'mType' => $mType,
                'type' => $pType,
                'amount' => $transactionAmount,
                'p_amount' => $pTransactionAmount,
                'c_amount' => $cTransactionAmount,
                'balance' => $balance,
                'description' => $description,
                'remark' => 'Transactional Entry',
                'status' => 1,
            ];

            if ($cUser->role == 1) {
                DB::table('tbl_transaction_admin')->insert($resultArr);
            } else {
                DB::table('tbl_transaction_parent')->insert($resultArr);
            }

            $summaryData['uid'] = $user->userId;
            $summaryData['type'] = $pType;
            $summaryData['ownPl'] = $transactionAmount;
            $summaryData['parentPl'] = $pTransactionAmount;

            if ($pType == 'DEBIT' && $mType == 'match_odd') {

                $transactionAmount = ($commission * $user->apl) / 100;
                $pTransactionAmount = ($commission * (100 - $user->gpl)) / 100;
                $cTransactionAmount = ($commission * ($user->gpl - $user->apl)) / 100;
                $balance = $this->getCurrentBalance($user->userId, $transactionAmount, 'CREDIT', $user->balance, $user->pl_balance, 'PARENT');

                $resultArr = [
                    'systemId' => $systemId,
                    'clientId' => $uid,
                    'userId' => $user->userId,
                    'childId' => $childId,
                    'parentId' => $parentId,
                    'sid' => $sportId,
                    'eid' => $eventId,
                    'mid' => $marketId,
                    'eType' => 3, // 3 for commission
                    'mType' => $mType,
                    'type' => 'CREDIT',
                    'amount' => $transactionAmount,
                    'p_amount' => $pTransactionAmount,
                    'c_amount' => $cTransactionAmount,
                    'balance' => $balance,
                    'description' => $description,
                    'remark' => 'Transactional Entry',
                    'status' => 1,
                ];

                if ($cUser->role == 1) {
                    DB::table('tbl_transaction_admin')->insert($resultArr);
                } else {
                    DB::table('tbl_transaction_parent')->insert($resultArr);
                }

                $summaryData['ownComm'] = $transactionAmount;
                $summaryData['parentComm'] = $pTransactionAmount;

            }

            $this->userSummaryUpdate($summaryData);

            $childId = $user->userId;

        }

    }

    //user Expose Update On Game Over
    public function userExposeUpdate($marketId, $tbl)
    {

        // Update User Market Expose
        if (DB::table('tbl_user_market_expose')
            ->where([['mid', $marketId]])->update(['status' => 2])) {

            $userList = DB::table($tbl)
                ->where([['status', 1], ['mid', $marketId]])->distinct()->count('uid');

            if (!empty($userList)) {
                foreach ($userList as $user) {
                    $userId = $user->uid;
                    $expose = 0;

                    $userExpose = DB::table('tbl_user_market_expose')
                        ->where([['uid' => $userId, 'status' => 1]])->sum('expose');

                    if ($userExpose != null) {
                        $expose = round($userExpose, 2);
                    }

                    DB::table('tbl_user_info')->where([['uid', $userId]])->update(['expose', $expose]);

                }
            }

        }

    }

    //user Expose Update On Game Recall
    public function userExposeUpdateRecall($marketId, $tbl)
    {

        // Update User Market Expose
        if (DB::table('tbl_user_market_expose')
            ->where([['mid', $marketId]])->update(['status' => 1])) {

            $userList = DB::table($tbl)
                ->where([['status', 1], ['mid', $marketId]])->distinct()->count('uid');

            if (!empty($userList)) {
                foreach ($userList as $user) {
                    $userId = $user->uid;
                    $expose = 0;

                    $userExpose = DB::table('tbl_user_market_expose')
                        ->where([['uid' => $userId, 'status' => 1]])->sum('expose');

                    if ($userExpose != null) {
                        $expose = round($userExpose, 2);
                    }

                    DB::table('tbl_user_info')->where([['uid', $userId]])->update(['expose', $expose]);

                }
            }

        }

    }

    //user Summary Update
    public function userSummaryUpdate($summaryData)
    {

        if (isset($summaryData['uid'])) {

            $uid = $summaryData['uid'];
            $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
            $parentPl = isset($summaryData['ownPl']) ? $summaryData['parentPl'] : 0;

            $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
            $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

            $userSummary = DB::table('tbl_settlement_summary')
                ->where([['status', 1], ['uid', $uid]])->first();

            if ($userSummary != null) {

                if (isset($summaryData['type']) && $summaryData['type'] == 'CREDIT') {
                    $ownPl = $ownPl + $userSummary->ownPl;
                    $parentPl = $parentPl + $userSummary->parentPl;
                } else {
                    $ownPl = $userSummary->ownPl - $ownPl;
                    $parentPl = $userSummary->parentPl - $parentPl;
                }

                $ownComm = $userSummary->ownComm + $ownComm;
                $parentComm = $userSummary->parentComm + $parentComm;

                $updateArr = ['ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm];

                DB::table('tbl_settlement_summary')->where([['status', 1], ['uid', $uid]])
                    ->update($updateArr);

            } else {

                $insertArr = ['uid' => $uid, 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm];

                DB::table('tbl_settlement_summary')->insert($insertArr);

            }

        }
    }

    //user Summary Update Recall
    public function userSummaryUpdateRecall($summaryData)
    {

        if (isset($summaryData['uid'])) {

            $uid = $summaryData['uid'];
            $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
            $parentPl = isset($summaryData['ownPl']) ? $summaryData['parentPl'] : 0;

            $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
            $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

            $userSummary = DB::table('tbl_settlement_summary')
                ->where([['status', 1], ['uid', $uid]])->first();

            if ($userSummary != null) {

                if (isset($summaryData['type']) && $summaryData['type'] == 'CREDIT') {
                    $ownPl = $userSummary->ownPl - $ownPl;
                    $parentPl = $userSummary->parentPl - $parentPl;
                } else {
                    $ownPl = $userSummary->ownPl + $ownPl;
                    $parentPl = $userSummary->parentPl + $parentPl;
                }

                $ownComm = $userSummary->ownComm - $ownComm;
                $parentComm = $userSummary->parentComm - $parentComm;

                $updateArr = ['ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm];

                DB::table('tbl_settlement_summary')->where([['status', 1], ['uid', $uid]])
                    ->update($updateArr);

            } else {

                $insertArr = ['uid' => $uid, 'ownPl' => 0, 'ownComm' => 0,
                    'parentPl' => 0, 'parentComm' => 0];

                DB::table('tbl_settlement_summary')->insert($insertArr);

            }

        }
    }

    // redisEventList
    public static function redisEventList($eventId, $key)
    {
        $redis = Redis::connection();

        $redisDataJson = $redis->get('Event_' . $eventId);

        $redisData = json_decode($redisDataJson, 16);
        if ($redisData != null) {
            $betAllowed = $redisData[$key] == 1 ? 0 : 1;
            $redisData[$key] = $betAllowed;
            $redis->set('Event_' . $eventId, json_encode($redisData));
        }

        $sportData = $eventDataArray = null;
        $conn = DB::connection('mysql2');
        $sportList = $conn->table('tbl_sport')->where('status', 1)->get();
        if ($sportList->isNotEmpty()) {
            foreach ($sportList as $sport) {
                $sportId = $sport->sportId;
                $sport_name = $sport->name;
                $eventData = null;
                $eventData = $conn->table('tbl_event')->select('sportId', 'compId', 'eventId', 'name', 'league', 'type', 'runners', 'ball_running', 'game_over', 'is_block', 'bet_allowed', 'unmatch_bet_allowed', 'status', 'suspended', 'time')
                    ->where([['status', 1], ['game_over', 0], ['sportId', $sportId], ['time', '>', strtotime(date('Y-m-d') . ' 23:59:59 -7 day')]])
                    ->whereIn('type', ['IN_PLAY', 'UPCOMING', 'CLOSED'])
                    ->orderBy('time', 'DESC')->get();

                $eventDataArray = ['sportId' => $sportId, 'sport_name' => $sport_name, 'eventData' => $eventData];
                $eventDataArray = json_encode($eventDataArray);
                $redis->set("Event_List_" . $sportId, $eventDataArray);
            }
        }
    }

    // redisEventList
    public static function redisEventListold()
    {
        $redis = Redis::connection();
        $sportData = $eventDataArray = null;
        $sportList = DB::connection('mysql2')->table('tbl_sport')->where('status', 1)->get();
        $sportList = $sportList->toArray();
        if (!empty($sportList)) {
            $sportData = json_encode($sportList);
            $redis->set('SportList', $sportData);
            foreach ($sportList as $sport_List) {
                $sportId = $sport_List->sportId;
                $sport_name = $sport_List->name;
                $sportDT = json_encode($sport_List);
                $redis->set('Sport_' . $sportId, $sportDT);
                $where = [['game_over', 0], ['sportId', $sportId]];
                $eventData = null;

                $eventData = DB::connection('mysql2')->table('tbl_event')->select('sportId', 'compId', 'eventId', 'name', 'league', 'type', 'runners', 'ball_running', 'game_over', 'is_block', 'bet_allowed', 'unmatch_bet_allowed', 'status', 'suspended', 'time')
                    ->where($where)
                    ->whereIn('type', ['IN_PLAY', 'UPCOMING', 'CLOSED'])//, 'CLOSED'
                    ->where('time', '>', strtotime(date('Y-m-d') . ' 23:59:59 -7 day'))
                    ->whereIn('status', [1])
                    ->orderBy('time', 'DESC')
                    ->get();

                if (!empty($eventData)) {
                    $eventData = $eventData->toArray();
                    foreach ($eventData as $event_Data) {
                        $eid = $event_Data->eventId;
                        $event_Data = json_encode($event_Data);
                        $redis->set("Event_" . $eid, $event_Data);
                    }
                }

                $eventDataArray = array('sportId' => $sportId, 'sport_name' => $sport_name, 'eventData' => $eventData);
                $eventDataArray = json_encode($eventDataArray);
                $redis->set("Event_List_" . $sportId, $eventDataArray);

            }
        }
    }

    // redisEventList
    public static function redisMarketUpdate($eventId, $marketId, $type)
    {
        $redis = Redis::connection();
        $redisDataJson = $redis->get('Market_' . $marketId);
        $redisData = json_decode($redisDataJson, 16);
        if ($redisData != null) {
            $betAllowed = $redisData['bet_allowed'] == 1 ? 0 : 1;
            $redisData['bet_allowed'] = $betAllowed;
            $redis->set('Market_' . $marketId, json_encode($redisData));
        }
        // odds update bet allowed
        $redisDataOddsJson = $redis->get($marketId);
        $redisDataOdds = json_decode($redisDataOddsJson, 16);
        if ($redisDataOdds != null && isset($redisDataOdds['bet_allowed'])) {
            $betAllowed = $redisDataOdds['bet_allowed'] == 1 ? 0 : 1;
            $redisDataOdds['bet_allowed'] = $betAllowed;
            $redis->set($marketId, json_encode($redisDataOdds));
        }

        $tbl = 'tbl_' . $type;
        if ($tbl == 'tbl_khado_session') {
            $tbl = 'tbl_khado_sesstion';
        }
        $conn = DB::connection('mysql2');

        if ( $type == 'fancy' || $type == 'fancy_2' || $type == 'fancy_3' || $type == 'odd_even' ) {
            if( $type == 'fancy_2' ){
                $sqlQuery = "SELECT *, (pb.title+0 > 0) tNum FROM tbl_fancy_2 as pb
                             LEFT JOIN tbl_market_limits as upl ON upl.mid = pb.marketId 
                            where pb.eid='" . $eventId . "' and pb.game_over=0 and pb.status=1 ORDER BY tNum DESC, (title+0), title";
                $fancyMarket =  $conn->select($sqlQuery);
            }else{
                $fancyMarket = $conn->table($tbl . ' as f')
                ->where([['f.eid', $eventId], ['f.game_over', 0]])->whereIn('f.status', [1, 2])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'f.marketId')->get();
            }

            if ( $fancyMarket != null && !empty($fancyMarket) ) {
                $fancyMarket = json_encode($fancyMarket);
                if( $type == 'odd_even' ){ $typeKey = 'Odd_Even'; }else{ $typeKey = ucfirst($type); }
                $redis->set( $typeKey. '_' . $eventId, $fancyMarket);
            }
        }
        if ($type == 'binary') {
            $binaryMarket = $conn->table($tbl . ' as f')
                ->where([['f.eid', $eventId], ['f.game_over', 0]])->whereIn('f.status', [1, 2])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'f.marketId')->get();
            if ($binaryMarket->isNotEmpty()) {
                $binaryMarket = json_encode($binaryMarket);
                $typeKey = ucfirst($type);
                $redis->set( $typeKey. '_' . $eventId, $binaryMarket);
            }
        }


        if ($type == 'khado_session') {
            $khadoMarket = $conn->table($tbl . ' as k')
                ->where([['k.eid', $eventId], ['k.game_over', 0]])->whereIn('k.status', [1, 2])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'k.marketId')->get();
            if ($khadoMarket->isNotEmpty()) {
                $khadoMarket = json_encode($khadoMarket);
                $redis->set('Khado_' . $eventId, $khadoMarket);
            }
        }

        if ($type == 'ball_session') {
            $ballMarket = $conn->table($tbl . ' as b')
                ->where([['b.eid', $eventId], ['b.game_over', 0]])->whereIn('b.status', [1])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'b.marketId')->limit(6)->get();
            if ($ballMarket->isNotEmpty()) {
                $ballMarket = json_encode($ballMarket);
                $redis->set('Ballbyball_' . $eventId, $ballMarket);
            }
        }

        if ($type == 'casino') {
            $casinoMarket = $conn->table($tbl . ' as b')
                ->where([['b.eid', $eventId], ['b.game_over', 0]])->whereIn('b.status', [1, 2])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'b.marketId')->get();
            if ($casinoMarket->isNotEmpty()) {
                $casinoMarket = json_encode($casinoMarket);
                $redis->set('Casino_' . $eventId, $casinoMarket);
            }
        }

        if ($type == 'bookmaker') {
            $bookMakerMarket = $conn->table($tbl . ' as bk')
                ->where([['bk.eid', $eventId], ['bk.game_over', 0]])->whereIn('bk.status', [1, 2])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'bk.marketId')->get();
            if ($bookMakerMarket->isNotEmpty()) {
                $bookMakerMarket = json_encode($bookMakerMarket);
                $redis->set('Bookmaker_' . $eventId, $bookMakerMarket);
            }
        }

        if ($type == 'virtual_session') {
            $virtualCricketMarket = $conn->table($tbl . ' as bk')
                ->where([['bk.eid', $eventId], ['bk.game_over', 0]])->whereIn('bk.status', [1, 2])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'bk.marketId')->get();
            if ($virtualCricketMarket->isNotEmpty()) {
                $virtualCricketMarket = json_encode($virtualCricketMarket);
                $redis->set('Virtual_' . $eventId, $virtualCricketMarket);
            }
        }

        if ($type == 'other_markets') {
            $otherMarket = $conn->table("tbl_other_markets as m")
                ->where([['m.eid', $eventId], ['m.game_over', 0]])->whereIn('m.status', [1, 2])
                ->join('tbl_market_limits', 'tbl_market_limits.mid', '=', 'm.marketId')->get();
            if ($otherMarket->isNotEmpty()) {
                foreach ($otherMarket as $marketArr) {
                    if ($marketArr->slug == 'match_odd') {
                        $redis->set("Match_Odd_" . $eventId, json_encode($marketArr));
                    } else if ($marketArr->slug == 'completed_match') {
                        $redis->set("Completed_Match_" . $eventId, json_encode($marketArr));
                    } else if ($marketArr->slug == 'tied_match') {
                        $redis->set("Tied_Match_" . $eventId, json_encode($marketArr));
                    } else if ($marketArr->slug == 'winner') {
                        $redis->set("Winner_" . $eventId, json_encode($marketArr));
                    }
                }
            }
        }

        if ($type == 'jackpot') {
            $market = $conn->table('tbl_jackpot')->select(['type'])
                ->where([['marketId', $marketId]])->first();
            if ($market != null) {
                $gameId = $market->type;
                $redisDataJson = $redis->get('Jackpot_market_' . $gameId . '_' . $eventId . '_' . $marketId);
                $redisData = json_decode($redisDataJson, 16);
                if ($redisData != null) {
                    $betAllowed = $redisData['bet_allowed'] == 1 ? 0 : 1;
                    $redisData['bet_allowed'] = $betAllowed;
                    $redis->set('Jackpot_market_' . $gameId . '_' . $eventId . '_' . $marketId, json_encode($redisData));
                }

                $where = [['eid', $eventId], ['type', $gameId], ['game_over', 0], ['status', 1]];
                $marketData = $conn->table('tbl_jackpot')->where($where)
                    ->orderBy('id', 'DESC')->get();

                if ($marketData->isNotEmpty()) {
                    $marketDataJson = json_encode($marketData);
                    $redis->set("Jackpot_data_" . $gameId . "_" . $eventId, $marketDataJson);
                }
            }
        }

    }
}



