<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Test Call
Route::get('history/test/check-user-settlement', 'History\TestController@checkUserSettlementNew');
Route::get('casino/parent-transaction', 'Casino\ParentTransactionController@actionTransaction');

// Game Over Cron Job
Route::get('game-over/action', 'GameOver\ActionController@index');
Route::get('main-game-over/action', 'GameOver\ActionController@mainGameover');
Route::post('game-over/result', 'GameOver\ResultController@actionInsert');
Route::get('game-over/recall', 'GameOver\RecallController@index');

//clear-user-data
Route::get('clear-user-data', 'AppUser\ActionController@clearUserDataNew');

// Casino API Route
Route::post('wallet/balance', 'Casino\WalletController@actionBalance');
Route::post('wallet/bet', 'Casino\WalletController@actionBet');
Route::post('wallet/win', 'Casino\WalletController@actionWin');
Route::post('wallet/rollback', 'Casino\WalletController@actionRollback');

//Login Or Auth
Route::get('site-mode', 'AuthController@siteMode');
Route::post('login', 'AuthController@login');
Route::post('login-new', 'AuthNewController@login');
Route::post('change-password', 'AuthController@changePassword');

// Action Bet Delete By BetId
Route::get('bet-delete-by-bet-id', 'Dashboard\ActionController@actionBetDeleteByBetId');

//All Api After Auth
Route::middleware('auth:api')->group( function () {

    Route::post('casino/game-url', 'Casino\GameController@actionGameUrl');
    //Logout
    Route::get('logout', 'AuthController@logout');

    //Menu List
    Route::get('menu-list', 'Dashboard\MainMenuController@list');

    //Dashboard
    Route::post('dashboard/search-user-data', 'Dashboard\ActionController@searchUserData');
    Route::get('dashboard/get-user-data', 'Dashboard\ActionController@getUserData');
    Route::get('dashboard/get-user-balance', 'Dashboard\ActionController@getUserBalance');
    Route::get('dashboard/data-list/{type}', 'Dashboard\ActionController@getDataList');

    Route::get('dashboard/data-list-all/{uid}', 'Dashboard\ActionController@getDataListByUser');

    Route::post('dashboard/data-list-by-system/{type}', 'Dashboard\ActionController@getDataListBySystem');

    Route::get('dashboard/event/bet-allowed/{id}', 'Dashboard\ActionController@actionBetAllowed');
    Route::post('dashboard/market/bet-allowed', 'Dashboard\ActionController@actionBetAllowedMarket');

    Route::post('dashboard/event/bet-delete', 'Dashboard\ActionController@actionBetDelete');
    Route::get('dashboard/all-user-logout', 'Dashboard\ActionController@actionAllLogout');

    Route::get('dashboard/list', 'Dashboard\ListController@sport');
    Route::get('dashboard/list/{id}', 'Dashboard\ListController@event');
    Route::get('dashboard/block-unblock/event/{id}', 'Dashboard\BlockUnblockController@event');
    Route::get('dashboard/block-unblock/sport/{id}', 'Dashboard\BlockUnblockController@sport');
    Route::get('dashboard/block-unblock-sport', 'Dashboard\BlockUnblockController@sportUser');

    //new detail api
    Route::get('dashboard/event-detail-new/{id}', 'Dashboard\DetailController@actionEventDetail');

    Route::post('dashboard/jackpot-details', 'Dashboard\DetailController@actionJackpotDetails');

    Route::get('dashboard/event-bet-list/{id}', 'Dashboard\DetailController@actionBetList');
    Route::post('dashboard/market-bet-list', 'Dashboard\DetailController@actionMarketBetList');

    Route::post('dashboard/match-odd/user-book-data', 'Dashboard\BookController@actionUserBookDataMatchOdd');
    Route::post('dashboard/book-maker/user-book-data', 'Dashboard\BookController@actionUserBookDataBookMaker');

    // new book api
    Route::post('dashboard/fancy/user-book-data', 'Dashboard\BookController@actionBookDataFancy');
    Route::post('dashboard/fancy3/user-book-data', 'Dashboard\BookController@actionBookDataFancy3');
    Route::post('dashboard/ball-session/user-book-data', 'Dashboard\BookController@actionBookDataBallSession');
    Route::post('dashboard/khado-session/user-book-data', 'Dashboard\BookController@actionBookDataKhadoSession');
    Route::post('dashboard/casino/user-book-data', 'Dashboard\BookController@actionBookDataCasino');
    Route::post('dashboard/binary/user-book-data', 'Dashboard\BookController@actionBookDataBinary');

    // System Dashboard
    // new event detail api
    Route::get('dashboard/system-event-detail-new/{id}', 'Dashboard\SystemDetailController@actionEventDetail');

    Route::post('dashboard/system-jackpot-details', 'Dashboard\SystemDetailController@actionJackpotDetails');

    Route::get('dashboard/system-event-bet-list/{id}', 'Dashboard\SystemDetailController@actionBetList');

    Route::post('dashboard/system-market-bet-list', 'Dashboard\SystemDetailController@actionMarketBetList');

    Route::post('dashboard/match-odd/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataMatchOdd');
    Route::post('dashboard/book-maker/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataBookMaker');

    // new book api
    Route::post('dashboard/fancy/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataFancy');
    Route::post('dashboard/fancy3/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataFancy3');
    Route::post('dashboard/ball-session/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataBallSession');
    Route::post('dashboard/khado-session/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataKhadoSession');
    Route::post('dashboard/casino/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataCasino');
    Route::post('dashboard/binary/system-user-book-data', 'Dashboard\BookController@actionSystemBookDataBinary');

    //Manage System
    Route::get('app-user/manage-system/manage', 'AppUser\ManageSystemController@manage');
    Route::post('app-user/manage-system/create', 'AppUser\ManageSystemController@create');

    //App User Super Admin
    Route::get('app-user/super-admin/manage', 'AppUser\SuperAdminController@manage');
    Route::post('app-user/super-admin/create', 'AppUser\SuperAdminController@create');

    //App User Super Master
    Route::get('app-user/super-master/manage', 'AppUser\SuperMasterController@manage');
    Route::get('app-user/super-master/reference/{id}', 'AppUser\SuperMasterController@reference');
    Route::post('app-user/super-master/create', 'AppUser\SuperMasterController@create');

    //App User Supervisor
    Route::get('app-user/supervisor/manage', 'AppUser\SupervisorController@manage');
    Route::post('app-user/supervisor/create', 'AppUser\SupervisorController@create');

    //App User Master
    Route::get('app-user/master/manage', 'AppUser\MasterController@manage');
    Route::get('app-user/master/reference/{id}', 'AppUser\MasterController@reference');
    Route::post('app-user/master/create', 'AppUser\MasterController@create');

    //App User Client
    Route::get('app-user/client/manage', 'AppUser\ClientController@manage');
    Route::get('app-user/client/reference/{id}', 'AppUser\ClientController@reference');
    Route::post('app-user/client/create', 'AppUser\ClientController@create');

    //App User Common Action
    Route::get('app-user/action/check-user-data', 'AppUser\ActionController@checkUserData');
    Route::get('app-user/action/check-user-data-both/{id}', 'AppUser\ActionController@checkUserDataBoth');
    Route::post('app-user/action/reset-password', 'AppUser\ActionController@resetPassword');
    Route::post('app-user/action/reset-activity-password', 'AppUser\ActionController@resetActivityPassword');
    Route::post('app-user/action/deposit-balance', 'AppUser\ActionController@depositBalance');
    Route::post('app-user/action/withdrawal-balance', 'AppUser\ActionController@withdrawalBalance');

    Route::get('app-user/action/delete/{id}', 'AppUser\ActionController@doDelete');

    //App User Setting
    Route::get('app-user/setting/limit/{id}', 'AppUser\SettingController@limit');
    Route::post('app-user/setting/limit-update', 'AppUser\SettingController@limitUpdate');

    Route::get('app-user/setting/limit-setting/{id}', 'AppUser\SettingController@limitSetting');
    Route::post('app-user/setting/limit-setting-update', 'AppUser\SettingController@limitSettingUpdate');

    Route::get('app-user/setting/block-unblock/{id}', 'AppUser\SettingController@userBlockUnblock');
    Route::get('app-user/setting/lock-unlock/{id}', 'AppUser\SettingController@userLockUnlock');

    //History
    Route::get('history/activity-log', 'History\ActivityLogController@list');
    Route::post('history/chip-history', 'History\ChipHistoryController@list');
    Route::post('history/chip-history/{id}', 'History\ChipHistoryController@list');

    Route::post('history/bet-history', 'History\BetHistoryController@list');
    Route::post('history/bet-history/{id}', 'History\BetHistoryController@list');

    //Account Statement
    Route::post('transaction/account-statement', 'Transaction\AccountStatementController@list');
    Route::post('transaction/account-statement/{id}', 'Transaction\AccountStatementController@list');
    Route::post('transaction/account-statement-month/{date}', 'Transaction\AccountStatementController@month');

    //Profit Loss
    Route::post('transaction/system-profit-loss', 'Transaction\SystemProfitLossController@list');
    Route::post('transaction/system-profit-loss-by-event', 'Transaction\SystemProfitLossController@listEventDetail');
    Route::post('transaction/system-profit-loss-by-market', 'Transaction\SystemProfitLossController@listMarketDetail');
    Route::post('transaction/system-teenpatti-profit-loss', 'Transaction\SystemProfitLossController@listTeenPatti');
    Route::post('transaction/system-casino-profit-loss', 'Transaction\SystemProfitLossController@listCasino');

    Route::post('transaction/profit-loss', 'Transaction\ProfitLossController@list');
    Route::post('transaction/profit-loss/{id}', 'Transaction\ProfitLossController@list');
    Route::post('transaction/profit-loss-by-event', 'Transaction\ProfitLossController@listEventDetail');
    Route::post('transaction/profit-loss-by-event/{id}', 'Transaction\ProfitLossController@listEventDetail');
    Route::post('transaction/profit-loss-by-market', 'Transaction\ProfitLossController@listMarketDetail');
    Route::post('transaction/profit-loss-by-market/{id}', 'Transaction\ProfitLossController@listMarketDetail');
    Route::post('transaction/teenpatti-profit-loss', 'Transaction\ProfitLossController@listTeenPatti');
    Route::post('transaction/casino-profit-loss', 'Transaction\ProfitLossController@listCasino');

    //Summary Settlement Live Testing
    Route::get('settlement-live/plus-account', 'Summary\SettlementLiveController@getPlusUsers');
    Route::get('settlement-live/plus-account/{id}', 'Summary\SettlementLiveController@getPlusUsers');
    Route::get('settlement-live/minus-account', 'Summary\SettlementLiveController@getMinusUsers');
    Route::get('settlement-live/minus-account/{id}', 'Summary\SettlementLiveController@getMinusUsers');

    //Summary Client Profit Loss
    Route::post('summery/client-profit-loss', 'Summary\ClientProfitLossController@getProfitLoss');
    Route::post('summery/client-profit-loss/{id}', 'Summary\ClientProfitLossController@getProfitLoss');

    //Summary Settlement
    Route::get('settlement/plus-account', 'Summary\SettlementController@getPlusUsers');
    Route::get('settlement/plus-account/{id}', 'Summary\SettlementController@getPlusUsers');
    Route::get('settlement/minus-account', 'Summary\SettlementController@getMinusUsers');
    Route::get('settlement/minus-account/{id}', 'Summary\SettlementController@getMinusUsers');

    Route::post('settlement/clear-settlement', 'Summary\SettlementController@clearSettlement');
    Route::post('settlement/clear-settlement-admin', 'Summary\SettlementController@clearSettlementAdmin');

    //system
    Route::get('settlement/system-plus-account', 'Summary\SystemSettlementController@getPlusUsers');
    Route::get('settlement/system-plus-account/{id}', 'Summary\SystemSettlementController@getPlusUsers');
    Route::get('settlement/system-minus-account', 'Summary\SystemSettlementController@getMinusUsers');
    Route::get('settlement/system-minus-account/{id}', 'Summary\SystemSettlementController@getMinusUsers');

    Route::post('settlement/system-clear-settlement', 'Summary\SystemSettlementController@clearSettlement');

    //Setting
    Route::get('setting/action/manage', 'Setting\ActionController@manage');
    Route::post('setting/action/create', 'Setting\ActionController@create');
    Route::post('setting/action/update', 'Setting\ActionController@update');
    Route::post('setting/action/status', 'Setting\ActionController@status');

    //event
    Route::get('get/event/setting', 'Setting\SettingController@getEventSetting');
    Route::post('update/event/setting', 'Setting\SettingController@updateEventSetting');

    //market
    Route::get('get/market/setting', 'Setting\SettingController@getMarketSetting');
    Route::post('update/market/setting', 'Setting\SettingController@updateMarketSetting');

    //market result
    Route::post('market-result', 'Market\MarketResultController@marketResult');

    Route::get('casino-list','Casino\CasinoController@viewList');
    Route::post('update-stack','Casino\CasinoController@updateMaxStack');

});

Route::fallback(function () {
    return abort(404);
});



//View Log
Route::get('log','LogController@mpcLog');
Route::get('activity-log','LogController@mpcActivityLog');
Route::get('casino-log','LogController@mpcCasinoLog');

Route::get('view-log/{id?}','LogController@viewLog')->name('viewlog');
Route::get('view-activity-log/{id?}','LogController@viewActivityLog')->name('activityLog');
Route::get('view-casino-log/{id?}','LogController@viewCasinoLog')->name('activityLog');

//Delete Log File 
Route::get('delete-log/{id?}','LogController@deleteLogFile');
Route::get('delete-activity/{id?}','LogController@deleteActivityFile');
Route::get('delete-casino/{id?}','LogController@deleteCasinoFile');