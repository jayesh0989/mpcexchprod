<?php
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return abort(404);
    // return view('welcome');
});

Route::get('users/{id}', function ($id) {
    echo 'Emp '.$id;
});

Route::get('/users', 'UserController@getIndex');
Route::post('/login', 'UserController@login');

//Route::controller('users', 'UserController');


